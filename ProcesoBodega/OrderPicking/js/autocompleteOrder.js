
$('#pickingName').change(function() {
    $('#pickingName').val($('#pickingName').val().toUpperCase());
    var name = $('#pickingName').val();
    var url = 'php/validateName.php?name=' + name;
    $.getJSON(url, function(result)
    {
        if (result['DATA'] == 'true')
        {
            var headerMsg = "Nombre existente";
            var comentMsg = "El nombre de la selección es único, " +
                    "por favor ingrese un nombre nuevo.";
            alertWindow(headerMsg, comentMsg);

        }

    }
    );
});

// ids a                            nd compare have to have the values at same level.
// ids[0] corresponds to compare[0]
var ids = [
    'numdoctte',
    'NITcliente',
    'ContactoCliente',
    'NombreCliente',
    'MarcaComercial'
];
var compare = [
    'ID',
    'NIT',
    'CONTACTO',
    'NOMBRE',
    'MARCA'
];
function fillRefe() {
//    alert("Cargando referencia");
    var transport = $('#transportDoc').val();
    var caso = 'BULTO';
    var urlStorage = 'php/listReferences.php?transporte=' + transport + '&case=' + caso;
    $.getJSON(urlStorage, function(result) {
        if (result['DATA'])
        {
            var storeTypeData = result['DATA'];
            var sizeofstoreTypeData = ObjectSize(storeTypeData);
            // Deleting combobox items.
            $('#reference').html('');
            $('#reference').data('combobox').refresh();

            // adding blank item.
            var blankItemjs = '<option value=""></option>';
            $('#reference').append(blankItemjs);
            $('#reference').data('combobox').refresh();
            // adding all items.
            for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
            {
                var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][0] + '</option>';
                $('#reference').append(appendItemjs);
                $('#reference').data('combobox').refresh();
            }
        }
    });
}
// Consult Transport's documents

$('#importation').change(function()
{
 
    if (this.value)
    {
        var url = lvlrootjs + "assets/php/consultTransportDocByImportNum.php?" + "importation=" + this.value;
        $.getJSON(url, function(result) {
            var transportDocAmount = ObjectSize(result['DATA']); // Size of result
            // if more than one transport's document, let the user choose
            if (transportDocAmount > 1)
            {
                // Enabling select
                $('#transportDoc').data('combobox').enable();
                // Showing warning message
                var headerMsg = "importación duplicada";
                var comentMsg = "El número de importación se encuentra " +
                        "duplicado, seleccionar el documento de transporte " +
                        "de la mercancía                            .";
                warningWindow(headerMsg, comentMsg);
                // Loading documents to load
                var storeTypeData = result['DATA'];
                var sizeofstoreTypeData = ObjectSize(storeTypeData);
                // Deleting combobox items.                         
                $('#transportDoc').html('');
                $('#transportDoc').data('combobox').refresh();
                // adding blank item.
                var blankItemjs = '<option value=""></option>';
                $('#transportDoc').append(blankItemjs);
                $('#transportDoc').data('combobox').refresh();
                // adding all items.
                for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
                {
                    var appendItemjs = '<option value="' + storeTypeData[ijs][1] + '">' + storeTypeData[ijs][1] + '</option>';
                    $('#transportDoc').append(appendItemjs);
                    $('#transportDoc').data('combobox').refresh();
                }

            } else

            {
                // Deleting combobox items.
                $('#transportDoc').html('');
                // adding all items.
                var appendItemjs = '<option value="' + result['DATA'][0][1] + '">' + result['DATA'][0][1] + '</option>';
                $('#transportDoc').append(appendItemjs);
                $('#transportDoc').val(result['DATA'][0][1]);
                $('#transportDoc').data('combobox').refresh();
                $('#transportDoc').data('combobox').disable();
                loadClientData($('#transportDoc').val());
            }
            fillRefe();
        });
    } else
    {
        // Clearing transport's document data
        clearCombobox($('#transportDoc'));
        // Clearing client data
        clearClientData();
        // Clearing output type data
        clearCombobox($('#tiposalidabox'));
        // Clearing table
        clearTable();
        // Hidding boxes's table panel
        $("#panel-boxes").attr("style", 'display:none;');
        // Hidding cancel-save buttons
        $("#cancel-save").attr("style", 'display:none;');

    }
});

$('#transportDoc').change(function()
{
    if ($('#transportDoc').val())
    {

        loadClientData($('#transportDoc').val());


    } else
    {
        // Clearing client data
        clearClientData();
        // Clearing output type data
        clearCombobox($('#tiposalidabox'));
        // Clearing table
        clearTable();
        // Hidding boxes's table panel
        $("#panel-boxes").attr("style", 'display:none;');
        // Hidding cancel-save buttons
        $("#cancel-save").attr("style", 'display:none;');
    }
});

function loadClientData(transportDoc)
{
    var url = 'php/clientInfo.php?transportDoc=' + transportDoc;
    $.getJSON(url, function(result)
    {
        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        } else
        {
            // loading client info
            $('#NITcliente').val(result['DATA'][0][0]);
            $('#NombreCliente').val(result['DATA'][0][1]);
            $('#ContactoCliente').val(result['DATA'][0][2]);
            $('#MarcaComercial').val(result['DATA'][0][3]);
        }
    });
}

// this function calculates the size of an object.
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}


$('#reference').change(function()
{
    var outputSelected = this.value;
    if (outputSelected)
    {
        var selectName = $('#pickingName').val();
        var importValue = $('#importation').val();
        var doctte = $('#transportDoc').val();
        var ref = $('#reference').val();
        if (!importValue || !doctte || !ref || !selectName)
        {
            // Clearing output type
            clearCombobox($("#tiposalidabox"));
            var headerMsg = "Error en los campos.";
            var comentMsg = "Ingrese primero el nombre de la selección, " +
                    "el número de importación" +
                    " y el documento de transporte y después seleccione " +
                    "el tipo de salida.";
            alertWindow(headerMsg, comentMsg);
        } else
        {
            // document.getElementById('alert-Header').innerHTML = "Referencias no nacionalizadas.";
            // document.getElementById('alert-comentary').innerHTML = "Las referencias no han sido nacionalizadas, para este tipo de salida la mercancía debe ser nacionalizada, recuerde nacionalizarlas antes de despachar la mercancía.";
            // document.getElementById('alert-trigger').click();
            loadAvailableRef(doctte, ref);
        }
    } else
    {
        // Clearing table
        clearTable();
        // Hidding boxes's table panel
        $("#panel-boxes").attr("style", 'display:none;');
        // Hidding cancel-save buttons
        $("#cancel-save").attr("style", 'display:none;');
    }
});
$("#tiposalidabox").change(function() {
    var salida = $("#tiposalidabox").val(),refe=$("#reference").val();
   
    var urls = 'php/validateSalida.php?salida=' + salida+'&referncia='+refe;
    $.getJSON(urls, function(result) {

        if (result['DATA'] == '1') {
            var headerMsg = "La Mercancía no esta nacionalizada";
            var comentMsg = "Primero nacionalice y continue con el Picking";
            alertWindow(headerMsg, comentMsg);
            $("#importation").val();

        }

    });
});
function loadAvailableRef(transportDoc, refe)
{
    // Load references or EAN.
    // $.getJSON('php/loadReferences.php?docTransporte='+doctte, function(data) {
    // 	/* called when request to archivo.php completes */
    // 	var referencesSize = ObjectSize(data);
    // 	createReferencesFields(data,nationalizedArray);
    // });

    var url = 'php/loadClientBoxes.php?transportDoc=' + transportDoc + '&refe=' + refe;
    $.getJSON(url, function(result)
    {
        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        } else
        {
            if (result['DATA'])
            {
                createBoxesTable(result);
            }
        }
    });

}

/**
 * This function creates a table and allows the user
 * to select the merchandice he want's.
 * 
 */
function createBoxesTable(boxes)
{
    var boxesRows = boxes['DATA'];
    // Showing boxes's table panel
    $("#panel-boxes").attr("style", 'display:true;');
    // Showing cancel-save buttons
    $("#cancel-save").attr("style", 'display:true;');
    // Constructing boxes table
    var columnsSize = ObjectSize(boxesRows[0]) + 2;
    var boxesTable = tableBoxesConstructor(boxes);

    var numberOfRows = ObjectSize(boxesRows);

    // Creating first row, to select boxes
    var selectBoxRow = createBoxSelectRow(boxes['HEADERS'][0]);
    // Adding rows to table
    boxesTable.row.add(selectBoxRow).draw();

    // Adding rows to the table.
    for (var loopRows = 0; loopRows < numberOfRows; loopRows++)
    {
        // If row has data
        if (ObjectSize(boxesRows[loopRows][0]))
        {
            // Creating check all and input amount to select
            var toDispatch = document.createElement('input');
            var checkrefjs = document.createElement('input');
            // Create tr element
            var tr = document.createElement('tr');

            // Adding row information to each column cell.
            for (var cellFor = 0; cellFor < columnsSize; cellFor++)
            {
                // Reference id to the fields
                var boxReference = boxesRows[loopRows][0];
                // Creating td container
                var td1 = document.createElement('td');
                if (cellFor < 4)
                {
                    var columnName = $(boxesTable.column(cellFor).header()).html();
                    td1.innerText = '' + boxesRows[loopRows][cellFor];
                    td1.setAttribute('id', boxReference + '-' + columnName);
                }
                if (cellFor == 4)
                {
                    // Setting amount attributes
                    toDispatch.setAttribute('id', boxReference);
                    toDispatch.setAttribute('name', 'toDispatch[]');
                    // toDispatch.setAttribute('onChange','validateToDispatch(this)');
                    // toDispatch.setAttribute('onChange','alert("right now")');
                    //toDispatch.setAttribute('onkeydown','if (event.keyCode == 13) { event.preventDefault(), validateToDispatch(this) }');
                    toDispatch.setAttribute('type', 'text');
                    toDispatch.setAttribute('style', 'width:50px;');
                    toDispatch.value = '0';
                    // adding validate data function
                    toDispatch.addEventListener('change',
                            function() {
                                validateToDispatch(this);
                            },
                            false
                            );
                    // appending to td container
                    td1.appendChild(toDispatch);
                }
                if (cellFor == 5)
                {
                    // Setting checkbox attributes
                    checkrefjs.setAttribute('name', 'dispatch[]');
                    checkrefjs.setAttribute('id', boxReference + '-check');
                    checkrefjs.setAttribute('type', 'checkbox');
                    // adding select all available function
                    checkrefjs.addEventListener('click',
                            function() {
                                checkit(this);
                            },
                            false
                            );
                    td1.appendChild(checkrefjs);
                }
                tr.appendChild(td1);
            }
            // Adding rows to table
            boxesTable.row.add(tr).draw();
        }
    }

    // Row of total amounts
    var totalsRow = createTotalsAmount();
    // Adding rows to table
    boxesTable.row.add(totalsRow).draw();
    // Adjusting columns size
    boxesTable.columns.adjust().draw();
    // Calculating the beggining total
    calculateBegginingTotal();
}

function createBoxSelectRow(firstHeader) {
    // Adding select bults by total amount row
    // Creating tr element
    var tr = document.createElement('tr');
    // Creating td columns elements
    var tdAllAmountC0 = document.createElement('td');
    var tdAllAmountC1 = document.createElement('td');
    var tdAllAmountC2 = document.createElement('td');
    var tdAllAmountC3 = document.createElement('td');
    var tdAllAmountC4 = document.createElement('td');
    var tdAllAmountC5 = document.createElement('td');

    // Setting columns values
    tdAllAmountC0.innerText = 'Digitar cantidad de bultos a seleccionar';
    tdAllAmountC1.innerText = '';
    tdAllAmountC2.innerText = '';
    tdAllAmountC3.innerText = '';
    tdAllAmountC4.innerText = '';
    // Setting select box attributes
    if (firstHeader == 'EAN')
    {
        var inputBoxAmount = document.createElement('input');
        inputBoxAmount.setAttribute('type', 'text');
        inputBoxAmount.setAttribute('style', 'width:50px;');
        inputBoxAmount.value = '0';
        // adding select all available function
        inputBoxAmount.addEventListener('change',
                function() {
                    checkTheFirst(this);
                },
                false
                );
        // appending column 4
        tdAllAmountC4.appendChild(inputBoxAmount);
    }
    // Check all button
    var checkAll = document.createElement('input');
    checkAll.setAttribute('id', 'checkAll');
    checkAll.setAttribute('type', 'checkbox');
    // adding select all available function
    checkAll.addEventListener('change',
            function() {
                // Toggle all checkbox with dispatch[] name.
                $("input:checkbox[name=\'dispatch[]\']").prop('checked', $(this).prop("checked"));
                checkitAll();
            },
            false
            );

    // Appendign columns to row
    tr.appendChild(tdAllAmountC0);
    tr.appendChild(tdAllAmountC1);
    tr.appendChild(tdAllAmountC2);
    tr.appendChild(tdAllAmountC3);
    tr.appendChild(tdAllAmountC4);
    tdAllAmountC5.appendChild(checkAll);
    tr.appendChild(tdAllAmountC5);
    return tr;
}

function createTotalsAmount() {
    // creating columns elements
    var trTotal = document.createElement('tr');
    var total = document.createElement('td');
    var bill = document.createElement('td');
    var real = document.createElement('td');
    var available = document.createElement('td');
    var dispatchTotal = document.createElement('td');
    var checkBox = document.createElement('td');

    // setting attributes
    bill.setAttribute('id', 'billTotal');
    real.setAttribute('id', 'realTotal');
    available.setAttribute('id', 'availableTotal');
    dispatchTotal.setAttribute('id', 'dispatchTotal');

    total.innerText = 'Total';
    bill.innerText = '0';
    real.innerText = '0';
    available.innerText = '0';
    dispatchTotal.innerText = '0';
    checkBox.innerText = '';

    // appending childs
    trTotal.appendChild(total);
    trTotal.appendChild(bill);
    trTotal.appendChild(real);
    trTotal.appendChild(available);
    trTotal.appendChild(dispatchTotal);
    trTotal.appendChild(checkBox);

    return trTotal;
}

/**
 * This function calculates the sum of the inicial values
 * of the columns
 */
function calculateBegginingTotal()
{
    // Loading all checkboxes
    var totalBox = $("input:text[name=\'toDispatch[]\']");
    var BoxSize = totalBox.length;
    var totalBillAmount = 0;
    var totalRealAmount = 0;
    var totalAvailableAmount = 0;
    for (var qjs = 0; qjs < BoxSize; qjs++)
    {
        // loading reference/EAN
        var reference = totalBox[qjs].id;
        var getBillElement = document.getElementById(reference + '-INGRESADOS');
        var getRealElement = document.getElementById(reference + '-RECIBIDOS');
        var getAvailableElement = document.getElementById(reference + '-DISPONIBLES');

        // Sum total
        totalBillAmount += parseInt(getBillElement.innerHTML);
        totalRealAmount += parseInt(getRealElement.innerHTML);
        totalAvailableAmount += parseInt(getAvailableElement.innerHTML);
    }
    // Loading elements
    var billTotal = document.getElementById('billTotal');
    var realTotal = document.getElementById('realTotal');
    var availableTotal = document.getElementById('availableTotal');

    // Setting sum values
    billTotal.innerText = '' + totalBillAmount;
    realTotal.innerText = '' + totalRealAmount;
    availableTotal.innerText = '' + totalAvailableAmount;
}

function tableBoxesConstructor(boxes) {
    var colDef = []; // column definition
    var boxesType = boxes['HEADERS'][0];
    var headers = ['INGRESADOS', 'RECIBIDOS', 'DISPONIBLES', 'CANTIDADES', 'TODOS'];
    // Prepending EAN or REFERENCIAS header
    headers.unshift(boxesType);
    var headersSize = ObjectSize(headers);
    // Creating columns from headers variable
    for (var headersFor = 0; headersFor < headersSize; headersFor++)
    {
        colDef[headersFor] = {
            aTargets: [headersFor],
            sTitle: headers[headersFor]};
    }

    // If dataTable exist, delete
    if ($.fn.dataTable.isDataTable('#referenceTable')) {
        table = $('#referenceTable').DataTable();
        table.destroy();
        $('#referenceTable').empty();
        // table = $('#referenceTable').DataTable();
        // table.clear();
    }
    // table construct
    table = $('#referenceTable').DataTable(
            {
                //"bJQueryUI": true,
                //"sPaginationType": "full_numbers",
                //"bProcessing": true,
                //"bDeferRender": true,
                //"bInfo" : false,
                //"bDestroy" : true,
                //"bFilter" : false,
                //"bPagination" : false,
                //"aaData": results,
                //"aoColumns": cols,
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "aoColumnDefs": colDef
            });

    return table;
}

// check uncheck all checkbox
$('#checkAll').change(function()
{
    $("input:checkbox[name=\'dispatch[]\']").prop('checked', $(this).prop("checked"));
    checkitAll();
});

function checkitAll()
{
    // Loading all the checkBox
    var totalBox = $("input:checkbox[name=\'dispatch[]\']");
    var BoxSize = totalBox.length;
    // Loading available references on the input box.
    for (var sjs = 0; sjs < BoxSize; sjs++)
    {
        checkit(totalBox[sjs]);
    }
}

// This function pass the available amount to dispatch box.
// obj checkbox type ref-check
function checkit(obj)
{
    var splitReference;
    splitReference = obj.id.split('-');
    var inputReference = document.getElementById(splitReference[0]);
    var availableReference = document.getElementById(splitReference[0] + '-DISPONIBLES');
    if (obj.checked)
    {
        inputReference.value = availableReference.innerText;
    } else
    {
        inputReference.value = 0;
    }
    sumToDispatch();
}

/** This function adds the total amount to dispatch
 * to the 'dispatchTotal' box.
 */
function sumToDispatch()
{
    var totalBox = $("input:text[name=\'toDispatch[]\']");
    var BoxSize = totalBox.length;
    var TotalAmount = 0;
    for (var qjs = 0; qjs < BoxSize; qjs++)
    {
        TotalAmount = TotalAmount + parseInt(totalBox[qjs].value);
    }
    var dispatchTotal = document.getElementById('dispatchTotal');
    dispatchTotal.innerText = '' + TotalAmount;

}

// This function validate the dispatch box and sum the total.
function validateToDispatch(inputValue)
{
    var Available = parseInt(document.getElementById(inputValue.id + '-DISPONIBLES').innerText);
    var IntValue = parseInt(inputValue.value);
    // If IntValue is greater than available reference.
    if (IntValue >= Available)
    {
        inputValue.value = Available;
        // Check checkbox
        $('#' + inputValue.id + '-check').prop('checked', true);
    } else
    {
        // Uncheck checkbox
        $('#' + inputValue.id + '-check').prop('checked', false);
        // If IntValue is character or empty insert '0'.
        if (!IntValue || IntValue < 0)
        {
            inputValue.value = 0;
        }
    }
    sumToDispatch();
}

function checkTheFirst(first)
{
    var IntFirst = parseInt(first.value);
    if (IntFirst > 0)
    {
        // Loading all the checkBox
        var totalBox = $("input:checkbox[name=\'dispatch[]\']");
        var BoxSize = totalBox.length;
        if (BoxSize < IntFirst)
        {
            IntFirst = BoxSize;
        }
        // Loading available references on the input box.
        for (var tjs = 0; tjs < IntFirst; tjs++)
        {
            // checking the box
            totalBox[tjs].checked = true;
            // passing the available amount to the dispatch box.
            checkit(totalBox[tjs]);
        }
    } else
    {	// If IntValue is character or empty insert '0'.
        first.value = 0;
    }
}
/* Filling selects combobox */
// Importations
var urlInput = lvlrootjs + "assets/php/importacionSalida.php";
$.getJSON(urlInput, function(result) {
    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#importation').html('');
        $('#importation').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#importation').append(blankItemjs);
        $('#importation').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][1] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#importation').append(appendItemjs);
            $('#importation').data('combobox').refresh();
        }
    }
});
// Output type
var urlInput = lvlrootjs + "assets/php/tiposalida.php";
$.getJSON(urlInput, function(result) {
    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#tiposalidabox').html('');
        $('#tiposalidabox').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#tiposalidabox').append(blankItemjs);
        $('#tiposalidabox').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#tiposalidabox').append(appendItemjs);
            $('#tiposalidabox').data('combobox').refresh();
        }
    }
});

/**
 * Clear table if exists
 */
function clearTable()
{
    // If dataTable exist, delete
    if ($.fn.dataTable.isDataTable('#referenceTable')) {
        table = $('#referenceTable').DataTable();
        table.destroy();
        $('#referenceTable').empty();
        // table = $('#referenceTable').DataTable();
        // table.clear();
    }
}
/**
 * Clear client input fields
 */
function clearClientData()
{
    // Cleaning client info
    $('#NITcliente').val('');
    $('#NombreCliente').val('');
    $('#ContactoCliente').val('');
    $('#MarcaComercial').val('');
}

/**
 * Clear a combobox object
 * @param [combobox object]: object combobox
 */
function clearCombobox(object) {
    // Cleaning output type
    // Clearing combobox
    object.data('combobox').clearTarget();
    object.data('combobox').clearElement();
}

/**
 * Cancel button pressed
 */
$('#cancelData').click(function() {
    // Clearing table
    clearTable();
    // Clearing Outpu type select
    clearCombobox($('#tiposalidabox'));
    // Clearing client info
    clearClientData();
    // Clearing transport's document
    clearCombobox($('#transportDoc'));
    // Clearing client's importation
    clearCombobox($('#importation'));
    // Clearing selection name
    $('#pickingName').val('');
    // Hidding boxes's table panel
    $("#panel-boxes").attr("style", 'display:none;');
    // Hidding cancel-save buttons
    $("#cancel-save").attr("style", 'display:none;');
});

/**
 * Save button pressed
 */
$('#save').click(function()
{
    // calling function from validationfields.js
    saveClicked();
});

