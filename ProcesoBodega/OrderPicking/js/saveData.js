$("#savess").click(function() {
    if ($("#clientform").parsley().validate())
    {

        // Confirmation message
        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar el picking " + $("#pickingName").val() + " ?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'saveSelection()');
        document.getElementById('warning-trigger').click();
    }
});

$("#saveSelection").click(function() {
    var ca = document.getElementById('dispatchTotal');
    var cant = ca.innerText;

    if (cant > '0') {
        // Confirmation message
        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar la selección de la referencia " + $("#reference").val() + " ?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'saveSelected()');
        document.getElementById('warning-trigger').click();
    } else {
        var headerMsg = "Cantidad Incorrecta";
        var comentMsg = "Seleccione una cantidad de bultos correcta";
        alertWindow(headerMsg, comentMsg);
    }
});

function saveSelection() {
    var tiposalida = $("#tiposalidabox").val();
    var nombre = $("#pickingName").val();
    var transporte = $("#transportDoc").val();
    var url = 'php/saveSelection.php?tiposalida=' + tiposalida + '&nombre=' + nombre + '&transporte=' + transporte;
    console.log(url);
    $.getJSON(url, function(result)
    {
        if (result['ERROR']) {
            var headerMsg = "Error al guardar el picking";
            var comentMsg = "Verificar Datos e Intente Nuevamente";
            alertWindow(headerMsg, comentMsg);
        }
        else
        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Selección Por Bultos Guardada Correctamente";
            successWindow(headerMsg, comentMsg);
        }

    });

}
setInterval(function() {

    var ref = $("#reference").val();
    var cal = parseInt("0") + $("#" + ref).val();

}, 10000);

function saveSelected() {
    var refe = $("#reference").val();
    var ca = document.getElementById('dispatchTotal');
    var cant = ca.innerText;
//        var refe = document.getElementById("#refe-REFERENCIA").textContent;
    var transporte = $("#transportDoc").val();

    //        for (var i = 0; i < cant; i++) {
    var url = 'php/saveSelectedBoxes.php?transporte=' + transporte + '&ref=' + refe + '&cant=' + cant;
    $.getJSON(url, function(result)
    {
        if (result['ERROR']) {
            var headerMsg = "Error en el picking";
            var comentMsg = "Verificar Datos e Intente Nuevamente";
            alertWindow(headerMsg, comentMsg);
        }
        else

        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Selección Por Bultos Guardada Correctamente";
            successWindow(headerMsg, comentMsg);

        }

    });
}

//   function loadtrasnport() {
//       var impo= $("#importation").val();
//       alert(impo);
//        var url = "../../assets/php/consultTransportDocByImportNum.php?importation=" + impo;
//        $.getJSON(url, function (result) {
//            var transportDocAmount = ObjectSize(result['DATA']); // Size of result
//            // if more than one transport's document, let the user choose
//            alert(result['DATA']);
//            if (transportDocAmount > 1)
//            {
//                // Enabling select
//                $('#transportDoc').data('combobox').enable();
//                // Showing warning message
//                var headerMsg = "importación duplicada";
//                var comentMsg = "El número de importación se encuentra " +
//                        "duplicado, seleccionar el documento de transporte " +
//                        "de la mercancía                            .";
//                warningWindow(headerMsg, comentMsg);
//                // Loading documents to load
//                var storeTypeData = result['DATA'];
//                var sizeofstoreTypeData = ObjectSize(storeTypeData);
//                // Deleting combobox items.                         
//                $('#transportDoc').html('');
//                $('#transportDoc').data('combobox').refresh();
//                // adding blank item.
//                var blankItemjs = '<option value=""></option>';
//                $('#transportDoc').append(blankItemjs);
//                $('#transportDoc').data('combobox').refresh();
//                // adding all items.
//                for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
//                {
//                    var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
//                    $('#transportDoc').append(appendItemjs);
//                    $('#transportDoc').data('combobox').refresh();
//                }
//            } else
//
//            {
//                // Deleting combobox items.
//                $('#transportDoc').html('');
//                // adding all items.
//                var appendItemjs = '<option value="' + result['DATA'][0][1] + '">' + result['DATA'][0][1] + '</option>';
//                $('#transportDoc').append(appendItemjs);
//                $('#transportDoc').val(result['DATA'][0][1]);
//                $('#transportDoc').data('combobox').refresh();
//                $('#transportDoc').data('combobox').disable();
//                loadClientData($('#transportDoc').val());
//            }
//        });
//   else
//    {
//        // Clearing transport's document data
//        clearCombobox($('#transportDoc'));
//        // Clearing client data
//        clearClientData();
//        // Clearing output type data
//        clearCombobox($('#tiposalidabox'));
//        // Clearing table
//        clearTable();
//        // Hidding boxes's table panel
//        $("#panel-boxes").attr("style", 'display:none;');
//        // Hidding cancel-save buttons
//        $("#cancel-save").attr("style", 'display:none;');
//
//    }
//
//   }