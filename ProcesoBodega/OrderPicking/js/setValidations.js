// Picking Name
$('#pickingName').attr('placeholder',"Nombre de la selección");
$('#pickingName').attr('style',"text-transform:uppercase");
$('#pickingName').attr('data-parsley-maxlength',"45");
$('#pickingName').attr('data-parsley-required',true);

// Importation number
$('#importation').attr('data-parsley-type',"alphanum");
$('#importation').attr('placeholder',"Importación del cliente");
$('#importation').attr('style',"text-transform:uppercase");
$('#importation').attr('data-parsley-required',true);

// transport's document
$('#numdoctte').attr('data-parsley-type',"alphanum");
$('#numdoctte').attr('placeholder',"Documento de transporte");
$('#numdoctte').attr('readonly','');
$("#numdoctte").attr('data-parsley-required', true);


// Clients information
$('#NITcliente').attr('data-parsley-type',"digits");
$('#NITcliente').attr('placeholder',"NIT");
$('#NITcliente').attr('readonly','');
$("#numdoctte").attr('data-parsley-required', true);


$('#ContactoCliente').attr('placeholder',"Contacto");
$('#ContactoCliente').attr('readonly','');

$('#NombreCliente').attr('placeholder',"Nombre empresa");
$('#NombreCliente').attr('readonly','');

$('#MarcaComercial').attr('placeholder',"Marca");
$('#MarcaComercial').attr('readonly','');

// Output's type
$('#tiposalidabox').attr('data-parsley-required',true);
$('#tiposalidabox').attr('data-toggle',"modal");