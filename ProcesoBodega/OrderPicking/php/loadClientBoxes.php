<?php
	include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();
    @$transportDoc = $_GET['transportDoc'];
    @$refe=$_GET['refe'];
	// Acentos de base de datos a html.
	$accents = $connection->query("SET NAMES 'utf8'");
	if(!$connection->link)
    {
        $result['ERROR'][0] = "Error de conexión";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
		 $query = "CALL SELECCION_BULTOS_WEB('$transportDoc','$refe');";
		 $queryResult = $connection->query($query);

		 if($queryResult)
		 {
		 	$data = array();
		 	while($tempData = $connection->fetch_array_assoc($queryResult))
                            {
		 		$data[] = $tempData;
                            }
			
                        $result['HEADERS'] = array_keys($data[0]);
		 	foreach($data as $row)
                            {
		 		foreach ($row as $key => $value) 
		 		{
		 			$temp[] = $value;
		 		}
		 		$result['DATA'][] = $temp;
		 		unset($temp);
                            }
		 }
                    else
                        {
                             $result['ERROR'][0] = "Error de consulta";
                             $result['ERROR'][1] = "No se pudo realizar la consulta.";
                        }
		
                //$result['HEADERS'] = array(0=>'REFERENCIAS',1=>'FACTURA',2=>'REALES',3=>'DISPONIBLES');
		//$result['DATA'][] = array(0=>'ref1',1=>320,2=>320,3=>320);
		//$result['DATA'][] = array(0=>'ref2',1=>310,2=>310,3=>50);
		$connection->close();
	}
	print json_encode($result);
?>