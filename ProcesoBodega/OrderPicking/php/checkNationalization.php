<?php

/* <!--
 * This file returns a JSON with the references/EAN not nationalized
  --> */
$data = json_decode($_POST['data'], true);
$transportDoc = $data['transportDoc'];
$boxesType = $data['boxesType'];
$valuesArray = $data['valuesArray'];

include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();
// Acentos de base de datos a html.
$accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    // $tmpResult = array();
    foreach ($valuesArray as $key => $value) {
        $reference = $value[0];
        $amount = $value[1];
        // 	$query = "CALL BULTOS_DISPONIBLES_NACIONALIZADOS('$transportDoc',"
        // 		."'$boxesType','$reference',$amount);";
        // 	$queryResult = $connection->query($query);
        // 	if($queryResult)
        // 	{
        // 		while($tempData = $connection->fetch_array_assoc($queryResult))
        // 		{
        // 			$tmpResult[] = $tempData;
        // 		}
        $tmpResult[0] = array('FALSE' => 0, 'DISPONIBLES' => 50);
        $tmpResult[1] = array('FALSE' => 0, 'DISPONIBLES' => 23);

        foreach ($tmpResult as $row) {
            foreach ($row as $key => $value) {
                $temp[] = $value;
            }
            $result['DATA'][] = array(0 => $temp[0], 1 => $reference, 2 => $amount, $temp[1]);
            unset($temp);
        }
        unset($tmpResult);
        // }
        // else
        // {
        // 	$result['ERROR'][0] = "Error de consulta";
        // 	$result['ERROR'][1] = "No se pudo realizar la consulta.";
        // }
    }

    $connection->close();
}
print json_encode($result);
?>