<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
include($lvlroot . "assets/css/datat.css");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?><script>
            window.location = "../../Home/exit.php";</script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
// function defined in js/autocomplete.js
$nosubmit = "if (event.keyCode == 13) event.preventDefault()";
$autoCompleteCall = "if (event.keyCode == 13) { autoCompleteOthers(this); event.preventDefault(); }";
// Prevent default post
$preventDefault = "if (event.keyCode == 13) { event.preventDefault(); }";
//unset($ingreso,$destino,$importacion);
//var_dump($ingreso,$destino,$importacion);
unset($resultado);
?>
<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;</script>
<script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="//www.opencpu.org/js/archive/opencpu-0.4.js"></script>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Proceso Bodega</a></li>
    <li class="active">Selección de Unidades</li>
</ol>
<h1 class="page-header">Selección de mercancía por unidades<small> Seleccionar unidades del pedido.</small></h1>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-edit fa-lg"></i>
                    Pedido de la mercancía
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form" id="panel-body-form">
                <!-- begin form -->
                <form data-parsley-validate="true" id="clientform" name="client-form" class="form-horizontal form-bordered" method="post" action="" onkeydown="<?php echo $nosubmit; ?>">
                    <div class="form-group">
                        <label class="control-label col-md-4 text-left">Nombre de la selección (*)</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="pickingName" name="pickingName"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 text-left" for="message">Número de importación del cliente (*)</label>
                        <div class="col-md-8">
                                <!--<input class="form-control" type="text" id="importacion" name="importacion" />-->
                            <select class="combobox" name="importation" id="importation" value="" onchange="ValidateNames();" >
                                <option value=""> Seleccione una importación</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 text-left" for="message">Número del documento de transporte (*)</label>
                        <div class="col-md-8">
                                <!--<input class="form-control" type="text" id="numdoctte" name="numdoctte" />  $autoCompleteDoctteCall -->
                            <select class="combobox" name="transportDoc" id="transportDoc" value="" >
                                <option value=""> Seleccione un documento de transporte</option>
                            </select>
                        </div>
                    </div>
                    <!-- begin client data -->
                    <div class="form-group">
                        <div class="col-md-2">
                            <label>NIT (*)</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="NITcliente" name="NITcliente"  readonly=”readonly”/>
                        </div>
                        <div class="col-md-2">
                            <label>Nombre empresa (*)</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="NombreCliente" name="NombreCliente"  readonly=”readonly”/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label>Nombre del contacto (*)</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="ContactoCliente" name="ContactoCliente"  readonly=”readonly”/>
                        </div>
                        <div class="col-md-2">
                            <label>Marca comercial (*)</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="MarcaComercial" name="MarcaComercial"  readonly=”readonly”/>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="hiddenidclient" name="hiddenidclient"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 text-left">Tipo de Salida (*)</label>
                        <div class="col-md-8">
                            <!-- onchange="this.form.action=''; this.form.submit()" -->
                            <select class="combobox" name="tiposalidabox" id="tiposalidabox" value="" onchange="loadTable();">
                                <option value=""> Seleccione una salida</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <form id="fileupload" action="../../assets/plugins/jquery-file-upload/server/upload-files/php-maestra/" method="POST" enctype="multipart/form-data">
                                <div class="row fileupload-buttonbar">
                                    <div class="col-md-12">
                                        <span class="btn btn-success fileinput-button">
                                            <i class="fa fa-plus"></i>
                                            <span>Adjuntar archivo</span>
                                            <input type="file" id="csvfile" >                                      
                                        </span>  
                                        <button id="submitbuttonn" type="button" class="btn btn-primary start" style="display:true;" read.csv >
                                            <i class="fa fa-upload"></i>
                                            <span>Abrir Archivo</span>
                                        </button>           
                                        <pre style="display:none;"><code id="output" style="display:none;"></code></pre>      

                                        
                                        <button  type="reset" class="btn btn-warning cancel" id="cancel-all-upload">
                                            <i class="fa fa-ban"></i>
                                            <span>Cancelar</span>
                                        </button>
                                        
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                    </div>
                                    
                                    <!-- The global progress state -->
                                                        <div class="form-group">
                                    <label class="left small  col-md-4 text-left" >
                                        Adjunte el documento con formato (REFERENCIA - CANTIDAD -IMPORTACIÓN) con el nombre batchpicking.csv
                                    </label>
                                
                                 <div class="col-md-6 col-sm-6" align="center">
                                     <button id="downloadExampleBoxes" type="button" class="btn btn-white m-r-6">
                            <i class="fa fa-download">
                                Descargar formato de referencias para selección de unidades
                            </i>
                        </button>
                  
                    </div>
                                </div>

                                <!-- The table listing the files available for upload/download -->
                                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>

                        </div>
                    </div>
            </div>
                                                </form>

        </div>
    </div>

</div>



    <div class="col-md-12">
        <div class="panel panel-inverse " id="panel-boxes" style="display:none;">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-check-square-o fa-lg"></i>
                    Mercancía disponible para este cliente
                </h4>
            </div>
            <div class=" panel-body panel-form " id="avalaibleunits" style="display:none; height: 30px;">

            </div>
        </div>
        <div class="panel panel-inverse" id="panel-csv" style="display:true;">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-file-zip-o fa-lg"></i>
                    Datos del Archivo Adjunto
                </h4>
            </div>
            <form id="references-form" name="references-form" class="panel-form form-horizontal form-bordered" method="post" action="" >
                        <div class="form-group">

        <div class="form-group" id="divTables">
             
        </div>
                        </div>
                <div class="form-group">
    <div class="col-md-12 col-sm-12 text-right">
        <div class="row fileupload-buttonbar">

            <button id="cancelData" name="cancelData" class="btn btn-warning ">
                <i class="fa fa-ban">
                    Cancelar
                </i>
            </button>
            <button id="btnsave" type="button" name="save" class="btn btn-primary" >
                <i class="fa fa-save">
                    Guardar
                </i>
            </button>
<!--             <button id="btnsave" type="button" name="save" class="btn btn-primary" >
                <i class="fa fa-save">
                    Finalizar
                </i>
            </button>-->

        </div>
    </div>
</div>
    </form>
</div> 
</div>
</div>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;</script>
    
<?php
include_once($lvlroot . "Body/AlertsWindows.php");
?>
</div>
<!-- end row -->
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>



<!-- // Loading autocomplete handler. -->
<script type="text/javascript" src="js/autocompleteBatch.js"></script>
<script type="text/javascript" src="js/fileUpload.js"></script>
<script type="text/javascript" src="js/loadStoredData.js"></script>
<script type="text/javascript" src="js/validateOnSave.js"></script>
<script type="text/javascript" src="js/validationBatch.js"></script>
<script type="text/javascript" src="js/validationfields.js"></script>
<script type="text/javascript" src="js/loadTableReferences.js"></script>
<script type="text/javascript" src="js/setValidations.js"></script>
<script type="text/javascript" src="js/Validation.js"></script>
<script type="text/javascript" src="js/fillData.js"></script>

<script type="text/javascript" src="js/generateReferencesExample.js"></script>
<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarProcesoBodega");
    Change2Activejs.className = "has-sub active";
    var Change2Activejs = document.getElementById("sidebarProcesoBodega-BatchPicking");
    Change2Activejs.className = "active";</script>


<!-- end Configuration of accepted files -->

<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">  
    <td>  
    <span class="preview">  
    {% if (file.thumbnailUrl) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
    {% } %}
    </span>  
    </td>    

    <td>   
    <p class="name">    
    {% if (file.url) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
    {% } else { %} 
    <span>{%=file.name%}</span>
    {% } %}

    </p>
    {% if (file.error) { %}
    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
    {% } %}
    </td>

    <td>   
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>         
    <td>
    {% if (file.deleteUrl) { %}  
    {% datacsv();%}

    <button id="eliminar"class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="glyphicon glyphicon-trash"></i>
    <span>Eliminar</span>  
    </button>   
    <input type="checkbox" name="delete" value="1" class="toggle">
    {% } else { %}
    <button id="cancell" class="btn btn-warning cancel">
    <i class="glyphicon glyphicon-ban-circle"></i>
    <span>Cancelar</span>
    </button> 
    {% } %}
    </td>  
    </tr>
    {% } %}
</script>


<script>

    function clearTable()
    {
        // If dataTable exist, delete
        if ($.fn.dataTable.isDataTable('#referencesTable')) {
            table = $('#referencesTable').DataTable();
            table.destroy();
            $('#referencesTable').empty();
            // table = $('#referenceTable').DataTable();
            // table.clear();
        }
    }
</script>


<!-- ================== BEGIN PAGE LEVEL JS ================== -->

<!-- ================== END PAGE LEVEL JS ================== -->