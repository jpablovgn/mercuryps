


function clearPartialData()
{
    $("#numimportacion").val("")
        .removeClass("parsley-success");
    $("#consecutivozf").val("")
        .removeClass("parsley-success");
    // Clearing bills
    $("#jquery-tagIt-primary").tagit('removeAll');
    // Clearing data
    $("#valorCIF").val("")
        .removeClass("parsley-success");
    $("#valorFOB").val("")
        .removeClass("parsley-success");
    // Clearing combobox
    $("#tipoingresobox").data('combobox').clearTarget();
    $("#tipoingresobox").data('combobox').clearElement();
    $("#tipocontenedorbox").data('combobox').clearTarget();
    $("#tipocontenedorbox").data('combobox').clearElement();
    $("#tipoalmacenamientobox").data('combobox').clearTarget();
    $("#tipoalmacenamientobox").data('combobox').clearElement();
    // Enabling combobox function from validateOnLoad.js
    $("#tipoingresobox").data('combobox').enable();
    $("#tipocontenedorbox").data('combobox').enable();
    $("#tipoalmacenamientobox").data('combobox').enable();
    // Clearing data
    $("#weight").val("")
        .removeClass("parsley-success");
    $("#descripcionMercancia").val("")
        .removeClass("parsley-success");
    $("#totalpalets").val("")
        .removeClass("parsley-success")
        .attr('disabled',false);
    $("#totalcajas").val("")
        .removeClass("parsley-success")
        .attr('disabled',false);
    $("#totalunidades").val("")
        .removeClass("parsley-success")
        .attr('disabled',false);
    $("#ProcesoUnidades").val("")
        .removeClass("parsley-success");
    $("#ProcesoSerial").val("")
        .removeClass("parsley-success");
    $("#observaciones").val("")
        .removeClass("parsley-success");
    // Clearing checkbox
    $('#ProcesoUnidades').prop('checked',false)
        .attr('disabled',false);
    $('#ProcesoSerial').prop('checked',false)
        .attr('disabled',false);
    // Canceling upload files
    $('#cancel-all-upload').click();
    // Clearing references combobox
    if($("#refbultosmanual").val())
    {
        if($("#refbultosmanual").prop("checked"))
        {
            $("#refbultosmanual").prop("checked",false);
            createBultosfields($("refbultosmanual"));
        }
    }

    // Enabling fields
    $("#numimportacion").attr('disabled',false);
    $("#weight").attr('disabled',false);
    $("#descripcionMercancia").attr('disabled',false);
    $('#refbultosmanual').attr('disabled',false);
}