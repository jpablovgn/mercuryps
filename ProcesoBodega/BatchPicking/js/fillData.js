function loadTable() {
        var nit = $('#NITcliente').val();
        $("#avalaibleunits").attr("style", "display:true;");
        $("#avalaibleunits").load('php/unitsavalaible.php?nit=' + nit);
    }
    var urlInput = lvlrootjs + 'assets/php/importacionSalida.php';
    $.getJSON(urlInput, function(result) {
        if (result['DATA'])
        {
            var storeTypeData = result['DATA'];
            var sizeofstoreTypeData = ObjectSize(storeTypeData);
            // Deleting combobox items.
            $('#importation').html('');
            $('#importation').data('combobox').refresh();
            // adding blank item.
            var blankItemjs = '<option value=""></option>';
            $('#importation').append(blankItemjs);
            $('#importation').data('combobox').refresh();
            // adding all items.
            for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
            {
                var appendItemjs = '<option value="' + storeTypeData[ijs][1] + '">' + storeTypeData[ijs][1] + '</option>';
                $('#importation').append(appendItemjs);
                $('#importation').data('combobox').refresh();
            }
        }
    });


    function ValidateNames() {
        var name = $("#pickingName").val();


        var url = 'php/validateName.php?name=' + name;

        $.getJSON(url, function(result) {

            if (result['DATA'] == "false") {
                fillForm();
            } else {
                var headerMsg = "Nombre Picking Duplicado";
                var comentMsg = "El nombre del Picking se encuentra duplicado";
                alertWindow(headerMsg, comentMsg);
                $('#importation').val('');
                $("#pickingName").val('');
                $("#pickingName").focus();
                return false;

            }

        });

    }
$("#submitbuttonn").click(function(){
   $("#panel-csv").attr("style","display:true") ;
});

    function fillForm() {
        var impo = $('#importation').val();
        var url = lvlrootjs + "assets/php/consultTransportDocByImportNum.php?importation=" + impo;
        $.getJSON(url, function(result) {
            var transportDocAmount = ObjectSize(result['DATA']); // Size of result
            // if more than one transport's document, let the user choose
            if (transportDocAmount > 1)
            {
                // Enabling select
                $('#transportDoc').data('combobox').enable();
                // Showing warning message
                var headerMsg = "importación duplicada";
                var comentMsg = "El número de importación se encuentra " +
                        "duplicado, seleccionar el documento de transporte " +
                        "de la mercancía                            .";
                warningWindow(headerMsg, comentMsg);
                // Loading documents to load
                var storeTypeData = result['DATA'];
                var sizeofstoreTypeData = ObjectSize(storeTypeData);
                // Deleting combobox items.                         
                $('#transportDoc').html('');
                $('#transportDoc').data('combobox').refresh();
                // adding blank item.
                var blankItemjs = '<option value=""></option>';
                $('#transportDoc').append(blankItemjs);
                $('#transportDoc').data('combobox').refresh();
                // adding all items.
                for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
                {
                    var appendItemjs = '<option value="' + storeTypeData[ijs][1] + '">' + storeTypeData[ijs][1] + '</option>';
                    $('#transportDoc').append(appendItemjs);
                    $('#transportDoc').data('combobox').refresh();
                }
            } else

            {
                // Deleting combobox items.
                $('#transportDoc').html('');
                // adding all items.
                var appendItemjs = '<option value="' + result['DATA'][0][1] + '">' + result['DATA'][0][1] + '</option>';
                $('#transportDoc').append(appendItemjs);
                $('#transportDoc').val(result['DATA'][0][1]);
                $('#transportDoc').data('combobox').refresh();
                $('#transportDoc').data('combobox').disable();
                loadClientData($('#transportDoc').val());
            }
        });
    }
//    $(function() {
//        $('#fileupload').fileupload({
//            acceptFileTypes: /(\.|\/)(csv)$/i
//        });
//    });

//$("#importation").change(function(){
//    alert("The text has been changed.");
//}); 
    

//            var cantFilas = $("#tableReferences tr").length;
//            var i;
//            for (i = 1; i < cantFilas; i++)
//            {
//                var impo = document.getElementById("tableReferences").rows[i].cells[4].innerText;
//                var Refe = document.getElementById("tableReferences").rows[i].cells[1].innerText;
//                var Cant = document.getElementById("tableReferences").rows[i].cells[3].innerText;
//                var destino = document.getElementById("tableReferences").rows[i].cells[2].innerText;
//
//                var url = 'php/saveReferencesUnits.php?ref=' + Refe + '&destino=' + destino + '&cant=' + Cant + '&imp=' + impo;
//                alert(url);
//                $.getJSON(url, function(result)
//                {
//                    if (result) {
//                        
//                    }
//                });


// Consult Transport's documents
//$('#importation').change(function ()


    function loadClientData(transportDoc)
    {
        var url = 'php/clientInfo.php?transportDoc=' + transportDoc;
        $.getJSON(url, function(result)
        {
            if (result['ERROR'])
            {
                alertWindow(result['ERROR'][0], result['ERROR'][1]);
            } else
            {
                // loading client info
                $('#NITcliente').val(result['DATA'][0][0]);
                $('#NombreCliente').val(result['DATA'][0][1]);
                $('#ContactoCliente').val(result['DATA'][0][2]);
                $('#MarcaComercial').val(result['DATA'][0][3]);
            }
        });
    }

    var urlInput = lvlrootjs + "assets/php/tiposalida.php";
    $.getJSON(urlInput, function(result) {
        if (result['DATA'])
        {
            var storeTypeData = result['DATA'];
            var sizeofstoreTypeData = ObjectSize(storeTypeData);
            // Deleting combobox items.
            $('#tiposalidabox').html('');
            $('#tiposalidabox').data('combobox').refresh();
            // adding blank item.
            var blankItemjs = '<option value=""></option>';
            $('#tiposalidabox').append(blankItemjs);
            $('#tiposalidabox').data('combobox').refresh();
            // adding all items.
            for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
            {
                var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
                $('#tiposalidabox').append(appendItemjs);
                $('#tiposalidabox').data('combobox').refresh();
            }
        }
    });



