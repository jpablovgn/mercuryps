
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}
var idP = getUrlVars()['idp'];
$("#loadtable").load("php/listarBulto.php?idp=" + idP);


$(function () {
    var url = 'php/deletefilecsv.php';
    $.getJSON(url, function (result)
    {

    });
});

$('#pickingName').change(function () {
    $('#pickingName').val($('#pickingName').val().toUpperCase());
});

$('#selectNITClient').change(function () {
    validateName();
});

$('#pickingName').change(function () {
    $('#pickingName').val($('#pickingName').val().toUpperCase());
});

// ids a                            nd compare have to have the values at same level.
// ids[0] corresponds to compare[0]
var ids = [
    'numdoctte',
    'NITcliente',
    'ContactoCliente',
    'NombreCliente',
    'MarcaComercial'
];
var compare = [
    'ID',
    'NIT',
    'CONTACTO',
    'NOMBRE',
    'MARCA'
];

function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;

$('#tiposalidabox').change(function () {
    clearTable();
});

function deletecsv()
{
    var url = 'php/deletefilecsv.php';
    $.getJSON(url, function (result)
    {

    });

}
function validateName()
{
    var name = $('#pickingName').val();
    var nitclient = $('#selectNITClient').val();

    var url = 'php/validateName.php?nitclient=' + nitclient + '&name=' + name;
    $.getJSON(url, function (result)
    {

        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        } else
        {
            if (result.TRUE)
            {
                var headerMsg = "Nombre existente";
                var comentMsg = "El nombre de la selección es único para cada cliente, " +
                        "por favor ingrese un nombre nuevo.";
                //$('#pickingName').val('');
                alertWindow(headerMsg, comentMsg);
                focusNit();

            } else
            {
                if (result.FALSE)
                {
                    //Guardar todo
                }
            }
        }
    });
}

function focusNit() {
    $('#pickingName').val('');
    $('#pickingName').focus();
    $('#pickingName').css("background-color", "#f5bcb6");

}
// Loading NIT and enterprice name combobox
// this function calculates the size of an object.
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;
// End function object size.
// ids and compare has to have the values at same level. ids[0] corresponds to compare[0]


$("#selectNITClient").change(function () {

    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});
$("#selectContact").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});
$("#selectEName").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});
$("#selectTradeMark").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});

function fillClientData(select)
{
    for (var j = 0; j < ObjectSize(ids); j++)
    {
        $("#" + ids[j]).val(select.value);
        $("#" + ids[j]).data('combobox').refresh();
    }
}

function clearClientSelects()
{
    // Clearing all the data function from validateOnSave.js file
    clearData();
    for (var j = 0; j < ObjectSize(ids); j++)
    {
        // Clearing combobox
        $("#" + ids[j]).data('combobox').clearTarget();
        $("#" + ids[j]).data('combobox').clearElement();
    }
    validateName();

}

function clearData()
{
    // Clearing combobox
    $("#selectNITClient").data('combobox').clearTarget();
    $("#selectNITClient").data('combobox').clearElement();
    $("#selectEName").data('combobox').clearTarget();
    $("#selectEName").data('combobox').clearElement();
    $("#selectContact").data('combobox').clearTarget();
    $("#selectContact").data('combobox').clearElement();
    $("#selectTradeMark").data('combobox').clearTarget();
    $("#selectTradeMark").data('combobox').clearElement();
}

// this function calculates the size of an object.
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;
function datacsv()
{
    // var url = 'php/loadClientBoxes.php  ';
    var url = 'php/csvtojson.php';
    $.getJSON(url, function (result)
    {
        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        } else
        {
            if (result)
            {
                //createBoxesTable(result);
                tablecsv(result);
                reviewAvalaible();
            }
        }
    });
}

function tablecsv(result)
{

    $('#referencesTable tfoot th').each(function (i) {
        var title = $('#referencesTable thead th').eq($(this).index()).text();
        $(this).html('<input type="text" placeholder="Buscar " data-index="' + i + '" />');
    });
    $("#panel-boxes").attr("style", 'display:true;');
    $("#cancel-save").attr("style", 'display:true;');
    var table = $('#referencesTable').DataTable({
        data: result,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        scrollY: "300px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        fixedColumns: true
    });

    // Filter event handler
    $(table.table().container()).on('keyup', 'tfoot input', function () {
        table
                .column($(this).data('index'))
                .search(this.value)
                .draw();
    });



    $("#avalaibleunits").attr("style", 'display:true;');
    $("#panel-csv").attr("style", 'display:true;');

}

function tableavalaibleunits()
{
    var url = 'php/unitsavalaible.php';
    $.getJSON(url, function (result)
    {
        if (result['DATA']) {
        }
    });
}

function loadAvailableRef()
{
    // Load references or EAN.
    // $.getJSON('php/loadReferences.php?docTransporte='+doctte, function(data) {
    // 	/* called when request to archivo.php completes */
    // 	var referencesSize = ObjectSize(data);
    // 	createReferencesFields(data,nationalizedArray);
    // }); //
    //var url = 'php/loadClientBoxes.php';
    var url = 'php/csvtojson.php';
    $.getJSON(url, function (result)
    {
        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        } else
        {
            if (result)
            {
                //createBoxesTable(result);
                displaytabledata(result);
            }
        }
    });
}

function reviewAvalaible() {

    var nit = $('#selectNITClient').val();

    var url = 'php/unitsavalaible.php?nit=' + nit;

    $.getJSON(url, function (result) {

        if (result["DATA"])
        {
            displayunitsclient(result["DATA"]);
        }

    });

}

function displayunitsclient(result)
{
    $('#realdaata').DataTable({
        data: result,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });

}



/**
 * This function creates a table and allows the user
 * to select the merchandice he want's.
 * 
 */
function createBoxesTable(boxes)
{
    var boxesRows = boxes['DATA'];
    alert(boxesRows);
    // Showing boxes's table panel

    $("#panel-boxes").attr("style", 'display:true;');
    // Showing cancel-save buttons
    $("#cancel-save").attr("style", 'display:true;');
    // Constructing boxes table
    var columnsSize = ObjectSize(boxesRows[0]);//5 ROW'S NUMBERS
    var boxesTable = tableBoxesConstructor(boxes);//return table 
    var numberOfRows = ObjectSize(boxesRows);//
    //  alert(boxes['HEADERS'][0]);//EAN

    // Creating first row, to select boxes
    var selectBoxRow = createBoxSelectRow(boxes['HEADERS'][0]);//RETURN THE FIRST ROW OF TITTLE
    // Adding rows to table
    boxesTable.row.add(selectBoxRow).draw();

    // Adding rows to the table.
    for (var loopRows = 0; loopRows < numberOfRows; loopRows++)
    {
        // If row has data
        if (ObjectSize(boxesRows[loopRows][0]))
        {
            // Creating check all and input amount to select
            var toDispatch = document.createElement('input');
            var checkrefjs = document.createElement('input');
            // Create tr element
            var tr = document.createElement('tr');
            // Adding row information to each column cell.

            for (var cellFor = 0; cellFor < columnsSize; cellFor++)
            {
                // Reference id to the fields
                var boxReference = boxesRows[loopRows][0];
                // Creating td container
                var td1 = document.createElement('td');
                if (cellFor < 4)
                {
                    var columnName = $(boxesTable.column(cellFor).header()).html();

                    td1.innerText = '' + boxesRows[loopRows][cellFor];
                    td1.setAttribute('id', boxReference + '-' + columnName);
                }
                if (cellFor == 4)
                {
                    // Setting amount attributes
                    toDispatch.setAttribute('id', boxReference);
                    toDispatch.setAttribute('name', 'toDispatch[]');
                    // toDispatch.setAttribute('onChange','validateToDispatch(this)');
                    // toDispatch.setAttribute('onChange','alert("right now")');
                    // toDispatch.setAttribute('onkeydown','if (event.keyCode == 13) { event.preventDefault(), validateToDispatch(this) }');
                    toDispatch.setAttribute('type', 'text');
                    toDispatch.setAttribute('style', 'width:50px;');
                    toDispatch.value = '0';
                    // adding validate data function
                    toDispatch.addEventListener('change',
                            function () {
                                validateToDispatch(this);
                            },
                            false
                            );
                    // appending to td container
                    td1.appendChild(toDispatch);
                }
                if (cellFor == 5)
                {
                    // Setting checkbox attributes
                    checkrefjs.setAttribute('name', 'dispatch[]');
                    checkrefjs.setAttribute('id', boxReference + '-check');
                    checkrefjs.setAttribute('type', 'checkbox');
                    // adding select all available function
                    checkrefjs.addEventListener('click',
                            function () {
                                checkit(this);
                            },
                            false
                            );
                    td1.appendChild(checkrefjs);
                }

                tr.appendChild(td1);
            }
            // Adding rows to table
            boxesTable.row.add(tr).draw();
        }
    }

    // Row of total amounts
    var totalsRow = createTotalsAmount();
    // Adding rows to table
    boxesTable.row.add(totalsRow).draw();
    // Adjusting columns size
    boxesTable.columns.adjust().draw();
    // Calculating the beggining total
    calculateBegginingTotal();
}
//
function createBoxSelectRow(firstHeader) {//(EAN)
//	 Adding select bults by total amount row
//	// Creating tr element
    var tr = document.createElement('tr');
//	// Creating td columns elements
//	
    var tdAllAmountC0 = document.createElement('td');
    var tdAllAmountC1 = document.createElement('td');
    var tdAllAmountC2 = document.createElement('td');
    var tdAllAmountC3 = document.createElement('td');
    var tdAllAmountC4 = document.createElement('td');
    var tdAllAmountC5 = document.createElement('td');

    // Setting columns values
    tdAllAmountC0.innerText = 'Digitar cantidad de unidades a seleccionar';
    tdAllAmountC1.innerText = '';
    //tdAllAmountC2.innerText = '';
    //tdAllAmountC3.innerText = '';
    tdAllAmountC4.innerText = '';
    // Setting select box attributes
    if (firstHeader == 'EAN')
    {
        var inputBoxAmount = document.createElement('input');
        inputBoxAmount.setAttribute('type', 'text');
        inputBoxAmount.setAttribute('style', 'width:50px;');
        inputBoxAmount.value = '0';
        // adding select all available function
        inputBoxAmount.addEventListener('change',
                function () {
                    checkTheFirst(this);
                },
                false
                );
        // appending column 4
        tdAllAmountC3.appendChild(inputBoxAmount);
    }
    // Check all button
    var checkAll = document.createElement('input');
    checkAll.setAttribute('id', 'checkAll');
    checkAll.setAttribute('type', 'checkbox');
    // adding select all available function
    checkAll.addEventListener('change',
            function () {
                // Toggle all checkbox with dispatch[] name.
                $("input:checkbox[name=\'dispatch[]\']").prop('checked', $(this).prop("checked"));
                checkitAll();
            },
            false
            );

    // Appendign columns to row
    tr.appendChild(tdAllAmountC0);
    tr.appendChild(tdAllAmountC1);
    tr.appendChild(tdAllAmountC2);
    tr.appendChild(tdAllAmountC3);
    tr.appendChild(tdAllAmountC4);
    //tdAllAmountC4.appendChild(checkAll);
    tr.appendChild(tdAllAmountC5);
    return tr;
}
//
function createTotalsAmount() {
    // creating columns elements
    var trTotal = document.createElement('tr');
    var total = document.createElement('td');
    var bill = document.createElement('td');
    //var real				= document.createElement('td');
    var available = document.createElement('td');
    var dispatchTotal = document.createElement('td');
    var checkBox = document.createElement('td');

    // setting attributes
    bill.setAttribute('id', 'billTotal');
    //real.setAttribute('id','realTotal');
    available.setAttribute('id', 'availableTotal');
    dispatchTotal.setAttribute('id', 'dispatchTotal');

    total.innerText = 'Total';
    bill.innerText = '0';
    //real.innerText			= '0';
    available.innerText = '0';
    dispatchTotal.innerText = '0';
    checkBox.innerText = '';

    // appending childs
    trTotal.appendChild(total);
    trTotal.appendChild(bill);
    //trTotal.appendChild(real);
    trTotal.appendChild(available);
    trTotal.appendChild(dispatchTotal);
    trTotal.appendChild(checkBox);

    return trTotal;
}

/**
 * This function calculates the sum of the inicial values
 * of the columns
 */
function calculateBegginingTotal()
{
    // Loading all checkboxes
    var totalBox = $("input:text[name=\'toDispatch[]\']");
    var BoxSize = totalBox.length;
    var totalBillAmount = 0;
    var totalRealAmount = 0;
    var totalAvailableAmount = 0;
    for (var qjs = 0; qjs < BoxSize; qjs++)
    {
        // loading reference/EAN
        var reference = totalBox[qjs].id;
        var getBillElement = document.getElementById(reference + '-INGRESADOS');
        var getRealElement = document.getElementById(reference + '-RECIBIDOS');
        var getAvailableElement = document.getElementById(reference + '-DISPONIBLES');

        // Sum total
        totalBillAmount += parseInt(getBillElement.innerHTML);
        totalRealAmount += parseInt(getRealElement.innerHTML);
        totalAvailableAmount += parseInt(getAvailableElement.innerHTML);
    }
    // Loading elements
    var billTotal = document.getElementById('billTotal');
    var realTotal = document.getElementById('realTotal');
    var availableTotal = document.getElementById('availableTotal');
//
    // Setting sum values
    billTotal.innerText = '' + totalBillAmount;
    realTotal.innerText = '' + totalRealAmount;
    availableTotal.innerText = '' + totalAvailableAmount;
}

function tableBoxesConstructor(boxes) {
    var colDef = []; // column definition
    //var boxesType = boxes['HEADERS'][0];
    //var headers = ['INGRESADOS','RECIBIDOS','DISPONIBLES','CANTIDADES','TODOS'];
    //var headers = ['DESTINOS','NUMERO IMPORTACIÓN'];
    var headers = ['EAN', 'REFERENCIA', 'DESTINO', 'CANTIDAD', 'IMPORTACIÓN'];
    // Prepending EAN or REFERENCIAS header
    //headers.unshift(boxesType);
    var headersSize = ObjectSize(headers);//5
    // Creating columns from headers variable

    for (var headersFor = 0; headersFor < headersSize; headersFor++)
    {
        colDef[headersFor] = {
            aTargets: [headersFor],
            sTitle: headers[headersFor]};
    }

    // If dataTable exist, delete
    if ($.fn.dataTable.isDataTable('#referenceTable')) {
        table = $('#referenceTable').DataTable();
        table.destroy();
        $('#referenceTable').empty();
        table = $('#referenceTable').DataTable();
//         table.clear();
//    }
//     table construct
        table = $('#referenceTable').DataTable(
                {
                    "bJQueryUI": true,
                    "sPaginationType": "full_numbers",
                    "bProcessing": true,
                    "bDeferRender": true,
                    "bInfo": false,
                    "bDestroy": true,
                    "bFilter": false,
                    "bPagination": false,
                    "aaData": results,
                    "aoColumns": cols,
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "No se encontraron resultados",
                        "sEmptyTable": "Ningún dato disponible en esta tabla",
                        "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "sInfoThousands": ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sLast": "Último",
                            "sNext": "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    },
                    "aoColumnDefs": colDef
                });
//
        return table;
    }

// check uncheck all checkbox
    $('#checkAll').change(function ()
    {
        $("input:checkbox[name=\'dispatch[]\']").prop('checked', $(this).prop("checked"));
        checkitAll();
    });
//
    function checkitAll()
    {
        // Loading all the checkBox
        var totalBox = $("input:checkbox[name=\'dispatch[]\']");
        var BoxSize = totalBox.length;
        // Loading available references on the input box.
        for (var sjs = 0; sjs < BoxSize; sjs++)
        {
            checkit(totalBox[sjs]);
        }
    }

// This function pass the available amount to dispatch box.
// obj checkbox type ref-check
    function checkit(obj)
    {
        var splitReference;
        splitReference = obj.id.split('-');
        var inputReference = document.getElementById(splitReference[0]);
        var availableReference = document.getElementById(splitReference[0] + '-DISPONIBLES');
        if (obj.checked)
        {
            inputReference.value = availableReference.innerText;
        } else
        {
            inputReference.value = 0;
        }
        sumToDispatch();
    }
//
///** This function adds the total amount to dispatch
// * to the 'dispatchTotal' box.
//*/
    function sumToDispatch()
    {
        var totalBox = $("input:text[name=\'toDispatch[]\']");
        var BoxSize = totalBox.length;
        var TotalAmount = 0;
        for (var qjs = 0; qjs < BoxSize; qjs++)
        {
            TotalAmount = TotalAmount + parseInt(totalBox[qjs].value);
        }
        var dispatchTotal = document.getElementById('dispatchTotal');
        dispatchTotal.innerText = '' + TotalAmount;
    }
//
//// This function validate the dispatch box and sum the total.
    function validateToDispatch(inputValue)
    {
        var Available = parseInt(document.getElementById(inputValue.id + '-DISPONIBLES').innerText);
        var IntValue = parseInt(inputValue.value);
        // If IntValue is greater than available reference.
        if (IntValue >= Available)
        {
            inputValue.value = Available;
            // Check checkbox
            $('#' + inputValue.id + '-check').prop('checked', true);
        } else
        {
            // Uncheck checkbox
            $('#' + inputValue.id + '-check').prop('checked', false);
            // If IntValue is character or empty insert '0'.
            if (!IntValue || IntValue < 0)
            {
                inputValue.value = 0;
            }
        }
        sumToDispatch();
    }

    function checkTheFirst(first)
    {
        var IntFirst = parseInt(first.value);
        if (IntFirst > 0)
        {
            // Loading all the checkBox
            var totalBox = $("input:checkbox[name=\'dispatch[]\']");
            var BoxSize = totalBox.length;
            if (BoxSize < IntFirst)
            {
                IntFirst = BoxSize;
            }
            // Loading available references on the input box.
            for (var tjs = 0; tjs < IntFirst; tjs++)
            {
                // checking the box
                totalBox[tjs].checked = true;
                // passing the available amount to the dispatch box.
                checkit(totalBox[tjs]);
            }
        } else
        {
            // If IntValue is character or empty insert '0'.
            first.value = 0;
        }
    }



    function clearTable()
    {
        // If dataTable exist, delete
        if ($.fn.dataTable.isDataTable('#referencesTable')) {
            table = $('#referencesTable').DataTable();
            table.destroy();
            $('#referencesTable').empty();
            // table = $('#referenceTable').DataTable();
            // table.clear();
        }
    }

    function clearClientData()
    {
// Cleaning client info
        $('#selectNITCliente').val('');
        $('#selectEName').val('');
        $('#selectContact').val('');
        $('#selectTradeMark').val('');
    }


    function clearCombobox(object)
    {
        // Cleaning output type
        // Clearing combobox
        object.data('combobox').clearTarget();
        object.data('combobox').clearElement();
    }


    $('#cancel-all-upload').click(function () {
        deletefilecsv();
    });
    $('#cancelData').click(function () {
        // Clearing table
        clearTable();
        // Clearing Outpuy type select
        clearCombobox($('#tiposalidabox'));
        // Clearing client info
        clearClientData();
        // Clearing transport's document
        clearCombobox($('#transportDoc'));
        // Clearing client's importation
        clearCombobox($('#importation'));
        // Clearing selection name
        $('#pickingName').val('');
        // Hidding boxes's table panel
        $("#panel-boxes").attr("style", 'display:none;');
        // Hidding cancel-save buttons
        $("#cancel-save").attr("style", 'display:none;');
    });

    $('#cancel').change(function () {
        clearTable();
    });
    /**
     * Save button pressed
     */
    $('#save').click(function ()
    {
        // calling function from validationfields.js
        //saveClicked();
        validateName();
        saveclicked();
        queryFile();
    });

    function saveclicked()
    {
        if ($('#pickingName').val() === '')
        {
            alert('El nombre de la selección no puede estar vacío');
        }
    }

    function queryFile()
    {
        var name = $('#pickingName').val();
        var nitclient = $('#selectNITClient').val();
        //var url = 'php/validateName.php?nitclient=' + nitclient + '&name=' + name;
        var url = "php/queryfile.php";

        $.getJSON(url, function (result)
        {
            alert(result['DATA']);
            if (result['ERROR'])
            {
                alertWindow(result['ERROR'][0], result['ERROR'][1]);
            } else
            {
                if (result['DATA'])
                {
                    var headerMsg = "";
                    var comentMsg = "";
                    //alertWindow(headerMsg, comentMsg);
                    var imp = result['DATA'];
                    savePicking(imp);

                } else
                {
                    //
                }
            }
        });
    }

    function savePicking(imp)
    {
        var name = $('#pickingName').val();
        var tamimpav;
        var impav;
        var nitclient = $('#selectNITClient').val();
        var tiposalida = $('#tiposalidabox').val();
        var tam = ObjectSize(imp);

        var url = 'php/importacionesdisponibles.php?nitclient=' + nitclient;

        var impav = $.getJSON(url, function (result)
        {
            if (result["DATA"]) {
                var headerMsg = "";
                var comentMsg = "";
                //alertWindow(headerMsg, comentMsg);
                var tamimpav = ObjectSize(impav);
            }
        });

        alert(impav["DATA"]);
        var vble;
        var urll = 'php/importacionesarchivo.php';
        var limpav = $.getJSON(urll, function (result)
        {
            vble = result["DATA"];
            alert(vble);
        });

        //alert(vble);

        for (var ijs = 0; ijs < tam; ijs++)
        {
            for (var ijt = 0; ijt < tamimpav; ijt++)
            {
                if (imp[ijs] === impav[ijt])
                {
                    alert('La importación' + imp[ijs]);
                }

            }

        }
    }
}