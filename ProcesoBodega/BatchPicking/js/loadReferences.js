/**
 * Units header
 */
var unitsHeader = {
	0: "REFERENCIA",
	1: "CANTIDADES"
}

/**
 * Boxes header
 */
var boxHeader = {
	0: "REFERENCIA",
	1: "CANTIDADES"
}

/**
 * This function loads the file name, and 
 * call a php file to load the references.
 * @param file: file.
 */
function loadReferences(file) {
    if(file.name == 'referencias bultos.csv')
    {
        loadReferencesFile(file.name,boxHeader,'box');
    }
    if(file.name == 'referencias unidades.csv')
    {
        
        loadReferencesFile(file.name,unitsHeader,'units');
    }
}

/**
 * Call php file to load the references
 */
function loadReferencesFile(fileName,header,type) {
    // converting variable in JSON string
    header = JSON.stringify(header);
    var filePath = $('#fileupload').attr('action')+'files/';
    var url = 'php/loadReferencesFile.php?filePath='+filePath+
        '&fileName='+fileName+'&header='+header;
    $.getJSON(url, function(references)
    {
        if(references['ERROR'])
        {
            var headerMsg = references['ERROR'][0];
            var comentMsg = references['ERROR'][1];
            alertWindow(headerMsg,comentMsg,saveClientData);
        }
        else
        {
            saveClientData(type,references);
        }
    });
}