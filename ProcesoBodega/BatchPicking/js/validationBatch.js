// This file validates the actions related to Batch Picking.
var GlobalFile;
// This is the global picking
var globalPicking;
	
// this function calculates the size of an object.
function ObjectSize(obj) 
{
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};
// End function object size.

// Submit page
function submit_Pick(arr_forms)
{
	var valid = true;
	var ouputbox		= document.getElementsByName('tiposalidabox')[0];
	var uploadformjs	= document.getElementById('fileupload');
	
	if(!ouputbox.value)
	{
		valid = false;
	}
	
	// if validations are good submit
	if(valid)
	{
		var className = document.getElementsByName["uploadName"];
		//alert(className);
		// upload files
		uploadformjs.elements['order-start-upload'].click();
		/*setTimeout(function()
		{
			var fields = ' ';
			$.each(arr_forms, function(index,value)
			{
				var serial = value.serialize();
				fields = fields +'&'+ serial;
			});
			// deletting the first &
			fields = fields.replace('&','');
			var repla;
			// Replacing all %5B by [ and %5D by ]
			repla = fields.replace(/%5B/g,'[');
			repla = repla.replace(/%5D/g,']');
			//alert(repla);
			var form = createFormBatch(repla);
			form.submit();
		}, 1000);*/
	}
}

/**
	*	Create a form using a hash of fields (input names and values).
	*	
	*	@var action {string} - the form action
	*	@var fields {hash} - hash of input names and values 
	*	@return {object} - returns a form object 
	*/
	function createFormBatch(action,fields)
	{
		var form = document.createElement('form');
		//form.style.display = 'none';
		$(form).attr("name","ghostform");
		$(form).attr("id","ghostform");
		//$(form).attr("novalidate","");
		//$(form).attr("data-parsley-validate","true");
		//$(form).attr("ng-submit","ghostform.$valid");
		//form.className = "form-horizontal form-bordered";
		document.getElementById('content').appendChild(form);
		form.method = 'post';
		//form.action = action;
		createFormFieldsBatch(form, fields);
		return form;
	}
	/**
	*	Create hidden input fields for a form.
	*	
	*	@var form {object} - the form
	*	@var fields {hash} - hash of input names and values 
	*/
	function createFormFieldsBatch(form, fields)
	{
		//fields= fields.deserialize();
		//alert("formfield fields: "+fields);
		//var res = fields.replace(/=/gi,":");
		var newfields = fields.split("&");
		//alert(newfields);
		$.each(newfields,function(key,value)
		{
			createInputBatch(form, key, value);
		});
		var submitb = document.createElement('button');
		$(submitb).attr('type',"submit");
		$(submitb).attr("class","btn btn-primary btn-block");
		$(submitb).attr("id","hiddentsubmit");
		//$(submitb).attr("style","display:none;");
		form.appendChild(submitb);
	}
	/**
	*	Add a single hidden input field to a form
	*	
	*	@var form {object} - the form
	*	@var hash_pair {object} - a hash pair of key and value 
	*/
	function createInputBatch(form, key, value)
	{
		var splitvalue = value.split("=");
		var input = document.createElement('input');
		input.setAttribute('type', 'hidden');
		input.setAttribute('name', splitvalue[0]);
		//input.setAttribute('data-parsley-type',"number");
		//input.setAttribute('required','');
		// Rails' check_box form helper uses the same name for a given checkbox and
		// its accompanying hidden field
		if ($.isArray(splitvalue[1]))
		{
			input.setAttribute('value',splitvalue[1].max());
		} else {
			input.setAttribute('value', splitvalue[1]);
		}
		form.appendChild(input);
	}
	
// If output type selected show upload file
$('#tiposalidabox').change(function()
{
	// Cleaning table.
//	$('#Batch-table').dataTable().destroy();
	var tiposalidaSelected = this.value;
	var uploadformjs	= document.getElementById('fileupload');
	if(tiposalidaSelected)
	{
		// Displaying fileupload
		//$('#fileupload').attr('style','display:true');
	}
	else
	{
		// Hiding fileupload
		$('#fileupload').attr('style','display:none');
		uploadformjs.elements["order-cancel-upload"].click();
		eraseFirstFile();
		// Erasing client data
		eraseClientData();
	}
});

/*$('#NITcliente').chage(function()
{
	alert('Ive change');
});*/

// Validating the file
function startPicking(file)
{
	checkPicking(file);
}

// write uploaded file.
function saveUploadPicking(file)
{
	var hiddenupfilesjs = document.getElementById('hiddenupfiles');
	hiddenupfilesjs.value = hiddenupfilesjs.value + ",["+file.name+","+file.size+"]";
}

// This function loads the request mercandice document
// create a temporary table on database and validate
// if the mercandice requested amount is available.
function checkPicking(file)
{
	if(!GlobalFile)
	{
		/*var uploadformjs	= document.getElementById('fileupload');
		uploadformjs.elements['order-start-upload'].click();/**/
		//var startUp = document.getElementById('start-upload-button '+file.name);
		//startUp.setAttribute('style','display:true;');/**/
		//startUp.click();
		GlobalFile = file.name;
		setTimeout(function()
		{
			var uploadformjs	= document.getElementById('fileupload');
			if(typeof trpool == 'undefined')
			{
				var trpool = document.createElement('div');
			}
			
			uploadformjs.elements['order-start-upload'].click();
			setTimeout(function()
			{
				var pathFile = "/var/www/Mercury/assets/plugins/jquery-file-upload/server/php/files/"+file.name;
				var client = document.getElementById('hiddenidclient').value;
				var urlvariable = "path="+pathFile+"&fileName="+file.name+"&fileSize="+file.size+"&clientid="+client; 
				$.getJSON('php/tmpTable.php?'+urlvariable, function(data)
				{
					//alert(JSON.stringify(data));
					globalPicking = data;
					var colDef=[];
					// columns size less color column
					var columnsSize = ObjectSize(data['picking'][0])-1;
					var headersSize = ObjectSize(data['headers']);
					for(var headersFor=0;headersFor<headersSize;headersFor++)
					{
						colDef[headersFor]={aTargets: [headersFor], sTitle: data['headers'][headersFor]};
					}
					
					if(data['picking'][0] !== null)
					{
						// Deleting table if exists
						if(typeof table !== 'undefined')
						{
							$('#Batch-table').DataTable().destroy();
							//$('#data-table').DataTable().destroy();
							table = undefined;
							// deletting all childs
							if(typeof trpool !== 'undefined')
							{
								while (trpool.firstChild)
								{
									trpool.removeChild(trpool.firstChild);
								}
							}
						}
						table = $('#Batch-table');
						//table = $('#data-table');
						table.DataTable(
						{
							//"bJQueryUI": true,
							//"sPaginationType": "full_numbers",
							//"bProcessing": true,
							//"bDeferRender": true,
							//"bInfo" : false,
							//"bDestroy" : true,
							//"bFilter" : false,
							//"bPagination" : false,
							//"aaData": results,
							//"aoColumns": cols,
							"aoColumnDefs": colDef
						});
						
						var colorArray=
						{
							GREEN:'success',
							RED:'danger'
						};
						
						var pickingAllSize = ObjectSize(data['picking']);
						for(var pickingAll=0;pickingAll<pickingAllSize;pickingAll++)
						{
							var tr = document.createElement('tr');
							for(var rowFor=0;rowFor<columnsSize;rowFor++)
							{
								var td1 = document.createElement('td');
								td1.innerText = ''+data['picking'][pickingAll][rowFor];
								tr.appendChild(td1);
							}
							if(data['picking'][pickingAll]['color'])
							{
								tr.setAttribute('class',colorArray[data['picking'][pickingAll]['color']]);
							}
							// Adding tr to trpool.
							trpool.appendChild(tr);
							//$('#Batch-table').dataTable().fnAddData(tr);
							table.dataTable().fnAddData(tr);
						}
						// Adjusting column size.
						//$('#Batch-table').dataTable().fnAdjustColumnSizing();
						table.dataTable().fnAdjustColumnSizing();
						// showing save and cancel button.
						$('#cancelpick').attr('style','display:true;');
						$('#save').attr('style','display:true;');
					}
					else
					{
						table = $('#Batch-table').DataTable({
							"aoColumnDefs": colDef
						});
						$('#cancelpick').attr('style','display:true;');
					}
					// hiding upload buttons.
					$('#order-start-upload').attr('style','display:none;');
					$('#order-cancel-upload').attr('style','display:none;');
					$('#order-delete-upload').attr('style','display:none;');
					$('#order-add-upload').attr('style','display:none;');
/**/				});
			}, 1000);
		}, 1000);
	}
}

function eraseFirstFile()
{
	GlobalFile='';
}

function eraseClientData()
{
	for(var j=0;j<ObjectSize(compare);j++)
		document.getElementById(ids[j]).value = '';
	document.getElementById('hiddenidclient').value = '';
}

function uploadData()
{
	var pickingName = document.getElementById('pickingName').value;
	var url ="php/consultPickingName.php?pickingName="+pickingName; 
	$.getJSON(url, function(resultName)
	{
		if(resultName)
		{
			// hiding upload buttons.
			$('#save').attr('style','display:none;');
			
			var clientId = document.getElementById('hiddenidclient').value;
			var tipoSalida = $('#tiposalidabox').val();
			var pickingName = $('#pickingName').val();
			var urls = "php/uploadPicking.php";
			var data = 
			{
				clientId:	clientId,
				pickingName: pickingName,
				tipoSalida:	tipoSalida,
				headers: 	globalPicking['headers'],
				picking: 	globalPicking['picking']
			};
			data = JSON.stringify(data);
			$.ajax({
				type:           'POST',
				cache:          false,
				dataType: 		"json",
				url:            urls,
				data:           {data: data},
				success: function(response)
				{
					// When the time changing between acceptSavePicking
					// and uploadData is too short, the page freezes
					// whit a time out the problem is solve. 
					setTimeout(function()
					{
						if(response)
						{
							if(response[0] == "Error")
							{
								document.getElementById('alert-Header').innerHTML = response[1];
								document.getElementById('alert-comentary').innerHTML = response[2];
								document.getElementById('alert-trigger').click();
							}
							else if(response[0] == "Exito")
							{
								document.getElementById('modal-Header').innerHTML = response[1];
								document.getElementById('modal-Note').innerHTML = response[2];
								document.getElementById('modal-accept-button').removeAttribute('onclick');
								document.getElementById('modal-trigger').click();
							}
							else if(response[0] == "Cuidado")
							{
								var functioncall = 'cancelPicking('+JSON.stringify(response[3])+')';
								document.getElementById('warning-Header').innerHTML = response[1];
								document.getElementById('warning-comentary').innerHTML = response[2];
								document.getElementById('warning-cancel-button').setAttribute('onclick',functioncall);
								document.getElementById('warning-trigger').click();
							}
						}
					},500);
		/**/    }
			});
		}
		else
		{
			document.getElementById('alert-Header').innerHTML = "Solicitud no realizada";
			document.getElementById('alert-comentary').innerHTML = "Nombre de picking ya existente, cambiar a un nombre diferente.";
			document.getElementById('alert-trigger').click();
		}/**/
	});
}

function acceptSavePicking()
{
	document.getElementById('modal-Header').innerHTML = "Guardar picking";
	document.getElementById('modal-Note').innerHTML = "Está seguro que desea guardar el picking.";
	document.getElementById('modal-accept-button').setAttribute('onclick','uploadData()');
	document.getElementById('modal-trigger').click();
}

function cancelPicking(tableName)
{
	var urls = "php/deletePicking.php";
	$.ajax({
		type:           'POST',
		cache:          false,
		dataType: 		"json",
		url:            urls,
		data:           {tableName: tableName},
		/*success: function(result) 
		{
			
		}/**/
	});
}