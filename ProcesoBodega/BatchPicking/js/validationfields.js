/**
 * This function validates the input data
 */
function saveClicked()
{
    var totalBoxes = $('#dispatchTotal').text();
    totalBoxes = parseInt(totalBoxes);
    if (totalBoxes)
    {
        // Validating forms
        var validateForms = false;
        var validateClient = $('#client-form').parsley().validate();
        validateForms = !validateClient;
        // if form validated
        if (!validateForms)
        {
            validateName();
        }
    } else
    {
        var headerMsg = "Consulta no realizada";
        var comentary = "No se ha seleccionado ninguna cantidad.";
        alertWindow(headerMsg, comentary);
    }
}

function validateName()
{
    var name = $('#pickingName').val();
    var transportDoc = $('#transportDoc').val();
    var url = 'php/validateName.php?name=' + name + '&transportDoc=' + transportDoc;
    $.getJSON(url, function (result)
    {
        var obj = JSON.stringify(result); //AUn no lo uso
        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        } else
        {
            if (result.TRUE)
            {
                var headerMsg = "Nombre existente";
                var comentMsg = "El nombre de la selección es único, " +
                        "por favor ingrese un nombre nuevo.";
                //$('#pickingName').val('');
                alertWindow(headerMsg, comentMsg);

            } else
            {

                if (result.FALSE)
                {
                    //Guardar todo

                }

            }
        }
    });
}

function saveClientDataConfirm()
{
    var underLimit = 400;
    var upperLimit = 600;
    var partialProcessing = 408;
    var maintenance = 409;
    var outputType = $('#tiposalidabox').val();
    // if true, the merchandice need nationalization
    //if((outputType > underLimit) && (outputType != maintenance) && (outputType != partialProcessing) && (outputType<upperLimit))
    //{
    validateNationalization();
    //}
}

function validateNationalization()
{
    var transportDoc = $('#transportDoc').val();
    var importation = $('#importation').val();
    // column 0 is the boxes's type
    var boxesType = $($('#referenceTable').DataTable().column(0).header()).html();
    var valuesArray = Array();
    // Loading all the input box to dispatch
    var totalBox = $("input:text[name=\'toDispatch[]\']");
    var boxSize = totalBox.length;
    var TotalAmount = 0;
    for (var qjs = 0; qjs < boxSize; qjs++)
    {
        if (parseInt(totalBox[qjs].value))
        {
            valuesArray.push(
                    {
                        0: totalBox[qjs].id,
                        1: totalBox[qjs].value
                    });
        }
    }
    var data = {
        transportDoc: transportDoc,
        boxesType: boxesType,
        valuesArray: valuesArray
    };
    dataStringify = JSON.stringify(data);
    var url = 'php/checkNationalization.php';
    $.ajax({
        type: "POST",
        url: url,
        async: true,
        cache: false,
        dataType: "json",
        data: {data: dataStringify},
        timeout: 50000, /* Timeout in ms */
        success: function (responce)
        { /* called when request to archivo.php completes */
            if (responce['ERROR'])
            {
                alertWindow(responce['ERROR'][0], responce['ERROR'][1]);
            } else
            {
                if (responce['DATA'])
                {
                    var respuesta;
                    var enter = "\n";
                    var valueImplode = "COMPLETAS;" +
                            boxesType + ";SOLICITADAS;DISPONIBLES" + enter;
                    var alertNotComplete = false;
                    responce['DATA'].forEach(function (value, key)
                    {
                        alertNotComplete = (value[0]) ? alertNotComplete : true;
                        value[0] = (value[0]) ? 'SI' : 'NO';
                        valueImplode += value.join(";") + enter;
                    }, this);
                    // var fileName = "detalle de bultos nacionalizados"+
                    // 	" solicitados y disponibles de la importación "
                    // 	+importation+".csv";
                    // var fileType = "text/csv";
                    // download(valueImplode,fileName, fileType);
                    if (alertNotComplete)
                    {
                        // Setting timeout to avoid messages windows 
                        // interposition
                        setTimeout(function () {
                            var headerMsg = "Hay butos NO nacionalizados";
                            var comentMsg = "En la selección actual, " +
                                    "hay bultos que no han sido nacionalizados, " +
                                    "¿Desea continuar con la selección?";
                            warningWindow(headerMsg, comentMsg, function () {
                                saveSelect(dataStringify);
                            });
                        }, 500);
                    }
                }
            }
        }
    });
}
function saveSelect(data) {
    var url = 'php/saveSelectedBoxes.php';
    $.ajax({
        type: "POST",
        url: url,
        async: true,
        cache: false,
        dataType: "json",
        data: {data: data},
        timeout: 50000, /* Timeout in ms */
        success: function (responce)
        { /* called when request to archivo.php completes */
            if (responce['ERROR'])
            {
                alertWindow(responce['ERROR'][0], responce['ERROR'][1]);
            } else
            {
                // Setting timeout to avoid messages windows 
                // interposition
                setTimeout(function () {
                    var headerMsg = "Mercancía seleccionada con éxito";
                    var comentMsg = "Se han seleccionado los bultos" +
                            "y las cantidades indicadas con éxito.";
                    successWindow(headerMsg, comentMsg);
                }, 500);
            }
        }
    });
}
/**
 *	Combine all forms in arr_forms into a new, hidden form. Submit the new form.
 *	
 *	@var action {string} - the form action
 *	@var arr_forms {array} - array of form objects 
 */
function submit_All(arr_forms)
{
    var valid = true;
    var referencesDispatch = document.getElementsByName('toDispatch[]');

    // if no reference was choose
    if (!referencesDispatch[0] && valid == true)
    {
        document.getElementById('alert-Header').innerHTML = "Solicitud no realizada.";
        document.getElementById('alert-comentary').innerHTML = "No se ha seleccionado ninguna referencia.";
        document.getElementById('alert-trigger').click();
        valid = false;
    } else
    {
        var sizeref = ObjectSizeAutocomplete(referencesDispatch);
        var anyref;
        for (var o = 0; o < sizeref; o++)
        {
            anyref = anyref || referencesDispatch[o].value > 0;
        }
        //alert(valid + " " + anyref);
        if (!anyref && valid == true)
        {
            document.getElementById('alert-Header').innerHTML = "Solicitud no realizada.";
            document.getElementById('alert-comentary').innerHTML = "No se ha seleccionado ninguna referencia.";
            document.getElementById('alert-trigger').click();
            valid = false;
        }
    }

    // if validations are good submit
    if (valid)
    {
        var fields = ' ';
        $.each(arr_forms, function (index, value)
        {
            var serial = value.serialize();
            fields = fields + '&' + serial;
        });
        // deletting the first &
        fields = fields.replace('&', '');
        var repla;
        // Replacing all %5B by [ and %5D by ]
        repla = fields.replace(/%5B/g, '[');
        repla = repla.replace(/%5D/g, ']');
        //alert(repla);
        var form = createForm(repla);
        form.submit();
    }
}

/**
 *	Create a form using a hash of fields (input names and values).
 *	
 *	@var action {string} - the form action
 *	@var fields {hash} - hash of input names and values 
 *	@return {object} - returns a form object 
 */
function createForm(fields)
{
    var form = document.createElement('form');
    //form.style.display = 'none';
    $(form).attr("name", "ghostform");
    $(form).attr("id", "ghostform");
    document.getElementById('content').appendChild(form);
    form.method = 'post';
    //form.action = action;
    createFormFields(form, fields);
    return form;
}
/**
 *	Create hidden input fields for a form.
 *	
 *	@var form {object} - the form
 *	@var fields {hash} - hash of input names and values 
 */
function createFormFields(form, fields)
{
    var newfields = fields.split("&");
    $.each(newfields, function (key, value)
    {
        createInput(form, key, value);
    });
    var submitb = document.createElement('button');
    $(submitb).attr('type', "submit");
    $(submitb).attr("class", "btn btn-primary btn-block");
    $(submitb).attr("id", "hiddentsubmit");
    $(submitb).attr("style", "display:none;");
    form.appendChild(submitb);
}
/**
 *	Add a single hidden input field to a form
 *	
 *	@var form {object} - the form
 *	@var hash_pair {object} - a hash pair of key and value 
 */
function createInput(form, key, value)
{
    var splitvalue = value.split("=");
    var input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', splitvalue[0]);
    //input.setAttribute('data-parsley-type',"number");
    //input.setAttribute('required','');
    // Rails' check_box form helper uses the same name for a given checkbox and
    // its accompanying hidden field
    if ($.isArray(splitvalue[1]))
    {
        input.setAttribute('value', splitvalue[1].max());
    } else {
        input.setAttribute('value', splitvalue[1]);
    }
    form.appendChild(input);
}