//function Validation() {
//
//}


$("#btnsave").click(function() {
    if ($("#clientform").parsley().validate())
    {
        // Confirmation message


        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar el Picking " + $("#pickingName").val() + " ?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'ValidationImportation()');
        document.getElementById('warning-trigger').click();
    }
});
function ValidationImportation() {

    var i;
    var impor = $("#importation").val();
    var cantFilas = $("#tableReferences tr").length;
    for (i = 1; i <= cantFilas; i++)
    {
        $('#tableReferences tr').eq(i).each(function() {
            var refe = $(this).find('td').eq(1).html();
            $(this).find('td').eq(3).each(function() {
                var imp = ($(this).html());

                if (impor === imp) {
                    ValidationReferencia()

                } else {
                    var headerMsg = "Número Importación Incorrecto";
                    var comentMsg = "Corresponde a la referencia " + refe;
                    alertWindow(headerMsg, comentMsg);
                    return false;

                }


            });
        });
    }
}
function ValidationReferencia() {
    var i;
    var cantFilas = $("#tableReferences tr").length;
    for (i = 1; i <= cantFilas; i++)
    {

        $('#tableReferences tr').eq(i).each(function() {

            $(this).find('td').eq(1).each(function() {
                var refe = ($(this).html());

                var url = 'php/validateReferences.php?ref=' + refe;

                $.getJSON(url, function(result) {
                    if (result['DATA'] == "1") {
                        ValidationCantidad();
                    } else {
                        var headerMsg = "Referencia Incorrecta";
                        var comentMsg = "La referencia " + refe + " del archivo no pertenece a la referencias guardadas";
                        alertWindow(headerMsg, comentMsg);
                        return false;
                    }

                });
            });
        });
    }
}


function ValidationCantidad() {
    var i;
    var cantFilas = $("#tableReferences tr").length;
    for (i = 1; i <= cantFilas; i++)
    {
        $('#tableReferences tr').eq(i).each(function() {
            var ref = $(this).find('td').eq(1).html();

            $(this).find('td').eq(2).each(function() {
                var cant = ($(this).html());

                var url = 'php/validateCantidad.php?ref=' + ref + '&cant=' + cant;
                console.log(url);
                $.getJSON(url, function(result) {

                    if (result['DATA'] == "1") {
                        saveSelectUnit();
                    } else {
                        var headerMsg = "Cantidades Incorrectas";
                        var comentMsg = "No existen cantidades suficientes para la referencia " + ref;
                        alertWindow(headerMsg, comentMsg);
                        return false;
                    }

                });
            });
        });
    }

}

function savePicking() {
    var tiposalida = $("#tiposalidabox").val();
    var nombre = $("#pickingName").val().toUpperCase();
    var transporte = $("#transportDoc").val();
    var url = 'php/saveSelection.php?tiposalida=' + tiposalida + '&nombre=' + nombre + '&transporte=' + transporte;
    $.getJSON(url, function(result)
    {
        if (result['ERROR']) {
            var headerMsg = "Error al guardar el picking";
            var comentMsg = "Revisar los datos e intentar nuevamente";
            alertWindow(headerMsg, comentMsg);
        } else {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Selección Por Unidades Guardada Correctamente";
            successWindow(headerMsg, comentMsg);
            return false;

        }

    });



}
function saveReferences() {

    var cantFilas = $("#tableReferences tr").length;
    var i;
    for (i = 1; i < cantFilas; i++)
    {
        var impo = document.getElementById("tableReferences").rows[i].cells[4].innerText;
        var Refe = document.getElementById("tableReferences").rows[i].cells[1].innerText;
        var Cant = document.getElementById("tableReferences").rows[i].cells[3].innerText;
        var destino = document.getElementById("tableReferences").rows[i].cells[2].innerText;
        var url = 'php/saveReferencesUnits.php?ref=' + Refe + '&destino=' + destino + '&cant=' + Cant + '&imp=' + impo;
        $.getJSON(url, function(result)
        {

            if (result) {
            } else {
                var headerMsg = "Error";
                var comentMsg = "Operación no realizada";
                errorWindow(headerMsg, comentMsg);
            }
        });

    }
}
function saveSelectUnit() {
   
    var cantFilas = $("#tableReferences tr").length;
    var i;

    for (i = 1; i < cantFilas; i++)
    {
        $(document.getElementById("tableReferences").rows[i].cells[1]).each(function() {

            var cant = document.getElementById("tableReferences").rows[i].cells[2].innerText;
            var x;
             
            for (x = 1; x <= cant; x++)
            {
                var trans = $("#transportDoc").val();
                var Refe = document.getElementById("tableReferences").rows[i].cells[1].innerText;
                var Cant = document.getElementById("tableReferences").rows[i].cells[2].innerText;
                var url = 'php/saveSelectedBoxes.php?transporte=' + trans + '&ref=' + Refe + '&cant=' + Cant;

                $.getJSON(url, function(result)
                {
                    if (result['ERROR']) {
                        var headerMsg = "Error en el picking";
                        var comentMsg = "Verificar Datos e Intente Nuevamente";
                        alertWindow(headerMsg, comentMsg);
                    }
                    else {
                        savePicking();
                    }
                });
            }
        });
    }
}
