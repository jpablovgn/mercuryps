$("#save").click(function(){
    // Validating forms
    var validateForms = false;
    var validateBox = $('#form-packages').parsley().validate();
    var validateClient = $("#formclient").parsley().validate();
    var validateImport = $('#demo-form-import').parsley().validate();
    var validateWareInfo = $('#demo-form-ware-info').parsley().validate();

    validateForms = !validateClient || !validateImport
        || !validateWareInfo || !validateBox;
	// if form validated
    if(!validateForms)
	{
        // Confirmation message
        var headerMsg = "GUARDAR";
        var comentMsg = "¿Está seguro que desea guardar los datos del cliente?";
        warningWindow(headerMsg,comentMsg,saveClientDataConfirm);
    }
});

function saveClientDataConfirm()
{
    var fileUploadPath = "../../../assets/plugins/jquery-file-upload/server/upload-files/";
    var NITjs = $('#selectNITClient').val();
    // Upper case the importation number.
    $('#numimportacion').val($('#numimportacion').val().toUpperCase());
    
    var importationjs = $('#numimportacion').val();
    var folderPath = fileUploadPath + NITjs + '/'+importationjs;
    var actionPath = "../../assets/plugins/jquery-file-upload/server/upload-files/"+ NITjs + '/'+importationjs+'/';
    $('#fileupload').attr('action',actionPath);
    var url = "php/copyBaseFolder.php?basePath="+fileUploadPath+
        '&NIT='+NITjs+
        '&importation='+importationjs;
    $.getJSON(url, function(route){
        if(route['ERROR'])
        {
            alertWindow(route['ERROR'][0],route['ERROR'][1]);
        }
        else
        {
            uploadFiles();
        }
    });
}

function uploadFiles() {
    // copy in index.php to upload the files.
    $('#TransportCopy').val($('#numdoctte').val());
    $('#transport-start-upload').click();
    var elements = document.getElementsByName('cancelIndividual');
    if(!ObjectSize(elements))
    {
        // if refbultosmanual checked load box's references
        saveClientData($('#refbultosmanual').prop('checked'));
    }
}

