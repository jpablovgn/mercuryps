// Picking Name
$('#pickingName').attr('placeholder',"Nombre de la selección");
$('#pickingName').attr('style',"text-transform:uppercase");
$('#pickingName').attr('data-parsley-maxlength',"25");
$('#pickingName').attr('data-parsley-required',true);

// Importation number
$('#importation').attr('data-parsley-type',"alphanum");
$('#importation').attr('placeholder',"Importación del clientee");
$('#importation').attr('style',"text-transform:uppercase");
$('#importation').attr('data-parsley-required',true);

// transport's document
$('#transportDoc').attr('data-parsley-type',"alphanum");
$('#transportDoc').attr('placeholder',"Documento de transporte");
$('#transportDoc').attr('style',"text-transform:uppercase");
$('#transportDoc').attr('readonly','');
$('#transportDoc').attr('data-parsley-required',true);


// Client selects
$('#selectNITClient').attr('data-parsley-required',true);
$('#selectNITClient').attr('placeholder',"NIT Cliente");

$('#selectEName').attr('data-parsley-required',true);
$('#selectEName').attr('placeholder',"Nombre de la Empresa");

$('#selectContact').attr('data-parsley-required',true);
$('#selectContact').attr('placeholder',"Contacto");

$('#selectTradeMark').attr('data-parsley-required',true);
$('#selectTradeMark').attr('placeholder',"Marca");

// Output's type
$('#tiposalidabox').attr('data-parsley-required',"true");
$('#tiposalidabox').attr('data-toggle', "modal");

