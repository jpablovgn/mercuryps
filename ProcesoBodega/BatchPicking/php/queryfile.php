<?php

//	$nitclient 	= $_GET['nitclient'];
//	$pickingName	= $_GET['name'];
//        $tiposalida     = $_GET['tiposalida'];

include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    $query = "SELECT NUMERO_IMPORTACION FROM MR_IMPORTACION_CSV";
    //$query = "CALL VALIDAR_NOMBRE_BATCH('$nitclient','$pickingName');";
    $queryResult = $connection->query($query);
    if ($queryResult) {
        $data = array();
        while ($tempData = $connection->fetch_array_assoc($queryResult)) {
            $data[] = $tempData;
        }
        $result['HEADERS'] = array_keys($data[0]);
        foreach ($data as $row) {
            foreach ($row as $key => $value) {
                $temp[] = $value;
            }
            $result['DATA'][] = $temp;
            unset($temp);
        }
    } else {
        $result['ERROR'][0] = "Error de consulta";
        $result['ERROR'][1] = "No se pudo realizar la consulta.";
    }

//                while($tmp = $connection->fetch_array($queryResult))
//                {
//                    $result = $tmp;
//                }
    $connection->close();
}

print json_encode($result);
?>