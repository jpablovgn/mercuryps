<?php
	/*
	* Este archivo elimina de base de datos
	* la tabla del picking recibido
	*/
	include_once('../../../assets/php/PhpMySQL.php');
	
	$DBdelete = new Database();
	
	$tableName = filter_input(INPUT_POST,'tableName');
	
	$queryDelete = "DROP TABLE IF EXISTS `$tableName`";
	$queryDeleteResult = $DBdelete->query($queryDelete);
	
	print json_encode($queryDeleteResult);
?>