    <?php
include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();
header('Content-Type: text/html; charset=ISO-8859-1');

$tiposalida = $_GET['tiposalida'];
$nombre = $_GET['nombre'];
$transporte = $_GET['transporte'];

// Acentos de base de datos a html.
$accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    $query = "CALL GUARDAR_PICKING('$tiposalida','$nombre','$transporte');";
     $querySavePalletsResult = $connection->query($query);

    if ($querySavePalletsResult) {
        $result = true;
    } else {
        $result['ERROR'][0] = "Error de consulta";
        $result['ERROR'][1] = "No se pudo realizar la consulta." . $query;
    }
    $connection->close();
}
print json_encode($result);
?>

