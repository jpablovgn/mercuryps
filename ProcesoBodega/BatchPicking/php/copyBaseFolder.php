<?php

/* <!--
 * This file copy the base folder
 * and paste a new folder for the importation
  --> */

$path = $_GET['basePath'];
$NIT = $_GET['NIT'];
$import = $_GET['importation'];

$result[] = $path;
$result[] = $NIT;
$result[] = $import;

$src = $path . '/baseFolder';
// $getError = true;
if (!file_exists($path . $NIT . '/' . $import)) {
    // Modifying Umask
    $oldmask = umask(0);
    // User's folder
    $getError = mkdir($path . $NIT . '/' . $import, 0777, true);
    // Restoring Umask
    umask($oldmask);
    if (!$getError) {
        $result['ERROR'][0] = "Error creando carpeta de importación";
        $result['ERROR'][1] = "No se pudo crear la carpeta de la importación "
                . "por favor consulte al administrador del servidor.";
    } else {
        copyFolder($src, $path . $NIT . '/' . $import);
    }
} else {
    // copyFolder($src,$path.$NIT.'/'.$import);
}

function copyFolder($src, $dst) {
    $dir = opendir($src);
    // @mkdir($dst);
    // Modifying Umask
    $oldmask = umask(0);
    while (false !== ( $file = readdir($dir))) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if (is_dir($src . '/' . $file)) {
                // Modifying Umask
                $oldmask = umask(0);
                // User's folder
                $getError = mkdir($dst . '/' . $file, 0777, true);
                if (!$getError) {
                    $result['ERROR'][0] = "Error copiando carpeta base";
                    $result['ERROR'][1] = "No se pudo copiar la carpeta base "
                            . "por favor consulte al administrador del servidor.";
                }
                // Restoring Umask
                umask($oldmask);
                copyFolder($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    // Restoring Umask
    umask($oldmask);
    closedir($dir);
}

print json_encode($result);
?>