<?php
    /*<!--
    * This file loads a master file
    * and pass it in a JSON
    -->*/
    $filePath = $_GET['filePath'];
    $fileName = $_GET['fileName'];
    $NIT = $_GET['NIT'];

	$newFilePath = str_replace('../../','/var/www/Mercury_wms/',$filePath);

    include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();

	if(!$connection->link)
    {
        $result['ERROR'][0] = "Error de conexión";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
		$fieldseparator = ";";
		$lineseparator = "\n";
		$csvfile = $newFilePath;
		
		$ERR_ARRAY=array(
			"Archivo no encontrado: ".$fileName,
			"Error abriendo el archivo: ".$fileName,
			"El archivo se encuentra vacío: ".$fileName
		);
		if(!file_exists($csvfile))
		{
			$result['ERROR'][0] = "No se pudo completar la acción";
			$result['ERROR'][1] = $ERR_ARRAY[0];
		}
		else
		{
			$file = fopen($csvfile,"r");
			if(!$file) 
			{
				$result['ERROR'][0]="No se pudo completar la acción";
				$result['ERROR'][1]=$ERR_ARRAY[1];
			}
			else
			{
				$size = filesize($csvfile);
				if(!$size) 
				{
					$result['ERROR'][0]="No se pudo completar la acción";
					$result['ERROR'][1]=$ERR_ARRAY[2];
				}
				else
				{
					$csvcontent = fread($file,$size);
					fclose($file);

					foreach(split($lineseparator,$csvcontent) as $line)
					{
						if($lines == 0)
						{
							$line = trim($line,"\t");
							$line = str_replace("\r","",$line);
							$line = str_replace(",",";",$line);
							// ************************************ //
							// This line escapes the special character. remove it if entries are already escaped in the csv file
							// ************************************ //
							$line = str_replace("'","\'",$line);
							// ************************************ //
							
							$linearray = explode($fieldseparator,$line);
							//$linearray = explode(',', $linearray);
							$lineSize = sizeof($linearray);
							$result['HEADERS'] = $linearray;
						}
						if($lines >0)
						{
							$line = trim($line,"\t");
							$line = str_replace("\r","",$line);
							$line = str_replace(",",";",$line);
							// ************************************ //
							// This line escapes the special character. remove it if entries are already escaped in the csv file
							// ************************************ //
							$line = str_replace("'","\'",$line);
							// ************************************ //
							
							$linearray = explode($fieldseparator,$line);
							$lengthValidation = sizeof($linearray);
							if($lengthValidation > $lineSize)
							{
								$errorLinesArray[] = $lines++;
							}
							else
							{
								$result['DATA'][] = $linearray;
							}
						}
						$lines++;
					}
					if($errorLinesArray)
					{
						$result['ERROR'][0] = "Error de archivo";
						$messageBase = "El archivo tiene más columnas "
							."de las permitidas, en las filas: ";
						$errorLines = implode(",",$errorLinesArray);
						$result['ERROR'][1] = $messageBase.$errorLines.".";
					}
				}
			}
			if(is_file($csvfile)) unlink($csvfile); // delete file
		}
    	$connection->close();
	}
    print json_encode($result);
?>