<?php
	// The lvlroot variable indicates the levels of direcctories
	// the file loaded has to up, to be on the root directory
	$lvlroot ="../../";
	// Including Head.
	include_once($lvlroot."Body/Head.php");
	// Including Begin Header.
	include_once($lvlroot."Body/BeginPage.php");
		if($_SESSION['NOMBREUSUARIO'] == NULL)
	{
	?>	<script>
	
		window.location = "../../Home/exit.php"
		</script><?php
	}
	// Including Side bar.
	include_once($lvlroot."Body/SideBar.php");
    // Including Php database.
	include_once($lvlroot."assets/php/PhpMySQL.php");

?>
<script>
	var lvlrootjs = "../../";
</script>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
	<li><a href="javascript:;">Inicio</a></li>
	<li><a href="javascript:;">Administracion</a></li>
	<li class="active">Crear Destinos</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
	<h1 class="page-header">Crear Destinos <small>Creación Destinos.</small></h1>
<!-- end page-header -->

<div class="row">
	<!-- begin main column -->
	<div class="col-md-12">
            
            <div class="panel panel-inverse ">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h1  class="panel-title">
                    <i class="fa fa-globe fa-lg"></i> Destinos</h1>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <!-- Start campo Nombre input desplegable  -->
                <div class="form-group">
                    <table class="table table-bordered table-condensed" id="displayresult" style="width: 100%;">
                        <thead>
                            <tr style="border:1px;" style="align-content: center;">

                                <th>Codigo</th>
                                <th>Destinos</th>
         
                            </tr><br>
                        </thead>

                    </table>
                </div>
            </div>
            <!-- end body panel -->
        </div>
        <!-- begin Create/Edit Client panel -->
        <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-follow fa-2x"></i> 
                    Crear/Nuevos Destinos
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <!-- begin form -->
				<form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
					
					<div class="form-group">    
						<div class="control-label col-md-4 col-sm-4">
							<label>Nombre Destino (*)</label>
						</div>
						<div class="col-md-6 col-sm-6">
							<input type="text" class="form-control" id="name" name="name" placeholder="Destino" required />
                                                        <br>
						
                                                        <button id="save" type="button" class="btn btn-primary" onclick="Save();">
							<i class="fa fa-sign-in"></i>
							Guardar
						</button>
					
                                                </div>
				
					</div>
                    
					
					
					</div>
					
				</form>
				<!-- end form -->
            </div>
        
        </div>
        <!-- end Create/Edit Client panel-->
    </div>
	<!-- end main column -->

    <!-- Including alerts windows -->
	<?php
		include_once($lvlroot."Body/AlertsWindows.php");
	?>
</div>
<!-- end row -->

<?php	
	// Including Js actions, put in the end.
	include_once($lvlroot."Body/JsFoot.php");
	// Including End Header.
	include_once($lvlroot."Body/EndPage.php");
?>

<!-- // Loading autocomplete handler. -->
<script type="text/javascript" src="js/list.js"></script>
<script type="text/javascript" src="js/save.js"></script>

<!-- // Loading validations. -->

<script type="text/javascript">
	// Activating the side bar.
	var Change2Activejs = document.getElementById("sidebarAdministracion");
	Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarAdministracion-Destinos");
	Changesub2Activejs.className = "active";
</script>