function Save() {
    var name = $('#name').val();
    if (name == "") {
        var headerMsg = "Campo Vacío";
        var comentMsg = "Indique un nombre para continuar";
        alertWindow(headerMsg, comentMsg);
    } else {
        var url = 'php/save.php?name=' + name;
        $.getJSON(url, function(result)
        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Tipo De Ingreso Guardado Correctamente";
            successWindow(headerMsg, comentMsg);
            $('#name').val("");
            $('#name').focus();
        });


    }
}