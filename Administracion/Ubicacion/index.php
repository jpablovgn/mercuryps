<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?>	<script>
            window.location = "../../Home/exit.php"
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
?>
<script>
    var lvlrootjs = "../../";
</script>
<script type="text/javascript" src="../../assets/plugins/parsley/dist/parsley.js"></script>

<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Administracion</a></li>
    <li class="active">Crear Ubicaciones</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Crear Nuevas Ubicaciones <small>Creación de Nuevas Ubicaciones.</small></h1>
<!-- end page-header -->

<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">

        <div class="panel panel-inverse ">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h1  class="panel-title">
                    <i class="fa fa-map-marker fa-2x"></i> Ubicaciones</h1>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <!-- Start campo Nombre input desplegable  -->
                <div class="form-group">
                    <table class="table table-bordered table-condensed" id="displayresult" style="width: 100%;">
                        <thead>
                            <tr style="border:1px;" style="align-content: center;">
                                <th>Codigo</th>
                                <th>Bodega</th>
                                <th>Cara Estantería</th>
                                <th>Niveles</th>
                                <th>Columna</th>
                                <th>Profundiad Espacios</th>
                            </tr><br>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- end body panel -->
        </div>
        <div class="panel panel-inverse ">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h1  class="panel-title">
                    <i class="fa fa-map-marker fa-2x"></i> Ingresar Nuevas Ubicaciones</h1>
            </div>
            <!-- begin body panel -->

            <!-- Start campo Nombre input desplegable  -->
            <div class="panel-body panel-form"  id="newlocation"  >
                <!-- begin form -->

                <div class="panel-body panel-form" >
                    <!-- begin form -->
                    <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                        <br>
                        <div class="form-group">    
                            <div class="control-label col-md-2 col-sm-2">
                                <label> Bodega (*)</label>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <select class="combobox" name="panel" id="selectBodega" value="">
                                    <option value=""> Seleccione La Bodega  </option>
                                </select>
                                <div>
                                </div>
                            </div>
                            <div class="control-label col-md-2 col-sm-2">
                                <label> Cara Estantería (*)</label>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <select class="combobox" name="panel" id="selectCara" value="">
                                    <option value=""> Seleccione La Estantería  </option>
                                </select>
                                <div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">    
                            <div class="control-label col-md-2 col-sm-2">
                                <label> Niveles (*)</label>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <select class="combobox" name="panel" id="selectNivel" value="">
                                    <option value=""> Seleccione El Nivel  </option>
                                </select>
                                <div>
                                </div>
                            </div>
                            <div class="control-label col-md-2 col-sm-2">
                                <label> Columna (*)</label>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <select class="combobox" name="panel" id="selectColumn" value="">
                                    <option value=""> Seleccione La Columna  </option>
                                </select>
                                <div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">    
                            <div class="control-label col-md-2 col-sm-2">
                                <label> Profundidad Espacios (*)</label>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <select class="combobox" name="panel" id="selectProf" value="">
                                    <option value=""> Seleccione La Profundidad  </option>
                                </select>
                                <div>
                                </div>
                            </div>
                            <div class="control-label col-md-2 col-sm-2">
                                <label> Codigo Unico (*)</label>
                            </div>

                            <div class="col-md-4">
                                <input class="form-control" type="text" id="ean" name="ean" placeholder="Ingrese el EAN" />

                            </div>

                        </div>
                        <div class="control-label col-md-2 col-sm-2">
                            <button id="btnsave" type="button" class="btn btn-primary">
                                <i class="fa fa-save fa-2x"></i>
                                Guardar
                            </button> 
                        </div>
                        <br>

                    </form><!-- end body panel -->
                </div>

            </div>
        </div>
        <!-- begin Create/Edit Client panel -->
        <div class="panel panel-inverse" data-sortable-id="form-plugins-1" >
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-plus-circle fa-2x"></i> 
                    Nuevas Ubicaciones
                </h4>
            </div>
            <br>
            <!-- begin body panel -->
            <div class="panel-body panel-form" >
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclients" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group">

                        <table class="table table-bordered table-condensed" id="createTable"  >
                            <tr>
                                <td> Crear Nuevas Bodegas</td>
                                <td> Crear Nuevas Caras de Estanterias</td>
                                <td> Crear Nuevos Niveles</td>
                                <td> Crear Nuevas Columnas</td>
                                <td> Crear Nuevas Profundidad de Esspacios</td>

                            </tr>
                            <tr>
                                <td>      
                                    <button id="save" type="button" class="btn btn-primary" onclick="Bodega();">
                                        <i class="fa fa-map-marker fa-2x"></i>
                                        Bodega
                                    </button>
                                </td>
                                <td>  

                                    <button id="save" type="button" class="btn btn-primary" onclick="CaraE();">
                                        <i class="fa fa-map-marker fa-2x"></i>
                                        Cara Estanteria
                                    </button>
                                </td>
                                <td> 

                                    <button id="save" type="button" class="btn btn-primary" onclick="Nivel();">
                                        <i class="fa fa-map-marker fa-2x"></i>
                                        Niveles
                                    </button>
                                </td> 

                                <td>  


                                    <button id="save" type="button" class="btn btn-primary" onclick="Columna();">
                                        <i class="fa fa-map-marker fa-2x"></i>
                                        Columna
                                    </button>
                                </td>
                                <td>

                                    <button id="save" type="button" class="btn btn-primary" onclick="Profundidad();">
                                        <i class="fa fa-map-marker fa-2x"></i>
                                        Profundidad Espacios
                                    </button>
                                </td>
                            </tr>
                        </table>


                    </div>
            </div>
            <br>
        </div>

        <div class="panel panel-inverse" data-sortable-id="form-plugins-1"  id="panel" style="display: none">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-plus-circle fa-2x"></i> 
                    Ingresar Nuevas Ubicaciones
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form"  id="bod"  style="display: none">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">

                    <div class="panel-body panel-form" >
                        <!-- begin form -->
                        <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                            <br>
                            <div class="form-group">    
                                <div class="control-label col-md-4 col-sm-4">
                                    <label>Nombre Bodega (*)</label>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" id="namebodega" name="namebodega" placeholder="Bodega" required />
                                    <br>

                                    <button id="save" type="button" class="btn btn-primary" onclick="SaveBodega();">
                                        <i class="fa fa-sign-in"></i>
                                        Guardar
                                    </button> 
                                    <button id="save" type="button" class="btn btn-warning cancel" onclick="Cancelar();">
                                        <i class="fa fa-ban"></i>
                                        Cancelar
                                    </button> <br><br>
                                </div>
                            </div>
                    </div>
            </div>
            <div class="panel-body panel-form"  id="estanteria"  style="display: none">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">

                    <div class="panel-body panel-form" >
                        <!-- begin form -->
                        <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                            <br>
                            <div class="form-group">    
                                <div class="control-label col-md-4 col-sm-4">
                                    <label>Nombre Estantería (*)</label>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" id="nameEstan" name="name" placeholder="Cara Estantería" required />
                                    <br>

                                    <button id="save" type="button" class="btn btn-primary" onclick="SaveCaraE();">
                                        <i class="fa fa-sign-in"></i>
                                        Guardar
                                    </button> 
                                    <button id="save" type="button" class="btn btn-warning cancel" onclick="Cancelar();">
                                        <i class="fa fa-ban"></i>
                                        Cancelar
                                    </button> 
                                    <br>
                                    <br>
                                </div>
                            </div>
                    </div>
            </div>
            <div class="panel-body panel-form"  id="Nivel"  style="display: none">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">

                    <div class="panel-body panel-form" >
                        <!-- begin form -->
                        <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                            <br>
                            <div class="form-group">    
                                <div class="control-label col-md-4 col-sm-4">
                                    <label>Nombre Nivel (*)</label>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" id="namenivel" name="name" placeholder="Nivel" required />
                                    <br>

                                    <button id="save" type="button" class="btn btn-primary" onclick="SaveNiveles();">
                                        <i class="fa fa-sign-in"></i>
                                        Guardar
                                    </button> 
                                    <button id="save" type="button" class="btn btn-warning cancel" onclick="Cancelar();">
                                        <i class="fa fa-ban"></i>
                                        Cancelar
                                    </button> 
                                    <br>
                                    <br>
                                </div>
                            </div>
                    </div>
            </div>
            <div class="panel-body panel-form"  id="Column"  style="display: none">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">

                    <div class="panel-body panel-form" >
                        <!-- begin form -->
                        <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                            <br>
                            <div class="form-group">    
                                <div class="control-label col-md-4 col-sm-4">
                                    <label>Nombre Columna (*)</label>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" id="nameColumn" name="name" placeholder="Columna" required />
                                    <br>

                                    <button id="save" type="button" class="btn btn-primary" onclick="SaveColumna();">
                                        <i class="fa fa-sign-in"></i>
                                        Guardar
                                    </button> 
                                    <button id="save" type="button" class="btn btn-warning cancel" onclick="Cancelar();">
                                        <i class="fa fa-ban"></i>
                                        Cancelar
                                    </button> 
                                    <br>
                                    <br>
                                </div>
                            </div>
                    </div>
            </div>
            <div class="panel-body panel-form"  id="Prof"  style="display: none">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">

                    <div class="panel-body panel-form" >
                        <!-- begin form -->
                        <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                            <br>
                            <div class="form-group">    
                                <div class="control-label col-md-4 col-sm-4">
                                    <label>Profundiad Espacio (*)</label>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" id="nameProf" name="name" placeholder="Profundiad Espacios" required />
                                    <br>

                                    <button id="save" type="button" class="btn btn-primary" onclick="SaveProfundidad();">
                                        <i class="fa fa-sign-in"></i>
                                        Guardar
                                    </button> 
                                    <button id="save" type="button" class="btn btn-warning cancel" onclick="Cancelar();">
                                        <i class="fa fa-ban"></i>
                                        Cancelar
                                    </button> 
                                    <br>
                                    <br>
                                </div>
                            </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<!-- Including alerts windows -->
<?php
include_once($lvlroot . "Body/AlertsWindows.php");
?>
</div>
<!-- end row -->

<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>

<!-- // Loading autocomplete handler. -->
<script type="text/javascript" src="js/list.js"></script>
<script type="text/javascript" src="js/save.js"></script>
<script type="text/javascript" src="js/setValidations.js"></script>

<!-- // Loading validations. -->
<script type="text/javascript" src="js/fillSelects.js"></script>
<script type="text/javascript">
                                        // Activating the side bar.
                                        var Change2Activejs = document.getElementById("sidebarAdministracion");
                                        Change2Activejs.className = "has-sub active";
                                        var Changesub2Activejs = document.getElementById("sidebarAdministracion-Ubicacion");
                                        Changesub2Activejs.className = "active";
</script>
<style> 
    table {
        text-align: center;
    }
    table th{
        text-align: center;

    }

</style>
