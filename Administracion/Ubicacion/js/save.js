
$("#btnsave").click(function() {
    if ($("#formclient").parsley().validate())
    {
        // Confirmation message


        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar la Ubicación?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'ValidateEan()');
        document.getElementById('warning-trigger').click();
    }
});


function SaveBodega() {
    var name = $('#namebodega').val();
    var url = 'php/saveBodega.php?name=' + name;
    $.getJSON(url, function(result)
    {

    });
    var headerMsg = "Operación Exitosa";
    var comentMsg = "Bodega Guardada Correctamente";
    successWindow(headerMsg, comentMsg);
    $('#namebodega').val("");
    $('#namebodega').focus();
}
function SaveCaraE() {
    var name = $('#nameEstan').val();

    var url = 'php/saveCaraEstan.php?name=' + name;
    $.getJSON(url, function(result)
    {
        var headerMsg = "Operación Exitosa";
        var comentMsg = "Cara Estantería Guardada Correctamente";
        successWindow(headerMsg, comentMsg);
        $('#nameEstan').val("");
        $('#nameEstan').focus();
    });
}
function SaveNiveles() {
    var name = $('#namenivel').val();

    var url = 'php/saveNivel.php?name=' + name;
    $.getJSON(url, function(result)
    {

    });
    var headerMsg = "Operación Exitosa";
    var comentMsg = "Bodega Guardada Correctamente";
    successWindow(headerMsg, comentMsg);
    $('#namenivel').val("");
    $('#namenivel').focus();
}
function SaveColumna() {
    var name = $('#nameColumn').val();

    var url = 'php/saveColumna.php?name=' + name;
    $.getJSON(url, function(result)
    {

    });
    var headerMsg = "Operación Exitosa";
    var comentMsg = "Bodega Guardada Correctamente";
    successWindow(headerMsg, comentMsg);
    $('#nameColumn').val("");
    $('#nameColumn').focus();
}
function SaveProfundidad() {
    var name = $('#nameProf').val();

    var url = 'php/saveColumna.php?name=' + name;
    $.getJSON(url, function(result)
    {

    });
    var headerMsg = "Operación Exitosa";
    var comentMsg = "Bodega Guardada Correctamente";
    successWindow(headerMsg, comentMsg);
    $('#nameProf').val("");
    $('#nameProf').focus();
}
function SaveLocations() {
    var Bodega = $('#selectBodega').val();
    var Nivel = $('#selectNivel').val();
    var Cara = $('#selectCara').val();
    var Column = $('#selectColumn').val();
    var Profun = $('#selectProf').val();
    var ean = $('#ean').val();
    var url = 'php/saveLocations.php?bodega=' + Bodega + '&cares=' + Cara + '&nivel=' + Nivel + '&column=' + Column + '&prof=' + Profun + '&ean=' + ean;
    console.log(url);   
    $.getJSON(url, function(result)
    {

        if (result['ERROR']) {
            var headerMsg = "Operación No Realizada";
            var comentMsg = "Verificar Datos e Intente Nuevamente";
            alertWindow(headerMsg, comentMsg);
        }
        else
        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Ubicacion Guardada Correctamente";
            successWindow(headerMsg, comentMsg);
        }
    });
}

function Bodega() {
    display();
    $("#panel").attr("style", "display:true;");
    $("#bod").attr("style", "display:true;");
}
function CaraE() {
    display();
    $("#panel").attr("style", "display:true;");
    $("#estanteria").attr("style", "display:true;");
}
function Nivel() {
    display();
    $("#panel").attr("style", "display:true;");
    $("#Nivel").attr("style", "display:true;");
}
function Columna() {
    display();
    $("#panel").attr("style", "display:true;");
    $("#Column").attr("style", "display:true;");
}
function Profundidad() {
    display();
    $("#panel").attr("style", "display:true;");
    $("#Prof").attr("style", "display:true;");
}

function display() {
    $("#bod").attr("style", "display:none;");
    $("#estanteria").attr("style", "display:none;");
    $("#Nivel").attr("style", "display:none;");
    $("#Column").attr("style", "display:none;");
    $("#Prof").attr("style", "display:none;");
}

function Cancelar() {
    $("#panel").attr("style", "display:none;");
}
function ValidateEan() {
    var ean = $("#ean").val();
    $.ajax({
        type: "GET",
        url: "php/validateEan.php",
        data: "ean=" + ean,
        dataType: "json",
        error: function() {
            var headerMsg = "Error";
            var comentMsg = "Operación no realizada, fallo de conexión";
            alertWindow(headerMsg, comentMsg);
        },
        success: function(result) {
            if (result['DATA'] == "true") {
                var headerMsg = "Ean Repetido";
                var comentMsg = "El Ean ingresado ya existe en la base de datos";
                alertWindow(headerMsg, comentMsg);
                $("#ean").val('');
            } else {
                SaveLocations();
            }

        }
    });
}