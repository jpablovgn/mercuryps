function Save() {
    var name = $('#name').val();
    var url = 'php/save.php?name=' + name;
    if (name == "") {
        var headerMsg = "Campo Vacío";
        var comentMsg = "Indique un nombre para continuar";
        alertWindow(headerMsg, comentMsg);
    } else {
        $.getJSON(url, function(result)
        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Tipo De Contenedor Guardado Correctamente";
            successWindow(headerMsg, comentMsg);
            $('#name').val("");
            $('#name').focus();
        });
    }

}