var url = 'php/tiposalida.php';
$.getJSON(url, function(result) {
    if (result['DATA']) {

        //$("#avalaibleunits").attr("style", 'display:true;');
        tableUnits(result);
    }
});


function tableUnits(result)
{
    $('#displayresult').DataTable({
        data: result['DATA'],
        responsive: true,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }

        }


    });


}
function SaveSalida() {
    var name = $('#name').val();
    if (name == "") {
        var headerMsg = "Campo Vacío";
        var comentMsg = "Indique un nombre para continuar";
        alertWindow(headerMsg, comentMsg);
    } else {
        var url = 'php/savesalida.php?name=' + name;
        $.getJSON(url, function(result)
        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Tipo Salida Guardada Correctamente";
            successWindow(headerMsg, comentMsg);

        });
}
}
