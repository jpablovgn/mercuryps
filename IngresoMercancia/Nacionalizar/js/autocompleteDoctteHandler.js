// This file was made to handle the autocomplete input
//// and the related actions
//var globalData;
//$.getJSON('php/docstransporte.php', function(data) {
// 			/* called when request to archivo.php completes */
//			globalData = data;
//			passDoctteData(data);
//});

function passDoctteData(data)
{
	var handleJquery = function()
	{
		var DoctteData=[''];
		var dataSize = ObjectSizeAutocomplete(data);
		
		// filling the vectors with the preloaded data.
		for(var l=0;l<dataSize;l++)
		{
			DoctteData.push(String(data[l]['ID']));
		}
		// linking the preloaded data to the <input> field.
		$('#numdoctte').autocomplete({source:DoctteData});
	};
	var FormPluginsAutoComplete = function () {
		"use strict";
		return {
			//main function
			init: function () {
				handleJquery();
			}
		};
	}();
	
	$(document).ready(function() {
		FormPluginsAutoComplete.init();
	});
}

// this function calculates the size of an object.
function ObjectSizeAutocomplete(obj) 
{
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

function autoCompleteDoctte(doctte)
{
	$.getJSON('php/clientInfo.php?docTransporte='+doctte.value, function(clientdata) {
			/* called when request to archivo.php completes */
			if(clientdata)
			{	fillClientData(clientdata);
				doctteNationalized(doctte);
			}
			else
			{	document.getElementById('numdoctte').value = '';
				for(var k=0;k<ObjectSizeAutocomplete(ids);k++)
				document.getElementById(ids[k]).value = '';
				document.getElementById('alert-Header').innerHTML = "Error de referencia.";
				document.getElementById('alert-comentary').innerHTML = "El documento de trasporte ingresado no se encuentra en base de datos.";
				document.getElementById('alert-trigger').click();
			}
	});
}
// ids and compare have to have the values at same level.
// ids[0] corresponds to compare[0]
var ids=[
	'NITcliente',
	'ContactoCliente',
	'NombreCliente',
	'MarcaComercial'
	  ];
var compare =[
	'NIT',
	'CONTACTO',
	'NOMBRE',
	'MARCA'
		 ];

function fillClientData(passClient)
{
	//var sizeData = ObjectSizeAutocomplete(passClient);
	//var stop=1, pos=0;
	
	for(var j=0;j<ObjectSizeAutocomplete(compare);j++)
				document.getElementById(ids[j]).value = passClient[0][compare[j]];
}

// function doctteNationalized(doctte)
// {
// 	$.getJSON('php/references.php?docTransporte='+doctte.value, function(nationalized){

// 		if(nationalized[0])
// 		{
// 			// if formgroupjs exist delete it.
// 			if(typeof formgroupjs !== 'undefined')
// 			{
// 				formgroupjs.removeChild(divlabelrefjs);
// 				formgroupjs.removeChild(divinputrefjs);
// 				$(formgroupjs).remove();
// 			}
// 			// creating HTML elements
// 			setselected = document.getElementsByName('tiporeferencia')[0];
// 			paneljs = document.getElementById('references-form');
// 			box = document.getElementById('combobox-form-group');
// 			formgroupjs = document.createElement('div');
// 			divlabelrefjs = document.createElement('div');
// 			labelrefjs	= document.createElement('label');
// 			divinputrefjs = document.createElement('div');
// 			inputrefjs	= document.createElement('input');
			
// 			// setting HTML attributes
// 			box.setAttribute('style', 'display:none');
// 			formgroupjs.setAttribute('id','form-group-id');
// 			formgroupjs.setAttribute('class','form-group');
// 			divlabelrefjs.setAttribute('class','col-md-7');
// 			labelrefjs.innerHTML = 'Seleccionar tipo de referencia (*)';
// 			divinputrefjs.setAttribute('class','col-md-5');
// 			inputrefjs.setAttribute('id','inputreferences');
// 			inputrefjs.setAttribute('type','text');
// 			inputrefjs.setAttribute('class','form-control')
// 			inputrefjs.setAttribute('readonly','');
// 			inputrefjs.setAttribute('value',nationalized[0]['TIPO']);
// 			setselected.value = nationalized[0]['TIPO'];
			
// 			// appending childs
// 			divlabelrefjs.appendChild(labelrefjs);
// 			divinputrefjs.appendChild(inputrefjs);
// 			formgroupjs.appendChild(divlabelrefjs);
// 			formgroupjs.appendChild(divinputrefjs);
// 			paneljs.appendChild(formgroupjs);
			
// 			loadreferences(doctte, nationalized);
// 		}
// 		else
// 		{
// 			box = document.getElementById('combobox-form-group');
// 			box.setAttribute('style', 'display:true');
// 			hideformjs = document.getElementById('form-group-id');
// 			// if form-group-id exist then hidde.
// 			if(hideformjs)
// 				hideformjs.setAttribute('style','display:none');
// 			$('#tiporeferencia').val('unidades');
// 			$('#tiporeferencia').data('combobox').refresh();
// 			loadreferences(doctte, 'none', 'unidades');
// 		}
// 	});
// }

// function loadreferences(doctte, nationalized, table)
// {
// 	// If the document was not nationalized.
// 	if(nationalized !== 'none')
// 	{
// 		$.getJSON('php/loadreferences.php?docTransporte='+doctte.value+'&tabla='+nationalized[0]['TIPO']+'&noload=true', function(references)
// 		{
// 			var typeSize = ObjectSizeAutocomplete(references[0]['TIPO']);
// 			createReferences(references);
// 		});
// 	}
// 	else
// 	{
// 		document.getElementsByName('tiporeferencia')[0].value = table;
// 		$.getJSON('php/loadreferences.php?docTransporte='+doctte.value+'&tabla='+table+'&noload=false', function(references)
// 		{
// 			if(references[1]["REFERENCIAS"][0])
// 			{
// 				createReferences(references);
// 			}
// 			else
// 			{
// 				// if formgroupjs exist delete it.
// 				if(typeof groupjs !== 'undefined')
// 				{
// 					// deletting all childs
// 					while (groupjs.firstChild) {
// 						groupjs.removeChild(groupjs.firstChild);
// 					}
// 					//formgroupjs.removeChild(divinputrefjs);
// 					$(groupjs).remove();
// 				}
// 				if(table === 'bultosingreso') table = 'bultos';
// 				// if table no exists do not show message.
// 				if(table)
// 				{
// 					document.getElementById('alert-Header').innerHTML = "Solicitud no realizada.";
// 					document.getElementById('alert-comentary').innerHTML = "No se encontraron referencias disponibles por "+table;
// 					document.getElementById('alert-trigger').click();
// 				}
// 			}
// 		});
// 	}
// }

function createReferences(ref)
{
	// if formgroupjs exist delete it.
	if(typeof groupjs !== 'undefined')
	{
		// deletting all childs
		while (groupjs.firstChild) {
			groupjs.removeChild(groupjs.firstChild);
		}
		//formgroupjs.removeChild(divinputrefjs);
		$(groupjs).remove();
	}
	// Creating div container
	groupjs 	= document.createElement('div');
	var allrefs = document.createElement('input');;
	var refsize = ObjectSizeAutocomplete(ref[1]['REFERENCIAS']);
	if(ref[1]['REFERENCIAS'][0])
	{
		// Static Header table.
		formjs	= document.getElementById('references-form');
		tableComplete	= document.createElement('table');
		var threferences	= document.createElement('thead');
		var trhead			= document.createElement('tr');
		tbody			= document.createElement('tbody');
		var headers	= [
			'Nacionalizar todas las disponibles',
			'Cantidades a Nacionalizar',
			'Referencia',
			'Cantidades disponibles',
			'Cantidades nacionalizadas',
			'Cantidades según factura'
		];
		// setting HTML attributes to Header table.
		tableComplete.setAttribute('class','table');
		for(var thjs=0; thjs<headers.length; thjs++)
		{
			var thhead = document.createElement('th');
			thhead.innerText = headers[thjs];
			// appending trhead childs
			trhead.appendChild(thhead);
		}
		tbody.setAttribute('id','reftbody');
		
		// appending childs
		threferences.appendChild(trhead);
		tableComplete.appendChild(threferences);
		tableComplete.appendChild(tbody);
	
		// Creating references rows.
		for(var n=0;n<refsize;n++)
		{
			// creating HTML elements
			groupjs.setAttribute('class','form-group');
			var tr 				= document.createElement('tr');
			var tdCheckref		= document.createElement('td');
			var checkrefjs		= document.createElement('input');
			var tdtoNationalize	= document.createElement('td');
			var toNationalize	= document.createElement('input');
			var referenceLoaded	= document.createElement('td');
			var available		= document.createElement('td');
			var nationalized	= document.createElement('td');
			var total			= document.createElement('td');
			var allAvailable	= document.createElement('input');
			
			var reference		= ref[1]['REFERENCIAS'][n];
			var totalref		= ref[1]['TOTALES'][n];
			var nationalizedref	= ref[1]['NACIONALIZADAS'][n];
			var availableref	= ref[1]['DISPONIBLES'][n];
			
			allrefs.setAttribute('type','hidden');
			allrefs.setAttribute('id','allrefs');
			allrefs.setAttribute('name','allrefs');
			checkrefjs.setAttribute('name','nationalize[]');
			
			checkrefjs.setAttribute('value',reference);
			checkrefjs.setAttribute('id',reference+'-check');
			checkrefjs.setAttribute('type','checkbox');
			checkrefjs.setAttribute('onChange','checkit(this);');
			toNationalize.setAttribute('id',reference);
			toNationalize.setAttribute('name','toNationalize[]');
			toNationalize.setAttribute('onChange','validatetoNationalize(this)');
			toNationalize.setAttribute('type','text');
			toNationalize.setAttribute('style','width:50px;');
			toNationalize.value = '0';
			referenceLoaded.innerText = reference;
			allrefs.value = allrefs.value?allrefs.value+","+reference:reference;
			available.setAttribute('id',reference+'available');
			allAvailable.setAttribute('name','availables[]');
			allAvailable.setAttribute('type','hidden');
			allAvailable.value = availableref;
			nationalized.setAttribute('id',reference+'nationalized');
			total.setAttribute('id',reference+'total');
			available.innerText = availableref;
			nationalized.innerText = nationalizedref;
			total.innerText	= ''+totalref;
			
			// appending childs
			tdCheckref.appendChild(checkrefjs);
			tdtoNationalize.appendChild(toNationalize);
			tr.appendChild(tdCheckref);
			tr.appendChild(tdtoNationalize);
			tr.appendChild(referenceLoaded);
			tr.appendChild(available);
			tr.appendChild(allAvailable);
			tr.appendChild(nationalized);
			tr.appendChild(total);
			tbody.appendChild(tr);
			groupjs.appendChild(tableComplete);
			groupjs.appendChild(allrefs);
			formjs.appendChild(groupjs);
		}
	}
	else
	{
		document.getElementById('alert-Header').innerHTML = "Solicitud no realizada.";
		document.getElementById('alert-comentary').innerHTML = "Todas las referencias están nacionalizadas.";
		document.getElementById('alert-trigger').click();
	}
}

// Type of refence selected
// $('#tiporeferencia').change(function()
// {
// 	var referenceSelected = this.value;
// 	if(referenceSelected)
// 	{
// 		var doctte = document.getElementById('numdoctte');
// 		if(referenceSelected === 'bultos') referenceSelected = 'bultosingreso';
// 		loadreferences(doctte,'none',referenceSelected);
// 	}
// });

// check uncheck all checkbox
$('#checkAll').change(function()
{
	$("input:checkbox[name=\'nationalize[]\']").prop('checked', $(this).prop("checked"));
	checkitAll();
});

// This function loads all the checkbutton
// and call checkit function one by one.
function checkitAll()
{
	// Loading all the checkBox
	var totalBox = $("input:checkbox[name=\'nationalize[]\']");
	var BoxSize	= totalBox.length;
	// Loading available references on the input box.
	for(var sjs=0; sjs<BoxSize; sjs++)
	{
		checkit(totalBox[sjs]);
	}
}

// This function pass the available amount to toNationalize box.
// obj checkbox type ref-check
function checkit(obj)
{
	var splitReference;
	splitReference = obj.id.split('-');
	var inputReference = document.getElementById(splitReference[0]);
	var availableReference = document.getElementById(splitReference[0]+'available');
	if(obj.checked)
	{
		inputReference.value = availableReference.innerText;
	}
	else
	{
		inputReference.value = 0;
	}
}

// This function validate the dispatch box and sum the total.
function validatetoNationalize(inputValue)
{
	var Available = parseInt(document.getElementById(inputValue.id+'available').innerText);
	var IntValue = parseInt(inputValue.value);
	// If IntValue is greater than available reference.
	if(IntValue >= Available)
	{
		inputValue.value = Available;
		// Check checkbox
		$('[id="'+inputValue.id+'-check"]').prop('checked',true);
	}
	else
	{
		// Uncheck checkbox
		$('[id="'+inputValue.id+'-check"]').prop('checked',false);
		// If IntValue is character or empty insert '0'.
		if(!IntValue || IntValue < 0)
		{
			inputValue.value = 0;
		}
	}
}

// Prevent default submit.
$('#references-form').submit(function(e)
{
	e.preventDefault();
});