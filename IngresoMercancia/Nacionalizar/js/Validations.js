
function validateCheck() {
    if ($('#docCsv').is(':checked')) {
        $("#addDoc").attr("style", "display:true");
        $("#submitbutton").attr("style", "display:true");
        $("#formdescarga").attr("style","display:true");
                           $("#divReferenceTable").attr("style","display:none");
                                                      $("#panel-nationalize").attr("style","display:true");


        
    } else {
        $("#addDoc").attr("style", "display:none");
        $("#submitbutton").attr("style", "display:none");
        $("#formdescarga").attr("style","display:none");
                                                      $("#panel-nationalize").attr("style","display:none");

    }
    if ($('#selectedTable').is(':checked')) {
        $("#divref").attr("style", "display:true");

    } else {
        $("#divref").attr("style", "display:none");

    }
}




function ValidateCantidadBultos() {
    var i;
    var impor = $("#importation").val();
    var doc = $("#transportDoc").val();
    var cantFilas = $("#tableReferences tr").length;
    for (i = 1; i <= cantFilas; i++)
    {
        $('#tableReferences tr').eq(i).each(function() {
            var refe = $(this).find('td').eq(1).html();
//            var cant = $(this).find('td').eq(2).html();
            $(this).find('td').eq(2).each(function() {
                var cant = ($(this).html());

                var url = 'php/validateCantidadBultos.php?imp=' + impor + '&doc=' + doc + '&refe=' + refe;
                $.getJSON(url, function(result) {
                    if (result['DATA'] >= cant) {
                    } else {
                        var headerMsg = "Cantidades Incorrectas";
                        var comentMsg = "No existen cantidades suficientes para la referencia " + refe;
                        alertWindow(headerMsg, comentMsg);
                        return false;

                    }

                });
            });
        });
    }
}
function ValidateCantidadUnidad() {
    var i;
    var impor = $("#importation").val();
    var doc = $("#transportDoc").val();
    var cantFilas = $("#tableReferences tr").length;
    for (i = 1; i <= cantFilas; i++)
    {
        $('#tableReferences tr').eq(i).each(function() {
            var refe = $(this).find('td').eq(1).html();
//            var cant = $(this).find('td').eq(2).html();
            $(this).find('td').eq(2).each(function() {
                var cant = ($(this).html());
                var url = 'php/validateCantidadUnidad.php?imp=' + impor + '&doc=' + doc + '&refe=' + refe;
                $.getJSON(url, function(result) {
                    if (result['DATA'] >= cant) {
                      
                    } else {
                        var headerMsg = "Cantidades Incorrectas";
                        var comentMsg = "No existen cantidades suficientes para la referencia " + refe;
                        alertWindow(headerMsg, comentMsg);
                        return false;

                    }

                });
            });
        });
    }
}
function ValidateReferenciaBultos() {
    var i;
    var impor = $("#importation").val();
    var doc = $("#transportDoc").val();
    var cantFilas = $("#tableReferences tr").length;
    for (i = 1; i <= cantFilas; i++)
    {
        $('#tableReferences tr').eq(i).each(function() {
//            var cant = $(this).find('td').eq(2).html();
            $(this).find('td').eq(1).each(function() {
                var refe = ($(this).html());

                var url = 'php/validateReferenciaBultos.php?imp=' + impor + '&doc=' + doc + '&refe=' + refe;
                $.getJSON(url, function(result) {
                    if (result['DATA'] =="1") {
                        
                    } else {
                        var headerMsg = "Referencias Incorrectas";
                        var comentMsg = "La Referencia " + refe +" No pertenece al número de Importación";
                        alertWindow(headerMsg, comentMsg);
                        return false;

                    }

                });
            });
        });
    }
}
function ValidateReferenciaUnidad() {
    var i;
    var impor = $("#importation").val();
    var doc = $("#transportDoc").val();
    var cantFilas = $("#tableReferences tr").length;
    for (i = 1; i <= cantFilas; i++)
    {
        $('#tableReferences tr').eq(i).each(function() {
//            var cant = $(this).find('td').eq(2).html();
            $(this).find('td').eq(1).each(function() {
                var refe = ($(this).html());

                var url = 'php/validateReferenciaUnidad.php?imp=' + impor + '&doc=' + doc + '&refe=' + refe;
                $.getJSON(url, function(result) {
                    if (result['DATA'] == "1") {

                    } else {
                        var headerMsg = "Referencias Incorrectas";
                        var comentMsg = "La Referencia " + refe + " No pertenece al número de Importación"
                                alertWindow(headerMsg, comentMsg);
                        return false;

                    }

                });
            });
        });
    }
}
function ValidateImport() {
    var i;
    var impor = $("#importation").val();
    var cantFilas = $("#tableReferences tr").length;
    for (i = 1; i <= cantFilas; i++)
    {
        $('#tableReferences tr').eq(i).each(function() {
            var refe = $(this).find('td').eq(1).html();
//            var cant = $(this).find('td').eq(2).html();
            $(this).find('td').eq(3).each(function() {
                var imp = ($(this).html());
                if (impor == imp) {
                } else {
                    var headerMsg = "Número Importación Incorrecto";
                    var comentMsg = "Corresponde a la referencia " + refe;
                    alertWindow(headerMsg, comentMsg);
                    return false;

                }


            });
        });
    }
}

