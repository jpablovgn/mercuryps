/**
 * Check if the file uploaded exists in database
 */
function checkFile(file)
{
    var NITjs = $('#NITcliente').val();
    if (NITjs)
    {
        //enabling cancel all button
        $('#cancel-all-upload').attr("disabled", false);
        var splitValue = file.name.split('.');
        // splitValue has the file extension in position 1
        if (splitValue[1] != 'pdf')
        {
            setTimeout(function() {
                // Cancelling file upload
                $("[id='cancel-upload-button " + file.name + "']").click();
            }, 3000);
        }
    } else
    {
        // Timeout to cancel the load.
        setTimeout(function() {
            var headerMsg = "Solicitud no realizada";
            var comentMsg = "Por favor seleccione primero la información del cliente.";
            warningWindow(headerMsg, comentMsg);
            $('#cancel-all-upload').click();
        }, 500);
        // disabling cancel all button
        $('#cancel-all-upload').attr("disabled", true);
    }
}

/**
 * Validate data on save click
 */
$('#save').click(function()
{
    var amountValidation = (parseInt($('#nationalizeTotal').text()) > 0) ? true : false;
    // if not amounts selected show warning message
    if (amountValidation)
    {
        var clientDataValidation = $("#nationalization-form").parsley().validate();
        if (clientDataValidation)
        {
            var headerMsg = "¿Está seguro que desea continuar?";
            var comentMsg = "¿Está seguro que desea nacionalizar " +
                    "las cantidades seleccionadas?";
            warningWindow(headerMsg, comentMsg, saveNationalization);
        }
    } else
    {
        var headerMsg = 'No hay datos seleccionados';
        var comentMsg = 'No se han seleccionado cantidades para nacionalizar';
        alertWindow(headerMsg, comentMsg);
    }
});

function confirmSave()
{
    var fileUploadPath = "/var/www/html/Mercury_wms/assets/plugins/jquery-file-upload/server/upload-files/";
    var NITjs = $('#NITcliente').val();
    var importationjs = $('#importation').val();
//    var actionPath = "../../../assets/plugins/jquery-file-upload/server/upload-files/";
    var actionPath = "/home/intertelco-server/";
    $('#fileupload').attr('action', actionPath);
    uploadFiles();
}

function uploadFiles() {
    // copy in index.php to upload the files.
    $('#TransportCopy').val($('#transportDoc').val());
    $('#nationalize-start-upload').click();
    var elements = document.getElementsByName('cancelIndividual');
    if (!ObjectSize(elements))
    {
        // Saving nationalization
        saveNationalization();
    }
}

/**
 * Saving selected amounts to nationalize
 */
function saveNationalization()
{
    var transportDoc = $('#transportDoc').val();
    var importation = $('#importation').val();
    // column 0 is the boxes's type
    var nationalizationType = $($('#referenceTable').DataTable().column(0).header()).html();
    var valuesArray = Array();
    // Loading all the input box to dispatch
    var totalMerchandice = $("input:text[name=\'toNationalize[]\']");
    var boxSize = totalMerchandice.length;
    var TotalAmount = 0;
    for (var qjs = 0; qjs < boxSize; qjs++)
    {
        if (parseInt(totalMerchandice[qjs].value))
        {
            valuesArray.push(
                    {
                        0: totalMerchandice[qjs].id,
                        1: totalMerchandice[qjs].value
                    })
        }
    }
    var data =
            {
                transportDoc: transportDoc,
                nationalizationType: nationalizationType,
                valuesArray: valuesArray
            }
    data = JSON.stringify(data);

    var url = 'php/nationalizeAmounts.php';
    $.ajax({
        type: "POST",
        url: url,
        async: true,
        cache: false,
        dataType: "json",
        data: {data: data},
        timeout: 50000, /* Timeout in ms */
        success: function(responce)
        { /* called when request to archivo.php completes */
            if (responce['ERROR'])
            {
                alertWindow(responce['ERROR'][0], responce['ERROR'][1]);
            } else
            {
                // Setting timeout to avoid messages windows 
                // interposition
                setTimeout(function() {
                    var headerMsg = "Mercancía seleccionada con éxito";
                    var comentMsg = "Se han seleccionado los bultos" +
                            "y las cantidades indicadas con éxito.";
                    successWindow(headerMsg, comentMsg);
                }, 500);
//                alert(JSON.stringify(responce));
            }
        }
    });
}

/**
 *	Combine all forms in arr_forms into a new, hidden form. Submit the new form.
 *	
 *	@var action {string} - the form action
 *	@var arr_forms {array} - array of form objects 
 */
function submit_All(arr_forms)
{

    var valid = true;
    var nationalizationformjs = document.getElementById('nationalization-form');
    var referencesformjs = document.getElementById('references-form');
    var uploadformjs = document.getElementById('fileupload');
    var typereferencejs = document.getElementsByName('tiporeferencia')[0];
    var referenceschecked = document.getElementsByName('nationalize[]');

    // RegExp validations functions
    var decimalEval = new RegExp('^\\d+(\\.\\d+)?$');
    //var decimalEvalnotrequired		= new RegExp('^(\\d+(\\.\\d+)?)?$');
    var digitEval = new RegExp('^\\d+$');
    var digitEvalnotrequired = new RegExp('^(\\d+)?$');
    var alphanumEval = new RegExp('^\\w+$', 'i');
    var nationalizationdocument = new RegExp("^(\\d{15}(-\\d))?$");
    //var nationalizationdocumentrequired = new RegExp("^\\d{15}(-\\d)$");

// 	var docttejs 		= nationalizationformjs.elements['numdoctte'];
    var formularysize = document.getElementsByName('formularioNacionalizacion[]').length;

// 	// validating transport's document
    var nationalizationResult = !alphanumEval.test(docttejs.value);

// 	// validating nationalization's document
// 	/*var firstFormulary = document.getElementById('formularyOne');
    nationalizationResult = nationalizationResult || !nationalizationdocumentrequired.test(firstFormulary.value);
// 	*/
    for (var m = 0; m < formularysize; m++)
    {
        var formulary = document.getElementsByName('formularioNacionalizacion[]')[m];
        nationalizationResult = nationalizationResult || !nationalizationdocument.test(formulary.value);
    }

    var referenceResult = !typereferencejs.value;

// 	// if validation fails call dashboard validation's warning
    if (nationalizationResult)
    {
        nationalizationformjs.elements["nationalization-submit"].click();
        valid = false;
    }

// 	// if validation fails call dashboard validation's warning
    if (referenceResult)
    {
        referencesformjs.elements["references-submit"].click();
        valid = false;
    }

    var individualAmount = document.getElementsByName('toNationalize[]');
    var individualAmountLength = individualAmount.length;
    var totalAmount = 0;
    for (var counter = 0; counter < individualAmountLength; counter++)
    {
        totalAmount += parseInt(individualAmount[counter].value);
    }
    if (!totalAmount)
    {
        document.getElementById('alert-Header').innerHTML = "Solicitud no realizada.";
        document.getElementById('alert-comentary').innerHTML = "No se ha seleccionado ninguna referencia.";
        document.getElementById('alert-trigger').click();
        valid = false;
// 	}

// 	// if no reference was choose
// 	/*if(!referenceschecked[0]  && valid == true)
// 	{
// 		//alert('no existe');
        document.getElementById('alert-Header').innerHTML = "Solicitud no realizada.";
        document.getElementById('alert-comentary').innerHTML = "No se ha seleccionado ninguna referencia.";
        document.getElementById('alert-trigger').click();
        valid = false;
    } else
    {
// 		//alert('existe');
        var sizechecked = ObjectSizeAutocomplete(referenceschecked);
        var anychecked;
        for (var o = 0; o < sizechecked; o++)
        {
            anychecked = anychecked || referenceschecked[o].checked;
        }
        if (!anychecked && valid == true)
        {
            //alert('no chequiado');
            document.getElementById('alert-Header').innerHTML = "Solicitud no realizada.";
            document.getElementById('alert-comentary').innerHTML = "No se ha seleccionado ninguna referencia.";
            document.getElementById('alert-trigger').click();
            valid = false;
        }
    }
// 	}/**/
// 	//alert(referencesformjs.elements['referenceschecked[]']);
// 	// if validations are good submit
    if (valid)
    {
        var className = document.getElementsByName["uploadName"];
        //alert(className);
        // upload files
        uploadformjs.elements['nationalize-start-upload'].click();
        setTimeout(function()
        {
            var fields = ' ';
            $.each(arr_forms, function(index, value)
            {
                var serial = value.serialize();
                fields = fields + '&' + serial;
            });
            // deletting the first &
            fields = fields.replace('&', '');
            var repla;
            // Replacing all %5B by [ and %5D by ]
            repla = fields.replace(/%5B/g, '[');
            repla = repla.replace(/%5D/g, ']');
            //alert(repla);
            var form = createForm(repla);
            form.submit();
        }, 1000);
    }
}

// /**
// *	Create a form using a hash of fields (input names and values).
// *	
// *	@var action {string} - the form action
// *	@var fields {hash} - hash of input names and values 
// *	@return {object} - returns a form object 
// */
function createForm(fields)
{
    var form = document.createElement('form');
    //form.style.display = 'none';
    $(form).attr("name", "ghostform");
    $(form).attr("id", "ghostform");
    document.getElementById('content').appendChild(form);
    form.method = 'post';
    //form.action = action;
    createFormFields(form, fields);
    return form;
}
// /**
// *	Create hidden input fields for a form.
// *	
// *	@var form {object} - the form
// *	@var fields {hash} - hash of input names and values 
// */
function createFormFields(form, fields)
{
    var newfields = fields.split("&");
    $.each(newfields, function(key, value)
    {
        createInput(form, key, value);
    });
    var submitb = document.createElement('button');
    $(submitb).attr('type', "submit");
    $(submitb).attr("class", "btn btn-primary btn-block");
    $(submitb).attr("id", "hiddentsubmit");
    $(submitb).attr("style", "display:none;");
    form.appendChild(submitb);
}
// /**
// *	Add a single hidden input field to a form
// *	
// *	@var form {object} - the form
// *	@var hash_pair {object} - a hash pair of key and value 
// */
function createInput(form, key, value)
{
    var splitvalue = value.split("=");
    var input = document.createElement('input');
    input.setAttribute('type', 'hidden');
    input.setAttribute('name', splitvalue[0]);
// 	//input.setAttribute('data-parsley-type',"number");
// 	//input.setAttribute('required','');
// 	// Rails' check_box form helper uses the same name for a given checkbox and
// 	// its accompanying hidden field
    if ($.isArray(splitvalue[1]))
    {
        input.setAttribute('value', splitvalue[1].max());
    } else {
        input.setAttribute('value', splitvalue[1]);
    }
    form.appendChild(input);
}

// // write uploaded file in database nationalization's table.
function saveUpload(file)
{
    var hiddenupfilesjs = document.getElementById('hiddenupfiles');
    hiddenupfilesjs.value = hiddenupfilesjs.value + ",[" + file.name + "," + file.size + "]";
}

function addnewfield()
{
    var sizeofform = document.getElementsByName('formularioNacionalizacion[]').length;

    if (sizeofform <= 19)
    {
// 		// Creating HTML elements.
        var infonationalizationform = document.getElementById('nationalization-form');
        var divcontainer = document.createElement('div');
        var labelform = document.createElement('label');
        var divinputcontainer = document.createElement('div');
        var newinputform = document.createElement('input');

// 		// Setting HTML attributes.
        divcontainer.setAttribute('class', 'form-group');
        labelform.innerHTML = 'Número del formulario de nacionalización (*)';
        labelform.setAttribute('class', 'control-label col-md-4');
        divinputcontainer.setAttribute('class', 'col-md-8');
        newinputform.setAttribute('class', 'form-control');
        newinputform.setAttribute('type', 'text');
        newinputform.setAttribute('name', 'formularioNacionalizacion[]');
        newinputform.setAttribute('data-parsley-type', '15-1');
        newinputform.setAttribute('placeholder', 'Número de formulario de nacionalización');
        newinputform.setAttribute('maxlength', '16');
        newinputform.setAttribute('onkeyup', 'if (event.keyCode == 13) { event.preventDefault() }; autocompleteForm(this);');
        newinputform.setAttribute('onkeydown', 'if (event.keyCode == 13) event.preventDefault() ');

// 		// appending childs
        divinputcontainer.appendChild(newinputform);
        divcontainer.appendChild(labelform);
        divcontainer.appendChild(divinputcontainer);
        infonationalizationform.appendChild(divcontainer);
    }
}

function autocompleteForm(toauto)
{
    // If some eval keyCode is pressed do nothing 8 = backspace, 46 = delete
    var eval = event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 16 || event.keyCode == 36 || event.keyCode == 35;
    if (!eval)
    {
        var auto = toauto.value;
        auto = auto.replace(/^(\d{15})(\d{1}$)/, "$1-$2");
        toauto.value = auto;
    }
}

function existFile(file)
{
    var getjsonjs = 'php/existFile.php?fileName=' + file.name + '&fileSize=' + file.size;
    $.getJSON(getjsonjs, function(response) {
        /* called when request to archivo.php completes */
        if (response)
        {
            document.getElementById('cancel-upload-button ' + file.name).click(); // se repite esta linea al final, por algún bug, no da click a cancel de forma inmediata.
            document.getElementById('alert-Header').innerHTML = "Solicitud rechazada.";
            document.getElementById('alert-comentary').innerHTML = "El archivo con nombre: " + file.name + " ya existe en base de datos, cambiar el nombre si se trata de un archivo diferente.";
            document.getElementById('alert-trigger').click();
            document.getElementById('cancel-upload-button ' + file.name).click();
        }
    });
}