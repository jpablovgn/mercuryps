
$("#saves").click(function() {
    if ($('#natBultos').prop('checked')) {
        if ($('#selectedTable').prop('checked')) {
            saveNacionalizacion();

        }
        if ($('#docCsv').prop('checked')) {
            ValidateCantidadBultos();
            ValidateReferenciaBultos();
            ValidateImport();
            saveNacionalizacionCsv();

        }
    }
    if ($('#natUnits').prop('checked')) {
        if ($('#selectedTable').prop('checked')) {
            saveNacionalizacionUnits();

        }
        if ($('#docCsv').prop('checked')) {
             ValidateImport();
            ValidateReferenciaUnidad();
            ValidateCantidadUnidad();
            saveNacionalizacionUnitsCsv();


        }

    }
});

function saveNacionalizacion() {
    var transportDoc = $("#transportDoc").val();
    var refe = $("#reference").val();
    var cant = document.getElementById($("#reference").val()).value;

    var url = 'php/nacionalizarBultos.php?doc=' + transportDoc + '&name=' + refe + '&cant=' + cant;

    $.getJSON(url, function(result) {
        var headerMsg = "Operación Exitosa";
        var comentMsg = "Mercancía Nacionalizada";
        successWindow(headerMsg, comentMsg);
    });
}

function saveNacionalizacionCsv() {


    var cantFilas = $("#tableReferences tr").length;
    var i;
//           
    for (i = 1; i < cantFilas; i++)
    {
        var transportDoc = $("#transportDoc").val();

        var Refe = document.getElementById("tableReferences").rows[i].cells[1].innerText;
        var Cant = document.getElementById("tableReferences").rows[i].cells[2].innerText;

        var url = 'php/nacionalizarBultos.php?doc=' + transportDoc + '&name=' + Refe + '&cant=' + Cant;

        $.getJSON(url, function(result) {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Mercancía Nacionalizada";
            successWindow(headerMsg, comentMsg);
        });
    }
}

function saveNacionalizacionUnitsCsv() {


    var cantFilas = $("#tableReferences tr").length;
    var i;
//           
    for (i = 1; i < cantFilas; i++)
    {
        var transportDoc = $("#transportDoc").val();

        var Refe = document.getElementById("tableReferences").rows[i].cells[1].innerText;
        var Cant = document.getElementById("tableReferences").rows[i].cells[2].innerText;

        var url = 'php/nacionalizarUnidades.php?doc=' + transportDoc + '&name=' + Refe + '&cant=' + Cant;
        $.getJSON(url, function(result) {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Mercancía Nacionalizada";
            successWindow(headerMsg, comentMsg);
        });
    }
}

function saveNacionalizacionUnits() {
    var transportDoc = $("#transportDoc").val();
    var refe = $("#reference").val();
    var cant = $("#" + refe).val();

    var url = 'php/nacionalizarUnidades.php?doc=' + transportDoc + '&name=' + refe + '&cant=' + cant;
    $.getJSON(url, function(result) {
        var headerMsg = "Operación Exitosa";
        var comentMsg = "Mercancía Nacionalizada";
        successWindow(headerMsg, comentMsg);
    });
}