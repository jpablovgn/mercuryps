//$("#client").data('combobox').disable();
var ids = [
    'numdoctte',
    'NITcliente',
    'ContactoCliente',
    'NombreCliente',
    'MarcaComercial'
];
var compare = [
    'ID',
    'NIT',
    'CONTACTO',
    'NOMBRE',
    'MARCA'
];

$('#importation').change(function()
{
    if (this.value)
    {
        var url = lvlrootjs + "assets/php/consultTransportDocByImportNum.php?" +
                "importation=" + this.value;

        $.getJSON(url, function(result) {
            var transportDocAmount = ObjectSize(result['DATA']);
            // if more than one transport's document, let the user choose
            if (transportDocAmount > 1)
            {
                // Enabling select
                $('#transportDoc').data('combobox').enable();
                //$('#client').prop('disabled',false);
                //$('#client').data('combobox').enable();
                // Showing warning message
                var headerMsg = "importación duplicada";
                var comentMsg = "El número de importación se encuentra " +
                        "duplicado, seleccionar el documento de transporte " +
                        "de la mercancía.";
                warningWindow(headerMsg, comentMsg);
                // Loading documents to load
                var storeTypeData = result['DATA'];
                var sizeofstoreTypeData = ObjectSize(storeTypeData);
                // Deleting combobox items.
                $('#transportDoc').html('');
                $('#transportDoc').data('combobox').refresh();

                // adding blank item.
                var blankItemjs = '<option value=""></option>';
                $('#transportDoc').append(blankItemjs);
                $('#transportDoc').data('combobox').refresh();
                // adding all items.
                for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
                {
                    var appendItemjs = '<option value="' + storeTypeData[ijs][1] + '">' + storeTypeData[ijs][1] + '</option>';
                    $('#transportDoc').append(appendItemjs);
                    $('#transportDoc').data('combobox').refresh();
                }
            }
            else
            {
                // Deleting combobox items.
                $('#transportDoc').html('');
                // adding all items.
                var appendItemjs = '<option value="' + result['DATA'][0][1] + '">' + result['DATA'][0][1] + '</option>';
                $('#transportDoc').append(appendItemjs);
                $('#transportDoc').val(result['DATA'][0][1]);
                $('#transportDoc').data('combobox').refresh();
                $('#transportDoc').data('combobox').disable();
                loadClientData($('#transportDoc').val());
            }
        });
    }
    else
    {
        // Clearing transport's document data
        clearCombobox($('#transportDoc'));
        // Clearing client data
        clearClientData();
        // Clearing table
        clearTable();
        // Hidding boxes's table panel
        $("#panel-nationalize").attr("style", 'display:none;');
        // Hidding cancel-save buttons
        $("#cancel-save").attr("style", 'display:none;');
        // disabling cancel all button
        $('#cancel-all-upload').attr("disabled", true);

    }
});

$('#transportDoc').change(function()
{
    if ($('#transportDoc').val())
    {
        loadClientData($('#transportDoc').val());
    }
    else
    {
        // Clearing client data
        clearClientData();
        // Clearing output type data
        // clearCombobox($('#tiposalidabox'));
        // Clearing table
        clearTable();
        // Hidding boxes's table panel
        $("#panel-nationalize").attr("style", 'display:none;');
        // Hidding cancel-save buttons
        $("#cancel-save").attr("style", 'display:none;');
        // disabling cancel all button
        $('#cancel-all-upload').attr("disabled", true);
    }
});

function loadClientData(transportDoc)
{
    var url = 'php/clientInfo.php?transportDoc=' + transportDoc;
    $.getJSON(url, function(result)
    {
        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        }
        else
        {
            // enabling cancel all button
            $('#cancel-all-upload').attr("disabled", false);
            // loading client info
            $('#NITcliente').val(result['DATA'][0][0]);
            $('#NombreCliente').val(result['DATA'][0][1]);
            $('#ContactoCliente').val(result['DATA'][0][2]);
            $('#MarcaComercial').val(result['DATA'][0][3]);
            if ($('#natBultos').is(':checked')) {

                fillRefe();
            }
            if ($('#natUnits').is(':checked')) {

                fillRefeUnit();
            }
        }
    });
}
// this function calculates the size of an object.
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;

function autoCompleteDoctte(numImport)
{

    $.getJSON('php/clientInfo.php?numImportacion=' + numImport.value, function(clientdata) {
        /* called when request to archivo.php completes */
        if (clientdata)
        {
            fillClientData(clientdata);
            doctteNationalized(doctte);
        }
        else
        {
            document.getElementById('numdoctte').value = '';
            for (var k = 0; k < ObjectSizeAutocomplete(ids); k++)
                document.getElementById(ids[k]).value = '';
            document.getElementById('alert-Header').innerHTML = "Error de referencia.";
            document.getElementById('alert-comentary').innerHTML = "El documento de trasporte ingresado no se encuentra en base de datos.";
            document.getElementById('alert-trigger').click();
        }
    });
}

/* Filling selects combobox */
// Importations
function Import() {
    if ($('#natUnits').is(':checked')) {
        var name = 'UNIDAD';

    }
    if ($('#natBultos').is(':checked')) {
             name = 'BULTO';

    }
    var urlInput = "php/importacion.php?name=" + name;
    $.getJSON(urlInput, function(result) {
        if (result['DATA'])
        {
            var storeTypeData = result['DATA'];
            var sizeofstoreTypeData = ObjectSize(storeTypeData);
            // Deleting combobox items.
            $('#importation').html('');
            $('#importation').data('combobox').refresh();

            // adding blank item.
            var blankItemjs = '<option value=""></option>';
            $('#importation').append(blankItemjs);
            $('#importation').data('combobox').refresh();
            // adding all items.
            for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
            {
                var appendItemjs = '<option value="' + storeTypeData[ijs][1] + '">' + storeTypeData[ijs][1] + '</option>';
                $('#importation').append(appendItemjs);
                $('#importation').data('combobox').refresh();
            }
        }
    });
}

function LoadReferences()
{
    var transportDoc = $('#transportDoc').val();
    var importation = $('#importation').val();
    var referencia = $('#reference').val();
    if ($('#natUnits').is(':checked')) {
        var url = "php/loadReferencesUnits.php?importation=" + importation + "&transportDoc=" + transportDoc + "&referencia=" + referencia;
        $.getJSON(url, function(result)
        {
            if (result['ERROR'])
            {
                alertWindow(result['ERROR'][0], result['ERROR'][1]);
            }
            else
            { 
//                           $("#divReferenceTable").attr("style","display:true");

                createTable(result);
            }
        });

    }
    if ($('#natBultos').is(':checked')) {

        var url = "php/loadReferences.php?importation=" + importation + "&transportDoc=" + transportDoc + "&referencia=" + referencia;
        $.getJSON(url, function(result)
        {
            if (result['ERROR'])
            {
                alertWindow(result['ERROR'][0], result['ERROR'][1]);
            }
            else
            {
                createTable(result);
            }
        });
    }


}
function fillRefe() {
    var transport = $('#transportDoc').val();
    var caso = 'BULTO';
    var urlStorage = 'php/listReferences.php?transporte=' + transport + '&case=' + caso;
    $.getJSON(urlStorage, function(result) {
        if (result['DATA'])
        {
            var storeTypeData = result['DATA'];
            var sizeofstoreTypeData = ObjectSize(storeTypeData);
            // Deleting combobox items.
            $('#reference').html('');
            $('#reference').data('combobox').refresh();

            // adding blank item.
            var blankItemjs = '<option value=""></option>';
            $('#reference').append(blankItemjs);
            $('#reference').data('combobox').refresh();
            // adding all items.
            for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
            {
                var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][0] + '</option>';
                $('#reference').append(appendItemjs);
                $('#reference').data('combobox').refresh();
            }
        }
    });
}
function fillRefeUnit() {
    var transport = $('#transportDoc').val();
    var cas = 'UNIDAD';
    var urlStorage = 'php/listReferences.php?transporte=' + transport + '&case=' + cas;
    $.getJSON(urlStorage, function(result) {
        if (result['DATA'])
        {
            var storeTypeData = result['DATA'];
            var sizeofstoreTypeData = ObjectSize(storeTypeData);
            // Deleting combobox items.
            $('#reference').html('');
            $('#reference').data('combobox').refresh();

            // adding blank item.
            var blankItemjs = '<option value=""></option>';
            $('#reference').append(blankItemjs);
            $('#reference').data('combobox').refresh();
            // adding all items.
            for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
            {
                var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][0] + '</option>';
                $('#reference').append(appendItemjs);
                $('#reference').data('combobox').refresh();
            }
        }
    });
}
function createTable(data)
{
    var dataRows = data['DATA'];
    // Showing boxes's table panel
    $("#panel-nationalize").attr("style", 'display:true;');
    // Showing cancel-save buttons
    $("#cancel-save").attr("style", 'display:true;');
    // Constructing boxes table
    var columnsSize = ObjectSize(dataRows[0]) + 2;
    var dataTable = tableReferencesConstructor(data);

    var numberOfRows = ObjectSize(dataRows);

    // Creating first row, to select boxes
    var selectReferencesRow = createReferencesSelectRow(data['HEADERS'][0]);
    // Adding rows to table
    dataTable.row.add(selectReferencesRow).draw();

    // Adding rows to the table.
    for (var loopRows = 0; loopRows < numberOfRows; loopRows++)
    {
        // If row has data
        if (ObjectSize(dataRows[loopRows][0]))
        {
            // Creating check all and input amount to select
            var toNationalize = document.createElement('input');
            var checkrefjs = document.createElement('input');
            // Create tr element
            var tr = document.createElement('tr');

            // Adding row information to each column cell.
            for (var cellFor = 0; cellFor < columnsSize; cellFor++)
            {
                // Reference id to the fields
                var boxReference = dataRows[loopRows][0];
                // Creating td container
                var td1 = document.createElement('td');
                if (cellFor < 4)
                {
                    var columnName = $(dataTable.column(cellFor).header()).html();
                    td1.innerText = '' + dataRows[loopRows][cellFor];
                    td1.setAttribute('id', boxReference + '-' + columnName);
                }
                if (cellFor == 4)
                {
                    // Setting amount attributes
                    toNationalize.setAttribute('id', boxReference);
                    toNationalize.setAttribute('name', 'toNationalize[]');
                    toNationalize.setAttribute('type', 'text');
                    toNationalize.setAttribute('style', 'width:50px;');
                    toNationalize.value = '0';
                    // adding validate data function
                    toNationalize.addEventListener('change',
                            function() {
                                validatetoNationalize(this);
                            },
                            false
                            );
                    // appending to td container
                    td1.appendChild(toNationalize);
                }
                if (cellFor == 5)
                {
                    // Setting checkbox attributes
                    checkrefjs.setAttribute('name', 'nationalize[]');
                    checkrefjs.setAttribute('id', boxReference + '-check');
                    checkrefjs.setAttribute('type', 'checkbox');
                    // adding select all available function
                    checkrefjs.addEventListener('click',
                            function() {
                                checkit(this);
                            },
                            false
                            );
                    td1.appendChild(checkrefjs);
                }
                tr.appendChild(td1);
            }
            // Numbering hidden column
            // Creating td container
            var td1 = document.createElement('td');
            // appending hiddenColumn sorting
            td1.innerText = 'b';
            tr.appendChild(td1);

            // Adding rows to table
            dataTable.row.add(tr).draw();
        }
    }

    // Row of total amounts
    var totalsRow = createTotalsAmount();
    // Adding rows to table
    dataTable.row.add(totalsRow).draw();
    // Adjusting columns size
    dataTable.columns.adjust().draw();
    // Calculating the beggining total
    calculateBegginingTotal();
}

function tableReferencesConstructor(boxes)
{
    var colDef = []; // column definition
    var boxesType = boxes['HEADERS'][0];
    var headers = ['INGRESADOS', 'NACIONALIZADOS', 'DISPONIBLES', 'CANTIDADES', 'TODOS'];
    // Prepending EAN or REFERENCIAS header
    headers.unshift(boxesType);
    var headersSize = ObjectSize(headers);
    // Creating columns from headers variable
    for (var headersFor = 0; headersFor < headersSize; headersFor++)
    {
        colDef[headersFor] = {
            aTargets: [headersFor],
            sTitle: headers[headersFor]};
    }
    // Hide column
    colDef[headersFor] =
            {
                aTargets: [headersSize],
                sTitle: headers[headersSize],
                bVisible: false
            };

    // If dataTable exist, delete
    if ($.fn.dataTable.isDataTable('#referenceTable')) {
        table = $('#referenceTable').DataTable();
        table.destroy();
        $('#referenceTable').empty();
        // table = $('#referenceTable').DataTable();
        // table.clear();
    }
    // table construct
    table = $('#referenceTable').DataTable(
            {
                //"bJQueryUI": true,
                //"sPaginationType": "full_numbers",
                //"bProcessing": true,
                //"bDeferRender": true,
                //"bInfo" : false,
                //"bDestroy" : true,
                //"bFilter" : false,
                //"bPagination" : false,
                //"aaData": results,
                //"aoColumns": cols,
                "aaSortingFixed": [[6, 'asc']],
                "oLanguage": {
                    "sProcessing": "Procesando...",
                    "sLengthMenu": "Mostrar _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados",
                    "sEmptyTable": "Ningún dato disponible en esta tabla",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                },
                "aoColumnDefs": colDef
            });

    return table;
}

function createReferencesSelectRow(firstHeader) {
    // Adding select bults by total amount row
    // Creating tr element
    var tr = document.createElement('tr');
    // Creating td columns elements
    var tdAllAmountC0 = document.createElement('td');
    var tdAllAmountC1 = document.createElement('td');
    var tdAllAmountC2 = document.createElement('td');
    var tdAllAmountC3 = document.createElement('td');
    var tdAllAmountC4 = document.createElement('td');
    var tdAllAmountC5 = document.createElement('td');
    // hidden column
    var tdAllAmountC6 = document.createElement('td');

    // Setting columns values
    tdAllAmountC0.innerText = 'Seleccionar todas las cantidades disponibles';
    tdAllAmountC1.innerText = '';
    tdAllAmountC2.innerText = '';
    tdAllAmountC3.innerText = '';
    tdAllAmountC4.innerText = '';
    // hiddenColumn sorting
    tdAllAmountC6.innerText = 'a';
    // Setting select box attributes
    if (firstHeader == 'EAN')
    {
        var inputBoxAmount = document.createElement('input');
        inputBoxAmount.setAttribute('type', 'text');
        inputBoxAmount.setAttribute('style', 'width:50px;');
        inputBoxAmount.value = '0';
        // adding select all available function
        inputBoxAmount.addEventListener('change',
                function() {
                    checkTheFirst(this);
                },
                false
                );
        // appending column 4
        tdAllAmountC4.appendChild(inputBoxAmount);
    }
    // Check all button
    var checkAll = document.createElement('input');
    checkAll.setAttribute('id', 'checkAll');
    checkAll.setAttribute('type', 'checkbox');
    // adding select all available function
    checkAll.addEventListener('change',
            function() {
                // Toggle all checkbox with nationalize[] name.
                $("input:checkbox[name=\'nationalize[]\']").prop('checked', $(this).prop("checked"));
                checkitAll();
            },
            false
            );

    // Appendign columns to row
    tr.appendChild(tdAllAmountC0);
    tr.appendChild(tdAllAmountC1);
    tr.appendChild(tdAllAmountC2);
    tr.appendChild(tdAllAmountC3);
    tr.appendChild(tdAllAmountC4);
    tdAllAmountC5.appendChild(checkAll);
    tr.appendChild(tdAllAmountC5);
    tr.appendChild(tdAllAmountC6);
    return tr;
}

/**
 * This function calculates all the checkbox amounts
 */
function checkitAll()
{
    // Loading all the checkBox
    var totalBox = $("input:checkbox[name=\'nationalize[]\']");
    var BoxSize = totalBox.length;
    // Loading available references on the input box.
    for (var sjs = 0; sjs < BoxSize; sjs++)
    {
        checkit(totalBox[sjs]);
    }
}

// This function pass the available amount to nationalize
// obj checkbox type ref-check
function checkit(obj)
{
    var splitReference;
    splitReference = obj.id.split('-');
    var inputReference = document.getElementById(splitReference[0]);
    var availableReference = document.getElementById(splitReference[0] + '-DISPONIBLES');
    if (obj.checked)
    {
        inputReference.value = availableReference.innerText;
    }
    else
    {
        inputReference.value = 0;
    }
    sumToDispatch();
}

// This function validate the dispatch box and sum the total.
function validatetoNationalize(inputValue)
{
    var Available = parseInt(document.getElementById(inputValue.id + '-DISPONIBLES').innerText);
    var IntValue = parseInt(inputValue.value);
    // If IntValue is greater than available reference.
    if (IntValue >= Available)
    {
        inputValue.value = Available;
        // Check checkbox
        $('#' + inputValue.id + '-check').prop('checked', true);
    }
    else
    {
        // Uncheck checkbox
        $('#' + inputValue.id + '-check').prop('checked', false);
        // If IntValue is character or empty insert '0'.
        if (!IntValue || IntValue < 0)
        {
            inputValue.value = 0;
        }
    }
    sumToDispatch();
}

/** This function adds the total amount to dispatch
 * to the 'dispatchTotal' box.
 */
function sumToDispatch()
{
    var totalBox = $("input:text[name=\'toNationalize[]\']");
    var BoxSize = totalBox.length;
    var TotalAmount = 0;
    for (var qjs = 0; qjs < BoxSize; qjs++)
    {
        TotalAmount = TotalAmount + parseInt(totalBox[qjs].value);
    }
    var dispatchTotal = document.getElementById('nationalizeTotal');
    dispatchTotal.innerText = '' + TotalAmount;
}

function createTotalsAmount() {
    // creating columns elements
    var trTotal = document.createElement('tr');
    var total = document.createElement('td');
    var bill = document.createElement('td');
    var real = document.createElement('td');
    var available = document.createElement('td');
    var nationalizeTotal = document.createElement('td');
    var checkBox = document.createElement('td');
    var hiddenColumn = document.createElement('td');

    // setting attributes
    bill.setAttribute('id', 'billTotal');
    real.setAttribute('id', 'realTotal');
    available.setAttribute('id', 'availableTotal');
    nationalizeTotal.setAttribute('id', 'nationalizeTotal');

    total.innerText = 'Total';
    bill.innerText = '0';
    real.innerText = '0';
    available.innerText = '0';
    nationalizeTotal.innerText = '0';
    checkBox.innerText = '';
    // hiddenColumn sorting
    hiddenColumn.innerText = 'zzz';

    // appending childs
    trTotal.appendChild(total);
    trTotal.appendChild(bill);
    trTotal.appendChild(real);
    trTotal.appendChild(available);
    trTotal.appendChild(nationalizeTotal);
    trTotal.appendChild(checkBox);
    trTotal.appendChild(hiddenColumn);

    return trTotal;
}

/**
 * This function calculates the sum of the inicial values
 * of the columns
 */
function calculateBegginingTotal()
{
    // Loading all checkboxes
    var totalBox = $("input:text[name=\'toNationalize[]\']");
    var BoxSize = totalBox.length;
    var totalBillAmount = 0;
    var totalRealAmount = 0;
    var totalAvailableAmount = 0;
    for (var qjs = 0; qjs < BoxSize; qjs++)
    {
        // loading reference/EAN
        var reference = totalBox[qjs].id;
        var getBillElement = document.getElementById(reference + '-INGRESADOS');
        var getRealElement = document.getElementById(reference + '-NACIONALIZADOS');
        var getAvailableElement = document.getElementById(reference + '-DISPONIBLES');

        // Sum total
        totalBillAmount += parseInt(getBillElement.innerHTML);
        totalRealAmount += parseInt(getRealElement.innerHTML);
        totalAvailableAmount += parseInt(getAvailableElement.innerHTML);
    }
    // Loading elements
    var billTotal = document.getElementById('billTotal');
    var realTotal = document.getElementById('realTotal');
    var availableTotal = document.getElementById('availableTotal');

    // Setting sum values
    billTotal.innerText = '' + totalBillAmount;
    realTotal.innerText = '' + totalRealAmount;
    availableTotal.innerText = '' + totalAvailableAmount;
}

function clearTable()
{
    // If dataTable exist, delete
    if ($.fn.dataTable.isDataTable('#referenceTable')) {
        table = $('#referenceTable').DataTable();
        table.destroy();
        $('#referenceTable').empty();
        // table = $('#referenceTable').DataTable();
        // table.clear();
    }
}
/**
 * Clear client input fields
 */
function clearClientData()
{
    // Cleaning client info
    $('#NITcliente').val('');
    $('#NombreCliente').val('');
    $('#ContactoCliente').val('');
    $('#MarcaComercial').val('');
}

/**
 * Clear a combobox object
 * @param [combobox object]: object combobox
 */
function clearCombobox(object) {
    // Cleaning output type
    // Clearing combobox
    object.data('combobox').clearTarget();
    object.data('combobox').clearElement();
}

/**
 * Cancel button pressed
 */
$('#cancelData').click(function() {
    // Clearing table
    clearTable();
    // Cancelling files upload
    $('#cancel-all-upload').click();
    // Clearing client info
    clearClientData();
    // Clearing transport's document
    clearCombobox($('#transportDoc'));
    // Clearing client's importation
    clearCombobox($('#importation'));
    // Hidding nationalize's table panel
    $("#panel-nationalize").attr("style", 'display:none;');
    // Hidding cancel-save buttons
    $("#cancel-save").attr("style", 'display:none;');
    // disabling cancel all button
    $('#cancel-all-upload').attr("disabled", true);
});