// Importation number
$('#importacion').attr('data-parsley-type',"alphanum");
$('#importacion').attr('placeholder',"Importación del cliente");
$('#importacion').attr('style',"text-transform:uppercase");
$('#importacion').attr('data-parsley-required',true);

// transport's document
$('#numdoctte').attr('data-parsley-type',"alphanum");
$('#numdoctte').attr('placeholder',"Documento de transporte");
$('#numdoctte').attr('style',"text-transform:uppercase");
$('#numdoctte').attr('readonly','');

// Clients information
$('#NITcliente').attr('data-parsley-type',"digits");
$('#NITcliente').attr('placeholder',"NIT");
$('#NITcliente').attr('readonly','');

$('#ContactoCliente').attr('placeholder',"Contacto");
$('#ContactoCliente').attr('readonly','');

$('#NombreCliente').attr('placeholder',"Nombre empresa");
$('#NombreCliente').attr('readonly','');

$('#MarcaComercial').attr('placeholder',"Marca");
$('#MarcaComercial').attr('readonly','');

// Nationalization panel
$('#panel-nationalize').attr('style',"display:none;");