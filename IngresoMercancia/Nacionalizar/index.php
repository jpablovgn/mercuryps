<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";

// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");

// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");

// On enter no submit
$nosubmit = "if (event.keyCode == 13) event.preventDefault()";
// functions defined in js/autocompleteDoctteHandler.js
$autoCompleteDoctteCall = "if (event.keyCode == 13) { event.preventDefault(); autoCompleteDoctte(this); }";
// function defined in js/validationfields.js
$autoCompleteForm = "autocompleteForm(this);";
?>
<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="//www.opencpu.org/js/archive/opencpu-0.4.js"></script>
<!--<script src="../../assets/plugins/switchery/switchery.min.js"></script>-->
<!--<script src="../../assets/plugins/bootstrap/js/bootstrap.min.js"></script>-->


<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Ingreso Mercancía</a></li>
    <li class="active">Nacionalizar</li>
</ol>

<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Nacionalizar mercancía <small>Nacionalizar la mercancía por número de importación.</small></h1>
<!-- end page-header -->

<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">
        <!-- begin panel Nationalization -->
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-globe fa-lg"></i>
                    Información de Nacionalización de mercancía
                </h4>
            </div>
            <!-- begin body panel -->

            <div class="panel-body panel-form " id="panel-body-form">
                <!-- begin form -->
                <form data-parsley-validate="true" id="nationalization-form" name="nationalization-form" class="form-horizontal form-bordered" method="post" action="">

                    <div class="form-group">
                        <label class="col-md-4 control-label text-left">Seleccione La Forma de Nacionalización</label>

                        <div class="form-group" id="description-form-group">
                            <div class="col-md-4 col-sm-4">
                                <div class="checkbox-inline">
                                    <label>
                                        <input type="checkbox" value="1" id="natUnits" name="ProcesoUnidades" style="display:none;"  onclick="Import();"/>
                                        <input type="checkbox" name="optionHidePassword" value='-1' id="chb1" data-render="switchery" data-theme="default" data-change="check-switchery-state-text"  />

                                        Nacionalizar por Unidades
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="checkbox-inline">
                                    <label>
                                        <input type="checkbox" value="2" id="natBultos" name="ProcesoSerial" style="display:none;" onclick="Import();"/>
                                        <input type="checkbox" name="optionHidePassword" value='-1' id="chb2"  data-render="switchery" data-theme="default" data-change="check-switchery-state-text"  />

                                        Nacionalizar por Bultos

                                    </label>
                                </div>                       
                            </div>

                        </div>



                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 text-left" for="message">Número de importación del cliente (*)</label>
                        <div class="col-md-4">
                                <!--<input class="form-control" type="text" id="importacion" name="importacion" />-->
                            <select class="combobox" name="importation" id="importation" value="">
                                <option value=""> Seleccione una importación</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 text-left" for="message">Número del documento de transporte (*)</label>
                        <div class="col-md-4">
                            <select class="combobox" name="transportDoc" id="transportDoc" value="">
                                <option value=""> Seleccione un documento de transporte</option>
                            </select>
                        </div>
                    </div>
                    <!--<div class="form-group">
                            <label class="control-label col-md-4 text-left" for="message">Número del documento de transporte (*)</label>
                            <div class="col-md-8">
                                    <input class="form-control" type="text" id="numdoctte" name="numdoctte" data-parsley-type="alphanum" placeholder="Documento de transporte" onkeydown="<?php echo $autoCompleteDoctteCall; ?>" style="text-transform:uppercase"  />
                            </div>
                    </div>-->
                    <!-- begin client data -->
                    <div class="form-group">
                        <div class="col-md-2">
                            <label>NIT</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="NITcliente" name="NITcliente"/>
                        </div>
                        <div class="col-md-2">
                            <label>Nombre del Contacto </label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="ContactoCliente" name="ContactoCliente"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2">
                            <label>Nombre Empresa </label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="NombreCliente" name="NombreCliente"/>
                        </div>
                        <div class="col-md-2">
                            <label>Marca Comercial </label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="MarcaComercial" name="MarcaComercial"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" class="form-control" id="hiddenidclient" name="hiddenidclient"/>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label text-left">Seleccione La Forma de Nacionalización</label>

                        <div class="col-md-4 col-sm-4">
                            <div class="checkbox-inline">
                                <label>
                                    <input type="checkbox" value="2" id="docCsv" name="docCsv" style="display:none;" onclick="validateCheck();"/>
                                    <input type="checkbox" name="optionHidePassword" value='-1' id="chb2"  data-render="switchery" data-theme="default" data-change="check-switchery-state-text"  />

                                    Importar Documento csv

                                </label>
                            </div>                       
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="checkbox-inline">
                                <label>
                                    <input type="checkbox" value="2" id="selectedTable" name="selectedTable" style="display:none;" onclick="validateCheck();"/>
                                    <input type="checkbox" name="optionHidePassword" value='-1' id="chb2"  data-render="switchery" data-theme="default" data-change="check-switchery-state-text"  />

                                    Selección en Tabla
                                </label>
                            </div>                       
                        </div>
                        <!--                                              <div class="col-md-4 col-sm-4">
                        
                                                <label class="control-label col-md-2 text-left">Número formulario de nacionalización (*)</label>
                                                <div class="col-md-4">
                        
                                                    <input class="form-control" type="text" id="formularyOne" name="formularioNacionalizacion[]" data-parsley-type="15-1" placeholder="Número de formulario de nacionalización" onkeyup="<?php echo $autoCompleteForm; ?>" onkeydown="<?php echo $nosubmit; ?>" maxlength="16" onchange="form();"/>
                        
                                                </div>
                                            </div>-->
                        <div class="form-group" id="divref" style="display:none;">

                            <label class="control-label col-md-2 text-left">Referencia (*)</label>
                            <div class="col-md-4">
                                <!-- onchange="this.form.action=''; this.form.submit()" -->
                                <select class="combobox" name="reference" id="reference" value="" onchange="LoadReferences();">
                                    <option value=""> Seleccione una Referencia</option>
                                </select>
                            </div>

                        </div>
                        <div class="form-group" id="formdescarga" style="display:none">
                            <div class="col-md-6 col-sm-6">
                                <label class="control-label col-md-12 text-right" style="font-weight: 100;" for="message" >
                                    Adjunte el archivo con formato (REFERENCIAS, CANTIDAD, IMPORTACIÓN) .csv separados por punto y coma, con nombre <b> (nacionalizarMercancia.csv). </b>
                                </label>

                            </div>
                            <div class="col-md-6 col-sm-6" >
                                <button id="downloadExampleBoxes" type="button" class="btn btn-white m-r-5 right">
                                    <i class="fa fa-download">
                                        Descargar formato  para nacionalizar 
                                    </i>
                                </button>
                            </div>


                        </div>
                        <div class="form-group">


                            <span class="btn btn-success fileinput-button" id="addDoc" style="display: none;">
                                <i class="fa fa-plus"></i>
                                <span>Agregar Documento csv</span>
                                <input type="file" id="csvfile" >
                            </span>
                            <button id="submitbutton" type="button" class="btn btn-primary start" style="display:none;" read.csv >
                                <i class="fa fa-upload"></i>
                                <span>Cargar Documento</span>
                            </button>
                            <pre style="display:none;"><code id="output" style="display:none;"></code></pre>

                        </div>


                </form>


            </div>
           
       
    
<!--                   
                    <div id="divTables" class="table table-striped table-bordered"></div>
                     The table listing the files available for upload/download 
                    <table role="presentation" class="table table-striped" id="table-upload">
                        <tbody class="files">
                        </tbody>
                    </table>-->
                   
                    <br>
            </div>
            
        </div>
        <!-- end body panel -->
    </div>
</div>  
<!-- end panel Nationalization -->
<!-- begin panel references -->
<div class="panel panel-inverse" id="panel-nationalize">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">
            <i class="fa fa-check-square fa-lg"></i>
            EAN/Referencias nacionalizadas
        </h4>
    </div>
    <!-- begin body panel -->
    <div class="panel-body panel-form form-bordered form-horizontal" id="panel-body-references">
        <!-- begin form -->
        <form id="references-form" name="references-form" method="post" action="">
            <div class="form-group" id="divReferenceTable" >
                <table class="table" id="referenceTable">
                </table>
            </div>
                                <div id="divTables" class="table table-striped table-bordered"></div>

            <div class="form-group" id="cancel-save" style="display:true;">
                        <div class="col-md-12 col-sm-12 text-right">
                            <button id="cancelData" name="cancelData" class="btn btn-warning btn-sm">
                                <i class="fa fa-ban">
                                    Cancelar
                                </i>
                            </button>
                            <button id="saves" name="save" class="btn btn-primary btn-sm" >
                                <i class="fa fa-floppy-o">
                                    Guardar
                                </i>
                            </button>

                        </div>
                    </div>
        </form>
        <!-- end form -->
    </div>
                                        <!-- end body panel -->
</div>

<!-- end panel references -->
</div>
<!-- end main column -->

<!-- begin save/cancel button -->

<!-- end save/cancel button -->
<!-- Including alerts windows -->
<?php
include_once($lvlroot . "Body/AlertsWindows.php");
//include("php/subirArchivo.php");
?>



<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
// Writing on database.
//	include_once("php/Insert2db.php");
?>


<!-- // Loading autocomplete handler. -->
<script type="text/javascript" src="js/autocompleteDoctteHandler.js"></script>
<!-- // Loading autocomplete nationalize. -->
<script type="text/javascript" src="js/autocompleteNationalize.js"></script>
<script type="text/javascript" src="js/saveData.js"></script>
<!-- // Loading autocomplete set validations. -->
<script type="text/javascript" src="js/setValidations.js"></script>
<script type="text/javascript" src="js/loadArchivoCsv.js"></script>
<script type="text/javascript" src="js/Validations.js"></script>
<script type="text/javascript" src="js/downloadFormato.js"></script>

<script type="text/javascript">
                                    // Activating the side bar.
                                    var Change2Activejs = document.getElementById("sidebarIngresoMercancia");
                                    Change2Activejs.className = "has-sub active";
                                    var Changesub2Activejs = document.getElementById("sidebarIngresoMercancia-Nacionalizar");
                                    Changesub2Activejs.className = "active";
</script>

<script type="text/javascript" src="js/validationfields.js"></script>

<!-- begin Configuration of accepted files -->
	
<!-- end Configuration of accepted files -->

<!-- The template to display files available for upload -->
<!--<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td class="col-md-1">
    <span class="preview"></span>
    </td>
    <td>
    <p class="name" name="uploadName">{%=file.name%}</p>
    <strong class="error text-danger"></strong>
    </td>
    <td>
    <p class="size">Procesandop>
    <div class="progress progress-striped active"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
    </td>
    <td>
    {% if (!i && !o.options.autoUpload) { %}
    <button class="btn btn-primary btn-sm start" disabled style="display:none;">
    <i class="fa fa-upload"></i>
    <span>Start</span>
    </button>
    {% } %}
    {% if (!i) { %}
    <button class="btn btn-white btn-sm cancel" id="cancel-upload-button {%=file.name%}" style="display:none;">
    <i class="fa fa-ban"></i>
    <span>Cancelar</span>
    </button>
    {% } %}
    </td>
    </tr>
    {% checkFile(file); %}
    {% } %}
</script>
 The template to display files available for download 
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <span class="preview">
    {% if (file.thumbnailUrl) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
    {% } %}
    </span>
    </td> 
    <td>
    <p class="name">
    {% if (file.url) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
    {% } else { %}
    <span>{%=file.name%}</span>
    {% } %}
    </p>
    {% if (file.error) { %}
    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
    {% } %}
    </td>
    <td>
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    </td>
    <td>
    {% if (file.deleteUrl) { %}
    <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="glyphicon glyphicon-trash"></i>
    <span>Delete</span>
    </button>
    <input type="checkbox" name="delete" value="1" class="toggle">
    {% } else { %}
    <button class="btn btn-warning cancel">
    <i class="glyphicon glyphicon-ban-circle"></i>
    <span>Cancelar</span>
    </button>
    {% } %}
    </td>
    </tr>
    {%  %}
    {% } %}
</script>-->