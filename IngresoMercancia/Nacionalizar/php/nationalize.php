<?php
	/*<!--
	* Este archivo inserta en base de datos
	* en la tabla de nacionalizacion
	* el ID del archivo y el ID del documento de transporte
	-->*/
	include_once('../../../assets/php/PhpMySQL.php');
	$docTransporte 	= filter_input(INPUT_GET, 'docTransporte');
	$fileName		= filter_input(INPUT_GET, 'fileName');
	$fileSize		= filter_input(INPUT_GET, 'fileSize');
	$nationalizeDB = new Database();
	
	// Acentos de base de datos a html.
	$acentos = $nationalizeDB->query("SET NAMES 'utf8'");
	
	$queryFileID = "SELECT ID_ARCHIVO FROM MR_ARCHIVOS WHERE "
		."NOMBRE='$fileName' AND TAMANO='$fileSize'";
	$queryFileIDResult = $nationalizeDB->query($queryFileID);
	
	while($fileAux = $nationalizeDB->fetch_array_assoc($queryFileIDResult))
	{
		$fileID = $fileAux['ID_ARCHIVO'];
	}
	
	$queryInsert = "INSERT INTO MR_ARCHIVO (`ID_TRANSPORTE`,`ID_ARCHIVO`) "
	." VALUES ('$docTransporte',$fileID)";
	$nationalizeDB->query($queryInsert);
	$nationalizeDB->close();
	print json_encode($fileID);
?>