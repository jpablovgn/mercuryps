<?php
@$data = json_decode($_POST['data'], true);
@$transportDoc = $data['transportDoc'];
@$nationalizationType = $data['nationalizationType'];
@$valuesArray = $data['valuesArray'];

include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();
// Acentos de base de datos a html.
$accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    foreach (@$valuesArray as $key => $value) {
        $reference = $value[0];
        $amount = $value[1];
        $query = "CALL INGRESO_NACIONALIZACION('$transportDoc',"
                . "'$nationalizationType','$reference',$amount);";
        $queryResult = $connection->query($query);
        if (!$queryResult) {
            $totalBox[] = $reference;
        }
        unset($queryResult);
        $result['DATA'][] = $query;
    }
    if (@$totalBox) {
        $result['ERROR'][0] = "Error de selección";
        $result['ERROR'][1] = "No se pudo seleccionar el bulto: " . implode(',', $totalBox);
    }

    $connection->close();
}
print json_encode(@$result);
?>