
$("#btnsaveData").click(function() {
    if ($("#formclient").parsley().validate())
    {
        // Confirmation message


        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar la Mercancía?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'Savess()');
        document.getElementById('warning-trigger').click();
    }
});

$("#btnupdate").click(function() {
    if ($("#formclient").parsley().validate())
    {
        // Confirmation message
        document.getElementById('warning-Header').innerHTML = "ACTUALIZAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea actualizar los datos de la Mercancía?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'update()');
        document.getElementById('warning-trigger').click();
    }
});

$("#saveRefe").click(function() {
    if ($("#tableReferences").is(':visible'))
    {
        // Confirmation message
        if ($('#chb1').prop('checked'))
        {
            document.getElementById('warning-Header').innerHTML = "GUARDAR";
            document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar las Referencias?";
            document.getElementById('warning-accept-button').setAttribute('onclick', 'savePallets()');
            document.getElementById('warning-trigger').click();
        }
        else if ($('#chb2').prop('checked'))
        {
            document.getElementById('warning-Header').innerHTML = "GUARDAR";
            document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar las Referencias?";
            document.getElementById('warning-accept-button').setAttribute('onclick', 'savePallets()');
            document.getElementById('warning-trigger').click();
        }
        else {
            var headerMsg = "Error al guardar referencias";
            var comentMsg = "Seleccione un proceso de la mercancía para continuar..";
            alertWindow(headerMsg, comentMsg);
        }
    } else {
        var headerMsg = "Archivo no encontrado";
        var comentMsg = "Cargue el archivo .csv  para continuar..";
        alertWindow(headerMsg, comentMsg);
    }
});

function update() {
    var clientNit = $("#selectNITClient").val(), transportDoc = $("#numdoctte").val(), importation = $("#numimportacion").val();
    var consecutiveZF = $("#consecutivozf").val(), FOGValue = $("#valorFOB").val();
    var inputType = $("#tipoingresobox").val(), containerType = $("#tipocontenedorbox").val(), storageType = $("#tipoalmacenamientobox").val();
    var totalWeight = $("#weight").val(), description = $("#descripcionMercancia").val(), totalBoxes = $("#totalcajas").val();
    var totalUnits = $("#totalunidades").val(), unitProcess = $("#ProcesoUnidades").val(), serialProcess = $("#ProcesoSerial").val();
    var observations = $("#observaciones").val(), manifiesto = $("#manifiesto").val(), flete = $("#fletes").val(), seguro = $("#seguros").val();
    var gastos = $("#otrosgastos").val(), paisProcedencia = $("#paisProcedencia").val(), neto = $("#weightNeto").val(), proveedor = $("#selectNITProveedor").val();
    var paiscompra = $("#paisCompra").val(), paisOrigen = $("#paisOrigen").val(), bandera = $("#bandera").val(), tasaC = $("#tasacambio").val();

    var url = 'php/update.php?clientNit=' + clientNit + '&transportDoc=' + transportDoc + '&inputType=' + inputType + '&containerType=' + containerType +
            '&storageType=' + storageType + '&importation=' + importation + '&observations=' + observations + '&totalWeight=' + totalWeight +
            '&description=' + description + '&consecutiveZF=' + consecutiveZF +  '&FOGValue=' + FOGValue + '&manifiesto=' + manifiesto +
            '&flete=' + flete + '&seguro=' + seguro + '&gastos=' + gastos + '&procedencia=' + paisProcedencia + '&compra=' + paiscompra + '&origen=' + paisOrigen +
            '&bandera=' + bandera + '&pesoneto=' + neto + '&tasacambio=' + tasaC + '&proveedor=' + proveedor;
    console.log(url);
    $.getJSON(url, function(result)
    {
       
        if (result['ERROR']) {
            var headerMsg = "Error";
            var comentMsg = "No Se Ha Podido Actualizar La Mercancía ";
            alertWindow(headerMsg, comentMsg);
        } else {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Actualización De Mercancía Realizado Correctamente ";
            successWindow(headerMsg, comentMsg);
        }
    });

}

function Savess() {
    var clientNit = $("#selectNITClient").val(), transportDoc = $("#numdoctte").val(), importation = $("#numimportacion").val();
    var consecutiveZF = $("#consecutivozf").val(), bills = $("#tagsinglefield").val(), FOGValue = $("#valorFOB").val();
    var inputType = $("#tipoingresobox").val(), containerType = $("#tipocontenedorbox").val(), storageType = $("#tipoalmacenamientobox").val();
    var totalWeight = $("#weight").val(), description = $("#descripcionMercancia").val(), totalBoxes = $("#totalcajas").val();
//    var totalUnits = $("#totalunidades").val(), unitProcess = $("#ProcesoUnidades").val(), serialProcess = $("#ProcesoSerial").val();
    var observations = $("#observaciones").val(), manifiesto = $("#manifiesto").val(), flete = $("#fletes").val(), seguro = $("#seguros").val();
    var gastos = $("#otrosgastos").val(), paisProcedencia = $("#paisProcedencia").val(), neto = $("#weightNeto").val(), proveedor = $("#selectNITProveedor").val();
    var paiscompra = $("#paisCompra").val(), paisOrigen = $("#paisOrigen").val(), bandera = $("#bandera").val(), tasaC = $("#tasacambio").val();

    var url = 'php/save.php?clientNit=' + clientNit + '&transportDoc=' + transportDoc + '&inputType=' + inputType + '&containerType=' + containerType +
            '&storageType=' + storageType + '&importation=' + importation + '&observations=' + observations + '&totalWeight=' + totalWeight +
            '&description=' + description + '&consecutiveZF=' + consecutiveZF + '&FOGValue=' + FOGValue + '&manifiesto=' + manifiesto +
            '&flete=' + flete + '&seguro=' + seguro + '&gastos=' + gastos + '&procedencia=' + paisProcedencia + '&compra=' + paiscompra + '&origen=' + paisOrigen +
            '&bandera=' + bandera + '&pesoneto=' + neto + '&tasacambio=' + tasaC + '&proveedor=' + proveedor;
    console.log(url);
    $.getJSON(url, function(result)
    {
        if (result['ERROR']) {
            var headerMsg = "No Se Ha Podido Ingresar La Mercancía";
            var comentMsg = "Verificar Datos e Intente Nuevamente";
            alertWindow(headerMsg, comentMsg);
        }
        else
        {
            savePallets();

            var headerMsg = "Operación Exitosa";
            var comentMsg = "Ingreso De Mercancía Realizado Correctamente ";
            successWindow(headerMsg, comentMsg);

        }
        
         Facturas();

    });

}
function savePallets() {
    var transportDoc = $("#numdoctte").val();
    var totalPallets = $("#totalpalets").val();
    var url = 'php/savePallets.php?transportDoc=' + transportDoc + '&pallets=' + totalPallets;
        console.log(url);

    $.getJSON(url, function(result) {
        if (result['ERROR']) {
            var headerMsg = "Error Al Crear Las Estibas";
            var comentMsg = "Verificar Datos e Intente Nuevamente";
            alertWindow(headerMsg, comentMsg);
        } else {
             if ($("#chb1").prop('checked'))
            {
                BultosUnits();
                Units();
            }
            if ($("#chb2").prop('checked'))
            {
                Bultos();

            }
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Ingreso De Mercancía Realizado Correctamente ";
            successWindow(headerMsg, comentMsg);

           
        }
    });
}

//
function Units() {

    var cantFila = $("#tableReferences tr").length;

    var l;
    for (l = 1; l < cantFila; l++)
    {
        var Transport = $("#numdoctte").val();
        var Ref = document.getElementById("tableReferences").rows[l].cells[1].innerText;
        var CantU = document.getElementById("tableReferences").rows[l].cells[2].innerText;

        var units = 1;

        var url = 'php/saveUnidad.php?transportDoc=' + Transport + '&cant=' + CantU + '&referencia=' + Ref + '&proceso=' + units;
        $.getJSON(url, function(result)
        {

            if (result['ERROR']) {
                var headerMsg = "Error Al Crear Las Unidades";
                var comentMsg = "Verificar Datos e Intente Nuevamente";
                alertWindow(headerMsg, comentMsg);
            }

        });
    }
//            
}

function Facturas() {

    var cantFila = $("#tabla tr").length;

    var l;
    for (l = 1; l < cantFila; l++)
    {
        var Transport = $("#numdoctte").val();
        var Fact = document.getElementById("tabla").rows[l].cells[0].innerText;
        var Moneda = document.getElementById("tabla").rows[l].cells[1].innerText;

        var url = 'php/saveFacturas.php?factura=' + Fact + '&moneda=' + Moneda + '&trans=' + Transport ;
        console.log(url);
        $.getJSON(url, function(result)
        {

            if (result['ERROR']) {
                var headerMsg = "Error Al Guardar las facturas";
                var comentMsg = "Verificar Datos e Intente Nuevamente";
                alertWindow(headerMsg, comentMsg);
            }

        });
    }
//              
}

function Bultos() {
    var cantFilas = $("#tableReferences tr").length;
    var i;
//           
    for (i = 1; i < cantFilas; i++)
    {
        var Transport = $("#numdoctte").val();
        var Refe = document.getElementById("tableReferences").rows[i].cells[1].innerText;
        var CantB = document.getElementById("tableReferences").rows[i].cells[2].innerText;

        var url = 'php/saveBulto.php?transportDoc=' + Transport + '&cant=' + CantB + '&referencia=' + Refe;

        $.getJSON(url, function(result)
        {
            if (result['ERROR']) {
                var headerMsg = "Error Al Crear Los Bultos";
                var comentMsg = "Verificar Datos e Intente Nuevamente";
                alertWindow(headerMsg, comentMsg);
            }
        });
    }
}
function BultosUnits() {
    var Transport = $("#numdoctte").val();
    var CantB = $("#totalcajas").val();
    var url = 'php/saveBulto.php?transportDoc=' + Transport + '&cant=' + CantB + '&referencia=' + 'NULL';
    $.getJSON(url, function(result)
    {
        if (result['ERROR']) {
            var headerMsg = "Error Al Crear Los Bultos";
            var comentMsg = "Verificar Datos e Intente Nuevamente";
            alertWindow(headerMsg, comentMsg);
        }
    });
}