function loadSavedData(transportDocument)
{
    cargart();
    // get JSON with stored data.
    var url = "php/loadStoredData.php?transportDoc=" + transportDocument;
    $.getJSON(url, function(result) {
        if (result)
        {
            if (result['ERROR'])
            {
                var headerMsg = result['ERROR'][0];
                var comentMsg = result['ERROR'][1];
                alertWindow(headerMsg, comentMsg);
            }
            if (result['DATA'])
            {

                // Clearing all the data function from validateOnSave.js file
                //clearData();
                // setting global variable to true
                globalReload = true;
                // // Loading client information q  
                $("#selectNITClient").val(result['DATA'][0][0]);
                $("#selectNITClient").data('combobox').refresh();
                $("#selectEName").val(result['DATA'][0][1]);
                $("#selectEName").data('combobox').refresh();
                $("#selectContact").val(result['DATA'][0][2]);
                $("#selectContact").data('combobox').refresh();
                $("#selectTradeMark").val(result['DATA'][0][3]);
                $("#selectTradeMark").data('combobox').refresh();
                // Loading import's information
//                $("#numdoctte").val(result['DATA'][0][4]);
                $("#numimportacion").val(result['DATA'][0][5]);
                $("#consecutivozf").val(result['DATA'][0][6]);
                // Loading bills
//                if (result['DATA'][0][7])
//                {
//                    var tagschilds = result['DATA'][0][7].split(',');
//                    for (var jk = 0; jk < tagschilds.length; jk++)
//                    {
//                        $("#jquery-tagIt-primary").tagit("createTag", tagschilds[jk]);
//                    }
//                }
                $("#valorFOB").val(result['DATA'][0][7]);
                // Loading merchandice's information
                $("#tipoingresobox").val(result['DATA'][0][8]);
                $("#tipoingresobox").data('combobox').refresh();
                $("#tipocontenedorbox").val(result['DATA'][0][9]);
                $("#tipocontenedorbox").data('combobox').refresh();
                $("#tipoalmacenamientobox").val(result['DATA'][0][10]);
                $("#tipoalmacenamientobox").data('combobox').refresh();
                $("#weight").val(result['DATA'][0][11]);
                $("#descripcionMercancia").val(result['DATA'][0][12]);
                $("#totalpalets").val(result['DATA'][0][13]);
                $("#totalcajas").val(result['DATA'][0][14]);
                $("#totalunidades").val(result['DATA'][0][15]);
                $("#ProcesoUnidades").prop('checked', (result['DATA'][0][16]) ? true : false);
                $("#ProcesoSerial").prop('checked', (result['DATA'][0][17]) ? true : false);
                $("#observaciones").val(result['DATA'][0][18]);
                $("#weightNeto").val(result['DATA'][0][19]);
                $("#manifiesto").val(result['DATA'][0][20]);
                $("#fletes").val(result['DATA'][0][21]);
                $("#seguros").val(result['DATA'][0][22]);
                $("#otrosgastos").val(result['DATA'][0][23]);
                $("#paisProcedencia").val(result['DATA'][0][24]);
                $("#paisProcedencia").data('combobox').refresh();
                $("#paisCompra").val(result['DATA'][0][25]);
                $("#paisCompra").data('combobox').refresh();
                $("#paisOrigen").val(result['DATA'][0][26]);
                $("#paisOrigen").data('combobox').refresh();
                $("#bandera").val(result['DATA'][0][27]);
                $("#bandera").data('combobox').refresh();
                $("#selectNITProveedor").val(result['DATA'][0][28]);
                $("#selectNITProveedor").data('combobox').refresh();
                $("#selectEProveedor").val(result['DATA'][0][29]);
                $("#selectEProveedor").data('combobox').refresh();
                $("#tasacambio").val(result['DATA'][0][30]);

                var impor = $("#numimportacion").val();
                $("#tableDocs").load("php/tableDocs.php?imp=" + impor);
                $("#saveRefe").attr('style', 'display:true');
                $('#btnsaveData').attr('disabled', true);



                // Disabling fields
//                $('#tipoingresobox').data('combobox').disable();
//                $('#tipocontenedorbox').data('combobox').disable();
//                $('#tipoalmacenamientobox').data('combobox').disable();
                $("#numimportacion").attr('disabled', true);



//                $('#totalpalets').attr('readonly', true);
//                $('#totalcajas').attr('readonly', true);
                $('#totalunidades').attr('readonly', true);
                $('#ProcesoUnidades').attr('readonly', true);
                $('#ProcesoSerial').attr('readonly', true);

                // validate if the user can edit the data.
                // function at validateOnLoad.js file.
                var url = "php/checkBoxReception.php?transportDoc=" + transportDocument;
                $.getJSON(url, function(result) {
                    if (result['ERROR'])
                    {
                        alertWindow(result['ERROR'][0], result['ERROR'][1]);
                    }
                    else
                    {
                        var validation = parseInt(result['SUCCESS']);
                        if (!validation)
                        {
                            //alert('el usuario p editar');
                            // call function from validateOnLoad.js
                            // Enabling user edition.
                            userCanEdit($("#numdoctte").val());
                        }
                        else
                        {
                            var headerMsg = "Mercancía ya recibida";
                            var comentMsg = "La mercancía ya ha sido " +
                                    "recibida físicamente, solo se podrá " +
                                    "actualizar la información de la mercancía.";
                            warningWindow(headerMsg, comentMsg);
//                                        clearPartialData();

                            // Disabling box manual edition
                            $('#refbultosmanual').attr('disabled', true);
                        }
                    }
                });
            }
        }
        else
        {
            var headerMsg = "No Hay Datos";
            var comentMsg = "Documento disponible para crear Mercancía" +
                    warningWindow(headerMsg, comentMsg);
            globalReload = false;
            // Clearing data
        }
    });
}

$('#numdoctte').change(function()
{
    $('#numdoctte').val($('#numdoctte').val().toUpperCase());
    loadSavedData(this.value);
});

function clearPartialData()
{
    $("#numimportacion").val("")
            .removeClass("parsley-success");
    $("#consecutivozf").val("")
            .removeClass("parsley-success");
    // Clearing bills
    $("#jquery-tagIt-primary").tagit('removeAll');
    // Clearing data
    $("#valorCIF").val("")
            .removeClass("parsley-success");
    $("#valorFOB").val("")
            .removeClass("parsley-success");
    // Clearing combobox
    $("#tipoingresobox").data('combobox').clearTarget();
    $("#tipoingresobox").data('combobox').clearElement();
    $("#tipocontenedorbox").data('combobox').clearTarget();
    $("#tipocontenedorbox").data('combobox').clearElement();
    $("#tipoalmacenamientobox").data('combobox').clearTarget();
    $("#tipoalmacenamientobox").data('combobox').clearElement();
    // Enabling combobox function from validateOnLoad.js
    $("#tipoingresobox").data('combobox').enable();
    $("#tipocontenedorbox").data('combobox').enable();
    $("#tipoalmacenamientobox").data('combobox').enable();
    // Clearing data
    $("#weight").val("")
            .removeClass("parsley-success");
    $("#descripcionMercancia").val("")
            .removeClass("parsley-success");
    $("#totalpalets").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#totalcajas").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#totalunidades").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#ProcesoUnidades").val("")
            .removeClass("parsley-success");
    $("#ProcesoSerial").val("")
            .removeClass("parsley-success");
    $("#observaciones").val("")
            .removeClass("parsley-success");
    // Clearing checkbox
    $('#ProcesoUnidades').prop('checked', false)
            .attr('disabled', false);
    $('#ProcesoSerial').prop('checked', false)
            .attr('disabled', false);
    // Canceling upload files
    $('#cancel-all-upload').click();
    // Clearing references combobox
    if ($("#refbultosmanual").val())
    {
        if ($("#refbultosmanual").prop("checked"))
        {
            $("#refbultosmanual").prop("checked", false);
            createBultosfields($("refbultosmanual"));
        }
    }

    // Enabling fields
    $("#numimportacion").attr('disabled', false);
    $("#weight").attr('disabled', false);
    $("#descripcionMercancia").attr('disabled', false);
    $('#refbultosmanual').attr('disabled', false);
}