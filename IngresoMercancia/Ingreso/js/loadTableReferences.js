   //because read.csv is in utils
                        ocpu.seturl("//public.opencpu.org/ocpu/library/utils/R");
                   //actual handler
                        $("#submitbutton").on("click", function() {

                            //arguments
                            var myheader = $("#header").val() == "true";
                            var myfile = $("#csvfile")[0].files[0];

                            if (!myfile) {
                                alert("No file selected.");
                                return;
                            }
                            //disable the button during upload
                            $("#submitbutton").attr("disabled", "disabled");

                            //perform the request
                            var req = ocpu.call("read.csv", {
                                "file": myfile
                            }, function(session) {
                                session.getConsole(function(outtxt) {
                                    $("#output").text(outtxt);
                   //            alert($("#output").text(outtxt).length);
                                    successFunction(outtxt);
                                });
                            });
                            //if R returns an error, alert the error message
                            req.fail(function() {
                                console.log("Server error: " + req.responseText);
                            });

                            //after request complete, re-enable the button 
                            req.always(function() {
                                $("#submitbutton").removeAttr("disabled");
                            });
                        });

                        function successFunction(data) {
                   //        alert(data + "data");
                            var allRows = data.split(/\r\n|\n/);
                   //        alert(allRows + "roe");
                            var table = '<table border="1" class="table table-striped table-bordered" id="tableReferences"  align="center">';
                            for (var singleRow = 1; singleRow < allRows.length; singleRow++) {
                                if (singleRow === 0) {
                                    table += '<thead>';
                                    table += '<tr>';
                                } else {
                                    table += '<tr>';
                                }
                   //            var a=allRows[singleRow].split(/\r?)|\n/);
                                var rowCells = allRows[singleRow].split(' ');
                   //            alert(rowCells);
                                for (var rowCell = 0; rowCell < rowCells.length; rowCell++) {
                                    if (singleRow === 0) {
                                        table += '<th>';
                                        table += rowCells[rowCell];
                                        table += '</th>';
                                    } else {
                                        if (singleRow === 1 && rowCell == 0) {
                                            rowCells[rowCell] = '0';
                                        }
                                        if (rowCells[rowCell] != '') {
                                            table += '<td>';
                                            table += rowCells[rowCell];
                                            table += '</td>';
                                        }
                                    }
                                }
                                if (singleRow === 0) {
                                    table += '</tr>';
                                    table += '</thead>';
                                    table += '<tbody>';
                                } else {
                                    table += '</tr>';
                                }
                            }
                            table += '</tbody>';
                            table += '</table>';
//                                                                                  table.cells[0].style.display="none";

                            $("#divTables").append(table);
                             $('td:nth-child(1)').hide();


                        }