// Activating the side bar.
var Change2Activejs = document.getElementById("sidebarIngresoMercancia");
Change2Activejs.className = "has-sub active";
var Changesub2Activejs = document.getElementById("sidebarIngresoMercancia-Ingreso");
Changesub2Activejs.className = "active";

// When one process is ckecked the other is unckecked
$('#ProcesoUnidades').click(function ()
{
    if ($('#ProcesoUnidades').prop('checked'))
    {
        $('#ProcesoSerial').prop('checked', false);
        $("#numbul").attr("style","display:true");
    }
});
$('#ProcesoSerial').click(function ()
{
    if ($('#ProcesoSerial').prop('checked'))
    {
        $('#ProcesoUnidades').prop('checked', false);
                $("#numbul").attr("style","display:none");

    }
});

// write uploaded file in database nationalization's table.
function saveDocUpload(file)
{
     var hiddenupfilesjs = document.getElementById('hiddenupfiles');
     hiddenupfilesjs.value = hiddenupfilesjs.value + ",["+file.name+","+file.size+"]";
}

// Consult if the file to load already exist in database.
function checkFile(file)
{
	var NITjs = $('#selectNITClient').val();
	if(NITjs)
	{
		var transportDocument = $('#numdoctte').val();
		if(transportDocument)
		{
			var getjsonjs = 'php/checkFile.php?fileName='+file.name+'&fileSize='+file.size;
			$.getJSON(getjsonjs, function(response) {
				/* called when request to archivo.php completes */
				if(response)
				{
					document.getElementById('cancel-upload-button '+file.name).click(); // se repite esta linea al final, por algún bug, no da click a cancel de forma inmediata.
					var headerMsg = "Solicitud rechazada.";
					var comentMsg = "El archivo con nombre: "+file.name+" ya existe en base de datos, cambiar el nombre si se trata de un archivo diferente.";
					alertWindow(headerMsg,comentMsg);
					// document.getElementById('cancel-upload-button '+file.name).click();
				}
			});
		}
		else
		{
			// Timeout to cancel the load.
			setTimeout(function() {
				var headerMsg = "Solicitud no realizada";
				var comentMsg = "Por favor ingrese primero el documento de transporte.";
				warningWindow(headerMsg,comentMsg);
				$('#cancel-all-upload').click();
			}, 500);
		}
	}
	else
	{
		// Timeout to cancel the load.
		setTimeout(function() {
			var headerMsg = "Solicitud no realizada";
			var comentMsg = "Por favor seleccione primero la información del cliente.";
			warningWindow(headerMsg,comentMsg);
			$('#cancel-all-upload').click();
		}, 500);
	}
}

// Bultos refence loaded
$('#myFile').change(function ()
{
    var x = document.getElementById("myFile");
    document.getElementById('alert-Header').innerHTML = "changed";
    document.getElementById('alert-comentary').innerHTML = x.value;
    document.getElementById('alert-trigger').click();
});

// Creating Bultos fields
function createBultosfields(objetoCheck)
{
    if (objetoCheck.checked)
    {
        reftable = document.getElementById('reftable');
        theadshow = document.getElementById('divbultos');
        /*var th1		= document.createElement('th');
         var th2		= document.createElement('th');
         thead	= document.createElement('thead');
         th1.innerText = "Referencia del bulto";
         th2.innerText = "Nombre de la referencia";
         thead.appendChild(th1);
         thead.appendChild(th2);
         reftable.appendChild(thead);
         */
        var refsize = 5;
        for (var n = 0; n < refsize; n++)
        {
            // creating HTML elements
            refbultostable = document.getElementById('refbultostable');
            //var refbultostable = document.createElement('tbody');
            var tr = document.createElement('tr');
            var td1 = document.createElement('td');
            // var td2		= document.createElement('td');
            var td3 = document.createElement('td');
            var refinput = document.createElement('input');
            // var descriptioninput	= document.createElement('input');
            var amountinput = document.createElement('input');

            // setting HTML attributes
            //td1.setAttribute('class','form-control');
            //td2.setAttribute('class','form-control');
            //td3.setAttribute('class','form-control');
            theadshow.setAttribute('style', 'display: true;');
            refinput.setAttribute('class', 'form-control');
            // descriptioninput.setAttribute('class','form-control');
            amountinput.setAttribute('class', 'form-control');
            refinput.setAttribute('type', 'text');
            // descriptioninput.setAttribute('type','text');
            amountinput.setAttribute('type', 'text');
            refinput.setAttribute('name', 'refbultosinput[]');
            // descriptioninput.setAttribute('name','description[]');
            amountinput.setAttribute('name', 'unidadesbultos[]');
            if (n == 0)
            {
                refinput.setAttribute('required', '');
                // descriptioninput.setAttribute('required','');
                amountinput.setAttribute('required', '');
            }
            // refinput.setAttribute('onkeydown','if (event.keyCode == 13) event.preventDefault() ');
            // descriptioninput.setAttribute('onkeydown','if (event.keyCode == 13) event.preventDefault() ');
            // amountinput.setAttribute('onkeydown','if (event.keyCode == 13) event.preventDefault() ');
            //refinput.setAttribute('data-parsley-type','nrequireddigits');
            //descriptioninput.setAttribute('data-parsley-type','nrequireddigits');
            amountinput.setAttribute('data-parsley-type', 'digits');
            refinput.setAttribute('placeholder', 'Referencia');
            // descriptioninput.setAttribute('placeholder','nombre referencia');
            amountinput.setAttribute('placeholder', 'cantidades');

            // appending childs
            td1.appendChild(refinput);
            // td2.appendChild(descriptioninput);
            td3.appendChild(amountinput);
            tr.appendChild(td1);
            // tr.appendChild(td2);
            tr.appendChild(td3);
            refbultostable.appendChild(tr);
            reftable.appendChild(refbultostable);
        }
    } else
    {
        // if formgroupjs exist delete it.
        if (typeof refbultostable !== 'undefined')
        {
            // deletting all childs
            while (refbultostable.firstChild) {
                refbultostable.removeChild(refbultostable.firstChild);
            }
        }
        theadshow.setAttribute('style', 'display: none;');
    }
}
