function callLoadBoxReferences()
{
    // Getting box's references
    var refBox = $("input[name=\'refbultosinput[]\']");
    var amountBox = $("input[name=\'unidadesbultos[]\']");
    var refBoxSize = refBox.length;

    // If box's references where fill manually
    if (refBox[0])
    {
        var references = Array();
        var amounts = Array();
        for (var j = 0; j < refBoxSize; j++) {
            if (refBox[j].value && amountBox[j].value)
            {
                references.push(refBox[j].value);
                amounts.push(amountBox[j].value);
            } else
            {
                if (refBox[j].value || amountBox[j].value)
                {
                    $("input[name=\'refbultosinput[]\']")[j].setAttribute('data-parsley-required', "true");
                    $("input[name=\'unidadesbultos[]\']")[j].setAttribute('data-parsley-required', "true");
                }
            }
        }
        // running validations
        var validationResult = $("#form-packages").parsley().validate();
        if (validationResult)
        {
            var endFor = references.length;
            var result = {
                DATA: Array(endFor)
            };
            var data = 'DATA';
            for (var i = 0; i < endFor; i++)
            {
                result[data][i] = Array(references[i], amounts[i]);
            }
            return result;
        } else
        {
            return false;
        }
    } else
    {
        // if box's references where attached

    }
}