


function userCanEdit(transportDocument)
{
    if(true)
    {
        // if edition inputType button not exists then create edit buttons
        if(!document.getElementById('inputTypeEdit'))
        {
            appendEditButton($('#inputTypeFormGroup'),'inputTypeEdit');
            appendEditButton($('#containerTypeFormGroup'),'containerTypeEdit');
            appendEditButton($('#storageTypeFormGroup'),'storageTypeEdit');
            appendEditButton($('#weight-form-group'),'weightEdit');
            appendEditButton($('#description-form-group'),'descriptionEdit');
        
            $('#inputTypeEdit').click(function(){enableInputCombobox($('#tipoingresobox'));});
            $('#containerTypeEdit').click(function(){enableInputCombobox($('#tipocontenedorbox'));});
            $('#storageTypeEdit').click(function(){enableInputCombobox($('#tipoalmacenamientobox'));});
            $('#weightEdit').click(function(){enableInput($('#weight'));});
            $('#descriptionEdit').click(function(){enableInput($('#descripcionMercancia'));});
            $('#totalpalets').click(function(){disEnableReadonly($('#totalpalets'));});
            $('#totalcajas').click(function(){disEnableReadonly($('#totalcajas'));});
            $('#totalunidades').click(function(){disEnableReadonly($('#totalunidades'));});
            $('#ProcesoUnidades').attr('disabled',false);
            $('#ProcesoSerial').attr('disabled',false);
        }
    }
}

// Enable object input
function enableInput(object) {
    object.prop('disabled',object.prop('disabled')?false:true);
}

function disEnableReadonly(object) {
    object.prop('readonly',object.prop('readonly')?false:true);
}

// Enable combobox input
function enableInputCombobox(object) 
{
    var isDisabled = object.data('combobox').disabled;
    (isDisabled ? object.data('combobox').enable() : object.data('combobox').disable());
}

/**
 * Appending eddit button to container
 * @param [object] : objContainer container of the button
 * @param [String] : buttonID button identifier
 */
function appendEditButton(objContainer,buttonID)
{
    objContainer.append(
        '<div class="col-md-1 col-sm-1" name="edit">'+
            '<a '+
                'class="btn  btn-icon btn-circle btn-lg" '+
                'id="'+buttonID+'" name="'+buttonID+'"'+
            '>'+
                '<i class="fa fa-pencil-square-o">'+
                    'Editar'+
                '</i>'+
            '</a>'+
        '</div>'
    );
}