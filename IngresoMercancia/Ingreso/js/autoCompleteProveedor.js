
var url = 'php/proveedorInfo.php';
$.getJSON(url, function(result) {
    if (result['DATA']) {

        loadSelects(result);
    }
}).fail(function(err) {
    loadSelects(JSON.parse(err.responseText));
});

// Loading NIT and enterprice name combobox
function loadSelects(result)
{
    var clientData = result['DATA'];
    var sizeofclientData = ObjectSizes(clientData);

    // Deleting combobox items.
    $('#selectNITProveedor').html('');
    $('#selectNITProveedor').data('combobox').refresh();

    $('#selectEProveedor').html('');
    $('#selectEProveedor').data('combobox').refresh();


    // adding new items to the combobox, importations numbers.
    // adding blank item.
    var blankNITj = '<option value=""></option>';
    $('#selectNITProveedor').append(blankNITj);
    $('#selectNITProveedor').data('combobox').refresh();
    var blankENamej = '<option value=""></option>';
    $('#selectEProveedor').append(blankENamej);
    $('#selectEProveedor').data('combobox').refresh();

    // for all importations items.
    for (var ijs = 0; ijs < sizeofclientData; ijs++)
    {
        var appendNITj = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][0] + '</option>';
        $('#selectNITProveedor').append(appendNITj);
        $('#selectNITProveedor').data('combobox').refresh();
        var appendENamej = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][1] + '</option>';
        $('#selectEProveedor').append(appendENamej);
        $('#selectEProveedor').data('combobox').refresh();

    }
}




/* combobox filled */

// this function calculates the size of an object.
function ObjectSizes(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;
// End function object size.

// ids and compare has to have the values at same level. ids[0] corresponds to compare[0]
var idss = [
    'selectNITProveedor',
    'selectEProveedor'
];
var compares = [
    'NIT',
    'NOMBRE_EMPRESA'

];

$("#selectNITProveedor").change(function() {
    var value = this.value;
    if (value)
        fillClientDatas(this);
    else
        clearClientSelect();
});
$("#selectEProveedor").change(function() {
    var value = this.value;
    if (value)
        fillClientDatas(this);
    else
        clearClientSelect();
});


function fillClientDatas(select)
{


    for (var j = 0; j < ObjectSizes(idss); j++)
    {
        $("#" + idss[j]).val(select.value);
        $("#" + idss[j]).data('combobox').refresh();
    }
}

function clearClientSelect()
{
    // Clearing all the data function from validateOnSave.js file
    clearDatas();
    for (var j = 0; j < ObjectSizes(idss); j++)
    {
        // Clearing combobox
        $("#" + idss[j]).data('combobox').clearTarget();
        $("#" + idss[j]).data('combobox').clearElement();
    }
}

function clearDatas()
{
    // Clearing combobox
    $("#selectEProveedor").data('combobox').clearTarget();
    $("#selectEProveedor").data('combobox').clearElement();
    $("#selectNITProveedor").data('combobox').clearTarget();
    $("#selectNITProveedor").data('combobox').clearElement();

}


