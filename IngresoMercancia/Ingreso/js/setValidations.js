/**
 * On this file there is the validations that
 * has each field on the index page.
 */

$('#selectNITClient').attr('data-parsley-required', true);
$('#selectEName').attr('data-parsley-required', true);
$('#selectContact').attr('data-parsley-required', true);
$('#selectTradeMark').attr('data-parsley-required', true);
// import's information
$("#numdoctte").attr('data-parsley-type', 'alphanum');
$("#numdoctte").attr('placeholder', "Documento de transporte");
$("#numdoctte").attr('data-parsley-required', "true");
//$("#numdoctte").attr('style',"text-transform:uppercase");
$("#numdoctte").attr('data-parsley-minlength', "6");
$("#numdoctte").attr('data-parsley-maxlength', "45");

$('#numimportacion').attr('data-parsley-required', true);
$('#numimportacion').attr('placeholder', "Número Importación");
$("#numimportacion").attr('data-parsley-minlength', "6");
$("#numimportacion").attr('data-parsley-maxlength', "45");
//$('#numimportacion').attr('style',"text-transform:uppercase");
$('#consecutivozf').attr('data-parsley-required', true);
$('#consecutivozf').attr('data-parsley-type', "digits");
$('#consecutivozf').attr('placeholder', "Consecutivo Ingreso");
$('#consecutivozf').attr('data-parsley-minlength', "2");
$('#consecutivozf').attr('data-parsley-maxlength', "15");

$('#tipoDoc').attr('data-parsley-required', true);



$('#nombrea').attr('data-parsley-required', true);
$('#nombrea').attr('data-parsley-type', "alphanum");
$('#nombrea').attr('placeholder', "Nombre del Archivo");
$('#nombrea').attr('data-parsley-minlength', "2");
$('#nombrea').attr('data-parsley-maxlength', "20");

$('#datepicker-default').attr('data-parsley-required', true);
$('#datepicker-default').attr('placeholder', "Fecha");
$('#datepicker-default').attr('data-parsley-minlength', "2");
$('#datepicker-default').attr('data-parsley-maxlength', "12");

$('#valorFOB').attr('data-parsley-required', true);
$('#valorFOB').attr('data-parsley-type', "pnumber");
$('#valorFOB').attr('placeholder', "Valor FOB ");
$('#valorFOB').attr('data-parsley-minlength', "2");
$('#valorFOB').attr('data-parsley-maxlength', "15");

$('#valorFOBTotal').attr('readonly', '');

$('#tasacambio').attr('data-parsley-required', true);
$('#tasacambio').attr('data-parsley-type', "pnumber");
$('#tasacambio').attr('placeholder', "Tasa de Cambio");
$('#tasacambio').attr('data-parsley-minlength', "2");
$('#tasacambio').attr('data-parsley-maxlength', "15");

$('#tipoingresobox').attr('data-parsley-required', "true");

$('#paisProcedencia').attr('data-parsley-required', "true");

$('#selectEProveedor').attr('data-parsley-required', "true");

$('#selectNITProveedor').attr('data-parsley-required', "true");

$('#bandera').attr('data-parsley-required', "true");

$('#paisOrigen').attr('data-parsley-required', "true");

$('#paisCompra').attr('data-parsley-required', "true");

//$('#Moneda').attr('data-parsley-required', "true")
$('#tipoDoc').attr('data-parsley-required', "true");
$('#tipoDoc').attr('placeholder', "Tipo Archivo");


$('#tipoa').attr('data-parsley-required', "true");
$('#tipoa').attr('placeholder', "Tipo Archivo");

$('#observacionDocs').attr('placeholder', "Observaciones");

$('#tipocontenedorbox').attr('data-parsley-required', "true");

$('#tipoalmacenamientobox').attr('data-parsley-required', "true");


$('#weight').attr('data-parsley-type', "pnumber");
$('#weight').attr('placeholder', "Peso(Kg)");
$('#weight').attr('data-parsley-maxlength', "7");

$('#descripcionMercancia').attr('placeholder', "Descripción");
$('#descripcionMercancia').attr('data-parsley-maxlength', "45");

$('#totalpalets').attr('data-parsley-type', "nrequireddigits");
$('#totalpalets').attr('placeholder', "Estibas");
$('#totalpalets').attr('data-parsley-required', "true");
$('#totalpalets').attr('data-parsley-maxlength', "9");

$('#totalcajas').attr('data-parsley-type', "nrequireddigits");
$('#totalcajas').attr('placeholder', "Bultos");
$('#totalcajas').attr('data-parsley-required', "true");
$('#totalcajas').attr('data-parsley-maxlength', "9");

//$("#totalunidades").attr('data-parsley-type', "nrequireddigits");
//$("#totalunidades").attr('placeholder', "Unidades");
//$("#totalunidades").attr('data-parsley-required', "true");
//$("#totalunidades").attr('data-parsley-maxlength', "9");

$('#weightNeto').attr('data-parsley-type', "pnumber");
$('#weightNeto').attr('placeholder', "Total Peso Neto");
$("#weightNeto").attr('data-parsley-maxlength', "7");

$('#manifiesto').attr('data-parsley-type', "pnumber");
$('#manifiesto').attr('placeholder', "Manifiesto");
$('#manifiesto').attr('data-parsley-minlength', "2");
$('#manifiesto').attr('data-parsley-maxlength', "45");

$('#seguros').attr('data-parsley-type', "pnumber");
$('#seguros').attr('placeholder', "Seguros");
$('#seguros').attr('data-parsley-minlength', "2");
$('#seguros').attr('data-parsley-maxlength', "45");

$('#fletes').attr('data-parsley-type', "pnumber");
$('#fletes').attr('placeholder', "Fletes");
$('#fletes').attr('data-parsley-minlength', "2");
$('#fletes').attr('data-parsley-maxlength', "45");

$('#otrosgastos').attr('data-parsley-type', "pnumber");
$('#otrosgastos').attr('placeholder', "Otros Gastos");
$('#otrosgastos').attr('data-parsley-minlength', "2");
$('#otrosgastos').attr('data-parsley-maxlength', "45");

$('#observaciones').attr('placeholder', "Escriba Observaciones");

//
//$('#chb2').attr('data-parsley-required', true);
//$('#chb1').attr('data-parsley-required', true);
//
//if ($('#chb1').prop('checked'))
//{
//    $('#chb2').attr('data-parsley-required', false);
//}
//if ($('#chb2').prop('checked'))
//{
//    $('#chb1').attr('data-parsley-required', false);
//}
