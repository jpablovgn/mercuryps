
$("#save").click(function() {
    // Validating forms
    var validateForms = false;
    var validateBox = $('#form-packages').parsley().validate();
    var validateClient = $("#formclient").parsley().validate();
    var validateImport = $('#demo-form-import').parsley().validate();
    var validateWareInfo = $('#demo-form-ware-info').parsley().validate();

    validateForms = !validateClient || !validateImport
            || !validateWareInfo || !validateBox;
    // if form validated
    if (!validateForms)
    {
        // Confirmation message
        var headerMsg = "GUARDAR";
        var comentMsg = "¿Está seguro que desea guardar los datos del cliente?";
        warningWindow(headerMsg, comentMsg, saveClientDataConfirm);
    }
});

function saveClientDataConfirm()
{
    var fileUploadPath = "../../../assets/plugins/jquery-file-upload/server/upload-files/";
    var NITjs = $('#selectNITClient').val();
    // Upper case the importation number.
    $('#numimportacion').val($('#numimportacion').val().toUpperCase());
    var importationjs = $('#numimportacion').val();
    var folderPath = fileUploadPath + NITjs + '/' + importationjs;
    var actionPath = "../../../assets/plugins/jquery-file-upload/server/upload-files/" + NITjs + '/' + importationjs + '/';
    $('#fileupload').attr('action', actionPath);
    var url = "php/copyBaseFolder.php?basePath=" + fileUploadPath + '&NIT=' + NITjs + '&importation=' + importationjs;

    $.getJSON(url, function(route) {
        if (route['ERROR'])
        {
            alertWindow(route['ERROR'][0], route['ERROR'][1]);
        } else
        {
            uploadFiles();
        }
    });
}

function uploadFiles() {
    // copy in index.php to upload the files.
    $('#TransportCopy').val($('#numdoctte').val());
    $('#transport-start-upload').click();
    var elements = document.getElementsByName('cancelIndividual');
    alert(elements);
    if (!ObjectSize(elements))
    {
        // if refbultosmanual checked load box's references
        saveClientData($('#refbultosmanual').prop('checked'));
    }
}

function saveClientData()
{
    // Loading global variable
    // global variable enable user to edit data
    var loadGlobalReload = globalReload;
    alert(loadGlobalReload);

    // creating variables
    var clientNit = $("#selectNITClient").val();
    var transportDoc = $("#numdoctte").val();
    var importation = $("#numimportacion").val();
    var consecutiveZF = $("#consecutivozf").val();
    var bills = $("#tagsinglefield").val();
    var CIFValue = $("#valorCIF").val();
    var FOGValue = $("#valorFOB").val();
    var inputType = $("#tipoingresobox").val();
    var containerType = $("#tipocontenedorbox").val();
    var storageType = $("#tipoalmacenamientobox").val();
    var totalWeight = $("#weight").val();
    var description = $("#descripcionMercancia").val();
    var totalPallets = $("#totalpalets").val();
    var totalBoxes = $("#totalcajas").val();
    var totalUnits = $("#totalunidades").val();
    var unitProcess = $("#ProcesoUnidades").val();
    var serialProcess = $("#ProcesoSerial").val();
    var observations = $("#observaciones").val();
    var referenceUnits = false;
    var referenceBox = false;
    alert($("#totalpalets").val());
    // Validating pallets
    if (!parseInt(totalPallets))
    {
        totalPallets = $("#totalpalets").val();
        alert(totalPallets);

    }
    // Validating boxes
    if (!parseInt(totalBoxes))
    {
        $("#totalcajas").val(1);
        totalBoxes = 1;
    }
    // Validating units
    if (!parseInt(totalUnits))
    {
        $("#totalunidades").val(1);
        totalUnits = 1;
    }
    // Type of process for the units.
    var unitsType = 0;
    if ($("#ProcesoUnidades").prop('checked'))
    {
        unitsType = unitProcess;
    }
    if ($("#ProcesoSerial").prop('checked'))
    {
        unitsType = serialProcess;
    }
    var data =
            {
//                updateData: loadGlobalReload,
                clientNit: clientNit,
                transportDoc: transportDoc,
                importation: importation,
                consecutiveZF: consecutiveZF,
                bills: bills,
                CIFValue: CIFValue,
                FOGValue: FOGValue,
                inputType: inputType,
                containerType: containerType,
                storageType: storageType,
                totalWeight: totalWeight,
                description: description,
                observations: observations
            };
    data = JSON.stringify(data);
    alert(data);
    var url = "php/save.php";
    $.ajax({
        type: "POST",
        url: url,
        async: true,
        cache: false,
        dataType: "json",
        data: {data: data},
        timeout: 50000, /* Timeout in ms */
        success: function(responce)
        { /* called when request to file.php completes */
            alert(responce);
            if (responce['ERROR'])
            {
                var headerMsg = responce['ERROR'][0];
                var comentMsg = responce['ERROR'][1];
                alertWindow(headerMsg, comentMsg);
            }
//            if (responce['SUCCESS'])
//            {
            // creating pallets
            var url = "php/savePallets.php?transportDoc=" + transportDoc + "&pallets=" + totalPallets;
            $.getJSON(url, function(result)
            {
                if (result['ERROR'])
                {
                    alertWindow(result['ERROR'][0], result['ERROR'][1]);
                    return false;
//                    } else
//                    {
                    // creating units or bults
                    if (type)
                    {
                        if (type === 'units')
                        {
                            if (unitsType)
                            {
                                referenceUnits = references;
                                var data =
                                        {
                                            transportDoc: transportDoc,
                                            unitsType: unitsType,
                                            reference: referenceUnits
                                        };
                                data = JSON.stringify(data);

                                var url = "php/saveUnitsReferences.php";
                                $.ajax({
                                    type: "POST",
                                    url: url,
                                    async: true,
                                    cache: false,
                                    dataType: "json",
                                    data: {data: data},
                                    timeout: 50000, /* Timeout in ms */

                                    success: function(responce)
                                    { /* called when request to file.php completes */
                                        alert(JSON.stringify(responce));
                                        alert(data);
                                        if (responce['ERROR'])
                                        {
                                            var headerMsg = responce['ERROR'][0];
                                            var comentMsg = responce['ERROR'][1];
                                            alertWindow(headerMsg, comentMsg);
                                            return false;
                                        }
                                        if (responce['AMOUNT'])
                                        {
                                            if ($('#totalunidades').val() != responce['AMOUNT'])
                                            {
                                                var headerMsg = "Cantidades diferentes";
                                                var comentMsg = "Las cantidades ingresadas del " +
                                                        "total de unidades, son diferentes a las " +
                                                        "cantidades totales del documento adjunto " +
                                                        "se cambiarán las de ingreso " +
                                                        "por las cantidades del documento.";
                                                warningWindow(headerMsg, comentMsg);
                                                $('#totalunidades').val(responce['AMOUNT']);
                                            }
                                        }
                                    }
                                });
                            } else
                            {
                                var headerMsg = "Error de proceso";
                                var comentMsg = "No se ha especificado ningún " +
                                        "proceso de la mercancía.";
                                alertWindow(headerMsg, comentMsg);
                                return false;
                            }
                        } else
                        {
                            if (type == 'box')
                            {
                                referenceBox = references;
                                var data =
                                        {
                                            transportDoc: transportDoc,
                                            reference: referenceBox
                                        };
                                data = JSON.stringify(data);
                            } else
                            {
                                // referenceBox = references;
                                referenceBox = callLoadBoxReferences();
                                if (!referenceBox)
                                {
                                    return false;
                                } else
                                {
                                    var data =
                                            {
                                                transportDoc: transportDoc,
                                                reference: referenceBox
                                            };
                                    data = JSON.stringify(data);
                                }
                            }
                            var url = "php/saveBoxReferences.php";
                            $.ajax({
                                type: "POST",
                                url: url,
                                async: true,
                                cache: false,
                                dataType: "json",
                                data: {data: data},
                                timeout: 50000, /* Timeout in ms */

                                success: function(responce)
                                { /* called when request to file.php completes */
                                    if (responce['ERROR'])
                                    {
                                        var headerMsg = responce['ERROR'][0];
                                        var comentMsg = responce['ERROR'][1];
                                        alertWindow(headerMsg, comentMsg);
                                        return false;
                                    }
                                    if (responce['AMOUNT'])
                                    {
                                        if ($('#totalcajas').val() != responce['AMOUNT'])
                                        {
                                            var headerMsg = "Cantidades diferentes";
                                            var comentMsg = "Las cantidades ingresadas del " +
                                                    "total de bultos, son diferentes a las " +
                                                    "cantidades totales del documento adjunto " +
                                                    "se cambiarán las de ingreso " +
                                                    "por las cantidades del documento.";
                                            warningWindow(headerMsg, comentMsg);
                                            $('#totalcajas').val(responce['AMOUNT']);
                                        }
                                    }
                                }
                            });
                        }
                    } else
                    {
                        // If type does not exists means that there is not
                        // attached references file.

                        // creating boxes
                        var data =
                                {
                                    transportDoc: transportDoc,
                                    reference: {
                                        DATA: {
                                            0: {
                                                0: '',
                                                1: totalBoxes
                                            }
                                        }
                                    }
                                };
                        data = JSON.stringify(data);
                        var url = 'php/saveBoxReferences.php';
                        $.ajax({
                            type: "POST",
                            url: url,
                            async: true,
                            cache: false,
                            dataType: "json",
                            data: {data: data},
                            timeout: 50000, /* Timeout in ms */

                            success: function(responce)
                            { /* called when request to file.php completes */
                                if (responce['ERROR'])
                                {
                                    alertWindow(responce['ERROR'][0], responce['ERROR'][1]);
                                    return false;
                                } else
                                {
                                    // creating units
                                    var data =
                                            {
                                                transportDoc: transportDoc,
                                                unitsType: unitsType,
                                                reference: {
                                                    DATA: {
                                                        0: {
                                                            0: '',
                                                            1: totalUnits
                                                        }
                                                    }
                                                }
                                            }
                                    data = JSON.stringify(data);
                                    var url = 'php/saveUnitsReferences.php?';
                                    $.ajax({
                                        type: "POST",
                                        url: url,
                                        async: true,
                                        cache: false,
                                        dataType: "json",
                                        data: {data: data},
                                        timeout: 50000, /* Timeout in ms */

                                        success: function(responce)
                                        { /* called when request to file.php completes */
                                            // alert(JSON.stringify(responce));
                                            if (responce['ERROR'])
                                            {
                                                alertWindow(responce['ERROR'][0], responce['ERROR'][1]);
                                                return false;
                                            } else
                                            {
                                                // successWindow(responce['AMOUNT'],responce['AMOUNT']);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            });
            var headerMsg = responce['SUCCESS'][0];
            var comentMsg = responce['SUCCESS'][1];
            successWindow(headerMsg, comentMsg);
        }
//        }
    });
}/**
 * This function clears all the frontend data.
 */
function clearData()
{
    // Clearing combobox
    $("#selectNITClient").data('combobox').clearTarget();
    $("#selectNITClient").data('combobox').clearElement();
    $("#selectEName").data('combobox').clearTarget();
    $("#selectEName").data('combobox').clearElement();
    $("#selectContact").data('combobox').clearTarget();
    $("#selectContact").data('combobox').clearElement();
    $("#selectTradeMark").data('combobox').clearTarget();
    $("#selectTradeMark").data('combobox').clearElement();
    // Clearing data
    $("#numdoctte").val("")
            .removeClass("parsley-success");
    $("#numimportacion").val("")
            .removeClass("parsley-success");
    $("#consecutivozf").val("")
            .removeClass("parsley-success");
    // Clearing bills
    $("#jquery-tagIt-primary").tagit('removeAll');
    // Clearing data
    $("#valorCIF").val("")
            .removeClass("parsley-success");
    $("#valorFOB").val("")
            .removeClass("parsley-success");
    // Clearing combobox
    $("#tipoingresobox").data('combobox').clearTarget();
    $("#tipoingresobox").data('combobox').clearElement();
    $("#tipocontenedorbox").data('combobox').clearTarget();
    $("#tipocontenedorbox").data('combobox').clearElement();
    $("#tipoalmacenamientobox").data('combobox').clearTarget();
    $("#tipoalmacenamientobox").data('combobox').clearElement();
    // Clearing data
    $("#weight").val("")
            .removeClass("parsley-success");
    $("#descripcionMercancia").val("")
            .removeClass("parsley-success");
    $("#totalpalets").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#totalcajas").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#totalunidades").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#ProcesoUnidades").val("")
            .removeClass("parsley-success");
    $("#ProcesoSerial").val("")
            .removeClass("parsley-success");
    $("#observaciones").val("")
            .removeClass("parsley-success");
    // Clearing checkbox
    $('#ProcesoUnidades').prop('checked', false)
            .attr('disabled', false);
    $('#ProcesoSerial').prop('checked', false)
            .attr('disabled', false);
    // Canceling upload files
    $('#cancel-all-upload').click();
    // Clearing references combobox
    if ($("#refbultosmanual").val())
    {
        if ($("#refbultosmanual").prop("checked"))
        {
            $("#refbultosmanual").prop("checked", false);
            createBultosfields($("refbultosmanual"));
        }
    }

    // Enabling fields
    $("#numimportacion").attr('disabled', false);
    $("#weight").attr('disabled', false);
    $("#descripcionMercancia").attr('disabled', false);
    // Enabling box manual edition checkbox
    $('#refbultosmanual').attr('disabled', false);
}

$('#cancelData').click(function()
{
    clearData();
});