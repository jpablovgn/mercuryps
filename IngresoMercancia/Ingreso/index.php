<link href="../../assets/plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
<link href="../../assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
<link href="../../assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?>		<script>
        window.location = "../../Home/exit.php";
    </script><?php
}
//
?> 
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://www.opencpu.org/js/archive/opencpu-0.4.js"></script>
<!--<link href=âhttp://hayageek.github.io/jQuery-Upload-File/uploadfile.min.cssâ rel=âstylesheetâ>-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!--<script src="http://hayageek.github.io/jQuery-Upload-File/jquery.uploadfile.min.js"></script>-->
<script type="text/javascript" src="../../assets/plugins/parsley/dist/parsley.js"></script>
<!--<link href="../../../assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="../../../assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />-->
<script src="http://code.jquery.com/jquery-latest.js"></script>

<script>
        var lvlrootjs = <?php print json_encode($lvlroot); ?>;
        /** Global variable true if 
         * transport's document has
         * data loaded from database.*/
        var globalReload = false;
</script>

<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Ingreso Mercancía</a></li>
    <li class="active">Recepción</li>
</ol>
<!-- end br
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Ingreso De Mercancía <small>Ingresar Datos De La Mercancía.</small></h1>
<!-- end page-header -->

<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">
        <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
            <!-- begin form -->
            <div class="panel panel-inverse ">
                <!-- begin panel Client info -->
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="ion-ios-people fa-lg"></i>
                        Información Del Cliente
                    </h4>
                </div>
                <!-- begin panel body -->


                <div class="panel-body panel-form">

                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>NIT (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectNITClient" id="selectNITClient" value="">
                                <option value="">Seleccione El NIT</option>
                            </select>
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Empresa (*) </label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectEName" id="selectEName" value="">
                                <option value="">Seleccione La Empresa</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Contacto (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectContact" id="selectContact" value="">
                                <option value="">Seleccione El Contacto</option>
                            </select>
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Marca (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectTradeMark" id="selectTradeMark" value="">
                                <option value="">Seleccione La Marca</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <!-- end panel body -->

            <!-- end panel client info -->
            <!-- begin panel Import's information -->
            <div class="panel panel-inverse ">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="ion-android-boat fa-lg"></i>
                        <i class="fa fa-plane fa-lg"></i>
                        Información De Importe
                    </h4>
                </div>
                <!-- begin body panel -->
                <div class="panel-body panel-form ">
                    <!-- begin form -->
                    <div class="form-group">
                        <label class="control-label col-md-2 text-left" for="message">Número Del Documento De Transporte  (*)</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="numdoctte" name="numdoctte" />
                            <input type="text" style="display:none"/>
                        </div>

                        <label class="control-label col-md-2 text-left">Número de Importación Del Cliente (*) </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="numimportacion" name="numimportacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2 text-left">Número De Consecutivo De Ingreso (*)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="consecutivozf" name="consecutivozf" />
                        </div>


                    </div>
                    <!--div class="form-group" style="text-align:center;">
                            <label class="col-md-12">Ingresar el valor CIF o el valor FOB (*)</label>
                    </div-->
                    <div class="form-group">

                        <label class="control-label col-md-2 text-left" for="message" >Tasa Cambio USD a COP (*)</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="tasacambio" name="tasaCambio" />
                        </div>

                        <label class="control-label col-md-2 text-left" for="message" >Valor FOB (*) </label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="valorFOB" name="valorFOB" />

                        </div>
                    </div>


                    <div class="panel-body panel-form">
                        <!-- begin form -->

                        <div class="form-group">
                            <div class="control-label col-md-2 col-sm-2 text-left">
                                <label>Proveedor (*)</label>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <select class="combobox" name="selectNITProveedor" id="selectNITProveedor" value="">
                                    <option value="">Seleccione El NIT</option>
                                </select>
                            </div>
                            <div class="control-label col-md-2 col-sm-2 text-left">
                                <label>Empresa (*)</label>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <select class="combobox" name="selectEProveedor" id="selectEProveedor" value="">
                                    <option value="">Seleccione La Empresa</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end body panel -->
            </div>
            <div class="panel panel-inverse ">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" onclick="noSubmit();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-list-ol fa-lg"></i>
                        <i class="fa fa-files-o fa-lg"></i>

                        Facturas de Importación
                    </h4>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2 text-left" for="message"  >Moneda Origen (*) </label>

                    <div class="col-md-4">
                        <select class="combobox" name="selectMoneda" id="Moneda" value="">
                            <option value="">Seleccione La Moneda</option>

                        </select>
                    </div> 
                    <label class="control-label col-md-2 text-left">Número De Factura De Importación (*)</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="facturaImp" name="consecutivozf" placeholder="Número de Factura"/>
                    </div>  

                </div>
                <div class="form-group" id="tableMoneda" align="center">

                    <div class="col-md-12">
                        <button type="button" id="add" class="btn btn-success m-r-5 m-b-5">
                            <i class="fa fa-plus"></i>
                            <span>Agregar Factura</span>
                        </button>
                        <button type="button" id="del" class="btn btn-danger m-r-5 m-b-5">
                            <i class="fa fa-minus"></i>
                            <span>Eliminar Factura</span>
                        </button>
                    </div>
                </div>
                <div class="form-group" >
                    <div class="panel-body">

                        <table id="tabla" class="table table-striped table-bordered" border="1" align="center">
                            <thead>
                                <tr>
                                    <th>Número Factura</th>
                                    <th>Moneda</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- end panel Import's information -->	
            <!-- begin panel Ware info -->
            <div class="panel panel-inverse ">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" onclick="noSubmit();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-globe fa-lg"></i>
                        <i class="fa fa-flag fa-lg"></i>

                        Información De La Nacionalidad de La Mercancía
                    </h4>
                </div>
                <div class="panel-body panel-form form-horizontal form-bordered">
                    <!-- begin form -->
                    <div class="form-group" id="storageTypeFormGroup">
                        <label class="control-label col-md-2 text-left">País De Procedencia (*)</label>
                        <div class="col-md-4">
                            <select class="combobox " name="paisProcedencia" id="paisProcedencia" value="">
                                <option value=""> Seleccione País</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 text-left">Bandera (*)</label>
                        <div class="col-md-4">
                            <select class="combobox " name="bandera" id="bandera" value="">
                                <option value=""> Seleccione Bandera</option>
                            </select>
                        </div>

                    </div>
                    <div class="form-group" id="storageTypeFormGroup">
                        <label class="control-label col-md-2 text-left">País De Origen</label>
                        <div class="col-md-4">
                            <select class="combobox " name="paisOrigen" id="paisOrigen" value="">
                                <option value=""> Seleccione País Origen</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 text-left">País De Compra (*)</label>
                        <div class="col-md-4">
                            <select class="combobox " name="paisCompra" id="paisCompra" value="">
                                <option value=""> Seleccione País Compra</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>

            <div class="panel panel-inverse ">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" onclick="noSubmit();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-cubes fa-lg"></i>
                        Información De La Mercancía
                    </h4>
                </div>
                <!-- begin panel body -->
                <div class="panel-body panel-form form-horizontal form-bordered">
                    <!-- begin form -->
                    <div class="form-group" id="inputTypeFormGroup">
                        <label class="control-label col-md-2 text-left">Tipo De Operación (*)</label>
                        <div class="col-md-4">
                            <select class="combobox" name="tipoingresobox" id="tipoingresobox" value="">
                                <option value=""> Seleccione Un Ingreso</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 text-left">Tipo De Contenedor (*)</label>
                        <div class="col-md-4">
                            <select class="combobox" name="tipocontenedorbox" id="tipocontenedorbox" value="">
                                <option value="">Seleccione Contenedor</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="storageTypeFormGroup">
                        <label class="control-label col-md-2 text-left">Tipo De Almacenamiento (*)</label>
                        <div class="col-md-4">
                            <select class="combobox " name="tipoalmacenamientobox" id="tipoalmacenamientobox" value="">
                                <option value=""> Seleccione Almacenamiento</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Descripción De La Mercancía (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="descripcionMercancia" name="descripcionMercancia"/>
                        </div>

                    </div>
                    <div class="form-group" id="description-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Peso Neto De La Importación (*) </label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="weightNeto" name="weight" />
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Peso Total De La Importación (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="weight" name="weight" />
                        </div>
                    </div>

                    <div class="form-group" id="description-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Manifiesto (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="manifiesto" name="manifiesto" />
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Seguros (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="seguros" name="seguro" />
                        </div>
                    </div>
                    <div class="form-group" id="description-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Fletes (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="fletes" name="manifiesto" />
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Otros Gastos (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="otrosgastos" name="seguro" />
                        </div>
                    </div>

                    <!--                    <div class="form-group">
                                             begin table responsive 
                                            <div class="col-md-4 col-sm-4">
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <th>Número Total De Estibas (*)</th>
                                                    <th>Número Total De Bultos (*)</th>
                                                    <th>Número Total De Unidades (*)</th>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <input class="form-control" type="text" id="totalpalets" name="totalpalets"/>
                                                            </td>
                                                            <td>
                                                                <input class="form-control" type="text" id="totalcajas" name="totalcajas"/>
                                                            </td>
                                                            <td>
                                                                <input class="form-control" type="text" id="totalunidades" name="totalunidades"/>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                             end table responsive 
                                            </div>
                                        </div>-->
                    <div class="form-group" id="description-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Procesos De La Mercancía (*)</label>
                        <div class="col-md-2 col-sm-2">
                            <div class="checkbox-inline">
                                <label>
                                    <input type="checkbox" value="" id="chb1" name="ProcesoUnidades" style="display:none;"/>
                                    <input type="checkbox" name="optionHidePassword" value='-1' id="chb1" data-render="switchery" data-theme="success" data-change="check-switchery-state-text"  />

                                    Proceso Por Unidades
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <div class="checkbox-inline">
                                <label>
                                    <input type="checkbox" value="" id="chb2" name="ProcesoSerial" style="display:none;"/>
                                    <input type="checkbox" name="optionHidePassword" value='-1' id="chb2"  data-render="switchery" data-theme="success" data-change="check-switchery-state-text"  />

                                    Proceso Por Bultos
                                </label>
                            </div>                       
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Observaciones Generales:</label>
                        <div class="col-md-4 col-sm-4">
                            <textarea id="observaciones" name="observaciones" rows="4" class="form-control" ></textarea>
                        </div>
                    </div>


                    <div class="form-group" id="palet-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Número Total De Estibas (*) </label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="totalpalets" name="totalpalets"/>
                        </div>

                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Número Total De Bultos (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="totalcajas" name="totalpalets"/>
                        </div>
                    </div>


                    <!-- end form -->		
                </div>
            </div>
        </form>
    </div>
</div>
<!-- end panel ware info -->
<!-- begin panel Transport's information -->
<!--<div class="row">
     begin main column 
    <div class="col-md-12">-->
<form data-parsley-validate="true" id="formDocs" name="formclient" class="form-horizontal form-bordered" method="post" action="">

    <div class="panel panel-inverse ">

        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">
                <i class="fa fa-paperclip fa-lg"></i>
                Documentos Adjuntos, Registros y Referencias De Mercancía
            </h4>
        </div>
        <!-- begin panel-body -->
        <div class="panel-body panel-form form-horizontal form-bordered">
            <div class="form-group" id="palet-form-group">
                <label class="control-label col-md-2 col-sm-2 text-left" >Tipo De Documento </label>
                <div class="col-md-4 col-sm-4">
                    <select class="combobox " name="tipoDoc" id="tipoDoc"  placeholder="Tipo Documento" value="">
                        <option value="">Seleccione el Documento</option>
                    </select>                        
                </div>
                <label class="control-label col-md-2 col-sm-2 text-left" >Nombre Documento </label>
                <div class="col-md-4 col-sm-4">
                    <input type="text" class="form-control" id="nombrea" name="nombrea" />
                </div>
            </div>
            <div class="form-group" id="palet-form-group">

                <label class="control-label col-md-2 col-sm-2 text-left">Fecha</label>
                <div class="col-md-3">
                    <input type="text" class="form-control" id="datepicker-default" placeholder="Seleccione Fecha" value="" />
                </div>
                <div class="col-md-1">
                    <button type="button" id="btnaddDocs" class="btn btn-primary save">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                <label class="control-label col-md-2 col-sm-2 text-left" >Observaciones </label>
                <div class="col-md-4 col-sm-4">
                    <input type="text" class="form-control" id="observacionDocs" name="comentarios" />
                </div>
            </div>
            <div class="form-group" id="tableDocs"></div>
        </div>



    </div>
</form>
<form data-parsley-validate="true" id="formDocs" name="formclient" class="form-horizontal form-bordered" method="" action="">

    <div class="panel panel-inverse ">

        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">
                <i class="fa fa-paperclip fa-lg"></i>
                Referencias De Los Bultos y Unidades De La Mercancía           </h4>
        </div>
        <div class="form-group" >
            <div class="col-md-6 col-sm-6">
                <label class="control-label col-md-12 text-right" style="font-weight: 100;" for="message">
                    <li> Referencias De Los Bultos en formato csv separados por punto y coma, con nombre <b> (refbultos.csv). </b></li>

                </label>

            </div>
            <div class="col-md-6 col-sm-6" >
                <button id="downloadExampleBoxes" type="button" class="btn btn-white m-r-5 right">
                    <i class="fa fa-download">
                        Descargar formato de referencias de bultos 
                    </i>
                </button>
            </div>


        </div>
        <div class="form-group" >
            <div class="col-md-6 col-sm-6">
                <label class="control-label col-md-12 text-right" style="font-weight: 100;" for="message">
                    <li> Referencias De Las Unidades en formato csv separados por punto y coma, con nombre <b> (refunidades.csv). </b></li>

                </label>

            </div>
            <div class="col-md-6 col-sm-6">

                <button id="downloadExampleUnits" type="button" class="btn btn-white m-r-5" >
                    <i class="fa fa-download">
                        Descargar formato de referencias de unidades
                    </i>
                </button>        
            </div>

        </div>


        <div class="form-group">
            <div class="col-md-9 col-sm-9">


                <input name="TransportCopy" id="TransportCopy" type="text" style="display:none"/>
                <div class="row fileupload-buttonbar">
                    <div class="col-md-7">
                        <span class="btn btn-success fileinput-button">
                            <i class="fa fa-plus"></i>
                            <span>Agregar Archivos...</span>
                            <input type="file" id="csvfile" >
                        </span>
                        <button id="submitbutton" type="button" class="btn btn-primary start" style="display:true;" read.csv >
                            <i class="fa fa-upload"></i> 
                            <span>Abrir Archivo</span>
                        </button>
                        <button id="saveRefe" type="button" class="btn btn-success start" style="display:none;">
                            <i class="fa fa-save"></i> 
                            <span>Guardar Referencias</span>
                        </button>
                        <pre style="display:none;"><code id="output" style="display:none;"></code></pre>
                                    </div>
                                
                                </div>
                        </div>
                    </div>
        <div class="form-group">
    <div id="divTables" ></div>
        </div>
        <div class="form-group">
            <div class="col-md-12 col-sm-12 text-right">
                <div class="row fileupload-buttonbar">


                    <button type="button" id="btnsaveData" name="save" class="btn btn-primary" >
                        <i class="fa fa-floppy-o ">
                            Guardar
                        </i>
                    </button>

                    <button type="button" id="btnupdate" name="save" class="btn btn-primary" >
                        <i class="fa fa-floppy-o ">
                            Actualizar
                        </i>
                    </button>
                    <button id="cancelData" name="cancelData" class="btn btn-warning ">
                        <i class="fa fa-ban">
                            Cancelar
                        </i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</form>

      
  

<div class="form-group"style="display:none;">
    <div class="col-md-12 col-sm-12">
        <div class="checkbox-inline">
            <label>
                <input type="checkbox" value="BULTOS" id="refbultosmanual" name="refbultosmanual" onClick="createBultosfields(this);"/>
                Ingresar Referencia De Bulto Manualmente.
            </label>
        </div>


        <div class="form-group" id="divbultos" style="display: true;">
            <!-- begin table responsive -->
            <div class="table-responsive">
                <table class="table table-bordered" id="reftable">
                    <thead>
                    <th>Referencia Del Bulto</th>
                    <th>Cantidades</th>
                    </thead>
                    <tbody id="refbultostable">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

                <!--  end panel-body -->
            <!-- end panel Transport's information -->
            <!-- begin form-packages/cancel button -->

            <!-- end save/cancel button -->
        
        <!-- end main column -->

        <!-- Including alerts windows -->
<?php
include_once($lvlroot . "Body/AlertsWindows.php");
include_once($lvlroot . "Body/JsFoot.php");
include_once($lvlroot . "Body/EndPage.php");
?>
    -->
    <script>

</script>
<script type="text/javascript" src="js/tableFacturas.js"></script>

    <script type="text/javascript" src="js/addFilasTable.js"></script>
    <script type="text/javascript" src="js/loadTableReferences.js"></script>
    <script type="text/javascript" src="js/validationTableDocs.js"></script>
    <script type="text/javascript" src="js/save.js"></script>
    <script type="text/javascript" src="js/setValidations.js"></script>
    <script type="text/javascript" src="js/fillComboBox.js"></script>
    <script type="text/javascript" src="js/autoCompleteProveedor.js"></script>
    <script type="text/javascript" src="js/autocompleteHandler.js"></script>
    <script type="text/javascript" src="js/loadStoredData.js"></script>
    <script type="text/javascript" src="js/validateOnSave.js"></script>
    <script type="text/javascript" src="js/validationNsubmit.js"></script>
    <script type="text/javascript" src="js/loadBoxReferences.js"></script>
    <script type="text/javascript" src="js/validateOnLoad.js"></script>
    <script type="text/javascript" src="js/generateReferencesExample.js"></script>
    

    <script>


    var url = 'php/consecutivo.php';
    $.getJSON(url, function(result) {
//    $("#consecutivozf").val(result['DATA']);
//   alert(result['DATA']);
    });
    $(function() {
        $('#jquery-tagIt-primary').tagit({
            singleField: !0,
            singleFieldNode: $('#tagsinglefield')
        });
    });
    // Configuring fileupload
    $(function() {
        $('#fileupload').fileupload({
            acceptFileTypes: /(\.|\/)(gif|jpg|png|odt|docx|doc|pdf|csv)$/i
        });
    });


</script>
 

<?php
include_once("php/downloadTemplate.php");
?>
