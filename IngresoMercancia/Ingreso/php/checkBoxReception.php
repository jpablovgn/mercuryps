<?php
	/*<!--
	* This file check if the merchandice was received.
	-->*/
	include_once('../../../assets/php/PhpMySQL.php');
	
	// Loading variables.
	$transportDoc 	= $_GET['transportDoc'];
	
        $connection = new Database();
	// Accents from database to html.
	// $acentos = $connection->query("SET NAMES 'utf8'");
	if(!$connection->link)
        {
            $result['ERROR'][0] = "Error de conexión";
            $result['ERROR'][1] = "No se pudo conectar a la base de datos";
        }
    
        else
        {
            $queryConsultReceived = "CALL VALIDAR_RECEPCION('$transportDoc');";
            $queryConsultReceivedResult = $connection->query($queryConsultReceived);
        
            if($queryConsultReceivedResult)
                {
                    while($tmpResult = $connection->fetch_array($queryConsultReceivedResult))
                    {
                        $result['SUCCESS'] = $tmpResult[0];
                    }
                }
        
                else
                    {
                        $result['ERROR'][0] = "Error en la consulta";
                        $result['ERROR'][1] = "Error consultando en el sistema "
                            +"la mercancía recibida físicamente. ";
                    }
		$connection->close();
	}
	print json_encode($result);
?>