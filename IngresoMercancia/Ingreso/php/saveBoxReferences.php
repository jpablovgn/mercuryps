<?php

/* <!--
 * This file saves the box's references in database.
  --> */
include_once('../../../assets/php/PhpMySQL.php');

// Loading variables from post.
$data = json_decode($_POST['data'], true);
$transportDoc = $data['transportDoc'];
$reference = $data['reference'];
$endFor = sizeof($reference['DATA']);
$amount= $_GET['boxes'];

$connection = new Database();
// Accents from database to html.
$accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    
} else {
    
    foreach ($reference['DATA'] as $key => $value) {
        
        if ($value[1]) {
            $queryDeleteBox = "CALL ELIMINAR_BULTOS('$transportDoc');";
            $queryDeleteBoxResult = $connection - query($queryDeleteBox);
            
            if ($querySaveBoxResult) {
                $querySaveBox = "CALL CREAR_BULTOS('$transportDoc',$value[1],'$value[0]')";
                $querySaveBoxResult = $connection->query($querySaveBox);
                
                if ($querySaveBoxResult) {
                    $totalAmount += $value[1];
                    
                } else {
                    
                    $result['ERROR'][0] = "Error creando bultos";
                    $result['ERROR'][1] = "Error creando bultos en el sistema " .
                            "por favor intentelo nuevamente o comuníquese con el " .
                            "administrador del servidor.";
                }
                
            } else {
                
                $result['ERROR'][0] = "Error creando bultos";
                $result['ERROR'][1] = "No se pudo iniciar el proceso " .
                        "base para crear los bultos.";
            }
        }
    }
    $result['AMOUNT'] = $totalAmount;
    $connection->close();
}
print json_encode($result);
?>