<?php

// <!--
// 	* This file returns the operation's type.
// -->
include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();
$trans = $_GET['transportDoc'];
$cant = $_GET['pallets'];
// Acentos de base de datos a html.
$accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    $query = "CALL CREAR_ESTIBA('$trans','$cant');";
    $queryResult = $connection->query($query);

    if ($queryResult) {
        $result = true;
    } else {
        $result['ERROR'][0] = "Error de consulta";
        $result['ERROR'][1] = "No se pudo realizar la consulta.";
    }
    $connection->close();
}

print json_encode($result);
?>

