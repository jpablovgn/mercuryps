<?php

include_once('../../../assets/php/PhpMySQL.php');

// Loading variables.
$data = json_decode($_POST['data'], true);
$unitsType = $data['unitsType'];
$transportDoc = $data['transportDoc'];
$reference = $data['reference'];
$endFor = sizeof($reference['DATA']);
$connection = new Database();
// Accents from database to html.
$acentos = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    foreach ($reference['DATA'] as $key => $value) {
        if ($value[1]) {
            $querySaveBox = "CALL CREAR_UNIDADES('$transportDoc','$value[1]','$value[0]','$unitsType')";
            $querySaveBoxResult = $connection->query($querySaveBox);
            if ($querySaveBoxResult) {
                $totalAmount += $value[1];
            } else {
                $result['ERROR'][0] = "Error creando unidades";
                $result['ERROR'][1] = "Error creando unidades en el sistema " .
                        "por favor intentelo nuevamente o comuníquese con el " .
                        "administrador del servidor. " . $querySaveBox;
            }
        }
    }
    $result['AMOUNT'] = $totalAmount;
    $connection->close();
}
print json_encode($result);
?>