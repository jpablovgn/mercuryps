
<?php

include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();
$clientNit = $_GET['clientNit'];
$transportDoc = $_GET['transportDoc'];
$importation = $_GET['importation'];
$consecutiveZF = $_GET['consecutiveZF'];
$FOGValue = $_GET['FOGValue'];
$inputType = $_GET['inputType'];
$containerType = $_GET['containerType'];
$storageType = $_GET['storageType'];
$totalWeight = $_GET['totalWeight'];
$description = $_GET['description'];
$observations = $_GET['observations'];
$manifiesto = $_GET['manifiesto'];
$flete = $_GET['flete'];
$seguro = $_GET['seguro'];
$gastos = $_GET['gastos'];
$procedencia = $_GET['procedencia'];
$compra = $_GET['compra'];
$origen = $_GET['origen'];
$bandera = $_GET['bandera'];
$neto = $_GET['pesoneto'];
$tasacambio = $_GET['tasacambio'];
$proveedor = $_GET['proveedor'];


// Acentos de base de datos a html.
$accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    $query = "CALL INGRESAR_MERCANCIA('$clientNit','$transportDoc','$inputType','$containerType','$storageType','$importation','$observations',"
            . "'$totalWeight','$description','$consecutiveZF','$FOGValue','$manifiesto','$flete','$seguro','$gastos','$procedencia',"
            . "'$compra','$origen','$bandera','$neto','$tasacambio','$proveedor');";


    $queryResult = $connection->query($query);

    if ($queryResult) {
        $result = true;
    } else {
        $result['ERROR'][0] = "Error de consulta";
        $result['ERROR'][1] = "No se pudo realizar la consulta.".$query;
    }
    $connection->close();
}

print json_encode($result);
?>

