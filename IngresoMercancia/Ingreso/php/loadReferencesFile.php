<?php

$filePath = $_GET['filePath'];
$fileName = $_GET['fileName'];
$Headers = json_decode($_GET['header'], true);

$newFilePath = str_replace('/var/www/html/Mercury_wms/', $filePath);

$fieldseparator = ";";
$lineseparator = "\n";
$csvfile = $newFilePath . $fileName;
$ERR_ARRAY = array(
    "Archivo no encontrado: " . $fileName,
    "Error abriendo el archivo: " . $fileName,
    "El archivo se encuentra vacío: " . $fileName
);
if (!file_exists($csvfile)) {
    $result['ERROR'][0] = "No se pudo completar la acción";
    $result['ERROR'][1] = $ERR_ARRAY[0];
} else {
    $file = fopen($csvfile, "r");
    if (!$file) {
        $result['ERROR'][0] = "No se pudo completar la acción";
        $result['ERROR'][1] = $ERR_ARRAY[1];
    } else {
        $size = filesize($csvfile);
        if (!$size) {
            $result['ERROR'][0] = "No se pudo completar la acción";
            $result['ERROR'][1] = $ERR_ARRAY[2];
        } else {
            $csvcontent = fread($file, $size);
            fclose($file);

            foreach (split($lineseparator, $csvcontent) as $line) {
                if ($lines == 0) {
                    $line = trim($line, "\t");
                    $line = str_replace("\r", "", $line);
                    $line = str_replace(",", ";", $line);
                    // ************************************ //
                    // This line escapes the special character. remove it if entries are already escaped in the csv file
                    // ************************************ //
                    $line = str_replace("'", "\'", $line);
                    // ************************************ //

                    $linearray = explode($fieldseparator, $line);
                    //$linearray = explode(',', $linearray);
                    $lineSize = sizeof($linearray);
                    $result['HEADERS'] = $linearray;
                }
                if ($lines > 0) {
                    $line = trim($line, "\t");
                    $line = str_replace("\r", "", $line);
                    $line = str_replace(",", ";", $line);
                    // ************************************ //
                    // This line escapes the special character. remove it if entries are already escaped in the csv file
                    // ************************************ //
                    $line = str_replace("'", "\'", $line);
                    // ************************************ //

                    $linearray = explode($fieldseparator, $line);
                    $lengthValidation = sizeof($linearray);
                    if ($lengthValidation > $lineSize) {
                        $errorLinesArray[] = $lines++;
                    } else {
                        $result['DATA'][] = $linearray;
                    }
                }
                $lines++;
            }
            // validating headers
            $baseHeadersSize = sizeof($Headers);
            $loadedHeadersSize = sizeof($result['HEADERS']);
            if ($baseHeadersSize == $loadedHeadersSize) {
                for ($i = 0; $i < $baseHeadersSize; $i++) {
                    if ($Headers[$i] != $result['HEADERS'][$i]) {
                        $errorHeaders = true;
                    }
                }
            } else {
                $errorHeaders = true;
            }
            // Headers error
            if ($errorHeaders) {
                $result['ERROR'][0] = "Error de encabezados";
                $result['ERROR'][1] = "Los encabezados no están correctos,"
                        . " están en desorden o el archivo no se encuentra "
                        . "separado por punto y comas ';', se recomienda descargar "
                        . "el formato de referencias. \n "
                        . "¿Desea continuar sin guardar las referencias?";
            }
            // Rows error
            if ($errorLinesArray) {
                $result['ERROR'][0] = "Error de archivo";
                $messageBase = "El archivo tiene más columnas "
                        . "de las permitidas, en las filas: ";
                $errorLines = implode(",", $errorLinesArray);
                $result['ERROR'][1] = $messageBase . $errorLines . ".";
            }
        }
    }
    if (is_file($csvfile)) {
        unlink($csvfile);
    } // delete file
}
print json_encode($result);
?>