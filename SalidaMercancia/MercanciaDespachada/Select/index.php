<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
include_once($lvlroot . "Body/SideBar.php");
include_once($lvlroot . "Body/BeginPage.php");
// Including Begin Header.
//	include_once($lvlroot."Body/BeginPage.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
$name = filter_input(INPUT_GET, 'exitName');
$nit = filter_input(INPUT_GET, 'clientNIT');
$clientid = filter_input(INPUT_GET, 'clientID');
?>
<script type="text/javascript">
    var namejs = "<?php echo $name; ?>";
    var nitjs = "<?php echo $nit; ?>";
    var clientjs = "<?php echo $clientid; ?>";
</script>
<script>
    var idS = getUrlVars()['id'];
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
            vars[key] = value;
        });
        return vars;
    }
</script>
<?php $id = "<script> document.write(idS) </script>"; ?>
<!-- begin page-header -->
<h1 class="page-header">Mercancía despachada <small>Información de la mercancía despachada de la salida tal.</small></h1>
<!-- end page-header -->

<div class="col-md-12" id="panelupdate" style="display:true;">

    <div class="panel panel-inverse ">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">
                <i class="ion-android-boat fa-lg"></i>
                <i class="fa fa-plane fa-lg"></i>
                Información De Importe
            </h4>
        </div>
        <!-- begin body panel -->
        <div class="panel-body panel-form " >
            <!-- begin form -->
            <form data-parsley-validate="true" id="demo-form-import" name="demo-form-import" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="control-label col-md-2 text-left" for="message">Peso Salida (*)</label>
                    <div class="col-md-4">
                        <input class="form-control" type="text" id="peso" name="peso" />
                    </div>

                    <label class="control-label col-md-2 text-left">Consecutivo de Salida (*)</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="consecutivo" name="consecutivo" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12">

                        <button class="btn btn-primary start" id="saveinfo" onclick="updateData();" >
                            <i class="fa fa-update"></i>
                            <span>Actualizar Información</span>
                        </button>
                    </div>
                </div>


            </form>
            <!-- end form -->
        </div>
        <!-- end body panel -->
    </div>
</div>
<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">
        <!-- begin panel Client Info -->
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="ion-ios-people fa-lg"></i>
                    Cargar Imágenes
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form" id="panel-clientInfo-form">
                <div class="container">		
                    <div class="panel panel-primary">
                        <div class="panel-body">

                            <form name="form1" id="form1" method="post" action="php/guardarImagenes.php" enctype="multipart/form-data">

                                <h4 class="text-center">Cargar Imágenes</h4>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Imágenes</label>
                                    <div class="col-sm-8">
                                        <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple="">
                                    </div>
                                    <input type="hidden" name="id" value="<?php echo $id; ?>" />
                                    <button type="submit" class="btn btn-primary" id="addImagenes">
                                        <i class="fa fa-save"></i>
                                        <span>Subir Archivos</span>
                                    </button>                               
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end body panel -->
        </div>
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="ion-ios-people fa-lg"></i>
                    Imágenes
                </h4>
            </div>

            <!-- begin body panel -->
            <div class="panel-body panel-form" id="panel-clientInfo-form">
                <?php
                include_once('../../../assets/php/PhpMySQL.php');

                
               $db = new Database();
                $sql = "SELECT * FROM MR_SALIDA_MERCANCIA where ID_SALIDA_MERCANCIA='16'";
                $query = $db->query($sql);
               
                echo $datos['IMAGENES'];

                if ($datos = $db->fetch_array_assoc($query)) {
                    if ($datos['IMAGENES'] == "") {
                        ?>
                        <p>NO tiene archivos</p>
                    <?php } else { ?>
                        <iframe src="<?php echo $datos['IMAGENES']; ?>" height="400" width="800"></iframe>

                        <?php
                    }
                }
                ?>
            </div>
            <!-- end body panel -->
        </div>

    </div>
</div>

<script>
    //funcion que obtiene parametros de una direccion url
//$.urlParam = function (name) {
//    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
//    return results[1] || 0;
//};


    $('#peso').val(getUrlVars()["peso"]);
    $('#consecutivo').val(getUrlVars()["cons"]);

    function updateData() {
// history.back();
        var id = getUrlVars()["id"];
        var cons = $('#consecutivo').val();
        var peso = $('#peso').val();

        var url = 'php/updateInfo.php?peso=' + peso + '&cons=' + cons + '&id=' + id;
        $.getJSON(url, function(result)
        {
            var headerMsg = "Salida Mercancía"[0];
            var comentMsg = "Guardado Exitosamente"[1];
            warningWindow(headerMsg, comentMsg);
        });

    }
</script>

<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>

<!-- Including consultDispatch -->
<script type="text/javascript" src="js/infoMercancia.js"></script>

<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarSalidaMercancia");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarSalidaMercancia-MercanciaDespachada");
    Changesub2Activejs.className = "active";
</script>