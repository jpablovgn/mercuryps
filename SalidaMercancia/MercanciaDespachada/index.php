<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
include_once($lvlroot . "Body/SideBar.php");
include_once($lvlroot . "Body/BeginPage.php");
// Including Begin Header.
//	include_once($lvlroot."Body/BeginPage.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
$name = filter_input(INPUT_GET, 'exitName');
$nit = filter_input(INPUT_GET, 'clientNIT');
$clientid = filter_input(INPUT_GET, 'clientID');
?>
<script type="text/javascript">
    var namejs = "<?php echo $name; ?>";
    var nitjs = "<?php echo $nit; ?>";
    var clientjs = "<?php echo $clientid; ?>";
</script>


<!-- begin page-header -->
<h1 class="page-header">Mercancía despachada <small>Información de la mercancía despachada de la salida tal.</small></h1>
<!-- end page-header -->

<!-- begin column -->
<div class="col-md-12">
    <!-- begin panel Exit info -->
    <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">
                <i class="fa fa-truck fa-lg"></i>
                Mercancía despachada
            </h4>
        </div>
        <!-- begin body panel -->
        <div class="panel-body" id="panel-info-form">
            <!-- begin tab panel -->
            <div class="panel panel-inverse panel-with-tabs" data-sortable-id="ui-unlimited-tabs-1">
                <div class="panel-heading p-0">
                    <div class="panel-heading-btn m-r-10 m-t-10">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    </div>
                    <!-- begin nav-tabs -->
                    <div class="tab-overflow" id="dispatched">
                        <ul class="nav nav-tabs nav-tabs-inverse">
                            <li class="prev-button"><a href="javascript:;" data-click="prev-tab" class="text-success"><i class="fa fa-arrow-left"></i></a></li>
                            <li class="next-button"><a href="javascript:;" data-click="next-tab" class="text-success"><i class="fa fa-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- begin tab content -->
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="nav-tab-1">
                        <h3 class="m-t-10" id="navTab1"></h3>
                        <table id="displayresult" class="table table-striped table-bordered dataTable table-responsive">
                            <thead>
                                <tr  align="center" >
                                    <td>SALIDA</td>
                                    <td id="peso">PESO SALIDA</td>
                                    <td>VALOR SALIDA</td>
                                    <td>CONSECUTIVO SALIDA</td>
                                    <td>CLIENTE</td>
                                    <td>FECHA SALIDA</td>
                                    <td>MÁS INFORMACIÓN</td>    
                                </tr>

                            </thead>
                            <tbody align="center">
                            </tbody>
                        </table>
                    </div>
                </div>
              
            </div>
            <!-- end tab panel -->
        </div>
        <!-- end body panel -->
    </div>
    <!-- end panel Exit info -->
</div>

<!-- end column -->


<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>
<!-- Including consultDispatch -->
<script type="text/javascript" src="js/infoMercancia.js"></script>
<script type="text/javascript" src="js/updateInfo.js"></script>

<script type="text/javascript">
                            // Activating the side bar.
                            var Change2Activejs = document.getElementById("sidebarSalidaMercancia");
                            Change2Activejs.className = "has-sub active";
                            var Changesub2Activejs = document.getElementById("sidebarSalidaMercancia-MercanciaDespachada");
                            Changesub2Activejs.className = "active";
</script>