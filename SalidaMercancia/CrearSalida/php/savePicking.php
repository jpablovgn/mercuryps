<?php

include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();
$nombre = $_GET['nombre'];
// Acentos de base de datos a html.
$accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    $query = "UPDATE MR_PICKING SET ID_ESTADO_PICKING = 2 WHERE NOMBRE ='$nombre';";

    $queryResult = $connection->query($query);

    if ($queryResult) {
        $data = array();
        while ($tempData = $connection->fetch_array_assoc($queryResult)) {
            $data[] = $tempData;
        }
        foreach ($data as $row) {
            foreach ($row as $key => $value) {
                $temp[] = $value;
            }
            $result['DATA'][] = $temp;
            unset($temp);
        }
    } else {
        $result['ERROR'][0] = "Error de consulta";
        $result['ERROR'][1] = "No se pudo realizar la consulta.";
    }
    $connection->close();
}

print json_encode($result);
?>