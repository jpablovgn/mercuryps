<?php
// <!--
// 	* This file returns the container's type.
// -->

	include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();
        $transportDoc = $_GET['transportDoc'];
	// Acentos de base de datos a html.
	$acentos = $connection->query("SET NAMES 'utf8'");
	if(!$connection->link)
    {
        $result['ERROR'][0] = "Error de conexión";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
        $queryLoadData = "CALL CONSULTAR_INFORMACION_SALIDA('$transportDoc');";
		$queryLoadDataResult = $connection->query($queryLoadData);

		if($queryLoadDataResult)
		{
			while($tmpData = $connection->fetch_array_assoc($queryLoadDataResult))
            {
                $data[] = $tmpData;
            }
            foreach($data as $row)
            {
                foreach ($row as $key => $value) 
                {
                    $temp[] = $value;
                }
                $result['DATA'][] = $temp;
                unset($temp);
            }
		}
        $connection->close();
	}
	print json_encode($result);
?>