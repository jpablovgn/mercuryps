<?php
	/*<!--
	* This file check if the merchandice was received.
	-->*/
	include_once('../../../assets/php/PhpMySQL.php');
	
	// Loading variables.
	$transportDoc = $_GET['transportDoc'];
	
        $connection = new Database();
	// Accents from database to html.
	// $acentos = $connection->query("SET NAMES 'utf8'");
	if(!$connection->link)
        {
            $result['ERROR'][0] = "Error de conexión";
            $result['ERROR'][1] = "No se pudo conectar a la base de datos";
        }
   
        else
        {
            $query = "SELECT IF ((SELECT 1 FROM MR_SALIDA_MERCANCIA WHERE ID_TRANSPORTE ='$transportDoc' LIMIT 1),'true','false');";
      
    $queryResult = $connection->query($query);

    if ($queryResult) {
        $data = array();
        while ($tempData = $connection->fetch_array_assoc($queryResult)) {
            $data[] = $tempData;
        }
        foreach ($data as $row) {
            foreach ($row as $key => $value) {
                $temp[] = $value;
            }
            $result['DATA'][] = $temp;
            unset($temp);
        }
    } else {
        $result['ERROR'][0] = "Error de consulta";
        $result['ERROR'][1] = "No se pudo realizar la consulta.";
    }
    $connection->close();
}

print json_encode($result);
?>