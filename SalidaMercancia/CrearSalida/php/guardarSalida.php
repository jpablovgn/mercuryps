<?php

include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();
// Loading variables from post.
$nombresalida = $_GET['nombresalida'];
$peso = $_GET['peso'];
$clientNit = $_GET['clientNit'];
$transportDoc = $_GET['transportDoc'];
$importation = $_GET['importation'];
$consecutiveZF = $_GET['consecutiveZF'];
$FOGValue = $_GET['FOGValue'];
$inputType = $_GET['inputType'];
$containerType = $_GET['containerType'];
$storageType = $_GET['storageType'];
$totalWeight = $_GET['totalWeight'];
$description = $_GET['description'];
$observations = $_GET['observations'];
$manifiesto = $_GET['manifiesto'];
$flete = $_GET['flete'];
$seguro = $_GET['seguro'];
$gastos = $_GET['gastos'];
$destino = $_GET['destino'];
$bandera = $_GET['bandera'];
$neto = $_GET['pesoneto'];
$tasacambio = $_GET['tasacambio'];
$bultos = $_GET['bultos'];
$modalidad=$_GET['modalidad'];
$conductor=$_GET['conductor'];

// Accents from database to html.
// $accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    $querySave = "CALL SALIDA_MERCANCIA('$clientNit','$transportDoc','$inputType','$containerType','$storageType','$importation','$observations',"
            . "'$totalWeight','$description','$consecutiveZF','$FOGValue','$manifiesto','$flete','$seguro','$gastos','$destino',"
            . "'$bandera','$neto','$tasacambio','$nombresalida','$bultos','$modalidad','$conductor');";
    
    $querySavePalletsResult = $connection->query($querySave);

    if ($querySavePalletsResult) {
        $result = true;
    } else {
        $result['ERROR'][0] = "Error al guardar la salida";
        $result['ERROR'][1] = "Error creando salida en el sistema " .
                "por favor intentelo nuevamente o comuníquese con el " .
                "administrador del servidor. " . $querySave;
    }
    $connection->close();
}
print json_encode($result);
?>

