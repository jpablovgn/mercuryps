<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
// include_once($lvlroot."Body/BeginPage.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
?>

<!-- begin page-header -->
<h1 class="page-header">Ingresar conductor <small>Ingresar conductor nuevo o modificar existentes.</small></h1>
<!-- end page-header -->

<!-- begin column -->
<div class="col-md-6">
    <!-- begin panel Driver info -->
    <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
        <div class="panel-heading">
            <div class="panel-heading-btn">
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            </div>
            <h4 class="panel-title">
                <i class="fa fa-truck fa-lg"></i>
                Ingresar Transportador(es)
            </h4>
        </div>
        <!-- begin body panel -->
        <div class="panel-body" id="panel-info-form">
            <div class="panel-form form-horizontal form-bordered">
                <div class="form-group">
                    <form method="POST" action="ingresarConductor.php">
                        <table id="Batch-table" >
                            <thead>
                                <tr>
                                    <th>Nombre del transportista</th>
                                    <th>Cédula de ciudadanía</th>
                                    <th>Placa del vehículo</th>
                                    <th>Empresa transportadora</th>
                                </tr>
                            </thead>
                            <tbody id="drivers-table">

                            </tbody>
                        </table>
                        <!--<input type="submit" value="submit">-->

                    </form>
                </div>
            </div>
        </div>
        <!-- end body panel -->
    </div>
    <!-- end panel Driver info -->
</div>
<!-- end column -->	

<div class="form-group">
    <div class="col-md-2 col-sm-2">
        <button class="btn btn-primary" onclick="addRow();">
            Conductor nuevo
        </button>
        <button class="btn  btn-primary" onclick="saveConductor();">
            Guardar
        </button>
    </div>
</div>

<?php
@$quant_values = $_GET['table']; // array of values 
echo $quant_values;
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>

<!-- // Loading diverData handler. -->
<script type="text/javascript" src="../js/driverData.js"></script>