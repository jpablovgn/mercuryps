$("#btnUpdateSalida").click(function() {
    if ($("#formclient").parsley().validate())
    {
        // Confirmation message

        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea Actualizar la Mercancía?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'updateSalida()');
        document.getElementById('warning-trigger').click();
    }
});

function updateSalida(){
    var clientNit = $("#selectNITClient").val(), transportDoc = $("#transportDoc").val(), importation = $("#importation").val();
    var consecutiveZF = $("#consecutivozf").val(),  FOGValue = $("#valorFOB").val();
    var inputType = $("#tiposalidabox").val(), containerType = $("#tipocontenedorbox").val(), storageType = $("#tipoalmacenamientobox").val();
    var totalWeight = $("#weight").val(), description = $("#descripcionMercancia").val(), totalBoxes = $("#totalcajas").val(), nombresalida = $('#salidaCliente').val();
    var observations = $("#observaciones").val(), manifiesto = $("#manifiesto").val(), flete = $("#fletes").val(), seguro = $("#seguros").val();
    var gastos = $("#otrosgastos").val(), destino = $("#paisDestino").val(), neto = $("#weightNeto").val();
    var bandera = $("#bandera").val(), tasaC = $("#tasacambio").val(), modalidad = $("#modalidad").val();

    var url = 'php/actualizarSalida.php?clientNit=' + clientNit + '&transportDoc=' + transportDoc + '&inputType=' + inputType + '&containerType=' + containerType +
            '&storageType=' + storageType + '&importation=' + importation + '&observations=' + observations + '&totalWeight=' + totalWeight +
            '&description=' + description + '&consecutiveZF=' + consecutiveZF + '&FOGValue=' + FOGValue + '&manifiesto=' + manifiesto +
            '&flete=' + flete + '&seguro=' + seguro + '&gastos=' + gastos + '&destino=' + destino + '&bandera=' + bandera + '&pesoneto=' + neto +
            '&tasacambio=' + tasaC +  '&nombresalida=' + nombresalida +  '&bultos=' + totalBoxes + '&modalidad=' + modalidad;
    console.log(url);
    $.getJSON(url, function(result)
    {
        if (result['ERROR']) {
            var headerMsg = "Error";
            var comentMsg = "No Se Ha Podido Actualizar La Salida ";
            alertWindow(headerMsg, comentMsg);
        } else {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "La Salida de Mercancía Se Ha Actualizado";
            successWindow(headerMsg, comentMsg);
        
        }
    });

}