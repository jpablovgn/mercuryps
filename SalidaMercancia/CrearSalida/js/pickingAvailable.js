function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}

var nit = $('#selectNITClient').val();
var url = 'php/pickingAvailable.php?nit'+nit;

$.getJSON(url, function (result) {
    if (result['DATA']) {
        tableUnits(result);
    }
}).fail(function (err) {
    tableUnits(JSON.parse(err.responseText));
});
//let nit = document.getElementById("selectNITClient").value;
//
//	for(var j=0;j<ObjectSize(ids);j++)
//	{
//		$("#"+ids[j]).data('combobox').refresh();
//                
//    
// $.getJSON('php/pickingAvailable.php?nit='+ nit , function(result){
//		if(result['DATA'])
//		{ 
//                    
//                   tableUnits(result);
//                  
//            } else{
//                       EmptySelect();
//                       
//                }
//	}).fail(function(err) {
//            tableUnits(JSON.parse(err.responseText));
//          let json = err.responseText;
//          console.log(json);
//        })
//	}


function tableUnits(result)
{
//    $('#checktable').DataTable({
//
//        data: result['DATA'],
//        responsive: true,
//
//        "language": {
//            "sProcessing": "Procesando...",
//            "sLengthMenu": "Mostrar _MENU_ registros",
//            "sZeroRecords": "No se encontraron resultados",
//            "sEmptyTable": "Ningún dato disponible en esta tabla",
//            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
//            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
//            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
//            "sInfoPostFix": "",
//            "sSearch": "Buscar:",
//            "sUrl": "",
//            "sInfoThousands": ",",
//            "sLoadingRecords": "Cargando...",
//            "oPaginate": {
//                "sFirst": "Primero",
//                "sLast": "Último",
//                "sNext": "Siguiente",
//                "sPrevious": "Anterior"
//            },
//            "oAria": {
//                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
//                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
//            }
//        }
//
//
//    });

    $('#checktable').DataTable( {
         data: result['DATA'],
  responsive: true,
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
      columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    } );


}
function EmptySelect(){
    // Deleting combobox items.
    $('#selectNITClient').html('');
    $('#selectNITClient').data('combobox').refresh();

    // adding blank item.    var urlInput = 'php/importationsCbox.php?nit='+nit;

    var blankItemjs = '<option value=""></option>';
    $('#selectNITClient').append(blankItemjs);
    $('#selectNITClient').data('combobox').refresh();
}
