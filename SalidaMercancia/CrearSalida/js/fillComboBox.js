//fill the combobox of countries

var url = 'php/listPaises.php';
$.getJSON(url, function(result) {

    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#paisDestino').html('');
        $('#paisDestino').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#paisDestino').append(blankItemjs);
        $('#paisDestino').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#paisDestino').append(appendItemjs);
            $('#paisDestino').data('combobox').refresh();
        }
    }
});

var url = 'php/listPaises.php';
$.getJSON(url, function(result) {

    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#bandera').html('');
        $('#bandera').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#bandera').append(blankItemjs);
        $('#bandera').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#bandera').append(appendItemjs);
            $('#bandera').data('combobox').refresh();
        }
    }
});



var url = 'php/moneda.php';
$.getJSON(url, function(result) {

    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#Moneda').html('');
        $('#Moneda').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#Moneda').append(blankItemjs);
        $('#Moneda').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][1] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#Moneda').append(appendItemjs);
            $('#Moneda').data('combobox').refresh();
        }
    }
});

/* Filling selects combobox */

var urlInput ="../../assets/php/tiposalida.php";
$.getJSON(urlInput, function(result) {
    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#tiposalidabox').html('');
        $('#tiposalidabox').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#tiposalidabox').append(blankItemjs);
        $('#tiposalidabox').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#tiposalidabox').append(appendItemjs);
            $('#tiposalidabox').data('combobox').refresh();
        }
    }
});

var urlContainer = 'php/tipocontenedor.php';
$.getJSON(urlContainer, function(result) {
    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#tipocontenedorbox').html('');
        $('#tipocontenedorbox').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#tipocontenedorbox').append(blankItemjs);
        $('#tipocontenedorbox').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#tipocontenedorbox').append(appendItemjs);
            $('#tipocontenedorbox').data('combobox').refresh();
        }
    }
});

var urlStorage = 'php/tipoalmacenamiento.php';
$.getJSON(urlStorage, function(result) {
    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#tipoalmacenamientobox').html('');
        $('#tipoalmacenamientobox').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#tipoalmacenamientobox').append(blankItemjs);
        $('#tipoalmacenamientobox').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#tipoalmacenamientobox').append(appendItemjs);
            $('#tipoalmacenamientobox').data('combobox').refresh();
        }
    }
});
var url = 'php/tipoarchivo.php';
$.getJSON(url, function(result) {

    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#tipoDoc').html('');
        $('#tipoDoc').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#tipoDoc').append(blankItemjs);
        $('#tipoDoc').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#tipoDoc').append(appendItemjs);
            $('#tipoDoc').data('combobox').refresh();
        }
    }


});

var urlStorage = 'php/tipoalmacenamiento.php';
$.getJSON(urlStorage, function(result) {
    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#tipoalmacenamientobox').html('');
        $('#tipoalmacenamientobox').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#tipoalmacenamientobox').append(blankItemjs);
        $('#tipoalmacenamientobox').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#tipoalmacenamientobox').append(appendItemjs);
            $('#tipoalmacenamientobox').data('combobox').refresh();
        }
    }
});


var urlContainer = 'php/tipocontenedor.php';
$.getJSON(urlContainer, function(result) {
    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#tipocontenedorbox').html('');
        $('#tipocontenedorbox').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#tipocontenedorbox').append(blankItemjs);
        $('#tipocontenedorbox').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#tipocontenedorbox').append(appendItemjs);
            $('#tipocontenedorbox').data('combobox').refresh();
        }
    }
});

var urlC = 'php/documentoTransporte.php';
$.getJSON(urlC, function(result) {
    if (result['DATA'])
    {
        var storeTypeData = result['DATA'];
        var sizeofstoreTypeData = ObjectSize(storeTypeData);
        // Deleting combobox items.
        $('#transportDoc').html('');
        $('#transportDoc').data('combobox').refresh();

        // adding blank item.
        var blankItemjs = '<option value=""></option>';
        $('#transportDoc').append(blankItemjs);
        $('#transportDoc').data('combobox').refresh();
        // adding all items.
        for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
        {
            var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][1] + '</option>';
            $('#transportDoc').append(appendItemjs);
            $('#transportDoc').data('combobox').refresh();
        }
    }
});
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}

