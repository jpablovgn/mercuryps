$('#transportDoc').change(function()
{
    if ($('#transportDoc').val())
    {

        loadClientData($('#transportDoc').val());


    } else
    {
        // Clearing client data
        clearClientData();
        // Clearing output type data
        // Clearing table
        clearTable();
        // Hidding boxes's table panel
        $("#panel-boxes").attr("style", 'display:none;');
        // Hidding cancel-save buttons
        $("#cancel-save").attr("style", 'display:none;');
    }
});

function loadClientData(transportDoc)
{
    var url = 'php/clientInfo.php?transportDoc=' + transportDoc;

    $.getJSON(url, function(result)
    {
        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        } else
        {
            // loading client info
            $('#selectNITClient').val(result['DATA'][0][0]);
            $('#selectEName').val(result['DATA'][0][1]);
            $('#selectContact').val(result['DATA'][0][2]);
            $('#selectTradeMark').val(result['DATA'][0][3]);
            $('#importation').val(result['DATA'][0][4]);
            $('#selectNITProveedor').val(result['DATA'][0][5]);
            $('#selectEProveedor').val(result['DATA'][0][6]);


        }
    });
}
function clearClientData()
{
    // Cleaning client info
    $('#NITcliente').val('');
    $('#NombreCliente').val('');
    $('#ContactoCliente').val('');
    $('#MarcaComercial').val('');
    $('#selectNITProveedor').val('');
    $('#selectEProveedor').val('');
}