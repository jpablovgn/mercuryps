
$(document).ready(function() {
    /**
     * Funcion para añadir una nueva columna en la tabla
     */
    $("#add").click(function() {
        if  ($("#facturaImp").val()!='')
        {
              // Obtenemos el numero de filas (td) que tiene la primera columna
        // (tr) del id "tabla"
//        var tds = $("#tabla tr:first th").length;
//        // Obtenemos el total de columnas (tr) del id "tabla"
//        var trs = $("#tabla tr").length;
//        var nuevaFila = "<tr>";
//        for (var i = 0; i < tds; i++) {
            var fact = $("#facturaImp").val();
            var moneda = $("#Moneda").val();
//
//            // añadimos las columnas
//            nuevaFila += "<td>" + (fact) + "</td>";
//            nuevaFila += "<td>" + (moneda) + "</td>";
//            $('td:nth-child(3)').hide();
//
//
//        }
//        // Añadimos una columna con el numero total de columnas.
//        // Añadimos uno al total, ya que cuando cargamos los valores para la
//        // columna, todavia no esta añadida
//        nuevaFila += "</tr>";
//        $("#tabla").append(nuevaFila);
 var table = document.getElementById("tabla");
    var row = table.insertRow(1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    cell1.innerHTML = fact;
    cell2.innerHTML = moneda;
        $("#facturaImp").val('');
        $("#facturaImp").focus(); 
        }
        else{
                    var headerMsg = "Número Factura Vacío";
        var comentMsg = "Ingrese el número de factura y la moneda para continuar !!";
    warningWindow(headerMsg, comentMsg)
        }
     
    });

    /**
     * Funcion para eliminar la ultima columna de la tabla.
     * Si unicamente queda una columna, esta no sera eliminada
     */
    $("#del").click(function() {
        // Obtenemos el total de columnas (tr) del id "tabla"
        var trs = $("#tabla tr").length;
        if (trs > 1)
        {
            // Eliminamos la ultima columna
            $("#tabla tr:last").remove();
        }
    });
});
