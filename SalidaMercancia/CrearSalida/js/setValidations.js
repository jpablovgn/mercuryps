/**
 * On this file there is the validations that
 * has each field on the index page.
 */

$('#selectNITClient').attr('data-parsley-required', true);
$('#selectNITClient').attr('placeholder', "NIT ");
$('#selectEName').attr('placeholder', "Nombre Empresa");
$('#selectContact').attr('placeholder', "Contacto");
$('#selectTradeMark').attr('placeholder', "Marca");

// import's information
$("#transportDoc").attr('placeholder', "Documento de transporte");
$("#transportDoc").attr('data-parsley-required', "true");


$('#importation').attr('placeholder', "Número Importación");
$("#importation").attr('data-parsley-required', "true");


//$('#numimportacion').attr('style',"text-transform:uppercase");
$('#consecutivozf').attr('data-parsley-required', true);
$('#consecutivozf').attr('data-parsley-type', "digits");
$('#consecutivozf').attr('placeholder', "Consecutivo Salida");
$('#consecutivozf').attr('data-parsley-minlength', "2");
$('#consecutivozf').attr('data-parsley-maxlength', "15");

$('#tipoDoc').attr('data-parsley-required', true);


$('#nombrea').attr('data-parsley-required', true);
$('#nombrea').attr('placeholder', "Nombre del Archivo");
$('#nombrea').attr('data-parsley-minlength', "2");
$('#nombrea').attr('data-parsley-maxlength', "20");

$('#datepicker-default').attr('data-parsley-required', true);
$('#datepicker-default').attr('placeholder', "Fecha");
$('#datepicker-default').attr('data-parsley-minlength', "2");
$('#datepicker-default').attr('data-parsley-maxlength', "12");

$('#valorFOB').attr('data-parsley-required', true);
$('#valorFOB').attr('data-parsley-type', "pnumber");
$('#valorFOB').attr('placeholder', "Valor FOB ");
$('#valorFOB').attr('data-parsley-minlength', "2");
$('#valorFOB').attr('data-parsley-maxlength', "15");

$('#valorFOBTotal').attr('readonly', '');

$('#tasacambio').attr('data-parsley-required', true);
$('#tasacambio').attr('data-parsley-type', "pnumber");
$('#tasacambio').attr('placeholder', "Tasa de Cambio");
$('#tasacambio').attr('data-parsley-minlength', "2");
$('#tasacambio').attr('data-parsley-maxlength', "15");

$('#tiposalidabox').attr('data-parsley-required', "true");

$('#modalidad').attr('data-parsley-required', "true");

$('#paisDestino').attr('data-parsley-required', "true");

$('#selectEProveedor').attr('data-parsley-required', "true");

$('#selectNITProveedor').attr('data-parsley-required', "true");

$('#bandera').attr('data-parsley-required', "true");

$("#dfrs").attr('data-parsley-required', "true");

//$('#Moneda').attr('data-parsley-required', "true");

$('#tipoa').attr('data-parsley-required', "true");
$('#tipoa').attr('placeholder', "Tipo Archivo");

$('#observacionDocs').attr('placeholder', "Observaciones");

$('#nameDriver').attr('data-parsley-required', "true");

$('#idDriver').attr('data-parsley-required', "true");

$('#salidaCliente').attr('data-parsley-required', "true");

$('#tipocontenedorbox').attr('data-parsley-required', "true");

$('#tipoalmacenamientobox').attr('data-parsley-required', "true");


$('#weight').attr('data-parsley-type', "pnumber");
$('#weight').attr('placeholder', "Peso(Kg)");
$('#weight').attr('data-parsley-maxlength', "7");

$('#descripcionMercancia').attr('placeholder', "Descripción");
$('#descripcionMercancia').attr('data-parsley-maxlength', "45");

$('#totalcajas').attr('data-parsley-type', "nrequireddigits");
$('#totalcajas').attr('placeholder', "Bultos");
$('#totalcajas').attr('data-parsley-required', "true");
$('#totalcajas').attr('data-parsley-maxlength', "9");

$("#totalunidades").attr('data-parsley-type', "nrequireddigits");
$("#totalunidades").attr('placeholder', "Unidades");
$("#totalunidades").attr('data-parsley-required', "true");
$("#totalunidades").attr('data-parsley-maxlength', "9");

$('#weightNeto').attr('data-parsley-type', "pnumber");
$('#weightNeto').attr('placeholder', "Total Peso Neto");
$("#weightNeto").attr('data-parsley-maxlength', "7");

$('#manifiesto').attr('data-parsley-type', "pnumber");
$('#manifiesto').attr('placeholder', "Manifiesto");
$('#manifiesto').attr('data-parsley-required', "true");
$('#manifiesto').attr('data-parsley-minlength', "2");
$('#manifiesto').attr('data-parsley-maxlength', "15");

$('#seguros').attr('data-parsley-type', "pnumber");
$('#seguros').attr('placeholder', "Seguros");
$('#seguros').attr('data-parsley-required', "true");
$('#seguros').attr('data-parsley-minlength', "2");
$('#seguros').attr('data-parsley-maxlength', "15");

$('#fletes').attr('data-parsley-type', "pnumber");
$('#fletes').attr('placeholder', "Fletes");
$('#fletes').attr('data-parsley-required', "true");
$('#fletes').attr('data-parsley-minlength', "2");
$('#fletes').attr('data-parsley-maxlength', "15");

$('#otrosgastos').attr('data-parsley-type', "pnumber");
$('#otrosgastos').attr('placeholder', "Otros Gastos");
$('#otrosgastos').attr('data-parsley-required', "true");
$('#otrosgastos').attr('data-parsley-minlength', "2");
$('#otrosgastos').attr('data-parsley-maxlength', "15");


$('#chb2').attr('data-parsley-required', true);
$('#chb1').attr('data-parsley-required', true);

if ($('#chb1').prop('checked'))
{
    $('#chb2').attr('data-parsley-required', none);
}
if ($('#chb2').prop('checked'))
{
    $('#chb1').attr('data-parsley-required', none);
}
;
