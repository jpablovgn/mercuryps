
function loadSavedData(transportDocument)
{

    // function at validateOnLoad.js file.
    var url = "php/checkBoxReception.php?transportDoc=" + transportDocument;
    $.getJSON(url, function(result) {

        if (result['DATA'] == 'true') {


            var headerMsg = "Salida ya creada";
            var comentMsg = "La mercancía ya ha sido " +
                    "seleccionada para despacho, solo se podrá " +
                    "modificar la información la mercancía";
            warningWindow(headerMsg, comentMsg);

            var url = "php/loadStoredData.php?transportDoc=" + transportDocument;

            $.getJSON(url, function(result) {
                if (result)
                {
                    if (result['ERROR'])
                    {
                        var headerMsg = result['ERROR'][0];
                        var comentMsg = result['ERROR'][1];
                        alertWindow(headerMsg, comentMsg);
                    }
                    if (result['DATA'])
                    {

                        $("#consecutivozf").val(result['DATA'][0][0]);     
                        $("#valorFOB").val(result['DATA'][0][1]);
                        $("#tiposalidabox").val(result['DATA'][0][2]);
                        $("#tiposalidabox").data('combobox').refresh();
                        $("#tipocontenedorbox").val(result['DATA'][0][3]);
                        $("#tipocontenedorbox").data('combobox').refresh();
                        $("#tipoalmacenamientobox").val(result['DATA'][0][4]);
                        $("#tipoalmacenamientobox").data('combobox').refresh();
                        $("#weight").val(result['DATA'][0][5]);
                        $("#descripcionMercancia").val(result['DATA'][0][6]);
                        $("#totalcajas").val(result['DATA'][0][7]);
                        $("#observaciones").val(result['DATA'][0][8]);
                        $("#weightNeto").val(result['DATA'][0][9]);
                        $("#manifiesto").val(result['DATA'][0][10]);
                        $("#fletes").val(result['DATA'][0][11]);
                        $("#seguros").val(result['DATA'][0][12]);
                        $("#otrosgastos").val(result['DATA'][0][13]);
                        $("#paisDestino").val(result['DATA'][0][14]);
                        $("#paisDestino").data('combobox').refresh();
                        $("#bandera").val(result['DATA'][0][15]);
                        $("#bandera").data('combobox').refresh();
                        $("#tasacambio").val(result['DATA'][0][16]);
                        $("#modalidad").val(result['DATA'][0][17]);
                        $("#salidaCliente").val(result['DATA'][0][18]);
                        $("#idDriver").val(result['DATA'][0][19]);
                        var impor = $("#transportDoc").val();
                        $("#tableDocs").load("php/tableDocs.php?imp=" + impor);
      
                    }
                }
                else
                {
                    var headerMsg = "No Hay Datos";
                    var comentMsg = "Documento disponible para crear Mercancía" +
                            warningWindow(headerMsg, comentMsg);
                    globalReload = false;
                    // Clearing data
                }
            });

        }
    });
}

$('#transportDoc').change(function()
{
    $('#transportDoc').val($('#transportDoc').val());

    loadSavedData(this.value);
});

function clearPartialData()
{
    $("#numimportacion").val("")
            .removeClass("parsley-success");
    $("#consecutivozf").val("")
            .removeClass("parsley-success");
    // Clearing bills
    $("#jquery-tagIt-primary").tagit('removeAll');
    // Clearing data
    $("#valorCIF").val("")
            .removeClass("parsley-success");
    $("#valorFOB").val("")
            .removeClass("parsley-success");
    // Clearing combobox
    $("#tipoingresobox").data('combobox').clearTarget();
    $("#tipoingresobox").data('combobox').clearElement();
    $("#tipocontenedorbox").data('combobox').clearTarget();
    $("#tipocontenedorbox").data('combobox').clearElement();
    $("#tipoalmacenamientobox").data('combobox').clearTarget();
    $("#tipoalmacenamientobox").data('combobox').clearElement();
    // Enabling combobox function from validateOnLoad.js
    $("#tipoingresobox").data('combobox').enable();
    $("#tipocontenedorbox").data('combobox').enable();
    $("#tipoalmacenamientobox").data('combobox').enable();
    // Clearing data
    $("#weight").val("")
            .removeClass("parsley-success");
    $("#descripcionMercancia").val("")
            .removeClass("parsley-success");
    $("#totalpalets").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#totalcajas").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#totalunidades").val("")
            .removeClass("parsley-success")
            .attr('disabled', false);
    $("#ProcesoUnidades").val("")
            .removeClass("parsley-success");
    $("#ProcesoSerial").val("")
            .removeClass("parsley-success");
    $("#observaciones").val("")
            .removeClass("parsley-success");
    // Clearing checkbox
    $('#ProcesoUnidades').prop('checked', false)
            .attr('disabled', false);
    $('#ProcesoSerial').prop('checked', false)
            .attr('disabled', false);
    // Canceling upload files
    $('#cancel-all-upload').click();
    // Clearing references combobox
    if ($("#refbultosmanual").val())
    {
        if ($("#refbultosmanual").prop("checked"))
        {
            $("#refbultosmanual").prop("checked", false);
            createBultosfields($("refbultosmanual"));
        }
    }

    // Enabling fields
    $("#numimportacion").attr('disabled', false);
    $("#weight").attr('disabled', false);
    $("#descripcionMercancia").attr('disabled', false);
    $('#refbultosmanual').attr('disabled', false);
}