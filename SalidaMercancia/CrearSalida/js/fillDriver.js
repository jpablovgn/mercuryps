
var url = 'php/driver.php';
$.getJSON(url, function (result) {
    if (result['DATA']) {

        loadSelec(result);
        
    }
}).fail(function (err) {
    loadSelec(JSON.parse(err.responseText));
});

// Loading NIT and enterprice name combobox
function loadSelec(result)
{
    var clientData = result['DATA'];
    var sizeofclientData = ObjectSize(clientData);
        
    // Deleting combobox items.
    $('#idDriver').html('');
    $('#idDriver').data('combobox').refresh();
    
    $('#nameDriver').html('');
    $('#nameDriver').data('combobox').refresh();
    

    var blankNITjs = '<option value=""></option>';
    $('#idDriver').append(blankNITjs);
    $('#idDriver').data('combobox').refresh();
    var blankENamejs = '<option value=""></option>';
    $('#nameDriver').append(blankENamejs);
    $('#nameDriver').data('combobox').refresh();
    for (var ijs = 0; ijs < sizeofclientData; ijs++)
    {
        var appendNITjs = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][2] + '</option>';
        $('#idDriver').append(appendNITjs);
        $('#idDriver').data('combobox').refresh();
        
        var appendENamejs = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][1] + '</option>';
        $('#nameDriver').append(appendENamejs);
        $('#nameDriver').data('combobox').refresh();
      
    }
}



/* combobox filled */

// this function calculates the size of an object.
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}

// End function object size.

// ids and compare has to have the values at same level. ids[0] corresponds to compare[0]
var idss = [
    'idDriver',
    'nameDriver'
];
var compare = [
    'CEDULA',
    'NOMBRE'
];

$("#idDriver").change(function () {
    var value = this.value;
    if (value)
        fillClientDataa(this);
    else
        clearClientSelectss();
});
$("#nameDriver").change(function () {
    var value = this.value;
    if (value)
        fillClientDataa(this);
    else
        clearClientSelectss();
});

function fillClientDataa(select)
{
    for (var j = 0; j < ObjectSize(idss); j++)
    {
        $("#" + idss[j]).val(select.value);
        $("#" + idss[j]).data('combobox').refresh();
    }
}

function clearClientSelectss()
{
    // Clearing all the data function from validateOnSave.js file
    clearDataa();
    for (var j = 0; j < ObjectSize(idss); j++)
    {
        // Clearing combobox
        $("#" + idss[j]).data('combobox').clearTarget();
        $("#" + idss[j]).data('combobox').clearElement();
    }
}

function clearDataa()
{
    // Clearing combobox
    $("#idDriver").data('combobox').clearTarget();
    $("#idDriver").data('combobox').clearElement();
    $("#nameDriver").data('combobox').clearTarget();
    $("#nameDriver").data('combobox').clearElement();

}

