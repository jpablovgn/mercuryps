function ReadDocs() {
    var impor = $("#transportDoc").val();
    $("#tableDocs").load("php/tableDocs.php?imp=" + impor);
}

function SaveDocs() {
    var name = $("#nombrea").val();
    var type = $("#tipoDoc").val();
    var date = $("#datepicker-default").val();
    var observation = $("#observacionDocs").val();
    var impor = $("#importation").val();
    var url = 'php/saveDocs.php?nombre=' + name + '&tipo=' + type + '&observation=' + observation + '&fecha=' + date + '&impo=' + impor;
    $.getJSON(url, function(result)
    {
                ReadDocs();

        if (result['ERROR']) {
            var headerMsg = "Ha Ocurrido Un Error";
            var comentMsg = "Verificar Datos e Intente Nuevamente";
            alertWindow(headerMsg, comentMsg);
        }
            if(result['DATA'])
        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Documentos Guardados Correctamente ";
            successWindow(headerMsg, comentMsg);
              
        }
    });
}
$("#btnaddDocs").click(function() {

    if ($("#formDocs").parsley().validate())
    {
        // Confirmation message
        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar los documentos?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'SaveDocs()');
        document.getElementById('warning-trigger').click();
    }
    ReadDocs();


});

