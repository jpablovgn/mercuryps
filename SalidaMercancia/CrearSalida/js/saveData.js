
$("#transportDoc").change(function() {
    Changevalue();
});



$("#btnSaveSalida").click(function() {
    if ($("#formclient").parsley().validate())
    {
        // Confirmation message

        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "Está seguro que desea guardar la Mercancía?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'saveSalida()');
        document.getElementById('warning-trigger').click();
    }
});


function Changevalue() {

    var nit = $("#transportDoc").val();
    $("#Loadtable").load("php/pickingAvailable.php?id=" + nit);
}

function checks()
{

    var i, vble;
    var elements = document.getElementsByClassName('chkbox');

    for (i = 0; i < elements.length; i++)
    {
        if (elements[i].checked)
        {
            var vble = $('#name' + i).html();

//            for (var i = 0; i < 3; i++) {
            var url = 'php/guardarPicking.php?nombre=' + vble;
            console.log(url);

            $.getJSON(url, function(result)
            {
                if (result) {

                    //$("#message").attr("style", 'display:true;');
                }

            });
            var urlp = 'php/guardarPickingSalida.php?nombre=' + vble;
            console.log(urlp);

            $.getJSON(urlp, function(result)
            {

            });
            var urls = 'php/savePicking.php?nombre=' + vble;
            console.log(urls);

            $.getJSON(urls, function(result)
            {
                if (result) {

                    //$("#message").attr("style", 'display:true;');
                }

            });
//            }
            var headerMsg = "Salida Mercancia";
            var comentMsg = "Guardado Exitosamente";
            successWindow(headerMsg, comentMsg);
            Changevalue(document.getElementById("selectNITClient").value);



        }
    }
}

function saveSalida() {

    var clientNit = $("#selectNITClient").val(), transportDoc = $("#transportDoc").val(), importation = $("#importation").val();
    var consecutiveZF = $("#consecutivozf").val(), FOGValue = $("#valorFOB").val();
    var inputType = $("#tiposalidabox").val(), containerType = $("#tipocontenedorbox").val(), storageType = $("#tipoalmacenamientobox").val();
    var totalWeight = $("#weight").val(), description = $("#descripcionMercancia").val(), totalBoxes = $("#totalcajas").val(), nombresalida = $('#salidaCliente').val();
    var observations = $("#observaciones").val(), manifiesto = $("#manifiesto").val(), flete = $("#fletes").val(), seguro = $("#seguros").val();
    var gastos = $("#otrosgastos").val(), destino = $("#paisDestino").val(), neto = $("#weightNeto").val();
    var bandera = $("#bandera").val(), tasaC = $("#tasacambio").val(), modalidad = $("#modalidad").val(), conductor = $('#idDriver').val();

    var url = 'php/guardarSalida.php?clientNit=' + clientNit + '&transportDoc=' + transportDoc + '&inputType=' + inputType + '&containerType=' + containerType +
            '&storageType=' + storageType + '&importation=' + importation + '&observations=' + observations + '&totalWeight=' + totalWeight +
            '&description=' + description + '&consecutiveZF=' + consecutiveZF + '&FOGValue=' + FOGValue + '&manifiesto=' + manifiesto +
            '&flete=' + flete + '&seguro=' + seguro + '&gastos=' + gastos + '&destino=' + destino + '&bandera=' + bandera + '&pesoneto=' + neto +
            '&tasacambio=' + tasaC + '&nombresalida=' + nombresalida + '&bultos=' + totalBoxes + '&modalidad=' + modalidad + '&conductor=' + conductor;
    console.log(url);
    $.getJSON(url, function(result)
    {
        if (result['ERROR']) {
            var headerMsg = "Error";
            var comentMsg = "No Se Ha Podido Guardar La Salida ";
            alertWindow(headerMsg, comentMsg);
        } else {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "La Salida de Mercancía Se Ha Guardado";
            successWindow(headerMsg, comentMsg);
            checks();
            Facturas();
            saveSalidaConductor();
        }
    });

}

function saveSalidaConductor() {
    var conductor = $('#idDriver').val();
    var url = 'php/guardarSalidaConductor.php?conductor=' + conductor;
    console.log(url);
    $.getJSON(url, function(result)
    {
        if (result) {


        }
    });

}

function Facturas() {

    var cantFila = $("#tabla tr").length;
    var l;
    for (l = 1; l < cantFila; l++)
    {
        var Transport = $("#transportDoc").val();
        var Fact = document.getElementById("tabla").rows[l].cells[0].innerText;
        var Moneda = document.getElementById("tabla").rows[l].cells[1].innerText;

        var url = 'php/saveFacturas.php?factura=' + Fact + '&moneda=' + Moneda + '&trans=' + Transport;
        console.log(url);
        $.getJSON(url, function(result)
        {
            if (result['ERROR']) {
                var headerMsg = "Error Al Guardar las facturas";
                var comentMsg = "Verificar Datos e Intente Nuevamente";
                alertWindow(headerMsg, comentMsg);
            }

        });
    }
//              
}
