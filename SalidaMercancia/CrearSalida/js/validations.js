//var driversGlobal = '';
//
//function availablePicks(clientid)
//{
//	$.getJSON('php/loadAvailablePicks.php?clientid='+clientid, function(data) {
//		/* called when request to archivo.php completes */
//		// if tablejs exist remove childs.
//		if(typeof tablejs !== 'undefined')
//		{
//			// deletting all childs
//			while (tablejs.firstChild) {
//				tablejs.removeChild(tablejs.firstChild);
//			}
//		}
//		if(data)
//		{
//			var pickingArray = Array();
//			if(typeof data['order'] !== 'undefined')
//			{
//				var orderSize = data['order'].length;
//				for(var ijs=0;ijs<orderSize;ijs++)
//				{
//					data['order'][ijs]['type'] = 'order';
//					pickingArray.push(data['order'][ijs]);
//				}
//			}
//			if(typeof data['batch'] !== 'undefined')
//			{
//				var batchSize = data['batch'].length;
//				for(var jjs=0;jjs<batchSize;jjs++)
//				{
//					data['batch'][jjs]['type'] = 'batch';
//					pickingArray.push(data['batch'][jjs]);
//				}
//			}
//			
//			tablejs	= document.getElementById('dispatchs');
//			// row color
//			var trcolor ={
//				0:'danger',
//				1:'success'
//			};
//			var endfor;
//			endfor = pickingArray.length;
//			// Creating references fields.
//			for(var kjs=0; kjs < endfor; kjs++)
//			{
//				// creating HTML elements
//				var tr 				= document.createElement('tr');
//				
//				var panelRefjs 		= document.getElementById('panel-select');
//				var tdCheckref		= document.createElement('td');
//				var checkrefjs		= document.createElement('input');
//				var referenceLoad	= document.createElement('td');
//				var exitType		= document.createElement('td');
//				
//				var dispatchName	= pickingArray[kjs][0];
//				var dispatchExitType= pickingArray[kjs][1];
//				var dispatchType	= pickingArray[kjs]['type'];
//				
//				panelRefjs.setAttribute('style','display:;');
//				checkrefjs.setAttribute('name','dispatch'+dispatchType+'[]');
//				checkrefjs.setAttribute('onclick','validateDispatch(this,'
//				+JSON.stringify(dispatchType)+
//				','+JSON.stringify(dispatchName)+','+JSON.stringify(dispatchExitType)+')');
//				checkrefjs.setAttribute('id',pickingArray[kjs][0]+'-check');
//				checkrefjs.setAttribute('type','checkbox');
//				referenceLoad.innerText = pickingArray[kjs][0];
//				exitType.setAttribute('id',pickingArray[kjs][0]+'exitType');
//				exitType.innerText = pickingArray[kjs][1];
//				
//				// appending childs
//				tdCheckref.appendChild(checkrefjs);
//				tr.appendChild(tdCheckref);
//				tr.appendChild(referenceLoad);
//				tr.appendChild(exitType);
//				tablejs.appendChild(tr);
//			};
//		}
//	});
//};

// check uncheck all checkbox
$('#checkAll').change(function()
{
    $("input:checkbox[name=\'dispatchorder[]\']").prop('checked', $(this).prop("checked"));
    $("input:checkbox[name=\'dispatchbatch[]\']").prop('checked', $(this).prop("checked"));
    validateAllDispatch();
});

// Prevent default submit.
$('#exit-form').submit(function(e)
{
    e.preventDefault();
});

function fillDriversData()
{
    var url = 'php/driverData.php';
    var width = '510';
    var height = '370';
    var windowSize = 'width=' + width + ',height=' + height;
    window.open(url, 'Datos del conductor', windowSize);
}
;

window.callback = function(result)
{
    driversGlobal = result;
    alert('callback: ' + JSON.stringify(result));
}

function verifyDispatch()
{
    //var exitName	= document.getElementById('salidaCliente');
    //var clientNIT	= document.getElementById('NITcliente');
//	var clientID	= document.getElementById('hiddenidclient');

//	if(exitName.value && clientNIT.value)
//	{
//		viewDispatched(exitName.value,clientNIT.value,clientID.value);
//	}
//	else
//	{
    document.getElementById('alert-Header').innerHTML = "No hay Mercancía Despacha con esta importación";
    document.getElementById('alert-comentary').innerHTML = "ingrese en la importación en la sección de Despachos";
    document.getElementById('alert-trigger').click();
//	}
}

function viewDispatched(name, nit, clid)
{
    var url = 'php/viewDispatch.php?exitName=' + name + '&clientNIT=' + nit + '&clientID=' + clid;
    var width = '800';
    var height = '430';
    var size = 'width=' + width + ',height=' + height;
    window.open(url, 'Mercancía despachada', size);
}

function validateAllDispatch()
{
    var totalOrderCheckBox = $("input:checkbox[name=\'dispatchorder[]\']");
    var totalBatchCheckBox = $("input:checkbox[name=\'dispatchbatch[]\']");
    var totalBoxesSize = Array();
    totalBoxesSize[0] = totalOrderCheckBox.length;
    totalBoxesSize[1] = totalBatchCheckBox.length;
    // validating availables references.
    for (var kjs = 0; kjs < totalBoxesSize[0]; kjs++)
    {
        // calling checkbox's onclick
        totalOrderCheckBox[kjs].onclick.apply(totalOrderCheckBox[kjs]);
    }
    for (var ljs = 0; ljs < totalBoxesSize[1]; ljs++)
    {
        // calling checkbox's onclick
        totalBatchCheckBox[ljs].onclick.apply(totalBatchCheckBox[ljs]);
    }
}

function validateDispatch(checkState, dispatchType, dispatchName, dispatchExitType)
{
    // Need to be nationalized the exits type between 401 and 435 less 408 and 409;
    if (checkState.checked)
    {
        var clientId = document.getElementById('hiddenidclient').value;
        var validateExitType = 400 < dispatchExitType && dispatchExitType < 436 && dispatchExitType != 408 && dispatchExitType != 409;
        if (validateExitType)
        {
            var validateURL = 'php/validateDispatch.php?dispatchName=' + dispatchName + '&dispatchType=' + dispatchType + '&clientId=' + clientId;
            $.getJSON(validateURL, function(validation)
            {
                alert(JSON.stringify(validation));
            });
        }
    }
}

/* fill TagIt
 if(factura)
 {
 var tagschilds = factura.split(',');
 
 for(var jk=0;jk<tagschilds.length;jk++)
 {
 $("#jquery-tagIt-primary").tagit("createTag", tagschilds[jk]);
 }
 }
 */