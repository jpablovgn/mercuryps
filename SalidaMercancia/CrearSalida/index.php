<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory

$lvlroot = "../../";
// Including Head.


include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
$vble2 = $_SESSION['NOMBREUSUARIO'];
include_once($lvlroot . "Body/BeginPage.php");

if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?>		<script>
        window.location = "../../Home/exit.php";
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
// function defined in js/autocomplete.js
$autoCompleteCall = "if (event.keyCode == 13) { autoCompleteOthers(this); event.preventDefault(); }";
?>
<!-- begin breadcrumb -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script> <!-- Integramos jQuery-->
<script type="text/javascript" src="../../assets/plugins/parsley/dist/parsley.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>


<meta charset="utf-8">
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Informes</a></li>
    <li class="active">Estado Mercancía</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Crear Salida <small>Crear salida de mercancía por selección de cliente.</small></h1>
<!-- end page-header -->
<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">
        <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">

            <!-- begin panel Client Info -->

            <!-- end panel Client Info -->
            <!-- end panel client info -->
            <!-- begin panel Import's information -->
            <div class="panel panel-inverse ">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="ion-android-boat fa-lg"></i>
                        <i class="fa fa-plane fa-lg"></i>
                        Información De Importe
                    </h4>
                </div>
                <!-- begin body panel -->
                <div class="panel-body panel-form ">
                    <!-- begin form -->
                    <div class="form-group">
                        <label class="control-label col-md-2 text-left" for="message">Número del documento de transporte (*)</label>
                        <div class="col-md-4">
                                <!--<input class="form-control" type="text" id="numdoctte" name="numdoctte" />  $autoCompleteDoctteCall -->
                            <select class="combobox" name="transportDoc" id="transportDoc" value=""  >
                                <option value=""> Seleccione un documento de transporte</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 text-left" for="message">Número de importación del cliente (*)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="importation" name="importation" readonly/>

                        </div>
                    </div>

                    <div class="form-group">

                        <label class="control-label col-md-2 text-left" for="message" >Tasa Cambio USD a COP (*)</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="tasacambio" name="tasaCambio" />
                        </div>

                        <label class="control-label col-md-2 text-left" for="message" >Valor FOB (*)</label>
                        <div class="col-md-4">
                            <input class="form-control" type="text" id="valorFOB" name="valorFOB" />

                        </div>
                    </div>
                    <div class="form-group" id="storageTypeFormGroup">
                        <label class="control-label col-md-2 text-left">País Destino (*)</label>
                        <div class="col-md-4">
                            <select class="combobox " name="paisProcedencia" id="paisDestino" value="">
                                <option value=""> Seleccione País</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 text-left">Bandera (*)</label>
                        <div class="col-md-4">
                            <select class="combobox " name="bandera" id="bandera" value="">
                                <option value=""> Seleccione Bandera</option>
                            </select>
                        </div>

                    </div>





                    <!-- end form -->
                </div>
                <!-- end body panel -->
            </div>

            <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="ion-ios-people fa-lg"></i>
                        Información del cliente
                    </h4>
                </div>
                <!-- begin body panel -->
                <div class="panel-body panel-form" id="panel-clientInfo-form">
                    <!-- begin form -->
                    <!-- begin form -->
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>NIT (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">

                            <input type="text" class="form-control" id="selectNITClient" name="NITcliente" readonly onchange="Changevalue();"/>

                            <!--OnChange="Changevalues($('#selectNITClient').val())-->
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Empresa (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">

                            <input type="text" class="form-control" id="selectEName" name="NombreCliente" readonly="" />

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Contacto (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input type="text" class="form-control" id="selectContact" name="ContactoCliente" readonly=""/>

                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Marca (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">

                            <input type="text" class="form-control" id="selectTradeMark" name="MarcaComercial" readonly=""/>

                        </div>
                    </div>
                    <!-- end form -->
                </div>
                <!-- end body panel -->
            </div>
            <div class="panel panel-inverse ">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" onclick="noSubmit();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-list-ol fa-lg"></i>
                        <i class="fa fa-files-o fa-lg"></i>

                        Facturas de Importación
                    </h4>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-2 text-left" for="message"  >Moneda Origen (*) </label>

                    <div class="col-md-4">
                        <select class="combobox" name="selectMoneda" id="Moneda" value="">
                            <option value="">Seleccione La Moneda</option>

                        </select>
                    </div> 
                    <label class="control-label col-md-2 text-left">Número De Factura De Importación (*)</label>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="facturaImp" name="consecutivozf" placeholder="Número de Factura"/>
                    </div>  

                </div>
                <div class="form-group" id="tableMoneda" align="center">

                    <div class="col-md-12">
                        <button type="button" id="add" class="btn btn-success m-r-5 m-b-5">
                            <i class="fa fa-plus"></i>
                            <span>Agregar Factura</span>
                        </button>
                        <button type="button" id="del" class="btn btn-danger m-r-5 m-b-5">
                            <i class="fa fa-minus"></i>
                            <span>Eliminar Factura</span>
                        </button>
                    </div>
                </div>
                <div class="form-group" >
                    <div class="panel-body">

                        <table id="tabla" class="table table-striped table-bordered" border="1" align="center">
                            <thead>
                                <tr>
                                    <th>Número Factura</th>
                                    <th>Moneda</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- begin panel Select-->
            <div class="panel panel-inverse " data-sortable-id="form-plugins-2">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa icon-basket fa-lg"></i>
                        Pickings disponibles
                    </h4>
                </div>
                <!-- begin body panel -->
                <div class="panel-body" id="panel-select">

                    <div class="form-group">
                    </div>
                    <div id="Loadtable" aling="center">

                    </div>


                    <div class="form-group">
                        <div class="col-md-1 col-sm-1">
                            <button class="btn btn-primary" type="button"  style="display:none;">

                                Guardar Selección
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        <!--div class="col-md-12 col-sm-12"-->
                        <button id="select-submit" type="submit" class="btn btn-primary btn-block" style="display:none;">
                            <i class="fa fa-sign-in"></i>
                            Guardar
                        </button>
                        <!--/div-->
                    </div>

                </div>
                <!-- end body panel -->
            </div>


            <div class="panel panel-inverse ">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" onclick="noSubmit();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-cubes fa-lg"></i>
                        Información De La Mercancía
                    </h4>
                </div>
                <!-- begin panel body -->
                <div class="panel-body panel-form form-horizontal form-bordered">
                    <!-- begin form -->
                    <div class="form-group" id="inputTypeFormGroup">
                        <label class="control-label col-md-2 text-left">Tipo De Salida (*)</label>
                        <div class="col-md-4">
                            <select class="combobox" name="tiposalidabox" id="tiposalidabox" value="">
                                <option value=""> Seleccione Una Sali$("#salidaCliente").val(result['DATA'][0][18]);da</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 text-left">Tipo De Contenedor (*)</label>
                        <div class="col-md-4">
                            <select class="combobox" name="tipocontenedorbox" id="tipocontenedorbox" value="">
                                <option value="">Seleccione Contenedor</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="storageTypeFormGroup">
                        <label class="control-label col-md-2 text-left">Tipo De Almacenamiento (*)</label>
                        <div class="col-md-4">
                            <select class="combobox " name="tipoalmacenamientobox" id="tipoalmacenamientobox" value="">
                                <option value=""> Seleccione Almacenamiento </option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 text-left">Modalidad (*)</label>
                        <div class="col-md-4">
                            <select class="combobox " name="tipoalmacenamientobox" id="modalidad" value="">
                                <option value=""> Seleccione Modalidad </option>
                                <option value="Reintegro"> Reintegro</option>
                                <option value="Sin Reintegro">Sin Reintegro</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group" id="description-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Peso Neto De La Salida (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="weightNeto" name="weight" />
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Peso Total De La Salida (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="weight" name="weight" />
                        </div>
                    </div>

                    <div class="form-group" id="description-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Manifiesto (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="manifiesto" name="manifiesto" />
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Seguros (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="seguros" name="seguro" />
                        </div>
                    </div>
                    <div class="form-group" id="description-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Fletes (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="fletes" name="manifiesto" />
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Otros Gastos (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="otrosgastos" name="seguro" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2 text-left">Número De Consecutivo De Salida (*)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="consecutivozf" name="consecutivozf" />

                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Descripción de La Mercancía (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="descripcionMercancia" name="descripcionMercancia"/>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end panel Select -->
            <!-- begin panel Exit info -->
            <div class="panel panel-inverse " data-sortable-id="form-plugins-3">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-info-circle fa-lg"></i>
                        Información de la salida
                    </h4>
                </div>
                <!-- begin body panel -->
                <div class="panel-body panel-form form-horizontal form-bordered" id="panel-info-form">
                    <div class="form-group">
                        <div class="col-md-4">
                            <label>Nombre de la salida (*)</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="salidaCliente" name="salidaCliente"  placeholder="Nombre de la salida" />
                        </div>

                    </div>
                    <div id="hello"></div>
                    <div class="form-group">
                        <!--                        <div class="col-md-4 col-sm-4">
                                                    <button class="btn btn-primary" onclick="fillDriversData();">
                                                        Ingresar o modificar conductor y placa
                                                    </button>
                                                </div>-->
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Cedula Conductor (*)</label>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectDriver" id="idDriver"  placeholder="Seleccione la Cedula del Conductor" >
                                <option value="">Seleccione el Conductor</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Nombre Conductor (*)</label>

                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectDriver" id="nameDriver"  placeholder="Seleccione el Nombre del Conductor" >
                                <option value="">Seleccione el Conductor</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Número Total De Bultos </label>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" id="totalcajas" name="totalpalets"/>
                        </div>

                        <label class="control-label col-md-2 col-sm-2 text-left" for="message">Observaciones Generales:</label>
                        <div class="col-md-4 col-sm-4">
                            <textarea id="observaciones" name="observaciones" rows="4" class="form-control" ></textarea>
                        </div>
                    </div>


                </div>
                <br>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 text-right">

                        <div class="row fileupload-buttonbar">

                            <button type="button" class="btn btn-primary" id="btnSaveSalida" >
                                <i class="fa fa-save"></i>
                                <span>Guardar</span>
                            </button>
                            <button type="button" class="btn btn-primary" id="btnUpdateSalida" >
                                <i class="fa fa-refresh"></i>
                                <span>Actualizar</span>
                            </button>

                            <button type="reset" class="btn btn-warning cancel">
                                <i class="fa fa-ban"></i>
                                <span>Cancelar</span>
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </form>  <!-- end body panel -->


        <form data-parsley-validate="true" id="formDocs" name="formclient" class="form-horizontal form-bordered" method="post" action="">

            <div class="panel panel-inverse ">

                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">
                        <i class="fa fa-paperclip fa-lg"></i>
                        Documentos Adjuntos, Registros y Referencias De Mercancía
                    </h4>
                </div>
                <!-- begin panel-body -->
                <div class="panel-body panel-form form-horizontal form-bordered">
                    <div class="form-group" id="palet-form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left" >Tipo De Documento </label>
                        <div class="col-md-4 col-sm-4">
                        
                               <select class="combobox" name="tipoDoc" id="tipoDoc"  placeholder="Seleccione el Tipo de documento" >
                                <option value="">Seleccione el Documento</option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" >Nombre Documento </label>
                        <div class="col-md-4 col-sm-4">
                            <input type="text" class="form-control" id="nombrea" name="nombrea" />
                        </div>
                    </div>
                    <div class="form-group" id="palet-form-group">

                        <label class="control-label col-md-2 col-sm-2 text-left">Fecha </label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" id="datepicker-default" placeholder="Seleccione Fecha" value="" />
                        </div>
                        <div class="col-md-1">
                            <button type="button" id="btnaddDocs" class="btn btn-primary save">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left" >Observaciones </label>
                        <div class="col-md-4 col-sm-4">
                            <input type="text" class="form-control" id="observacionDocs" name="comentarios" />
                        </div>

                    </div>


                    <div class="form-group" id="tableDocs"></div>
                </div>
            </div>
        </form>
        <!-- end row -->
        <!-- end row -->

        <?php
// Including Js actions, put in the end.
        include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
        include_once($lvlroot . "Body/EndPage.php");
//Including alerts windows -->
        include_once($lvlroot . "Body/AlertsWindows.php");
//include("php/checkdetected.php");
        ?>
        <script type="text/javascript" src="js/addFilasTable.js"></script>
        <script type="text/javascript" src="js/validations.js"></script>
        <script type="text/javascript" src="js/validationTableDocs.js"></script>

        <script type="text/javascript" src="js/autocompleteDeparture.js"></script>
        <script type="text/javascript" src="js/validationOnSave.js"></script>
        <script type="text/javascript" src="js/fillDriver.js"></script>
        <script type="text/javascript" src="js/saveData.js"></script>
        <script type="text/javascript" src="js/fillComboBox.js"></script>
        <script type="text/javascript" src="js/setValidations.js"></script>
        <script type="text/javascript" src="js/loadStoredData.js"></script>
        <script type="text/javascript" src="js/tableFacturas.js"></script>
        <script type="text/javascript" src="js/actualizarSalida.js"></script>





        <script type="text/javascript">
                            // Activating the side bar.
                            var Change2Activejs = document.getElementById("sidebarSalidaMercancia");
                            Change2Activejs.className = "has-sub active";
                            var Changesub2Activejs = document.getElementById("sidebarSalidaMercancia-CrearSalida");
                            Changesub2Activejs.className = "active";
        </script>



