/**	On this file there are some functions that
 * validates information pass by the user
 * */
/*
 $( "#enterpriceName" ).keydown(function( event ) {
 prevent(event);
 });*/

// NITclient maxlength
$("#nit").parsley();
$("#nit").attr('data-parsley-minlength', 7);
$("#nit").attr('data-parsley-maxlength', 10);

// enterpriceName maxlength
$("#nombreE").parsley();
$("#nombreE").attr('data-parsley-maxlength', 30);

// contact maxlength
$("#contacto").parsley();
$("#contacto").attr('data-parsley-maxlength', 30);

// tradeMark maxlength
$("#correo").parsley();
$("#correo").attr('data-parsley-maxlength', 30);

// telephone maxlength
$("#tel").parsley();
$("#tel").attr('data-parsley-maxlength', 20);

// email maxlength
$("#correo").parsley();
$("#correo").attr('data-parsley-maxlength', 45);

// address maxlength
$("#direccion").parsley();
$("#direccion").attr('data-parsley-maxlength', 30);

$("#saveData").click(function() {
    if ($("#FormCrearUsuario").parsley().validate())
    {
        // Confirmation message
        document.getElementById('warning-Header').innerHTML = "GUARDAR";
        document.getElementById('warning-comentary').innerText = "¿Está seguro que desea guardar los datos del Proveedor?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'saveData()');
        document.getElementById('warning-trigger').click();
    }
});


function saveData() {
    var globalData;

    var nit = $("#nit").val();
    var nombreE = $("#nombreE").val();
    var tel = $("#tel").val();
    var correo = $("#correo").val();
    var direccion = $("#direccion").val();
    var contact = $("#contacto").val();


    var url = 'php/registro.php?nit=' + nit + '&nombre=' + nombreE + '&tel=' + tel + '&correo=' + correo + '&direccion=' + direccion + '&contacto=' + contact;
    console.log(url);
    $.getJSON(url, function(responce) {
        if (responce['ERROR'])
        {
            alertWindow(responce['ERROR'][0], responce['ERROR'][1]);
        } else
        {

//            globalData = responce;
//            passData(responce['DATA']);
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Proveedor Ingresado Correctamente ";
            successWindow(headerMsg, comentMsg);
            clearClientData();
            enableClientEdition();
        }
    });

}


function clearClientData()
{
    $("#nit").val("");
    $("#nombreE").val("");
    $("#contacto").val("");
    $("#correo").val("");
    $("#tel").val("");
    $("#direccion").val("");

}

function enableClientEdition()
{
    $("#nit").prop('disabled', false);
    $("#nombreE").prop('disabled', false);
    $("#contacto").prop('disabled', false);
    $("#direccion").prop('disabled', false);
    $("#tel").prop('disabled', false);
    $("#correo").prop('disabled', false);
}