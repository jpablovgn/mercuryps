<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";


// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?>	<script>

                window.location = "../../Home/exit.php";
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
include_once($lvlroot . "Body/AlertsWindows.php");

?>
<script>
// The lvlroot variable indicates the levels of direcctories (required to auth)
// the file loaded has to up, to be on the root directory
    var lvlroot = "../../";

</script>
<script type="text/javascript" src="../../assets/plugins/parsley/dist/parsley.js"></script>


<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Admin Proveedor</a></li>
    <li class="active">Crear Proveedor</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Crear Proveedor <small> Ingrese los datos del nuevo Proveedpr</small></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-follow fa-2x"></i> 
                    Crear Proveedor</h4>
            </div>
            <div class="panel-body panel-form">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="FormCrearUsuario" id = "FormCrearUsuario">
                    <!-- Start campo nombre -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="nit">Nit  (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="nit" name="nombre" placeholder="Nit " data-parsley-required="true" value ="" />
                        </div>
                    </div>
                    <!-- End campo nombre -->

                    <!-- Start campo nombre de usuario -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="nombreE">Nombre Empresa (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="nombreE" name="usuario" placeholder="Nombre de la Empresa" data-parsley-required="true" data-parsley-length="[3, 15]" />

                        </div>
                    </div>
                    <!-- End campo nombre de usuario -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="correo">Correo Electrónico (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="correo" name="correo" placeholder="ejemplo@gmail.com" data-parsley-type="email"  data-parsley-required="true" />

                        </div>
                    </div>

                    <!-- End campo nombre de usuario -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="telefono">Teléfono (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="tel" name="tel" placeholder="Teléfono" data-parsley-required="true" />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="direccion">Dirección (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="direccion" name="tel" placeholder="Dirección" data-parsley-required="true" />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="contacto">Contacto (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="contacto" name="tel" placeholder="Nombre Contacto" data-parsley-required="true" />

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4"><b>Los campos con (*) son obligatorios</b></label>									
                        <div class="col-md-6 col-sm-6">
                            <button type="button" class="btn btn-primary" id="saveData"  name="AceptRegUser" >
                                Crear Proveedor
                            </button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
        <!-- end panel inverse -->
    </div>
    <!-- end col-12 -->

    <!-- #modal-alert -->
    <!-- modal-trigger -->
    <a href="#modal-alert" id="alert-trigger" class="btn btn-sm btn-danger" data-toggle="modal" style="display:none;">Demo</a>
    <div class="modal fade" id="modal-alert">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Ocurrió un error</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger m-b-0">
                        <h4 id="alert-Header"><i class="fa fa-info-circle"></i> Alerta</h4>
                        <p id="alert-comentary"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-danger" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal-alert -->
    <!-- #modal-dialog -->
    <!-- modal-trigger -->
    <a href="#modal-dialog" id="modal-trigger" class="btn btn-sm btn-success" data-toggle="modal" style="display:none;">Demo</a>
    <div class="modal fade" id="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modal-Header">Modal Dialog</h4>
                </div>
                <div class="modal-body" >
                    <div class="alert m-b-o alert-success" id="modal-Note">
                        Modal body content here...
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-success" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal-dialog -->				
</div>
<!-- end row -->
<script type="text/javascript" src="js/validations.js"></script>
<script>
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarAdminProveedor");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarAdminProveedor-CrearProveedor");
    Changesub2Activejs.className = "active";


</script>


<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>

