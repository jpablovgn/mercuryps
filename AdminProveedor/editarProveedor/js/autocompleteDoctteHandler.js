var nit = getUrlVars()['id'];
var url = 'php/cargarProveedor.php?nit=' + nit;

$.getJSON(url, function(result)
{
    if (result['ERROR'])
    {
        alertWindow(result['ERROR'][0], result['ERROR'][1]);
    }
    else
    {


        // loading client info
        $('#nit').val(result['DATA'][0][0]);
        $('#name').val(result['DATA'][0][1]);
        $('#correo').val(result['DATA'][0][3]);
        $('#tel').val(result['DATA'][0][2]);
        $('#contacto').val(result['DATA'][0][5]);
        $('#direccion').val(result['DATA'][0][4]);


    }
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}


// NITclient maxlength
$("#nit").parsley();
$("#nit").attr('readonly', '');


// enterpriceName maxlength
$("#name").parsley();
$("#name").attr('data-parsley-maxlength', 30);

// contact maxlength
$("#contacto").parsley();
$("#contacto").attr('data-parsley-maxlength', 30);


// telephone maxlength
$("#tel").parsley();
$("#tel").attr('data-parsley-maxlength', 20);

// email maxlength
$("#correo").parsley();
$("#correo").attr('data-parsley-maxlength', 45);

// address maxlength
$("#direccion").parsley();
$("#direccion").attr('data-parsley-maxlength', 30);

$("#updateP").click(function() {
    // if form validated
    if ($("#FormCrear").parsley().validate())
    {
        // Confirmation message
        document.getElementById('warning-Header').innerHTML = "ACTUALIZAR";
        document.getElementById('warning-comentary').innerText = "¿Está seguro que desea actualizar los datos del Proveedor?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'updateClientData()');
        document.getElementById('warning-trigger').click();
    }
});

function updateClientData()
{
    // creating variables
    var nit = $("#nit").val();
    var nombreE = $("#name").val();
    var tel = $("#tel").val();
    var correo = $("#correo").val();
    var direccion = $("#direccion").val();
    var contacto = $("#contacto").val();
    var url = "php/actualizar.php?nit=" + nit + "&nombre=" + nombreE + "&tel=" + tel + "&correo=" + correo + "&direccion=" + direccion + "&contacto=" + contacto;
    $.getJSON(url, function(result) {
        if (result['ERROR'])
        {    var globalData;

            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        }
        else
        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Proveedor Actualizado Correctamente ";
            successWindow(headerMsg, comentMsg);
          return false;

         
        }
    });
}
