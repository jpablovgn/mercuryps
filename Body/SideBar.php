<!-- MENU LATERAL IZQUIERDO DE NAVEGACION -->    
​
<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                            <!--<a href="javascript:;"><img src="<?php echo $lvlroot; ?>assets/img/user-8.jpg" alt="" /></a>-->
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <!--<i class="fa fa-diamond text-primary">    MERCURY</i>-->


            </li>
        </ul>
        <!-- end sidebar user -->
        ​
        <!-- begin sidebar nav -->
        <ul class="nav">
            ​
            <li class="has-sub" id="sidebarHome"> <!-- Home -->
                <a href="<?php echo $lvlroot ?>Informes/EstadoMercancia">
                    <i class="fa fa-2x fa-home"></i>
                    <span style="font-size:15px;">Inicio</span>
                </a>
            </li> <!-- End Home -->

            <li class="has-sub" id="sidebarIngresoMercancia"> <!-- Ingreso Mercancía -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-archive"></i>
                    <span style="font-size:15px;">Ingreso de Mercancía</span>
                </a>
                <ul class="sub-menu">
                    <li id="sidebarIngresoMercancia-Ingreso">
                        <a href="<?php echo $lvlroot ?>IngresoMercancia/Ingreso" style="font-size:15px;">
                            Ingreso
                        </a>
                    </li>
                    <li id="sidebarIngresoMercancia-Nacionalizar">
                        <a href="<?php echo $lvlroot ?>IngresoMercancia/Nacionalizar" style="font-size:15px;">
                            Nacionalizar
                        </a>
                    </li>
                </ul>
            </li> <!-- End Ingreso Mercancía -->

            <li class="has-sub" id="sidebarProcesoBodega"> <!-- Proceso Bodega -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-cubes"></i>
                    <span style="font-size:15px;">Proceso en Bodega</span>
                </a>
                <ul class="sub-menu">
                    <li id="sidebarProcesoBodega-OrderPicking">
                        <a href="<?php echo $lvlroot ?>ProcesoBodega/OrderPicking" style="font-size:15px;">
                            Selección de Bultos
                        </a>
                    </li>
                    <li id="sidebarProcesoBodega-BatchPicking">
                        <a href="<?php echo $lvlroot ?>ProcesoBodega/BatchPicking" style="font-size:15px;">
                            Selección de Unidades
                        </a>
                    </li>
                </ul>
            </li> <!-- End Proceso Bodega -->

            <li class="has-sub" id="sidebarSalidaMercancia"> <!-- Salida Mercancía -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-send"></i>
                    <span style="font-size:15px;">Salida de Mercancía</span>
                </a>
                <ul class="sub-menu">
                    <li id="sidebarSalidaMercancia-CrearSalida">
                        <a href="<?php echo $lvlroot ?>SalidaMercancia/CrearSalida" style="font-size:15px;">
                            Crear salida
                        </a>
                    </li>
                    <li id="sidebarSalidaMercancia-MercanciaDespachada"  style="display:none;">
                        <a href="<?php echo $lvlroot ?>SalidaMercancia/MercanciaDespachada" style="font-size:15px;">
                            Mercancía Despachada
                        </a>
                    </li>
                </ul>
            </li> <!-- End Salida Mercancía -->

            <li class="has-sub" id="sidebarInformes"> <!-- Informes -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa ion-clipboard"></i>
                    <span style="font-size:15px;">Informes</span>
                </a>
                <ul class="sub-menu">
                    <li id="sidebarInformes-EstadoMercancia">
                        <a href="<?php echo $lvlroot ?>Informes/EstadoMercancia" style="font-size:15px;">
                            Estado de la Mercancía
                        </a>
                    </li>
                    <li id="sidebarInformes-EstadoNacionalizacion">
                        <a href="<?php echo $lvlroot ?>Informes/EstadoNacionalizacion" style="font-size:15px;">
                            Estado Nacionalización
                        </a>
                    </li>
                    <li id="sidebarInformes-UbicacionesDisponibles">
                        <a href="<?php echo $lvlroot ?>Informes/UbicacionesDisponibles" style="font-size:15px;">
                            Ubicaciones Disponibles
                        </a>
                    </li>

                    <li id="sidebarInformes-Historial" style="display:none">
                        <a href="<?php echo $lvlroot ?>Informes/HistorialUsuarios" style="font-size:15px;">
                            Usuarios en el Sistema
                        </a>
                    </li>
                    <li id="sidebarInformes-IngresoMercancia">
                        <a href="<?php echo $lvlroot ?>Informes/IngresoMercancia" style="font-size:15px;">
                            Ingreso Mercancia
                        </a>
                    </li>
                    <li id="sidebarInformes-SalidaMercancia">
                        <a href="<?php echo $lvlroot ?>Informes/SalidaMercancia" style="font-size:15px;">
                            Salida Mercancia
                        </a>
                    </li>
  		    <li id="sidebarInformes-InventarioMercancia">
                        <a href="<?php echo $lvlroot ?>Informes/Inventario" style="font-size:15px;">
                            Inventario Mercancia
                        </a>
                    </li>
                    <li id="sidebarInformes-TrazabilidadMercancia">
                        <a href="<?php echo $lvlroot ?>Informes/Traza_Mercancia" style="font-size:15px;">
                            Trazabilidad Mercancia
                        </a>
                    </li>
                    <li id="sidebarInformes-Picking" style="display:none;">
                        <a href="<?php echo $lvlroot ?>Informes/Picking" style="font-size:15px;" style="font-size:15px;">
                            Pickings
                        </a>
                    </li>
                </ul>
            </li> <!-- End Informes -->


            <li class="has-sub" id="sidebarMovimientos"> <!-- movimientos -->
                <a href="<?php echo $lvlroot ?>Movimientos/index.php">

                    <i class="fa fa-barcode"></i>
                    <span style="font-size:15px;">Movimientos</span>
                </a>

            </li>

            <li class="has-sub" id="sidebarAdminClientes"> <!-- Begin Creación de Maestra -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-users"></i>
                    <span style="font-size:15px;">Admin Clientes</span>
                </a>
                <ul class="sub-menu">
                    <li id="sidebarAdminClientes-CrearEditarCliente">
                        <a href="<?php echo $lvlroot ?>AdminClientes/Clientes/index.php" style="font-size:15px;">
                            Crear/Editar Cliente
                        </a>
                    </li>
                    <li id="sidebarAdminClientes-CrearMaestra" style="display:none;">
                        <a href="<?php echo $lvlroot ?>AdminClientes/Maestras/index.php" v>
                            Crear/Editar Maestra
                        </a>
                    </li>
                </ul>
            </li> <!-- End Usuarios -->

            <li class="has-sub" id="sidebarAdminUsuarios"> <!-- Begin Admin Usuarios -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-user"></i>
                    <span style="font-size:15px;">Admin Usuarios</span> 
                </a>
                <ul class="sub-menu">
                    <li id="sidebarAdminUsuarios-CrearUsuario">
                        <a href="<?php echo $lvlroot ?>RegistroUsuarios/crearUsuario/index.php" style="font-size:15px;">
                            Crear Usuario
                        </a>
                    </li>
                    <li id="sidebarAdminUsuarios-EditarUsuario">
                        <a href="<?php echo $lvlroot ?>RegistroUsuarios/editarUsuario/index.php" style="font-size:15px;">
                            Editar Usuario
                        </a>
                    </li>

                </ul>
            </li> 
            <li class="has-sub" id="sidebarAdminProveedor"> <!-- Begin Admin Usuarios -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa ion-android-people"></i>
                    <span style="font-size:15px;">Admin Proveedores</span> 
                </a>
                <ul class="sub-menu">
                    <li id="sidebarAdminProveedor-CrearProveedor">
                        <a href="<?php echo $lvlroot ?>AdminProveedor/crearProveedor/index.php" style="font-size:15px;">
                            Crear Proveedor
                        </a>
                    </li>
                    <li id="sidebarAdminProveedor-EditarProveedor">
                        <a href="<?php echo $lvlroot ?>AdminProveedor/editarProveedor/index.php" style="font-size:15px;">
                            Editar Proveedor
                        </a>
                    </li>

                </ul>
            </li>
            <li class="has-sub" id="sidebarAdminComprador"> <!-- Begin Admin Usuarios -->
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-truck"></i>
                    <span style="font-size:15px;">Admin Conductores</span> 
                </a>
                <ul class="sub-menu">
                    <li id="sidebarAdminComprador-CrearComprador">
                        <a href="<?php echo $lvlroot ?>AdminConductor/crearConductor/index.php" style="font-size:15px;">
                            Crear Conductor
                        </a>
                    </li>
                    <li id="sidebarAdminComprador-EditarComprador">
                        <a href="<?php echo $lvlroot ?>AdminConductor/editarConductor/index.php" style="font-size:15px;">
                            Editar Conductor
                        </a>
                    </li>

                </ul>
            </li> 
            <!-- End Usuarios -->
            <li class="has-sub" id="sidebarAdministracion">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-cogs"></i>
                    <span style="font-size:15px;">Administración</span> 
                </a>
                <ul class="sub-menu">
                    <li id="sidebarAdministracion-Almacenamiento">
                        <a href="<?php echo $lvlroot ?>Administracion/Almacenamiento/index.php" style="font-size:15px;">
                            Tipos Almacenamiento
                        </a>
                    </li>
                    <li id="sidebarAdministracion-Contenedor">
                        <a href="<?php echo $lvlroot ?>Administracion/Contenedor/index.php" style="font-size:15px;">
                            Tipos Contenedor
                        </a>
                    </li>
                    <li id="sidebarAdministracion-Destinos">
                        <a href="<?php echo $lvlroot ?>Administracion/Destinos/index.php" style="font-size:15px;">
                            Tipos Destinos
                        </a>
                    </li>
                    <li id="sidebarAdministracion-Ingresos">
                        <a href="<?php echo $lvlroot ?>Administracion/Ingresos/index.php" style="font-size:15px;">
                            Tipos Ingresos
                        </a>
                    </li>
                    <li id="sidebarAdministracion-Salida">
                        <a href="<?php echo $lvlroot ?>Administracion/Salida/index.php" style="font-size:15px;">
                            Tipos Salidas
                        </a>
                    </li>
                    <li id="sidebarAdministracion-Ubicacion">
                        <a href="<?php echo $lvlroot ?>Administracion/Ubicacion/index.php" style="font-size:15px;">
                            Ubicaciones
                        </a>
                    </li>




                </ul>

            </li> 
            <li class="has-sub" id="sidebarHome"> <!-- Home -->
                <a href="<?php echo $lvlroot ?>Home/exit.php">
                    <i class="fa fa-2x fa-bell-o"></i>
                    <span style="font-size:15px;">Cerrar Sesión</span>
                </a>
            </li> 


            ​
            <li class="has-sub"> <!-- Empty -->
                <a href="javascript:;">
                    <b class=""></b>
                    <i class=""></i>
                    <span></span> 
                </a>
                <ul class="sub-menu">
                    <li><a href=""></a></li>
                </ul>
            </li> <!-- End Empty -->
            ​

            <!-- begin sidebar minify button -->
            <!--<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>-->
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->
