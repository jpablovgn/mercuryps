var url = 'php/clientInfo.php';
$.getJSON(url, function (result) {
    if (result['ERROR'])
    {
        alertWindow(result['ERROR'][0], result['ERROR'][1]);
    } else {

        loadSelect(result['DATA']);    }
    
});

// Loading NIT and enterprice name combobox
function loadSelect(clientData)
{
    var sizeofclientData = ObjectSize(clientData);
    // Deleting combobox items.
    $('#selectNITClient').html('');
    $('#selectNITClient').data('combobox').refresh();
    $('#selectEName').html('');
    $('#selectEName').data('combobox').refresh();
    $('#selectContact').html('');
    $('#selectContact').data('combobox').refresh();
    $('#selectTradeMark').html('');
    $('#selectTradeMark').data('combobox').refresh();
    // adding new items to the combobox, importations numbers.
    // adding blank item.
    var blankNITjs = '<option value=""></option>';
    $('#selectNITClient').append(blankNITjs);
    $('#selectNITClient').data('combobox').refresh();
    var blankENamejs = '<option value=""></option>';
    $('#selectEName').append(blankENamejs);
    $('#selectEName').data('combobox').refresh();

    $('#selectContact').append(blankENamejs);
    $('#selectContact').data('combobox').refresh();

    $('#selectTradeMark').append(blankENamejs);
    $('#selectTradeMark').data('combobox').refresh();
    // for all importations items.
    for (var ijs = 0; ijs < sizeofclientData; ijs++)
    {
        var appendNITjs = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][0] + '</option>';
        $('#selectNITClient').append(appendNITjs);
        $('#selectNITClient').data('combobox').refresh();
        var appendENamejs = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][1] + '</option>';
        $('#selectEName').append(appendENamejs);
        $('#selectEName').data('combobox').refresh();
        var appendContactjs = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][2] + '</option>';
        $('#selectContact').append(appendContactjs);
        $('#selectContact').data('combobox').refresh();
        var appendTMarkjs = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][3] + '</option>';
        $('#selectTradeMark').append(appendTMarkjs);
        $('#selectTradeMark').data('combobox').refresh();
    }
}
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;
// End function object size.

// ids and compare has to have the values at same level. ids[0] corresponds to compare[0]
var ids = [
    'selectNITClient',
    'selectContact',
    'selectEName',
    'selectTradeMark'
];
var compare = [
    'NIT',
    'CONTACTO',
    'NOMBRE',
    'MARCA'
];

$("#selectNITClient").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});
$("#selectContact").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);

    else
        clearClientSelects();
});
$("#selectEName").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});
$("#selectTradeMark").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});

function fillClientData(select)
{
    for (var j = 0; j < ObjectSize(ids); j++)
    {
        $("#" + ids[j]).val(select.value);
        $("#" + ids[j]).data('combobox').refresh();
    }
}

function clearClientSelects()
{
    // Clearing all the data function from validateOnSave.js file
    clearData();
    for (var j = 0; j < ObjectSize(ids); j++)
    {
        // Clearing combobox
        $("#" + ids[j]).data('combobox').clearTarget();
        $("#" + ids[j]).data('combobox').clearElement();
    }
}

function clearData()
{
    // Clearing combobox
    $("#selectNITClient").data('combobox').clearTarget();
    $("#selectNITClient").data('combobox').clearElement();
    $("#selectEName").data('combobox').clearTarget();
    $("#selectEName").data('combobox').clearElement();
    $("#selectContact").data('combobox').clearTarget();
    $("#selectContact").data('combobox').clearElement();
    $("#selectTradeMark").data('combobox').clearTarget();
    $("#selectTradeMark").data('combobox').clearElement();
}