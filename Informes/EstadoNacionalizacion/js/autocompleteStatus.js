
$("#selectNITClient").change(function() {
    var impo = $("#selectNITClient").val();

    $.getJSON('php/importationsCbox.php?nit=' + impo, function(result) {
        if (result['DATA'])
        {
            var storeTypeData = result['DATA'];
            var sizeofstoreTypeData = ObjectSize(storeTypeData);
            // Deleting combobox items.
            $('#numimportation').html('');
            $('#numimportation').data('combobox').refresh();

            // adding blank item.
            var blankItemjs = '<option value=""></option>';
            $('#numimportation').append(blankItemjs);
            $('#numimportation').data('combobox').refresh();
            // adding all items.
            for (var ijs = 0; ijs < sizeofstoreTypeData; ijs++)
            {

                var appendItemjs = '<option value="' + storeTypeData[ijs][0] + '">' + storeTypeData[ijs][0] + '</option>';
                $('#numimportation').append(appendItemjs);
                $('#numimportation').data('combobox').refresh();
            }
        }
    });
});

function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;

function fillData() {
    if ($('#mercancia').val()=='Bultos') {
         var id = $('#numimportation').val();
    $("#Loadtable").load("php/clientsemaphore.php?numimpo=" + id);
    }
     if ($('#mercancia').val()=='Unidades') {
         var id = $('#numimportation').val();
    $("#Loadtable").load("php/clientsemaphoreUnidad.php?numimpo=" + id);
    }
   
}