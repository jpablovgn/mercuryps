<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.

include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?><script>
        window.location = "../../Home/exit.php";
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
$nosubmit = "if (event.keyCode == 13) event.preventDefault()";
// functions defined in js/autocompleteHandler.js
$autoCompleteCall = "if (event.keyCode == 13) { autoCompleteOthers(this); event.preventDefault(); }";
$autoCompleteInportation = "if (event.keyCode == 13) { loadSemaphore(this); event.preventDefault(); }";
?>
<script>
    var lvlrootjs = "../../";
</script>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Informes</a></li>
    <li class="active">Estado Mercancía Nacionalizada</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Estado Mercancía Nacionalizada<small>Consultar el estado de la mercancía Nacionalizada por número de Importación del cliente.</small></h1>
<!-- end page-header -->

<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">
        <!-- begin panel Client Info -->
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="ion-ios-people fa-lg"></i>
                    Información del cliente
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form" id="panel-clientInfo-form">
                <form data-parsley-validate="true" id="client-form" name="client-form" class="form-horizontal form-bordered " method="post" action="" onkeydown="<?php echo $nosubmit; ?>">
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>NIT (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectNITClient" id="selectNITClient" >
                                <option  value="">  Seleccione el NIT</option>
                            </select>
                            <!--OnChange="Changevalues($('#selectNITClient').val())-->
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Empresa (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectEName" id="selectEName" value="">
                                <option value="">Seleccione la empresa</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Contacto (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectContact" id="selectContact" value="">
                                <option value="">Seleccione el contacto</option>
                            </select>
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Marca (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectTradeMark" id="selectTradeMark" value="">
                                <option value="">Seleccione la marca</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 text-left">Listar Mercancía Por:  (*)</label>
                        <div class="col-md-4">
                            <select class="combobox" name="numimportation" id="mercancia" value="" >
                                <option value=""> Seleccione mercancía</option>
                                <option id="optBultos" value="Bultos"> Bultos</option>
                                <option id="optUnidad"value="Unidades">Unidades </option>
                            </select>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 text-left">Importación a consultar (*)</label>
                        <div class="col-md-4">
                            <select class="combobox" name="numimportation" id="numimportation" value="" onchange="fillData()">
                                <option value=""> Seleccione una importación</option>
                            </select>
                        </div>
                    </div>

                </form>
            </div>
            <!-- end body panel -->
        </div>
        <!-- end panel Client Info -->
        <!-- begin panel Semaphore -->
        <div class="panel panel-inverse " data-sortable-id="form-plugins-2">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-circle-o fa-lg"></i>
                    <i class="fa fa-circle fa-lg"></i>
                    <i class="fa fa-check-circle fa-lg"></i>
                    Semáforo de estado de la mercancía Nacionalizada
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body " id="Loadtable">

            </div>
            <!-- end body panel -->
        </div>
        <div class="panel panel-inverse " data-sortable-id="form-plugins-2">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-circle-o fa-lg"></i>
                    <i class="fa fa-circle fa-lg"></i>
                    <i class="fa fa-check-circle fa-lg"></i>
                    Simbolos del estado de la Mercancía
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body ">
                <table class="table table-bordered">
                    <tr>
                        <th>NACIONALIZADO</th>
                        <td><img src="../../assets/img/semaphore/Green.png" alt="Inter-Telco" style="width:50px; "></td>
                    </tr>
             
                    <tr>
                        <th>PENDIENTE POR NACIONALIZAR</th>
                        <td><img src="../../assets/img/semaphore/Red.png" alt="Inter-Telco" style="width:50px;" ></td>

                    </tr>
                 
                </table>
            </div>
        </div>
        <!-- end body panel -->
    </div>
    <!-- end panel Semaphore -->
</div>
<!-- end main column -->

<!-- begin right column -->	<!-- end right column -->
<style> 
       table {
           text-align: center;
       }
       table th{
           text-align: center;

       }

</style>
</div>
<!-- end row -->

<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
//Including alerts windows -->
include_once($lvlroot . "Body/AlertsWindows.php");
?>

<!-- // Loading autocomplete Status. -->
<script type="text/javascript" src="js/autocompleteClient.js"></script>
<script type="text/javascript" src="js/autocompleteStatus.js"></script>
<script type="text/javascript" src="js/setValidations.js"></script>


<script type="text/javascript">
                                // Activating the side bar.
                                var Change2Activejs = document.getElementById("sidebarInformes");
                                Change2Activejs.className = "has-sub active";
                                var Changesub2Activejs = document.getElementById("sidebarInformes-EstadoNacionalizacion");
                                Changesub2Activejs.className = "active";
</script>
