<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.

include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?><script>
        window.location = "../../Home/exit.php";
    </script><?php
}

// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
$nosubmit = "if (event.keyCode == 13) event.preventDefault()";
// functions defined in js/autocompleteHandler.js
$autoCompleteCall = "if (event.keyCode == 13) { autoCompleteOthers(this); event.preventDefault(); }";
$autoCompleteInportation = "if (event.keyCode == 13) { loadSemaphore(this); event.preventDefault(); }";
?>
<script>
    var lvlrootjs = "../../";
</script>
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <div class="panel panel-inverse ">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-cubes fa-lg"></i>
                    Ubicaciones Disponibles
                </h4>
            </div>
            <!-- begin panel body -->
            <div class="panel-body panel-form">
                <div class="col-md-12">
                    <h4>Estanterías Ocupadas</h4>
                    <div class="progress progress-striped active">
                        <div class="progress-bar progress-bar-danger" id="progressbarOcupado"  style="width: 70%"></div>
                        <!--                        <div class="progress-bar progress-bar-primary" id="progressbarOcupado" style="width: 50%">100%</div>
                                                <div class="progress-bar progress-bar-info" id="progressbarDisponible"  style="width: 50%">100%</div>-->
                    </div>
                    <h4>Estanterías Disponibles</h4>
                    <div class="progress progress-striped active">

                        <div class="progress-bar progress-bar-success" id="progressbarDisponible"  style="width: 70%"></div>
                        <!--                        <div class="progress-bar progress-bar-primary" id="progressbarOcupado" style="width: 50%">100%</div>
                                                <div class="progress-bar progress-bar-info" id="progressbarDisponible"  style="width: 50%">100%</div>-->
                    </div>
                </div>
            </div>
            <div class="panel-body panel-form">
                <br>
                <div class="form-group" >

                    <table id="checktable" class="table table-striped table-bordered" >
                        <thead>
                            <tr id="" >            
                                <th>Estanterías Disponibles</th>

                            </tr>
                        </thead>
                    </table>

                    <!-- Start campo Nombre input desplegable  -->

                </div>
            </div>

            <!-- end panel body -->
        </div>

    </div>

</div>

<style>
    table th {
        text-align: center;
    }
    table  tr{
        text-align: center;
    }
</style>
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
//Including alerts windows -->
include_once($lvlroot . "Body/AlertsWindows.php");
?>

<script type="text/javascript" src="js/informDate.js"></script>
<script type="text/javascript" src="js/promedio.js"></script>
<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarInformes");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarInformes-UbicacionesDisponibles");
    Changesub2Activejs.className = "active";
</script>



