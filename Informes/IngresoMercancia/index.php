<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.

include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?><script>
        window.location = "../../Home/exit.php";
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
$nosubmit = "if (event.keyCode == 13) event.preventDefault()";
// functions defined in js/autocompleteHandler.js
$autoCompleteCall = "if (event.keyCode == 13) { autoCompleteOthers(this); event.preventDefault(); }";
$autoCompleteInportation = "if (event.keyCode == 13) { loadSemaphore(this); event.preventDefault(); }";
?>
<script>
    var lvlrootjs = "../../";
</script>
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <div class="panel panel-inverse " data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="ion-ios-people fa-lg"></i>
                    Información del cliente
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form" id="panel-clientInfo-form">
                <form data-parsley-validate="true" id="client-form" name="client-form" class="form-horizontal form-bordered " method="post" action="" onkeydown="<?php echo $nosubmit; ?>">
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>NIT (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectNITClient" id="selectNITClient" >
                                <option  value="">  Seleccione el NIT</option>
                            </select>
                            <!--OnChange="Changevalues($('#selectNITClient').val())-->
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Empresa (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectEName" id="selectEName" value="">
                                <option value="">Seleccione la empresa</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Contacto (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectContact" id="selectContact" value="">
                                <option value="">Seleccione el contacto</option>
                            </select>
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Marca (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="selectTradeMark" id="selectTradeMark" value="">
                                <option value="">Seleccione la marca</option>
                            </select>
                        </div>
                    </div>


                </form>
            </div>
            <!-- end body panel -->
        </div>
        <!-- begin panel -->

        <div class="panel panel-inverse ">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" onclick="noSubmit();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-calendar fa-lg"></i>

                    Elegir Las Fechas
                </h4>
            </div>
            <!-- begin panel body -->
            <div class="panel-body panel-form form-horizontal form-bordered">
                <!-- begin form -->
                <form data-parsley-validate="true" id="demo-form-ware-info" name="demo-form-ware-info" class="form-horizontal form-bordered" method="post" action="" >
                    <div class="form-group">
                        <label class="col-md-2 control-label">Elegir Las Fechas</label>
                        <div class="col-md-8">
                            <div class="input-group input-daterange" data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control" name="start" placeholder="Fecha Inicial" id="fecha1"  />

                                <span class="input-group-addon">Entre</span>
                                <input type="text" class="form-control"  name="end" placeholder="Fecha Final" id="fecha2" onchange="Load();"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-cubes fa-lg"></i> 
                    <?php echo "Reporte De Ingreso De La Mercancía " ?>
                </h4>
            </div>

            <div class="panel-body panel-form">
                <br>
                <!-- Start campo Nombre input desplegable  -->
                <div class="form-group" id="loadtable">

                </div>
            </div>



            <!-- end panel inverse -->
        </div>
    </div>
</div>

<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
//Including alerts windows -->
include_once($lvlroot . "Body/AlertsWindows.php");
?>
<script type="text/javascript" src="js/informDate.js"></script>
<script type="text/javascript" src="js/autocompleteClient.js"></script>

<script type="text/javascript">
                                // Activating the side bar.
                                var Change2Activejs = document.getElementById("sidebarInformes");
                                Change2Activejs.className = "has-sub active";
                                var Changesub2Activejs = document.getElementById("sidebarInformes-IngresoMercancia");
                                Changesub2Activejs.className = "active";
</script>


