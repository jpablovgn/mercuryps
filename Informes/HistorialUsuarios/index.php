<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";

// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?>	<script>

                window.location = "../../Home/exit.php";
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
include_once("../../assets/php/start_sesion.php");
require_once ($lvlroot . "assets/php/PhpMySQL.php"); //Funciones base de datos.
$usuario = $_SESSION['NOMBREUSUARIO'];
?>
<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-follow fa-2x"></i> 
<?php echo "Historial acciones de usuario. " ?>
                </h4>
            </div>
            <div class="panel-body panel-form">

                <!-- Start campo Nombre input desplegable  -->
                <div class="form-group">
                    <table id="data-table" class="table table-striped table-bordered" >

                        <thead >

                            <tr style="border:1px;">
                                <th>Usuario</th>
                                <th>Perfil</th>
                                <th>Accion</th>
                                <th>Fecha</th>
                            </tr><br>
                        </thead>
<?php
$Client = new Database();
$perfiles = "SELECT * FROM MR_USUARIOS";
$queryResult = $Client->query($perfiles);
while ($clientData = $Client->fetch_array_assoc($queryResult)) {
    ?>
                            <tr >
                                <td><?php echo $clientData['USUARIO'] ?></td>
                                <td><?php echo $clientData['ID_PERFIL'] ?></td>
                                <td><?php echo $clientData['ID_PERFIL'] ?></td>
                                <!--<td><?php echo $clientData[date(" Y-m-d")] ?></td>-->

                            </tr>
    <?PHP
}
$Client->close();
?>
                    </table>
                </div>
            </div>

            <div class="panel-body panel-form">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="FormCrearUsuario" id = "FormCrearUsuario">
                    <!-- Start campo nombre -->
                    <div class="form-group">
                        <div class="col-md-6">
                            <div class="panel panel-inverse">
                                <div class="m-t-20 m-b-40">



                                </div>
                            </div>
                        </div>
                        <div class="news-image">
                            <img src="../assets/img/login-bg/puntol.jpg" data-id="login-cover-image" alt="" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- end panel inverse -->
    </div>

    <script type="text/javascript">
        // Activating the side bar.
        var Change2Activejs = document.getElementById("sidebarInformes");
        Change2Activejs.className = "has-sub active";
        var Changesub2Activejs = document.getElementById("sidebarInformes-Historial");
    Changesub2Activejs.className = "active";
    </script>
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
//session_destroy();
//	session_unset();
?>
