<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../../";
// Including Head.

include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");

// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
//nosubmit = "if (event.keyCode == 13) event.preventDefault()";
// functions defined in js/autocompleteHandler.js
$autoCompleteCall = "if (event.keyCode == 13) { autoCompleteOthers(this); event.preventDefault(); }";
$autoCompleteInportation = "if (event.keyCode == 13) { loadSemaphore(this); event.preventDefault(); }";
?>
<script>
    var lvlrootjs = "../../../";
    
</script>
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">




        <div class="panel panel-inverse " id="tablaInventario"data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="ion-ios-people fa-lg"></i>
                    Lista Inventario                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form" id="panel-clientInfo-form">
                <form data-parsley-validate="true" id="client-form" name="client-form" class="form-horizontal form-bordered " method="post" action="" onkeydown="<?php echo $nosubmit; ?>">

                    <div class="panel-body panel-form ">
                        <div class="form-group">
                            <table  class="table" id="displayresult" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Referencia</th>
                                        <th>Código</th>
                                                                                <th>Fecha</th>

                                    </tr><br>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Referencia</th>
                                        <th>Código</th>
                                                                                <th>Fecha</th>

                                    </tr><br>
                                </tfoot>

                            </table>
                        </div>

                    </div>


                </form>
            </div>
        </div>




    </div>
</div>
<script type="text/javascript">
    MostrarLectura();
    function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}

                                function MostrarLectura() {

                                    var name = getUrlVars()['nombre'];
                                    var urlI = 'listainventario.php?nombre=' + name;
                                    $.getJSON(urlI, function (result) {
                                        mostrarInventario(result['DATA']);
//                               

                                    });
//                                    $('#eanbulto').val("");
//                                    $('#eanbulto').focus();
                                }

               



//


                                function mostrarInventario(result) {
                                    

                                    $('#displayresult').DataTable({
                                        data: result,
                                        responsive: true,
//        destroy: true,
//    searching: false,


                                        "language": {
                                            "sProcessing": "Procesando...",
                                            "sLengthMenu": "Mostrar _MENU_ registros",
                                            "sZeroRecords": "No se encontraron resultados",
                                            "sEmptyTable": "Ningún dato disponible en esta tabla",
                                            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                                            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                                            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                                            "sInfoPostFix": "",
                                            "sSearch": "Buscar:",
                                            "sUrl": "",
                                            "sInfoThousands": ",",
                                            "sLoadingRecords": "Cargando...",
                                            "oPaginate": {
                                                "sFirst": "Primero",
                                                "sLast": "Último",
                                                "sNext": "Siguiente",
                                                "sPrevious": "Anterior"
                                            },
                                            "oAria": {
                                                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                            }

                                        }


                                    });

                                }



</script>
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
//Including alerts windows -->
include_once($lvlroot . "Body/AlertsWindows.php");
?>
<script type="text/javascript" src="js/autocompleteClient.js"></script>
<!--<script type="text/javascript" src="js/autocompleteImportation.js"></script>-->

<!--
<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarInformes");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarInformes-IngresoMercancia");
    Changesub2Activejs.className = "active";
</script>
-->

