<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../";

include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");

// functions defined in js/autocompleteDoctteHandler.js
$nosubmit = "if (event.keyCode == 13) event.preventDefault()";
$autoCompleteImportCall = "if (event.keyCode == 13) { autoCompleteImportCall(this); event.preventDefault(); }";
// functions defined in js/autocompleteDoctteHandler.js
$autoCompleteDoctteCall = "if (event.keyCode == 13) { event.preventDefault(); autoCompleteDoctte(this); }";
// Prevent default post
$preventDefault = "if (event.keyCode == 13) { event.preventDefault(); }";
//$vble1 = $_GET['imp'];
//$vble2 = $_GET['pallet'];
//$vble3 = $_GET['mer'];
?>
<script>
    var vble3 = "<?php echo $vble1; ?>";
</script>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>
<!-- begin breadcrumb -->
<!-- end breadcrumb -->

<script>
    var obj = <?php print json_encode($vble3); ?>;
</script>
<!-- end page-header -->

<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">
        <!-- begin panel select -->
        <div class="panel panel-inverse ">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-file fa-lg"></i>
                    Seleccione Un Número de Importación                 
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <!-- Start campo Nombre input desplegable  -->
                <div class="table-responsive">

                    <table class="table" id="tableList" >
                        <thead>
                            <tr style="border:1px;">
                                <th>Seleción</th>
                                <th>Número Importación</th>
                              
                            </tr>
                        </thead>

                    </table>

                </div>
            </div>
            <!-- end body panel -->
        </div>
        <!-- end panel select -->
        <!-- begin panel Package references -->

        <!-- end panel Package references -->
    </div>
    <!-- end main column -->

    <!-- end save/cancel button -->

    <!-- Including alerts windows -->
    <?php
    include_once($lvlroot . "Body/AlertsWindows.php");
    ?>
</div>
<!-- end row -->

<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
// Writing on database.
//include_once("php/Insert2db.php");
?>

<!-- // Loading set validations. -->
<script type="text/javascript" src="js/listMercancia.js"></script> 
<!-- // Loading autocomplete handler. -->


<script type="text/javascript">
    // Activating the side bar
    var Change2Activejs = document.getElementById("sidebarMovimientos");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarMovimientos-Movimientos");
    Changesub2Activejs.className = "active";
</script>

