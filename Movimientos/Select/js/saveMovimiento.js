function checkBulto()
{
    var i, x, j;
    var elements = document.getElementsByClassName('chkboxBulto');
    var selectPalet = document.getElementsByClassName('selectPalet');
    var imp = getUrlVars()['imp'];

    for (i = 0; i < elements.length; i++)
    {
        if (elements[i].checked)
        {
            var bulto = $('#bulto' + i).html();

            if ($('#tablePalet').is(":visible")) {
                for (x = 0; x < selectPalet.length; x++) {
                    if (selectPalet[x].checked) {
                        var palet = $('#palet' + x).html();
                    }
                }
            } else {
                palet = "NULL";
            }

            var url = 'php/saveBulto.php?bulto=' + bulto + '&palet=' + palet + '&imp=' + imp;
            $.getJSON(url, function(result)
            {
                if (result) {

                    var headerMsg = "Operación Exitosa";
                    var comentMsg = "Movimiento de Bultos Guardado Correctamente ";
                    successWindow(headerMsg, comentMsg);

                }

            });


        }
    }
}
function checkPalet()
{
    var i, j;
    var elements = document.getElementsByClassName('chkboxPalet');
    var selectEstan = document.getElementsByClassName('selectEstanteria');
    var imp = getUrlVars()['imp'];

    for (i = 0; i < elements.length; i++)
    {
        if (elements[i].checked)
        {
            var palet = $('#palet' + i).html();


            if ($('#tableUbicacion').is(":visible")) {
                for (j = 0; j < selectEstan.length; j++) {
                    if (selectEstan[j].checked) {
                        var estan = $('#estan' + j).html();
                    }
                }
            } else {
                estan = "NULL";
            }

            var url = 'php/savePalet.php?palet=' + palet + '&estan=' + estan + '&imp=' + imp;
            $.getJSON(url, function(result)
            {
                if (result) {

                    var headerMsg = "Operación Exitosa";
                    var comentMsg = "Movimiento de Estibas Guardado Correctamente ";
                    successWindow(headerMsg, comentMsg);

                }

            });


        }
    }
}
function checkUnidad()
{
    var i, x, j, z;
    var elements = document.getElementsByClassName('chkboxUnidad');
    var selectBulto = document.getElementsByClassName('selectBulto');

    var imp = getUrlVars()['imp'];

    for (i = 0; i < elements.length; i++)
    {
        if (elements[i].checked)
        {
            var unidad = $('#unidad' + i).html();

            if ($('#tableBults').is(":visible")) {
                for (z = 0; z < selectBulto.length; z++) {
                    if (selectBulto[z].checked) {
                        var bulto = $('#bult' + z).html();
                    }
                }
            } else {
                bulto = "NULL";
            }
    
            var url = 'php/saveUnidad.php?bulto=' + bulto +  '&imp=' + imp + '&unidad=' + unidad;
            $.getJSON(url, function(result)
            {
                if (result) {

                    var headerMsg = "Operación Exitosa";
                    var comentMsg = "Movimiento de Unidades Guardado Correctamente ";
                    successWindow(headerMsg, comentMsg);

                }

            });


        }
    }
}
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}
