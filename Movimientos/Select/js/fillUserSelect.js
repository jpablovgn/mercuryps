var url = 'php/userInfo.php';
$.getJSON(url, function (result) {
    if (result['DATA']) {

        loadSelect(result);
    }
}).fail(function (err) {
    loadSelect(JSON.parse(err.responseText));
});

// Loading NIT and enterprice name combobox
function loadSelect(result)
{
    var clientData = result['DATA'];
    var sizeofclientData = ObjectSize(clientData);
        
    // Deleting combobox items.
    $('#selectUser').html('');
    $('#selectUser').data('combobox').refresh();
    
    $('#selectName').html('');
    $('#selectName').data('combobox').refresh();
    
   
    // adding new items to the combobox, importations numbers.
    // adding blank item.
    var blankNITjs = '<option value=""></option>';
    $('#selectUser').append(blankNITjs);
    $('#selectUser').data('combobox').refresh();
    var blankENamejs = '<option value=""></option>';
    $('#selectName').append(blankENamejs);
    $('#selectName').data('combobox').refresh();


    // for all importations items.
    for (var ijs = 0; ijs < sizeofclientData; ijs++)
    {
        var appendNITjs = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][3] + '</option>';
        $('#selectUser').append(appendNITjs);
        $('#selectUser').data('combobox').refresh();
        var appendENamejs = '<option value="' + clientData[ijs][0] + '">' + clientData[ijs][2] + '</option>';
        $('#selectName').append(appendENamejs);
        $('#selectName').data('combobox').refresh();

    }
}

/* combobox filled */

// this function calculates the size of an object.
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;
// End function object size.

// ids and compare has to have the values at same level. ids[0] corresponds to compare[0]
var ids = [
    'selectUser',
    'selectName',

];
var compare = [
    'USUARIO',
    'NOMBRE',
];

$("#selectUser").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});
$("#selectName").change(function () {
    var value = this.value;
    if (value)
        fillClientData(this);
    else
        clearClientSelects();
});


function fillClientData(select)
{


    for (var j = 0; j < ObjectSize(ids); j++)
    {
        $("#" + ids[j]).val(select.value);
        $("#" + ids[j]).data('combobox').refresh();
    }
}

function clearClientSelects()
{
    // Clearing all the data function from validateOnSave.js file
    clearData();
    for (var j = 0; j < ObjectSize(ids); j++)
    {
        // Clearing combobox
        $("#" + ids[j]).data('combobox').clearTarget();
        $("#" + ids[j]).data('combobox').clearElement();
    }
}

function clearData()
{
    // Clearing combobox
    $("#selectUser").data('combobox').clearTarget();
    $("#selectUser").data('combobox').clearElement();
    $("#selectName").data('combobox').clearTarget();
    $("#selectName").data('combobox').clearElement();
  
}
