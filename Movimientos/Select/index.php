<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";

include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");

// functions defined in js/autocompleteDoctteHandler.js
$nosubmit = "if (event.keyCode == 13) event.preventDefault()";
$autoCompleteImportCall = "if (event.keyCode == 13) { autoCompleteImportCall(this); event.preventDefault(); }";
// functions defined in js/autocompleteDoctteHandler.js
$autoCompleteDoctteCall = "if (event.keyCode == 13) { event.preventDefault(); autoCompleteDoctte(this); }";
// Prevent default post
$preventDefault = "if (event.keyCode == 13) { event.preventDefault(); }";
//$vble1 = $_GET['imp'];
//$vble2 = $_GET['pallet'];
//$vble3 = $_GET['mer'];
?>
<script>
    var vble3 = "<?php echo $vble1; ?>";
</script>

<script>
    var lvlrootjs = <?php print json_encode($lvlroot); ?>;
</script>
<!-- begin breadcrumb -->
<!-- end breadcrumb -->

<script>
    var obj = <?php print json_encode($vble3); ?>;
</script>
<!-- end page-header -->

<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">
        <!-- begin panel select -->
        <div class="panel panel-inverse ">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-file fa-lg"></i>
                    Seleccione Un Usuario                
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Usuario (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="user" id="selectUser" value="" required>
                                <option value="">Seleccione El Usuario</option>
                            </select>
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Nombre (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="name" id="selectName" value="">
                                <option value="">Seleccione El Nombre</option>
                            </select>
                        </div>
                        <br>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2 text-left">Número De Importación </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" id="numImpo" name="numImpo"  disabled/>
                        </div>
                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Mercancía (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" name="user" id="selectMove" value="">
                                <option value="">Seleccione Movimiento</option>

                            </select>
                        </div>

                    </div>

                </form>
            </div>
            <!-- end body panel -->
        </div>
        <!-- end panel select -->
        <!-- begin panel Package references -->
        <div class="panel panel-inverse " style="display:none;" id="moves">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-file fa-lg"></i>
                    Seleccione Los Movimientos                
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group" id="loadTable">

                    </div>

                </form>
            </div>
            <!-- end body panel -->
        </div>

        <!-- begin panel Package references -->
        <div class="panel panel-inverse " style="display: none;" id="destin">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-file fa-lg"></i>
                    Seleccione La Nueva Ubicación                
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group">

                        <div class="control-label col-md-2 col-sm-2 text-left">
                            <label>Seleccione Movimiento (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input type="text" class="form-control" id="location" name="location" onchange="loadtable();"  disabled/>
                        </div>
                    </div>
                    <div class="panel-body panel-form">
                        <!-- begin form -->
                        <div class="form-group" id="loadTableNew">

                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 text-right">

                            <button id="save" type="button" class="btn btn-primary start" style="display:true;">
                                <i class="fa fa-upload"></i>
                                <span>Autorizar Movimiento</span>
                            </button>
                            <button id="cancel" type="button" class="btn btn-warning cancel" style="display:true;">
                                <i class="fa fa-upload"></i>
                                <span>Cancelar</span>
                            </button>
                        </div></div>
                </form>
            </div>
            <!-- end body panel -->
        </div>
        <!-- end panel Package references -->
    </div>
    <!-- end main column -->

    <!-- end save/cancel button -->

    <!-- Including alerts windows -->
    <?php
    include_once($lvlroot . "Body/AlertsWindows.php");
    ?>
</div>
<!-- end row -->

<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
// Writing on database.
//include_once("php/Insert2db.php");
?>

<!-- // Loading set validations. -->
<script type="text/javascript" src="js/fillUserSelect.js"></script> 
<script type="text/javascript" src="js/saveData.js"></script>
<script type="text/javascript" src="js/selectMovimiento.js"></script>
<script type="text/javascript" src="js/saveMovimiento.js"></script>

<!-- // Loading autocomplete handler. -->
<script>
                                $('#selectMove').append('<option value="op1" >Bultos</option>', '<option value="op2" >Estibas</option>', '<option value="op3" >Unidades</option>');
                                function fillBulto() {
                                    $("#location").append('<option value="op1" >Estiba</option>', '<option value="op2" >Estantería</option>', );

                                }
                                function fillUnit() {
                                    $("#location").append('<option value="op1" >Estiba</option>', '<option value="op2" >Estantería</option>', '<option value="op2" >Bulto</option>');

                                }
                                function fillPalet() {
                                    $("#location").append('<option value="op2" >Estantería</option>');

                                }
//                        $("#location").append('<option value="op1" >Estiba</option>', '<option value="op2" >Estantería</option>', '<option value="op3" >Bulto</option>');




</script>


<script type="text/javascript">
    // Activating the side bar
    var Change2Activejs = document.getElementById("sidebarMovimientos");
    Change2Activejs.className = "has-sub active";

</script>

