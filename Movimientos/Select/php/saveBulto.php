<?php

include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();

$bulto = $_GET['bulto'];
$imp = $_GET['imp'];
$palet = $_GET['palet'];
// Acentos de base de datos a html.
$accents = $connection->query("SET NAMES 'utf8'");
if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    $query = "CALL WEB_GUARDAR_MOVIMIENTOS('BULTO','$bulto','$palet',NULL,'$imp',NULL);";

    $querySavePalletsResult = $connection->query($query);

    if ($querySavePalletsResult) {
        $result = true;
    } else {
        $result['ERROR'][0] = "Error de consulta";
        $result['ERROR'][1] = "No se pudo realizar la consulta." . $query;
    }
    $connection->close();
}
print json_encode($result);
?>