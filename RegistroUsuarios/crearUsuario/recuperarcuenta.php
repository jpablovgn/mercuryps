<?php
	// The lvlroot variable indicates the levels of direcctories
	// the file loaded has to up, to be on the root directory
	$lvlroot ="../../";
	// Including Head.
	include_once($lvlroot."Body/Head.php");
	// Including Begin Header.
	include_once($lvlroot."Body/BeginPage.php");
	// Including Side bar.
	//include_once($lvlroot."Body/SideBar.php");
?>
	<script>
	// The lvlroot variable indicates the levels of direcctories (required to auth)
	// the file loaded has to up, to be on the root directory
	var lvlroot = "../../" ;
	</script>
<?php
	/**************************************************************/
    //require_once($lvlroot."assets/php/auth.php"); //Autenticación para iniciar sesión
    //require_once ($lvlroot."assets/php/PhpMySQL.php"); //Funciones base de datos.
	/**************************************************************/
?>
	<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Inicio</a></li>
				<li><a href="javascript:;">Admin Usuarios</a></li>
				<li class="active">Crear Usuario</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			                    <form data-parsley-validate="true" action="" method="POST" class="margin-bottom-0" >
						  				<h4 class="panel-title">
								<i class="icon-user-follow fa-2x"></i> 
								</h4>
							<div class="form-group">
								<label class="control-label col-md-2 col-sm-2" for="usuario">Escriba el Correo electrónico asociado a esta cuenta (*):</label>
								<div class="col-md-2 col-sm-2">
									<input class="form-control" type="text" id="email" name="email" placeholder="ejemplo@gmail.com" data-parsley-required="true" data-parsley-length="[3, 15]" />
								</div>
							</div>
							<div class="login-buttons">
                            <button type="submit" class="btn">Enviar</button>
                        </div>
	
							</form>
                <!-- end col-12 -->
				
				<!-- #modal-alert -->
				<!-- modal-trigger -->
				<a href="#modal-alert" id="alert-trigger" class="btn btn-sm btn-danger" data-toggle="modal" style="display:none;">Demo</a>
				<div class="modal fade" id="modal-alert">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title">Ocurrió un error</h4>
							</div>
							<div class="modal-body">
								<div class="alert alert-danger m-b-0">
									<h4 id="alert-Header"><i class="fa fa-info-circle"></i> Alert Header</h4>
									<p id="alert-comentary"></p>
								</div>
							</div>
							<div class="modal-footer">
								<a href="javascript:;" class="btn btn-sm btn-danger" data-dismiss="modal">Cerrar</a>
							</div>
						</div>
					</div>
				</div>
				<!-- End modal-alert -->
				<!-- #modal-dialog -->
					<!-- modal-trigger -->
				<a href="#modal-dialog" id="modal-trigger" class="btn btn-sm btn-success" data-toggle="modal" style="display:none;">Demo</a>
				<div class="modal fade" id="modal-dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="modal-Header">Modal Dialog</h4>
							</div>
							<div class="modal-body" >
								<div class="alert m-b-o alert-success" id="modal-Note">
									Modal body content here...
								</div>
							</div>
							<div class="modal-footer">
								<a href="javascript:;" class="btn btn-sm btn-success" data-dismiss="modal">Cerrar</a>
							</div>
						</div>
					</div>
				</div>
				<!-- End modal-dialog -->				
			</div>
			<!-- end row -->

<?php	
	// Including Js actions, put in the end.
	include_once($lvlroot."Body/JsFoot.php");
	// Including End Header.
	include_once($lvlroot."Body/EndPage.php");
?>

<?php
	//Validate and insert user values
	include("registrarUsuario.php");
?>

<script type="text/javascript">
	// Activating the side bar.
	var Change2Activejs = document.getElementById("sidebarAdminUsuarios");
	Change2Activejs.className = "has-sub active";
	var Changesub2Activejs = document.getElementById("sidebarAdminUsuarios-CrearUsuario");
	Changesub2Activejs.className = "active";
</script>

<script>
	//Script to change type text to password 
    function changeTypeTxtPassword()
    {
		document.FormCrearUsuario.password.type=(document.FormCrearUsuario.optionHidePassword.value=(document.FormCrearUsuario.optionHidePassword.value==1)?'-1':'1')=='1'?'text':'password';
    }
	
	//Script to change type text to password 
    function changeTypeTxtPasswordAgain()
    {
        document.FormCrearUsuario.passwordAgain.type=(document.FormCrearUsuario.optionHidePasswordAgain.value=(document.FormCrearUsuario.optionHidePasswordAgain.value==1)?'-1':'1')=='1'?'text':'password';
    }
</script>




