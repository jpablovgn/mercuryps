<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";


// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if($_SESSION['NOMBREUSUARIO'] == NULL)
	{
	?>	<script>
	
		window.location = "../../Home/exit.php";
		</script><?php
	}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");

?>
<script>
// The lvlroot variable indicates the levels of direcctories (required to auth)
// the file loaded has to up, to be on the root directory
    var lvlroot = "../../";

</script>


<?php
include_once($lvlroot . "assets/php/PhpMySQL.php");

$Client = new Database();
if (isset($_POST['nombre'])) {
    $nombre = $_POST['nombre'];
    $usuario = $_POST['usuario'];
    $correo = $_POST['correo'];
    $password = $_POST['password'];
    $passwordAgain = $_POST['passwordAgain'];
    $perfil = $_POST['perfil'];
    $query ="CALL INGRESAR_USUARIOS($perfil,'$nombre','$usuario','$password','$correo')";
    $queryResult = $Client->query($query);
    if ($queryResult) {
        ?>
        <div class="alert m-b-o alert-success" id="modal-Note">Usuario creado con éxito.</div>

    <?php } else {
        ?>
        <div class="alert m-b-o alert-danger" id="modal-Note"> Error al ingresar el usuario </div>
        <?php
    }
}

//require_once($lvlroot."assets/php/auth.php");
include_once('../../assets/php/PhpMySQL.php');
?>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Admin Usuarios</a></li>
    <li class="active">Crear Usuario</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Crear Usuario <small> Ingrese los datos del nuevo usuario</small></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-follow fa-2x"></i> 
                    Crear Usuario</h4>
            </div>
            <div class="panel-body panel-form">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="FormCrearUsuario" id = "FormCrearUsuario">
                    <!-- Start campo nombre -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="fullname">Nombre  (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="fullname" name="nombre" placeholder="Nombre del nuevo usuario" data-parsley-required="true" value ="<?php echo $campos['nombre'] ?>" />
                        </div>
                    </div>
                    <!-- End campo nombre -->

                    <!-- Start campo nombre de usuario -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="usuario">Nombre de usuario (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="usuario" name="usuario" placeholder="Nombre de usuario" data-parsley-required="true" data-parsley-length="[3, 15]" />

                        </div>
                    </div>
                    <!-- End campo nombre de usuario -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="correo">Correo Electrónico (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="correo" name="correo" placeholder="ejemplo@gmail.com" data-parsley-required="true" />

                        </div>
                    </div>

                    <!-- Start campo contraseña -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">Contraseña (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input name="password" type="password" id="password" placeholder="Contraseña del nuevo usuario" data-parsley-required="true" data-parsley-length="[4, 15]" class="form-control m-b-5" />
                            <!--<div id="passwordStrengthDiv" class="is0 m-t-5"></div><br>-->
                            <input type="checkbox" name="optionHidePassword" value='-1' onchange="changeTypeTxtPassword()" data-render="switchery" data-theme="blue" data-change="check-switchery-state-text"  />
                            <!--<a href="#" class="btn btn-xs btn-primary m-l-5" data-id="">Ocultar/Mostrar Contraseña</a>-->
                        </div>

                        <label class="control-label col-md-4 col-sm-4">Repita la Contraseña (*):</label>	
                        <div class="col-md-6 col-sm-6">
                            <input name="passwordAgain" data-parsley-equalto="#password" type="password" class="form-control" placeholder="Repita la contraseña del nuevo usuario" data-parsley-required="true" data-parsley-length="[4, 15]" /><br>
                            <input type="checkbox" name="optionHidePasswordAgain" value='-1' onchange="changeTypeTxtPasswordAgain()" data-render="switchery" data-theme="blue" data-change="check-switchery-state-text"  />
                            <!--<a href="#" class="btn btn-xs btn-primary m-l-5" data-id="">Ocultar/Mostrar Contraseña</a>-->
                        </div>
                    </div>
                    <!-- End campo repita la contraseña -->

                    <!-- Start campo perfil -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4">Perfil (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <?php
                            ?>
                            <select class="form-control" id="select-required" name="perfil" data-parsley-required="true">

                                <option value="" selected >Seleccione tipo de perfil para el nuevo usuario:</option>
                                <?php
                                $perfiles = "CALL CONSULTAR_PERFILES()";
                                $queryResult = $Client->query($perfiles);
                                while ($clientData = $Client->fetch_array_assoc($queryResult)) {
                                    ?>
                                <option value="<?php echo $clientData['ID_PERFIL'] ?>"  <?php  echo $clientData['ID_PERFIL'] == $campos['perfil'] ? "selected" : "" ?>><?php echo $clientData['PERFIL'] ?></option>
                                    <?PHP
                                }
                                $Client->close();
                                ?>
                            </select>
                        </div>
                    </div>
                    <!-- End campo perfil -->

                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4"><b>Los campos con (*) son obligatorios</b></label>									
                        <div class="col-md-6 col-sm-6">
                            <button type="submit" class="btn btn-primary"  name="AceptRegUser">Crear Usuario</button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
        <!-- end panel inverse -->
    </div>
    <!-- end col-12 -->

    <!-- #modal-alert -->
    <!-- modal-trigger -->
    <a href="#modal-alert" id="alert-trigger" class="btn btn-sm btn-danger" data-toggle="modal" style="display:none;">Demo</a>
    <div class="modal fade" id="modal-alert">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Ocurrió un error</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger m-b-0">
                        <h4 id="alert-Header"><i class="fa fa-info-circle"></i> Alerta</h4>
                        <p id="alert-comentary"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-danger" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal-alert -->
    <!-- #modal-dialog -->
    <!-- modal-trigger -->
    <a href="#modal-dialog" id="modal-trigger" class="btn btn-sm btn-success" data-toggle="modal" style="display:none;">Demo</a>
    <div class="modal fade" id="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modal-Header">Modal Dialog</h4>
                </div>
                <div class="modal-body" >
                    <div class="alert m-b-o alert-success" id="modal-Note">
                        Modal body content here...
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-success" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal-dialog -->				
</div>
<!-- end row -->

<script>
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarAdminUsuarios");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarAdminUsuarios-CrearUsuario");
    Changesub2Activejs.className = "active";

    $(document).ready(function () {

        // process the form
        $('FormCrearUsuario').submit(function (event) {

            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'fullname': $('#fullname').val(),
                'usuario': $('#usuario').val(),
                'correo': $('#correo').val(),
                'password': $('#password').val(),
                'id_perfil': $('#select-required').val()
            };

            // process the 
form
            $.ajax({
                type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                url: '/php/registro.php', // the url where we want to POST
                data: formData, // our data object
                encode: true
            })
                    // using the done promise callback
                    .done(function (data) {

                        // log data to the console so we can see
                        console.log(data);

                        // here we will handle errors and validation messages
                    });

            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });

    });

</script>

<script>
    //Script to change type text to password 
    function changeTypeTxtPassword()
    {
        document.FormCrearUsuario.password.type = (document.FormCrearUsuario.optionHidePassword.value = (document.FormCrearUsuario.optionHidePassword.value == 1) ? '-1' : '1') == '1' ? 'text' : 'password';
    }

    //Script to change type text to password 
    function changeTypeTxtPasswordAgain()
    {
        document.FormCrearUsuario.passwordAgain.type = (document.FormCrearUsuario.optionHidePasswordAgain.value = (document.FormCrearUsuario.optionHidePasswordAgain.value == 1) ? '-1' : '1') == '1' ? 'text' : 'password';
    }
</script>
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>

