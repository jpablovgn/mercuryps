var nit = getUrlVars()['id'];
var url = 'php/cargarProveedor.php?nit=' + nit;

$.getJSON(url, function(result)
{
    if (result['ERROR'])
    {
        alertWindow(result['ERROR'][0], result['ERROR'][1]);
    }
    else
    {


        // loading client info
        $('#nit').val(result['DATA'][0][0]);
        $('#conductor').val(result['DATA'][0][1]);
        $('#placa').val(result['DATA'][0][3]);
        $('#empresa').val(result['DATA'][0][2]);
        $('#correo').val(result['DATA'][0][4]);
        $('#telefono').val(result['DATA'][0][5]);


    }
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
        vars[key] = value;
    });
    return vars;
}


// NITclient maxlength
$("#nit").parsley();
$("#nit").attr('readonly', '');


// enterpriceName maxlength
$("#conductor").parsley();
$("#conductor").attr('data-parsley-maxlength', 30);

// contact maxlength
$("#placa").parsley();
$("#placa").attr('data-parsley-maxlength', 8);


// telephone maxlength
$("#telefono").parsley();
$("#telefono").attr('data-parsley-maxlength', 15);
$('#telefono').attr('data-parsley-type', "pnumber");

// email maxlength
$("#correo").parsley();
$("#correo").attr('data-parsley-maxlength', 45);

// address maxlength
$("#empresa").parsley();
$("#empresa").attr('data-parsley-maxlength', 30);

$("#updateP").click(function() {
    // if form validated
    if ($("#FormCrear").parsley().validate())
    {
        // Confirmation message
        document.getElementById('warning-Header').innerHTML = "ACTUALIZAR";
        document.getElementById('warning-comentary').innerText = "¿Está seguro que desea actualizar los datos del Proveedor?";
        document.getElementById('warning-accept-button').setAttribute('onclick', 'updateClientData()');
        document.getElementById('warning-trigger').click();
    }
});

function updateClientData()
{
    // creating variables
    var nit = $("#nit").val();
    var nombreE = $("#conductor").val();
    var tel = $("#telefono").val();
    var correo = $("#correo").val();
    var placa = $("#placa").val();
    var empresa = $("#empresa").val();
    
    var url = 'php/actualizar.php?nit=' + nit + '&nombre=' + nombreE + '&placa=' + placa + '&empresa=' + empresa + '&correo=' + correo + '&tel=' + tel;
    $.getJSON(url, function(result) {
        if (result['ERROR'])
        {
            alertWindow(result['ERROR'][0], result['ERROR'][1]);
        }
        else
        {
            var headerMsg = "Operación Exitosa";
            var comentMsg = "Proveedor Actualizado Correctamente ";
            successWindow(headerMsg, comentMsg);
            return false;


        }
    });
}
