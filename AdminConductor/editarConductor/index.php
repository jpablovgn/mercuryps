<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?>	<script>
        window.location = "../../Home/exit.php";
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
//include("assets/php/start_sesion.php");
@session_start();
$usuario = $_SESSION['NOMBREUSUARIO'];
?>
<script>
    // The lvlroot variable indicates the levels of direcctories (required to auth)
    // the file loaded has to up, to be on the root directory
    var lvlroot = "../../";
</script>
<?php
/* * *********************************************************** */
//require_once($lvlroot."assets/php/auth.php"); //Autenticación para iniciar sesión
require_once ($lvlroot . "assets/php/PhpMySQL.php"); //Funciones base de datos.
/* * *********************************************************** */
//include("php/userValidate.php");
include_once ("php/registrarCambioUsuario.php");
?>
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Admin Importadores</a></li>
    <li class="active">Editar Importador</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Editar Importador <small> Elige el Importador que deseas editar</small></h1>
<!-- end page-header -->
<!-- begin row -->
<div class="row">
    <!-- begin col-6 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                                    <!--<a href="javascript:;" onclick="noSubmit();" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>-->
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-following fa-2x"></i>
                    Editar Importador
                </h4>
            </div>
            <div class="panel-body panel-form">
                <!-- Start campo Nombre input desplegable  -->
                <div class="form-group">
                    <table  class="table" id="usuariostable" style="width: 100%;">
                        <thead>
                            <tr style="border:1px;">
                                <th>Identificación</th>
                                <th>Conductor</th>
                                <th>Matricula</th>
                                <th>Empresa</th>
                                <th>Correo</th>
                                <th>Teléfono </th>
                            </tr><br>
                        </thead>
                        <?php
                        $Client = new Database();
                        $perfiles = "SELECT * FROM MR_CONDUCTOR";
                        $queryResult = $Client->query($perfiles);
                        while ($clientData = $Client->fetch_array_assoc($queryResult)) {
                            ?>
                            <tr>
                                <td><?php echo $clientData['CEDULA_CONDUCTOR'] ?></td>
                                <td><?php echo $clientData['NOMBRE_CONDUCTOR'] ?></td>
                                <td><?php echo $clientData['MATRICULA'] ?></td>
                                <td><?php echo $clientData['EMPRESA'] ?></td>
                                <td><?php echo $clientData['EMAIL'] ?></td>
                                <td><?php echo $clientData['TELEFONO'] ?></td>

                                <td><a class="fa fa-pencil-square-o" href="editar.php?id=<?php echo $clientData['ID_CONDUCTOR'] ?>"> Editar</a></td>
                            </tr>
                            <?PHP
                        }
                        $Client->close();
                        ?>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <a href="#modal-alert" id="alert-trigger" class="btn btn-sm btn-danger" data-toggle="modal" style="display:none;">Demo</a>
    <div class="modal fade" id="modal-alert">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title">Error</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger m-b-0">
                        <h4 id="alert-Header"><i class="fa fa-info-circle"></i> Alert Header</h4>
                        <p id="alert-comentary"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-danger" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal-alert -->
    <!-- #modal-dialog -->
    <!-- modal-trigger -->
    <a href="#modal-dialog" id="modal-trigger" class="btn btn-sm btn-success" data-toggle="modal" style="display:none;">Demo</a>
    <div class="modal fade" id="modal-dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="modal-Header">Modal Dialog</h4>
                </div>
                <div class="modal-body" >
                    <div class="alert m-b-o alert-success" id="modal-Note">
                        Modal body content here...
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-success" data-dismiss="modal">Cerrar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- End modal-dialog -->
</div>
<!-- end row -->
<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>
<!-- // Loading autocomplete handler. -->
<script>
    
    var Change2Activejs = document.getElementById("sidebarAdminComprador");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarAdminComprador-EditarComprador");
    Changesub2Activejs.className = "active";
</script>


<script>
    $('#usuariostable').DataTable({
        data: result,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

    });
</script>
