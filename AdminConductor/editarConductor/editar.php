<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script src="js/sweet-alert.min.js"></script>
<?php
if ($_SESSION['IDPERFIL'] == 2) {
    ?>
    <script>
        $("#Activecheck").hide();

    </script><?php
}
$lvlroot = "../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?><script>
            window.location = "../../Home/exit.php"
    </script><?php
}

if ($_SESSION['IDPERFIL'] == 1) {
    ?><script>
            $("#Activecheck").show();
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/AlertsWindows.php");

include_once($lvlroot . "Body/SideBar.php");
//include("assets/php/start_session.php"); //Para hacer el LogIn en el App
@session_start();
?>
<script>
// The lvlroot variable indicates the levels of direcctories (required to auth)
// the file loaded has to up, to be on the root directory
    var lvlroot = "../../";

</script>

<script type="text/javascript" src="../../assets/plugins/parsley/dist/parsley.js"></script>

<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Admin Conductores</a></li>
    <li class="active">Editar Conductor</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Actualizar Conductor <small> Campos Obligatorios  (*) </small></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->

        <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-follow fa-2x"></i> 
                    Actualizar información del Conductor</h4>
            </div>
            <div class="panel-body panel-form">
                <form method="post" class="form-horizontal form-bordered" data-parsley-validate="true" name="FormCrearUsuario" id = "FormCrear">
                    <!-- Start campo nombre -->
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="nit">Identificación (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="nit" name="nombre" placeholder="" data-parsley-required="true" value =""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="name">Conductor (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="conductor" name="nombre" placeholder="Nombre del nuevo Conductor" data-parsley-required="true" value =""/>
                        </div>
                    </div>
            
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="correo">Matricula (*):</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" type="text" id="placa" name="correo" placeholder="Nueva Matricula" data-parsley-required="true" value =""/>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="tel">Nombre Empresa (*):</label>
                        <div class="col-md-6 col-sm-6">

                            <input class="form-control" type="text" id="empresa" name="usuario" placeholder="Nueva Empresa" data-parsley-required="true" data-parsley-length="[3, 15]" value =""/>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="direccion">Correo (*):</label>
                        <div class="col-md-6 col-sm-6">

                            <input class="form-control" type="email" id="correo" name="usuario" placeholder="ejemplo@gmail.com"  data-parsley-required="true" data-parsley-length="[3, 15]" value =""/>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4" for="contacto">Teléfono (*):</label>
                        <div class="col-md-6 col-sm-6">

                            <input class="form-control" type="text" id="telefono" name="usuario" placeholder="Nuevo Teléfono" data-parsley-required="true" data-parsley-length="[3, 15]" value =""/>

                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-4 col-sm-4"><b>Los campos con (*) son obligatorios</b></label>									
                        <div class="col-md-6 col-sm-6">
                            <button type="button" id="updateP"class="btn btn-primary"  name="AceptRegUser" >Actualizar Conductor</button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
        <!-- end panel inverse -->
    </div>
    <!-- end col-12 -->
<a href="#modal-warning" id="warning-trigger" class="btn btn-sm btn-danger" data-toggle="modal" style="display:none;">Demo</a>
	<div class="modal fade" id="modal-warning">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title">Advertencia</h4>
				</div>
				<div class="modal-body">
					<div class="alert alert-warning m-b-0">
						<h4 id="warning-Header"><i class="fa fa-info-circle"></i> Alert Header</h4>
						<p id="warning-comentary"></p>
					</div>
				</div>
				<div class="modal-footer">
					<a id="warning-cancel-button" class="btn btn-sm btn-white" data-dismiss="modal">Cancelar</a>
					<a id="warning-accept-button" class="btn btn-sm btn-warning" data-dismiss="modal">Aceptar</a>
				</div>
			</div>
		</div>
	</div>
    <!-- #modal-alert -->
    <!-- modal-trigger -->
    <script type="text/javascript" src="js/autocompleteDoctteHandler.js"> </script>
    <script>

        var Change2Activejs = document.getElementById("sidebarAdminComprador");
        Change2Activejs.className = "has-sub active";
        var Changesub2Activejs = document.getElementById("sidebarAdminComprador-EditarComprador");
        Changesub2Activejs.className = "active";
    </script>

</div>


<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>







