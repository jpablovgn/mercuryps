<?php
// The lvlroot variable indicates the levels of direcctories
// the file loaded has to up, to be on the root directory
$lvlroot = "../../";
// Including Head.
include_once($lvlroot . "Body/Head.php");
// Including Begin Header.
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?>	<script>

        window.location = "../../Home/exit.php"
    </script><?php
}
// Including Side bar.
include_once($lvlroot . "Body/SideBar.php");
// Including Php database.
include_once($lvlroot . "assets/php/PhpMySQL.php");
?>
<script>
    var lvlrootjs = "../../";
</script>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Clientes</a></li>
    <li class="active">Crear/Editar cliente</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Crear/Editar cliente <small>Creación o edición de clientes del sistema.</small></h1>
<!-- end page-header -->

<div class="row">
    <!-- begin main column -->
    <div class="col-md-12">
        <!-- begin Create/Edit Client panel -->
        <div class="panel panel-inverse" data-sortable-id="form-plugins-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-follow fa-2x"></i> 
                    Crear/Editar Cliente
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <!-- begin form -->
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group">
                        <div class="control-label col-md-4 col-sm-4">
                            <label>NIT (*)</label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" id="NITclient" name="NITclient" data-parsley-type="digits" placeholder="NIT" required />
                        </div>
                    </div>
                    <div class="form-group">    
                        <div class="control-label col-md-4 col-sm-4">
                            <label>Nombre empresa (*)</label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" id="enterpriceName" name="enterpriceName" placeholder="Empresa" required />
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a class="btn  btn-icon btn-circle btn-lg" id="buttonEditEnterpriceName">
                                <i class="fa fa-pencil-square-o">
                                    Editar
                                </i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">    
                        <div class="control-label col-md-4 col-sm-4">
                            <label>Nombre del contacto (*)</label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" id="contact" name="contact" placeholder="Contacto" required />
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a class="btn  btn-icon btn-circle btn-lg" id="buttonEditContact">
                                <i class="fa fa-pencil-square-o">
                                    Editar
                                </i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">                        
                        <div class="control-label col-md-4 col-sm-4">
                            <label>Marca comercial</label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" id="tradeMark" name="tradeMark" placeholder="Marca" />
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a class="btn  btn-icon btn-circle btn-lg" id="buttonEditTradeMark">
                                <i class="fa fa-pencil-square-o">
                                    Editar
                                </i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">                        
                        <div class="control-label col-md-4 col-sm-4">
                            <label>Teléfono (*)</label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Teléfono" data-parsley-type="digits" required />
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a class="btn  btn-icon btn-circle btn-lg" id="buttonEditTelephone">
                                <i class="fa fa-pencil-square-o">
                                    Editar
                                </i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">                        
                        <div class="control-label col-md-4 col-sm-4">
                            <label>Correo electrónico (*)</label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Correo electrónico" data-parsley-type="email" required />
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a class="btn btn-icon btn-circle btn-lg" id="buttonEditEmail">
                                <i class="fa fa-pencil-square-o">
                                    Editar
                                </i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group">                        
                        <div class="control-label col-md-4 col-sm-4">
                            <label>Dirección (*)</label>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="text" class="form-control" id="address" name="address" placeholder="Dirección" required />
                        </div>
                        <div class="col-md-1 col-sm-1">
                            <a class="btn  btn-icon btn-circle btn-lg" id="buttonEditAdderess">
                                <i class="fa fa-pencil-square-o">
                                    Editar
                                </i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 col-md-offset-9 col-sm-12">
                        <button id="clientSave" type="button" class="btn btn-primary">
                            <i class="fa fa-sign-in"></i>
                            Guardar
                        </button>
                    </div>
                </form>
                <!-- end form -->
            </div>
        </div>
        <!-- end Create/Edit Client panel-->
    </div>
    <!-- end main column -->

    <!-- Including alerts windows -->
<?php
include_once($lvlroot . "Body/AlertsWindows.php");
?>
</div>
<!-- end row -->

<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>

<!-- // Loading autocomplete handler. -->
<script type="text/javascript" src="js/autocompleteHandler.js"></script>
<!-- // Loading validations. -->
<script type="text/javascript" src="js/validations.js"></script>

<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarAdminClientes");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarAdminClientes-CrearEditarCliente");
    Changesub2Activejs.className = "active";
</script>