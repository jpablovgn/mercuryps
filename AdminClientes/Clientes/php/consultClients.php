<?php
    /*<!--
	* This file consults the clients data
	* creating a database conection
	-->*/
    include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();

    if(!$connection->link)
    {
        $result['ERROR'][0] = "Error de conexión";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
        $queryConsultClients = "CALL WEB_CLIENTE('READ', NULL,NUll ,NUll, NUll,NUll, NUll, NUll);";
        $queryConsultClientsResult = $connection->query($queryConsultClients);

    
        while($tmpclients = $connection->fetch_array_assoc($queryConsultClientsResult))
        {
            $clientsInfoList[] = $tmpclients;
        }
        $result['DATA'] = $clientsInfoList;
        $connection->close();
    }
    print json_encode($result);
?>