<?php
    /*<!--
	* This file save a client data
	* creating a database conection
	-->*/
    $NITphp     = $_GET['NIT'];
    $ENAME      = "'".$_GET['ENAME']."'"; // Enterprice Name
    $CONTACT    = "'".$_GET['CONTACT']."'";
    $TRADEMARK  = ($_GET['TRADEMARK'])?"'".$_GET['TRADEMARK']."'":"NULL";
    $TELEPHONE  = "'".$_GET['TEL']."'";
    $EMAIL      = "'".$_GET['EMAIL']."'";
    $ADDRESS    = "'".$_GET['ADDRESS']."'";

    include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();
    if(!$connection->link)
    {
        $result['ERROR'][0] = "Error de conexión";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
        $queryConsultClient = "CALL WEB_CLIENTE('INSERT', $NITphp,$ENAME ".
            ",$CONTACT, $TRADEMARK,$TELEPHONE,$EMAIL,$ADDRESS);";
        $queryConsultClientResult = $connection->query($queryConsultClient);

        if($queryConsultClientResult)
        {
            $result['SUCCESS'][0] = "Información guardada en base de datos";
            $result['SUCCESS'][1] = "Se guardó la información del cliente con éxito";
        }
        else
        {
            $result['ERROR'][0] = "Solicitud no realizada";
            $result['ERROR'][1] = "No se pudo guardar información en base de datos.";
        }
        $connection->close();
    }
    print json_encode($result);
?>