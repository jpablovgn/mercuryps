<?php
    /*<!--
	* This file consults a client data
	* creating a database conection
	-->*/
    $NITphp     = ($_GET['NIT'])?$_GET['NIT']:"NULL";
    $ENAME      = ($_GET['ENAME'])?"'".$_GET['ENAME']."'":"NULL"; // Enterprice Name
    $CONTACT    = ($_GET['CONTACT'])?"'".$_GET['CONTACT']."'":"NULL";

    include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();
    
    if(!$connection->link)
    {
        $result['ERROR'][0] = "Solicitud no realizada";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
        $queryConsultClient = "CALL WEB_CLIENTE('SEARCH', $NITphp,$ENAME ,$CONTACT, NULL,NULL, NULL, NULL);";
        $queryConsultClientResult = $connection->query($queryConsultClient);

        if($queryConsultClientResult)
        {
            while($tmpclient = $connection->fetch_array($queryConsultClientResult))
            {
                $clientInfoList[] = $tmpclient;
            }
            $result['DATA'] = $clientInfoList;
        }
        else
        {
            $result['ERROR'][0] = "Solicitud no realizada";
            $result['ERROR'][1] = "Error insertando los campos en base de datos";
        }
        $connection->close();
    }
    
    print json_encode($result);
?>