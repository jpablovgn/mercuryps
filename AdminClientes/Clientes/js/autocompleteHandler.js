/*
 function prevent(event)
 {
 if(event.keyCode == 13)
 {
 event.preventDefault();
 }
 }*/
/* globalData global variable with clients info list */
var globalData;
var globalOldValue = "";

var url = 'php/consultClients.php';
$.getJSON(url, function (responce) {
    if (responce['ERROR'])
    {
        alertWindow(responce['ERROR'][0], responce['ERROR'][1]);
    } else
    {
        globalData = responce;
        passData(responce['DATA']);
    }
});

function passData(data) {
    var handleJqueryAutocomplete = function () {
        var NITjs = [''];
        var ENAMEjs = [''];

        var dataSize = ObjectSize(data);
        // filling the vectors with the preloaded data.
        for (var i = 0; i < dataSize; i++)
        {
            NITjs.push(String(data[i]['NIT']));
            ENAMEjs.push(data[i]['NOMBRE_EMPRESA']);
        }
        // linking the preloaded data to the <input> field.
        //$("#jquery-autocomplete").autocomplete({source:e});
        $("#NITclient").autocomplete({source: NITjs});
        $("#enterpriceName").autocomplete({source: ENAMEjs});
    };
    var FormPluginsAutoComplete = function () {
        "use strict";
        return {
            //main function
            init: function () {
                handleJqueryAutocomplete();
            }
        };
    }();

    $(document).ready(function () {
        FormPluginsAutoComplete.init();
    });
}

// this function calculates the size of an object.
function ObjectSize(obj)
{
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key))
            size++;
    }
    return size;
}
;

$("#NITclient").click(function () {
    globalOldValue = $("#NITclient").val();
});

$("#NITclient").focusout(function (event) {
    // Validating NIT not leading 0
    if ($("#NITclient").val()[0] != 0)
    {
        if (globalOldValue != $("#NITclient").val())
        {
            searchClient(this);
        }
    } else
    {
        var message = "El NIT no puede ser cero o contener ceros al inicio.";
        document.getElementById('alert-Header').innerHTML = "NIT no válido";
        document.getElementById('alert-comentary').innerHTML = message;
        document.getElementById('alert-trigger').click();
    }

});

$("#enterpriceName").focusout(function (event) {
    // if enter
    /*	if(event.keyCode == 13)
     {
     */		searchClient(this);

});

// search client
function searchClient(object)
{
    // Creating variables
    var NITjs = "", enterpriceNamejs = "", contact = "";
    // Passing data to variables
    switch (object.id) {
        case "NITclient":
            NITjs = object.value;
            // Emptying the fields
            $("#enterpriceName").val("")
                    .prop('disabled', false);
            $("#contact").val("")
                    .prop('disabled', false);
            $("#tradeMark").val("")
                    .prop('disabled', false);
            $("#telephone").val("")
                    .prop('disabled', false);
            $("#email").val("")
                    .prop('disabled', false);
            $("#address").val("")
                    .prop('disabled', false);
            break;
        case "enterpriceName":
            enterpriceNamejs = object.value;
            break;
        default:
            break;
    }

    // Searching client by NIT, enterprice name or contact
    var url = "php/searchClient.php?NIT=" + NITjs +
            "&ENAME=" + enterpriceNamejs + "&CONTACT=" + contact;
    $.getJSON(url, function (clientData) {
        // calling function to autocomplete
        if (clientData['ERROR'])
        {
            alertWindow(clientData['ERROR'][0], clientData['ERROR'][1]);
        }
        if (clientData['DATA'])
        {
            autocompleteCall(clientData['DATA'][0]);
        }
    });
}

// filling the data in the form
function autocompleteCall(clientData)
{
    $("#NITclient").val(clientData[0]);
    $("#enterpriceName").val(clientData[1])
            .prop('disabled', true);
    $("#contact").val(clientData[2])
            .prop('disabled', true);
    $("#tradeMark").val(clientData[3])
            .prop('disabled', true);
    $("#telephone").val(clientData[4])
            .prop('disabled', true);
    $("#email").val(clientData[5])
            .prop('disabled', true);
    $("#address").val(clientData[6])
            .prop('disabled', true);
}

// Calling function to enable the field edition
$("#buttonEditEnterpriceName").click(function () {
    toggleEdition("enterpriceName")
});
$("#buttonEditContact").click(function () {
    toggleEdition("contact")
});
$("#buttonEditTradeMark").click(function () {
    toggleEdition("tradeMark")
});
$("#buttonEditTelephone").click(function () {
    toggleEdition("telephone")
});
$("#buttonEditEmail").click(function () {
    toggleEdition("email")
});
$("#buttonEditAdderess").click(function () {
    toggleEdition("address")
});

// toggle field edition
function toggleEdition(objectName) {
    // toggling the enable
    $("#" + objectName).prop('disabled', !$("#" + objectName).attr('disabled'));
}