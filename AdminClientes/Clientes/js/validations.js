/**	On this file there are some functions that
 * validates information pass by the user
 * */
/*
$( "#enterpriceName" ).keydown(function( event ) {
	prevent(event);
});*/

// NITclient maxlength
$("#NITclient").parsley();
$("#NITclient").attr('data-parsley-minlength',7);
$("#NITclient").attr('data-parsley-maxlength',20);

// enterpriceName maxlength
$("#enterpriceName").parsley();
$("#enterpriceName").attr('data-parsley-maxlength',30);

// contact maxlength
$("#contact").parsley();
$("#contact").attr('data-parsley-maxlength',30);

// tradeMark maxlength
$("#tradeMark").parsley();
$("#tradeMark").attr('data-parsley-maxlength',30);

// telephone maxlength
$("#telephone").parsley();
$("#telephone").attr('data-parsley-maxlength',20);

// email maxlength
$("#email").parsley();
$("#email").attr('data-parsley-maxlength',45);

// address maxlength
$("#address").parsley();
$("#address").attr('data-parsley-maxlength',30);

$("#clientSave").click(function(){
	// if form validated
	if($("#formclient").parsley().validate())
	{
		// Confirmation message
		document.getElementById('warning-Header').innerHTML = "GUARDAR";
		document.getElementById('warning-comentary').innerText = "¿Está seguro que desea guardar los datos del cliente?";
		document.getElementById('warning-accept-button').setAttribute('onclick','saveClientData()');
		document.getElementById('warning-trigger').click();
	}
});

function saveClientData()
{
	document.getElementById('warning-accept-button').removeAttribute('onclick');
	// creating variables
		var nitjs 	=$("#NITclient").val(), enamejs=$("#enterpriceName").val();
		var contactjs=$("#contact").val(), tradeMarkjs=$("#tradeMark").val();
		var telephonejs=$("#telephone").val(), emailjs=$("#email").val();
		var addressjs=$("#address").val();
		var url ="php/saveClient.php?NIT="+nitjs+
			"&ENAME="+enamejs+"&CONTACT="+contactjs+
			"&TRADEMARK="+tradeMarkjs+"&TEL="+telephonejs+"&EMAIL="+emailjs+
			"&ADDRESS="+addressjs;
		
	$.getJSON(url, function(result){
		if(result['ERROR'])
		{
			alertWindow(result['ERROR'][0],result['ERROR'][1]);
		}
		else
		{
			successWindow(result['SUCCESS'][0],result['SUCCESS'][1]);
			clearClientData();
			enableClientEdition();
		}
	});
}

function clearClientData()
{
	$("#NITclient").val("");
	$("#enterpriceName").val("");
	$("#contact").val("");
	$("#tradeMark").val("");
	$("#telephone").val("");
	$("#email").val("");
	$("#address").val("");
}

function enableClientEdition()
{
	$("#NITclient").prop('disabled', false);
	$("#enterpriceName").prop('disabled', false);
	$("#contact").prop('disabled', false);
	$("#tradeMark").prop('disabled', false);
	$("#telephone").prop('disabled', false);
	$("#email").prop('disabled', false);
	$("#address").prop('disabled', false);
}