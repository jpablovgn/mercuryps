<?php
$lvlroot = "../../";
include_once($lvlroot . "Body/Head.php");
include_once($lvlroot . "Body/BeginPage.php");
if ($_SESSION['NOMBREUSUARIO'] == NULL) {
    ?><script>window.location = "../../Home/exit.php";</script><?php
}
include_once($lvlroot . "Body/SideBar.php");
include_once($lvlroot . "assets/php/PhpMySQL.php");
?>
<script>
    var lvlrootjs = "../../";
</script>
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Inicio</a></li>
    <li><a href="javascript:;">Clientes</a></li>
    <li class="active">Crear/Editar maestra</li>
</ol>

<h1 class="page-header">Crear/Editar maestra <small>Creación o edición de la maestra de los clientes.</small></h1>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="icon-user-follow fa-2x"></i> 
                    Datos del Cliente
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body panel-form">
                <form data-parsley-validate="true" id="formclient" name="formclient" class="form-horizontal form-bordered" method="post" action="">
                    <div class="form-group">
                        <div class="control-label col-md-2 col-sm-2">
                            <label>NIT (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" data-parsley-required="true" name="selectNITClient" id="selectNITClient" value="">
                                <option value="">Seleccione el NIT</option>
                            </select>
                        </div>
                        <div class="control-label col-md-2 col-sm-2">
                            <label>Empresa (*)</label>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="combobox" data-parsley-required="true" name="selectEName" id="selectEName" value="">
                                <option value="">Seleccione la empresa</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-inverse" id="uploadMasterPanel">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-2x fa-upload"></i> 
                    Subir maestra
                </h4>
            </div>
            <div class="panel-body panel-form">
                <div class="form-group">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-2 col-sm-2">
                            <br>
                            <a id="downloadExampleMaster" class="btn btn-white btn-sm">
                                <i class="fa fa-download">
                                    Descargar formato de maestra
                                </i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label class="col-md-4 text-left" for="message">
                            <p> Adjunte la maestra del cliente en formato csv. </p>
                            Nota: las separaciones de las columnas deben ser por punto y coma (;).
                        </label>
                        <form id="fileupload" method="POST" enctype="multipart/form-data" >   
                            <div class="row fileupload-buttonbar">
                                <div class="col-md-7">
                                    <span class="btn btn-success fileinput-button" id="addFileButton" style="display:true;">
                                        <i class="fa fa-plus"></i>
                                        <span>Agregar archivo...</span>
                                        <input type="file" name="files[]">
                                    </span>
                                    <button type="submit" class="btn btn-primary start" id="start-global-upload" disabled="true">
                                        <i class="fa fa-upload"></i>
                                        <span>Cargar archivo</span>
                                    </button>
                                    <button type="reset" class="btn btn-warning cancel" id="cancelUploadButton" disabled="true">
                                        <i class="fa fa-ban"></i>
                                        <span>Cancelar carga</span>
                                    </button>
                                    <button type="button" class="btn btn-danger delete" id="deleteAllButton" style="display:none;">
                                        <i class="glyphicon glyphicon-trash"></i>
                                        <span>Eliminar</span>
                                    </button>
                                    <!-- The global file processing state -->
                                    <span class="fileupload-process"></span>
                                </div>
                                <!-- The global progress state -->
                                <div class="col-md-12 fileupload-progress fade">
                                    <!-- The global progress bar -->
                                    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                    <div class="progress-bar progress-bar-success" style="width:0%;">
                                    </div>
                                    <!-- The extended global progress state -->
                                    <div class="progress-extended">&nbsp;</div>
                                </div>
                            </div>
                            <!-- The table listing the files available for upload/download -->
                            <table role="presentation" class="table table-striped">
                                <tbody class="files">
                                </tbody>
                            </table>    
                        </form>
                        <!-- end Master form -->
                    </div>
                </div>

            </div>
        </div>
        <!-- end Master upload panel-->
        <!-- begin Master table panel -->
        <div class="panel panel-inverse" id="masterTable">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">
                    <i class="fa fa-table fa-2x"></i> 
                    Tabla de la maestra
                </h4>
            </div>
            <!-- begin body panel -->
            <div class="panel-body">
                <div class="form-group" id="masterTable">
                    <table id="Master-table" class="table">
                    </table>
                </div>
                <!-- begin save button-->
                <div class="col-md-12 col-sm-12 text-right">
                    <button id="clientCancel" type="button" class="btn btn-warning">
                        <i class="fa fa-ban"></i>
                        Cancelar
                    </button>
                    <button id="clientSave" type="button" class="btn btn-primary">
                        <i class="fa fa-sign-in"></i>
                        Guardar
                    </button>
                </div>
                <!-- end save button-->
            </div>
        </div>
        <!-- end Master table panel-->
    </div>
    <!-- end main column -->

    <!-- Including alerts windows -->
<?php
include_once($lvlroot . "Body/AlertsWindows.php");
?>
</div>
<!-- end row -->

<?php
// Including Js actions, put in the end.
include_once($lvlroot . "Body/JsFoot.php");
// Including End Header.
include_once($lvlroot . "Body/EndPage.php");
?>

<!-- // Loading autocomplete handler. -->
<script type="text/javascript" src="js/autocompleteHandler.js"></script>
<!-- // Loading building table. -->
<script type="text/javascript" src="js/buildTable.js"></script>
<!-- // Loading validations. -->
<script type="text/javascript" src="js/validations.js"></script>
<!-- // Loading file to download example master. -->
<script type="text/javascript" src="js/generateExampleMaster.js"></script>
<!-- // Loading file to upload files. -->
<script type="text/javascript" src="js/fileUpload.js"></script>

<script type="text/javascript">
    // Activating the side bar.
    var Change2Activejs = document.getElementById("sidebarAdminClientes");
    Change2Activejs.className = "has-sub active";
    var Changesub2Activejs = document.getElementById("sidebarAdminClientes-CrearMaestra");
    Changesub2Activejs.className = "active";
</script>

<?php
include_once("php/downloadTemplate.php");
?>