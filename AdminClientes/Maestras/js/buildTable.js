/** This function creates a master table
 * @param {String} headers the table headers
 * @param {Array: {String} EAN, {String} REFERENCIA, 
 * {String} TALLA, {String} COLOR}   
 * rows the table rows
 * @param {Array: {String} ROWCOLOR} color of each row
 */
function buildTable(headers, rows, color)
{
    // showing master's table panel
        $("#masterTable").attr("hidden",false);
    var colDef=[]; // column definition
    var columnsSize = ObjectSize(rows[0]);
    var headersSize = ObjectSize(headers);
    // Creating columns from headers variable
    for(var headersFor=0;headersFor<headersSize;headersFor++)
    {
        colDef[headersFor]={
            aTargets: [headersFor], 
            sTitle: headers[headersFor]};
    }
    
    // If color status exist, create column
    if(color)
    {
        // Adding column header
        var targets = ObjectSize(colDef);
        colDef.push({
            aTargets: [targets],
            sTitle: 'ESTADO'
        });
        // Destroying table and div's childs
        if ( $.fn.dataTable.isDataTable( '#Master-table' ) ) {
            table = $('#Master-table').DataTable();
            table.destroy();
            $('#Master-table').empty();
        }
    }
    
    // If dataTable exist, clear
    // if not, create table.
    if ( $.fn.dataTable.isDataTable( '#Master-table' ) ) {
        table = $('#Master-table').DataTable();
        table.clear();
    }
    else
    {
        // table construct
        table = $('#Master-table').DataTable(
        {
            //"bJQueryUI": true,
            //"sPaginationType": "full_numbers",
            //"bProcessing": true,
            //"bDeferRender": true,
            //"bInfo" : false,
            //"bDestroy" : true,
            //"bFilter" : false,
            //"bPagination" : false,
            //"aaData": results,
            //"aoColumns": cols,
            "oLanguage":{
                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            "aoColumnDefs": colDef
        });
    }

    // array of colors
    var colorArray =
    {
        1:'success',
        2:'warning',
        3:'white'
    };
    // array of status
    var statusArray =
    {
        1:'Campo nuevo',
        2:'Campo modificado',
        3:'Sin cambios'
    };
    
    var numberOfRows = ObjectSize(rows);
//    alert(JSON.stringify(rows));

    // Adding rows to the table.
    for(var loopRows=0; loopRows<numberOfRows; loopRows++)
    {
        // If row has data
        if(ObjectSize(rows[loopRows][0]))
        {
            // create tr element
            var tr = document.createElement('tr');
            // adding row information to each column cell.
            for(var cellFor=0;cellFor<columnsSize;cellFor++)
            {
                var td1 = document.createElement('td');
                td1.innerText = ''+rows[loopRows][cellFor];
                tr.appendChild(td1);
            }
            // if color variable with status exist, set row's color
            // else set default row's color.
            if(color)
            {
                tr.setAttribute('class',colorArray[color[loopRows]]);
                var td1 = document.createElement('td');
                td1.innerText = ''+statusArray[color[loopRows]];
                tr.appendChild(td1);
            }
            else
            {
                tr.setAttribute('class',colorArray[3]);
            }
            //table.fnAddData(tr);
            table.row.add(tr).draw();
        }
    }
}