// Configuring fileupload
$(function(){
	$('#fileupload').fileupload({
		maxFileSize: 10242880,
		maxNumberOfFiles: 1,
		acceptFileTypes:/(\.|\/)(csv)$/i
	});
});

function maxFileValidation(file)
{
	
}

/**
 * Managing new file added
 */
function newFileAdded()
{
	// manage enabling buttons
	disEnableAttachedFile();
	// Confirm if master exists
	if(existMasterGlobal)
	{
		var headerMsg = "Maestra ya existente";
		var comentMgs = "La maestra ya existe en base de datos, "+
			"¿desea modificarla?";
		warningWindow(headerMsg,comentMgs,acceptModification,cancelModification);
	}
}

/**
 * funtion called when the file finish to upload
 * @param {file} file object with the file information
 */
function fileUploaded(file)
{
	// manage enable buttons
	disEnableUploadedFile();
	// path of the file.
	var filePath = $('#fileupload').attr('action');
	filePath += "files/"+file.name; //adding file name.
	var NIT = $("#selectNITClient").val();
	// calling php file to load the master file attached
	var url = "php/loadMasterFile.php?filePath="+filePath+"&fileName="+file.name+"&NIT="+NIT;
	$.getJSON(url, function(result){
		// deleting uploaded file
		clearingUploadedFiles();
		// Getting the master's table
		if(result['ERROR'])
		{
			// manage enable buttons
			disEnableAttachFile();
			// showing message error.
			alertWindow(result['ERROR'][0], result['ERROR'][1]);
		}
		else
		{
			// validating headers master file
			var validateResult = validateHeadersMasterFile(result['HEADERS']);
			if(validateResult)
			{
				// Check on data base.
				modifyMaster(result);
			}
			else
			{
				// manage enable buttons
				disEnableAttachFile();
				var headerMsg = "Error de encabezados";
				var comentMgs = "Los encabezados no están correctos,"
					+" están en desorden o el archivo no se encuentra "
					+"separado por punto y comas (;), se recomienda descargar "
					+"el formato de la maestra.";
				alertWindow(headerMsg, comentMgs);
			}
		}
	});
}

function cancelModification()
{
	// Cancelling file to upload
	$("#cancelUploadButton").click();
	// manage enable buttons
	disEnableAttachFile();
}

/**
 * calling when the user accepts 
 * the modification of the master table.
 */
function acceptModification()
{
	// Starting upload file
	$("#start-global-upload").click();
}

/**
 * call a php function, and compares the attached master
 * and the existing master in database
 * the function inserts the attached master in a temporal
 * table, and gets the comparison result table with
 * the status of each row.
 */
function modifyMaster(table)
{
	var NITjs = $('#selectNITClient').val();
	var urls = "php/insertTempMasterTable.php";
	// creating array with data
	var data = 
	{
		nit:	NITjs,
		headers: 	table['HEADERS'],
		data: 	table['DATA']
	};
	// converting variable to ajax format
	data = JSON.stringify(data);
	
	// ajax object to call php file
	$.ajax({
    type: "POST",
    url: urls,
    async: true,
    cache: false,
    dataType: "json",
    data:           {data: data},
    timeout:50000, /* Timeout in ms */

        success: function(responce)
        {
			// loading success
			var headerMsg = "Maestra cargada";
			var comentMgs = "archivo de maestra cargado, "
				+"revisar y guardar datos de la maestra.";
			successWindow(headerMsg,comentMgs);
			// alert(JSON.stringify(responce['DATA']));
			// showing master table to the user
            buildTable(responce['HEADERS'],responce['DATA'],responce['STATUS']);
        }
    });
}