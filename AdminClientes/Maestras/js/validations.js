/** Original master header 
 * */
var masterHeader = {
	0: "EAN",
	1: "REFERENCIA",
	2: "TALLA",
	3: "COLOR",
	4: "DESCRIPCION"
}

// One selected picked autocompletes the other select
$("#selectNITClient").change(function(){
    var value = this.value;
    if(value)
    {
        // Setting the value on the Enterprice name select
        $("#selectEName").val(value);
        $("#selectEName").data('combobox').refresh();
        // showing Master's panels
        // showing Master's panels
        $("#uploadMasterPanel").attr("hidden",false);
        consultMaster($("#selectNITClient").val());
    }
    else
    {
        // manage enabling buttons
        disEnableAttachFile();
        // hidding panels
        hiddeMasterPanels();
        // clearing table if exist
        if ( $.fn.dataTable.isDataTable( '#Master-table' ) ) {
            table = $('#Master-table').DataTable();
            table.clear();
            table.destroy();
            $('#Master-table').empty();
        }
        // Clearing combobox
        $('#selectEName').data('combobox').clearTarget();
        $('#selectEName').data('combobox').clearElement();
        // Cancel upload
        $("#cancelUploadButton").click();
        // Clearing uploaded file from form
	    clearingUploadedFiles();
    }
});
// One selected picked autocompletes the other select
$("#selectEName").change(function(){
    var value = this.value
    if(value)
    {
        // Setting the value on the NIT select
        $("#selectNITClient").val(value);
        $("#selectNITClient").data('combobox').refresh();
        // showing Master's panels
        $("#uploadMasterPanel").attr("hidden",false);
        consultMaster($("#selectNITClient").val());
    }
    else
    {
        // manage enabling buttons
        disEnableAttachFile();
        // hidding panels
        hiddeMasterPanels();
        // clearing table if exist
        if ( $.fn.dataTable.isDataTable( '#Master-table' ) ) {
            table = $('#Master-table').DataTable();
            table.clear();
            table.destroy();
            $('#Master-table').empty();
        }
        // Clearing combobox
        $('#selectNITClient').data('combobox').clearTarget();
        $('#selectNITClient').data('combobox').clearElement();
        // Cancel upload
        $("#cancelUploadButton").click();
        // Clearing uploaded file from form
	    clearingUploadedFiles();
    }
});

/**
 * enabling master's panels view
 */
function showMasterPanels() {
    // showing Master's panels
    $("#uploadMasterPanel").attr("hidden",false);
    $("#masterTable").attr("hidden",false);
}

/**
 * disabling master's panels view
 */
function hiddeMasterPanels() {
    // showing Master's panels
    $("#uploadMasterPanel").attr("hidden",true);
    $("#masterTable").attr("hidden",true);
}

/**
 * Enable/Disable buttons to attach a file
 * 
 */
function disEnableAttachFile(){
    $("#addFileButton").attr("style","display:true;");
    $("#start-global-upload").attr("disabled",true);
    $("#cancelUploadButton").attr("disabled",true);
}

/**
 * Enable/Disable buttons on attache file
 * 
 */
function disEnableAttachedFile(){
    $("#addFileButton").attr("style","display:none;");
    $("#start-global-upload").removeAttr("disabled");
    $("#cancelUploadButton").removeAttr("disabled");
}

/**
 * Enable/Disable buttons file uploaded
 */
function disEnableUploadedFile(){
    $("#addFileButton").attr("style","display:none;");
    $("#start-global-upload").attr("disabled",true);
    $("#cancelUploadButton").attr("disabled",true);
}

/**
 * consulting the master by client's NIT
 */
function consultMaster(nitjs)
{
    // clearing table if exist
    if ( $.fn.dataTable.isDataTable( '#Master-table' ) ) {
        table = $('#Master-table').DataTable();
        table.clear();
        table.destroy();
        $('#Master-table').empty();
    }
    // calling php file to consult the master
    var url = "php/consultMaster.php?NIT="+nitjs;
    $.getJSON(url, function(responce){
        // checking if responce is valid
        if(responce['HEADERS'])
        {
            // drawing table
            buildTable(responce['HEADERS'],responce['DATA']);
            // setting global variable, master exist
            existMasterGlobal = 1;
        }
        else
        {
            alertWindow(responce['ERROR'][0],responce['ERROR'][1]);
            // Cleaning global variable
            existMasterGlobal =0;
        }
    });
}

/**
 * validating attached file headers
 */
function validateHeadersMasterFile(headerLoaded)
{
    // getting object's size
    var endFor = ObjectSize(masterHeader);
    var headerLoadedSize = ObjectSize(headerLoaded);
    if(endFor != headerLoadedSize)
    {
        return null;
    }
    else
    {
        for(var loopVar=0; loopVar <endFor; loopVar++)
        {
            if(headerLoaded[loopVar] != masterHeader[loopVar])
            {
                return null;
            }
        }
        return true;
    }
}

$("#clientCancel").click(function(){
    // manage enable buttons
    disEnableAttachFile();
    // hidding master's table
    $("#masterTable").attr("hidden",true);
    // Clearing table
    $("#Master-table").DataTable().clear();
    // Cancel upload
    $("#cancelUploadButton").click();
    // Clear upload
    clearingUploadedFiles();
});

$("#clientSave").click(function(){
    var headerMsg = "Confirmar creación de maestra";
    var comentMsg = "¿Está seguro de guardar los cambios?";
    warningWindow(headerMsg, comentMsg, saveMaster);
    // Clearing uploaded file from form
    clearingUploadedFiles();
});

function saveMaster()
{
    var NITjs = $("#selectNITClient").val();
    var url = "php/saveMaster.php?NIT="+NITjs;
    $.getJSON(url,function (responce){
        if(responce['ERROR'])
        {
            alertWindow(responce['ERROR'][0],responce['ERROR'][1]);
        }
        else
        {
            successWindow(responce['SUCCESS'][0],responce['SUCCESS'][1]);
            consultMaster($("#selectNITClient").val());
        }
    });
}

function clearingUploadedFiles()
{
    // Clearing uploaded file from form
	var totalBox = $("input:checkbox[name=delete]").prop('checked', true);
    $("#deleteAllButton").click();
}