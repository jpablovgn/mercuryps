// global
var existMasterGlobal = 0;
// Hidding upload master's panels
$("#uploadMasterPanel").attr("hidden",true);
// Hidding master's table panels
$("#masterTable").attr("hidden",true);

// Consulting client list
var url = 'php/clientsList.php';
$.getJSON(url, function(responce){
    if(responce['ERROR'])
    {
        alertWindow(responce['ERROR'][0], responce['ERROR'][1]);
    }
    else loadSelect(responce['DATA']);
});

// Loading NIT and enterprice name combobox
function loadSelect(clientData)
{
    var sizeofclientData = ObjectSize(clientData);
    // Deleting combobox items.
    $('#selectNITClient').html('');
    $('#selectNITClient').data('combobox').refresh();
    $('#selectEName').html('');
    $('#selectEName').data('combobox').refresh();
    // adding new items to the combobox, importations numbers.
        // adding blank item.
        var blankNITjs = '<option value=""></option>';
        $('#selectNITClient').append(blankNITjs);
        $('#selectNITClient').data('combobox').refresh();
        var blankENamejs = '<option value=""></option>';
        $('#selectEName').append(blankENamejs);
        $('#selectEName').data('combobox').refresh();
        // for all importations items.
    for(var ijs=0;ijs < sizeofclientData; ijs++)
    {
        var appendNITjs = '<option value="'+clientData[ijs][0]+'">'+clientData[ijs][0]+'</option>';
        $('#selectNITClient').append(appendNITjs);
        $('#selectNITClient').data('combobox').refresh();
        var appendENamejs = '<option value="'+clientData[ijs][0]+'">'+clientData[ijs][1]+'</option>';
        $('#selectEName').append(appendENamejs);
        $('#selectEName').data('combobox').refresh();
    }
}

/** this function calculates the size of an object.
 * @param {object} obj Object to calculate the size
 * */
function ObjectSize(obj) 
{
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};