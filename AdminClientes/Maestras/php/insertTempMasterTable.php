<?php

/* <!--
 * This file gets a master table by rows and headers
 * and compares it with the original master
  --> */
$data = json_decode($_POST['data'], true);
$NIT = $data['nit'];
$headers = $data['headers'];
$headersAmount = sizeof($headers);
$rows = $data['data'];
$rowsLength = sizeof($rows);

include_once('../../../assets/php/PhpMySQL.php');
$connection = new Database();

if (!$connection->link) {
    $result['ERROR'][0] = "Error de conexión";
    $result['ERROR'][1] = "No se pudo conectar a la base de datos";
} else {
    $queryCreateTemporary = "CALL WEB_MAESTRA('CREATE_TEMPORARY',"
            . "$NIT,NULL,NULL,NULL,NULL,NULL);";
    $queryCreateTemporaryResult = $connection->query($queryCreateTemporary);

    if ($queryCreateTemporaryResult) {
        foreach ($rows as $key => $value) {
            if ($value[0]) {
                $data2Insert = $NIT . ",'" . implode("','", $value) . "'";
                $queryInserRow = "CALL WEB_MAESTRA('INSERT_TEMPORARY',"
                        . "$data2Insert);";
                $queryInserRowResult = $connection->query($queryInserRow);
            }
        }
        if ($queryInserRowResult) {
            $queryValidateMasters = "CALL WEB_MAESTRA('VALIDATE_TEMPORARY', "
                    . "$NIT,NULL,NULL,NULL,NULL,NULL);";
            $queryValidateMastersResult = $connection->query($queryValidateMasters);
            if ($queryValidateMastersResult) {
                $queryShowTemporaryTable = "CALL WEB_MAESTRA('SHOW_TEMPORARY',"
                        . "$NIT,NULL,NULL,NULL,NULL,NULL);";
                $queryShowTemporaryTableResult = $connection->query($queryShowTemporaryTable);

                // $queryTemporaryStatus = "CALL WEB_MAESTRA('SHOW_TEMP_STATUS',"
                //          ."$NIT,NULL,NULL,NULL,NULL,NULL);";
                // $queryTemporaryStatusResult = $connection->query($queryTemporaryStatus);

                if ($queryShowTemporaryTableResult) {
                    while ($tmpTable = $connection->fetch_array_assoc($queryShowTemporaryTableResult)) {
                        $master[] = $tmpTable;
                    }

                    foreach ($master as $row) {
                        $tempChunk = array_chunk($row, 5, true);
                        $result['STATUS'][] = $tempChunk[1]['ESTADO'];
                        foreach ($tempChunk[0] as $key => $value) {
                            $temp[] = $value;
                        }
                        $result['DATA'][] = $temp;
                        unset($temp);
                    }
                    $tempHeaderChunk = array_chunk($master[0], 5, true);
                    $result['HEADERS'] = array_keys($tempHeaderChunk[0]);
                } else {
                    $result['ERROR'][0] = "Error cargando tabla comparativa";
                    $result['ERROR'][1] = "Error cargando tabla comparativa "
                            . "entre tabla maestra y maestra adjuntada.";
                }
            } else {
                $result['ERROR'][0] = "Error validación maestra";
                $result['ERROR'][1] = "Error validando datos "
                        . "entre tabla maestra y maestra adjuntada.";
            }
        } else {
            $result['ERROR'][0] = "Error tabla maestra";
            $result['ERROR'][1] = "Error insertando datos "
                    . "a la tabla temporal.";
        }
    } else {
        $result['ERROR'][0] = "Error tabla maestra";
        $result['ERROR'][1] = "Error creando la tabla temporal "
                . "de la maestra en base de datos.";
    }

    $connection->close();
}
print json_encode($result);
?>