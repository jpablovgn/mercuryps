<?php
    /*<!--
	* This file saves the client's' master
	* creating a database conection
	-->*/
    $NIT = $_GET['NIT'];

    include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();

    if(!$connection->link)
    {
        $result['ERROR'][0] = "Error de conexión";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
        $querySaveMaster = "CALL WEB_MAESTRA('CREATE',$NIT,NULL,NULL,NULL,NULL,NULL);";
        $querySaveMasterResult = $connection->query($querySaveMaster);

        if($querySaveMasterResult)
        {
            $result['SUCCESS'][0]="Guardado con éxito";
            $result['SUCCESS'][1]="Maestra guardada con éxito "
                ."en base de datos";
        }
        else
        {
            $result['ERROR'][0] = "Error de guardado";
            $result['ERROR'][1] = "Error guardando maestra "
                ."en base de datos. ";
        }
        $connection->close();
    }
    print json_encode($result);
?>