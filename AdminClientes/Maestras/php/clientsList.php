<?php
    /*<!--
	* This file consults the clients data
	* creating a database conection
	-->*/
    include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();

    if(!$connection->link)
    {
        $result['ERROR'][0] = "Error de conexión";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
//        $queryConsultClients = "CALL WEB_CLIENTE('READ', NULL,NUll ,NUll, NUll,NUll, NUll, NUll);";
        $queryConsultClients = "SELECT NIT,NOMBRE_EMPRESA FROM MR_CLIENTES;";

        $queryConsultClientsResult = $connection->query($queryConsultClients);

        while($tmpclients = $connection->fetch_array($queryConsultClientsResult))
        {
            $clientsInfoList[] = $tmpclients;
        }
        $result['DATA'] = $clientsInfoList;
        $connection->close();
    }
    print json_encode($result);
?>