<?php
    /*<!--
	* This file consults the clients master
	* creating a database conection
	-->*/
    $NIT = "'".$_GET['NIT']."'";

    include_once('../../../assets/php/PhpMySQL.php');
    $connection = new Database();

    if(!$connection->link)
    {
        $result['ERROR'][0] = "Error de conexión";
        $result['ERROR'][1] = "No se pudo conectar a la base de datos";
    }
    else
    {
        $queryConsultMaster = "CALL WEB_MAESTRA('SEARCH', $NIT,NUll ,NUll, NUll,NUll, NUll);";
        $queryConsultMasterResult = $connection->query($queryConsultMaster);

        while($tmpMaster = $connection->fetch_array_assoc($queryConsultMasterResult))
        {
            $master[] = $tmpMaster;
        }
        foreach($master as $row)
        {
            foreach ($row as $key => $value) 
            {
                $temp[] = $value;
            }
            $result['DATA'][] = $temp;
            unset($temp);
        }
//        $result['DATA'] = $master;
        $result['HEADERS'] = array_keys($master[0]);
        $connection->close();
    }
    print json_encode($result);
?>